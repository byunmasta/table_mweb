package demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import syruptable.SyrupTableMobileWebApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class SyrupTableMobileWebApplicationTests {

	@Test
	public void contextLoads() {
	}

}
