package syruptable.http;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.POIImages;
import syruptable.support.ApiHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SyrupTableMobileWebApplication.class })
@WebAppConfiguration
public class POIImageListTest {
	
private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * GPS 좌표에 대한 값을 cache 하기 위한 최소 값
	 *  0.00001° 차이는 우리나라의 남부지방(위도 35°)에서 0.91m 
	 */
	private static final DecimalFormat decimalFormat = new DecimalFormat(".######");

	@Autowired
	private ApiHelper apiHelper;

	@Test
	public void test() {
		
		/**
		 * POI의 pick 이미지 조회
		 */
//		POIIMAGEFORPICK("https://sapi.pickat.in/v2/pois/{poi_id}/images/picks?cursor={cursor}&limit={limit}"),
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("poi_id", "2132024");
		variables.put("cursor", "");
		variables.put("limit", "20");
		
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.POIIMAGEFORPICK)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		
		logger.info(responseNode);
		POIImages poiImagesPick =  new Gson().fromJson(responseNode, POIImages.class);
		
		logger.info(poiImagesPick.toString());
		logger.info(poiImagesPick.getImages()[0].getPick().toString());
		/**
		 * POI의 블로그 이미지 조회
		 */
//		POIIMAGEFORBLOG("https://sapi.pickat.in/v2/pois/{poi_id}/images/blogs?cursor={cursor}&limit={limit}"),
		
		
		final String responseNode1 = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.POIIMAGEFORBLOG)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		
		logger.info(responseNode1);
		POIImages poiImagesBlog =  new Gson().fromJson(responseNode1, POIImages.class);
		
		logger.info(poiImagesBlog.toString());
		
		
	}

}
