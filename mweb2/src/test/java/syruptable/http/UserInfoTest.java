package syruptable.http;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.exception.UserTokenAPIErrorHandler;
import syruptable.model.UserInfo;
import syruptable.support.ApiHelper;
import syruptable.support.AuthHelper;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class UserInfoTest {
	
	@Autowired
	private ApiHelper apiHelper;
	
	@Autowired
	private AuthHelper authHelper;

	@Test
	public void test() {
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("pitoken", "eyJhbGciOiAiSFMyNTYiLCAidHlwIjogIkpXVCIsICJraWQiOiAiMSJ9.eyJzdWIiOiAiMTAyNDMyMiIsICJpc3MiOiAiMTAwMDA5IiwgImp0aSI6ICI3NjRxOF9RRktOV2ZaSWVKOHR3UDJPdDdWZUxUMlRLamtRb0NXeDViWWlOVjBlMU15XzBZczZmRi02SjFReUl2dHpSbl9CamFwM05EVW9OVE1yV2hLQT09IiwgImV4cCI6IDE0NDA0OTE3MjEsICJpYXQiOiAxNDQwNDkxNDIxLCAiYXVkIjogInVybjpwaWNrYXQ6Y2xhaW1zOnBpdG9rZW4ifQ.rWSrS5If9ARoYqFOWOIh09hP0ljdBgSwaDYHigxCyO0");
		
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.USERINFO)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setErrorHandler(new UserTokenAPIErrorHandler())
				.setResponse(String.class)
				.build();
		
		System.out.println("=====>" + responseNode);
//		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.SSS").create();

		UserInfo api =  new Gson().fromJson(responseNode, UserInfo.class);
		System.out.println(api.getError() == null);
		
		if ( api.getError() == null ) {
			int userId = api.getUserId();
			String syrupId = api.getSyrupMemberId();
			System.out.println(api.getExpired_Date());
		} 
		
		
//		String jwtoken = authHelper.createJsonWebToken(api.getUserId(), 10L);
//		System.out.println("jwtoken : "+jwtoken);
		//사용자에게 보여질 메시지 정의 요청 
	
//		System.out.println(api.toString());
	}

}
