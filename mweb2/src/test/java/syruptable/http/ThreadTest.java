package syruptable.http;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.common.CurrentAOI;
import syruptable.model.common.ReverseGeocodeJSON;
import syruptable.service.SyrupTableService;
import syruptable.util.TableSyrupCallable;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class ThreadTest {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SyrupTableService syrupTableService;
	
	ExecutorService executor = Executors.newCachedThreadPool();
	
	DecimalFormat decimalFormat = new DecimalFormat(".######");
	
	@Test
	public void test() throws InterruptedException, ExecutionException {
		
		String longitude6 = decimalFormat.format(126.97791682833257);
		String latitude6 = decimalFormat.format(37.56626139533117);
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("lon", longitude6);         // 경도 필수
		variables.put("lat", latitude6);		  // 위도 필수

		Future<ReverseGeocodeJSON> reverseFuture = makeReverseGeocode(variables);
		Future<CurrentAOI> rbspFuture = rbspZone(variables);
		
		ReverseGeocodeJSON reverse = reverseFuture.get();
		logger.info(reverse.addressInfo.fullAddress);
		
		CurrentAOI rbsp = rbspFuture.get();
		logger.info(rbsp.getAoi().getType());
		logger.info(rbsp.getAoi().getName());
		
	}
	
	/**
	 * 좌표를 통해 주소 정보를 전송합니다.
	 * @param longitude 경도(필수) longitude
	 * @param latitude 위도(필수) latitude
	 * @return
	 */
	private final Future<ReverseGeocodeJSON> makeReverseGeocode(Map<String, Object> args) {
		Map<String, Object> variables = args;
		TableSyrupCallable<ReverseGeocodeJSON> callable = new TableSyrupCallable<ReverseGeocodeJSON>(HttpAPIList.REVERSEGEOCODE
																									,variables
																									,syrupTableService);
		return executor.submit(callable);
	}
	
	/**
	 * 좌표를 통해 Table Syrup Geofence Zone를 획득한다.
	 * @param longitude 경도(필수) longitude
	 * @param latitude 위도(필수) latitude
	 * @return
	 */
	private final Future<CurrentAOI> rbspZone(Map<String, Object> args) {
		Map<String, Object> variables = args;
		TableSyrupCallable<CurrentAOI> callable = new TableSyrupCallable<CurrentAOI>(HttpAPIList.CURRENTAOI
																				,variables
																				,syrupTableService);
		return executor.submit(callable);
	}

}
