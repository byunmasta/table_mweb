package syruptable.http;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.support.AuthHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class AuthHelperTest {
	
	@Autowired
	private AuthHelper authHelper;

	@Test
	public void test() {
		String token = authHelper.createJsonWebToken(1001, 1L);
		System.out.println(token);
	}

}
