package syruptable.http;

import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.Notices;
import syruptable.model.Notices.Notice;
import syruptable.support.ApiHelper;
import syruptable.util.CharsetCut;

import com.google.gson.Gson;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class NoticeListTest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ApiHelper apiHelper;
	
	
	@Test
	public void test() {
		
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("cursor", "");  //상용 발행 DB
		variables.put("limit", "20");  //상용 발행 DB
		
		final String responseNode = apiHelper.getPickatAuthHttp()
									.setApiName(HttpAPIList.NOTICELIST)
									.setHttpMethod(HttpMethod.GET)
									.setUriVariableValues(variables)
									.setResponse(String.class)
									.build();
		
		logger.info(responseNode);
		
		Notices api =  new Gson().fromJson(responseNode, Notices.class);
		assertNotNull(api);
		
		List<Notice> notices = Arrays.asList(api.getNotice());
		
		for(Notice notice : notices) {
			logger.info("title : {}, 시간 : {}, 번호 : {}", notice.getTitle(), notice.getCreated(), notice.getNoticeId());
			logger.info("내용 : {}", CharsetCut.getPixelSizedText(notice.getContents().toString(), 200, false));
		}
		
		logger.info("전체공지수 : {}", api.getTotalCount());
		logger.info("다음페이지 : {}", api.getPaging().getNext());
		
	}

}
