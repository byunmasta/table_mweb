package syruptable.http;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.Categories;
import syruptable.support.ApiHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class CategoryTest {
	
	@Autowired
	private ApiHelper apiHelper;
	
	@Test
	public void test() {
		
//		String category = "{\"categories\":[{\"category_id\":\"01\",\"type\":\"large\",\"name\":\"음식점\",\"sub_categories\":[{\"category_id\":\"01-01\",\"type\":\"medium\",\"name\":\"한식\"},{\"category_id\":\"01-02\",\"type\":\"medium\",\"name\":\"양식\"},{\"category_id\":\"01-03\",\"type\":\"medium\",\"name\":\"일식\"},{\"category_id\":\"01-04\",\"type\":\"medium\",\"name\":\"중식\"},{\"category_id\":\"01-05\",\"type\":\"medium\",\"name\":\"치킨\"},{\"category_id\":\"01-06\",\"type\":\"medium\",\"name\":\"패밀리레스토랑\"},{\"category_id\":\"01-07\",\"type\":\"medium\",\"name\":\"패스트푸드\"},{\"category_id\":\"01-08\",\"type\":\"medium\",\"name\":\"세계요리\"},{\"category_id\":\"01-09\",\"type\":\"medium\",\"name\":\"분식\"},{\"category_id\":\"01-10\",\"type\":\"medium\",\"name\":\"부페\"},{\"category_id\":\"01-11\",\"type\":\"medium\",\"name\":\"도시락\"},{\"category_id\":\"01-12\",\"type\":\"medium\",\"name\":\"배달음식\"},{\"category_id\":\"01-13\",\"type\":\"medium\",\"name\":\"술집\"},{\"category_id\":\"01-14\",\"type\":\"medium\",\"name\":\"기타\"}]},{\"category_id\":\"02\",\"type\":\"large\",\"name\":\"카페\\/제과\",\"sub_categories\":[{\"category_id\":\"02-01\",\"type\":\"medium\",\"name\":\"카페\\/디저트\"},{\"category_id\":\"02-02\",\"type\":\"medium\",\"name\":\"베이커리\"},{\"category_id\":\"02-03\",\"type\":\"medium\",\"name\":\"키즈카페\"}]},{\"category_id\":\"07\",\"type\":\"large\",\"name\":\"영화\\/공연장\",\"sub_categories\":[{\"category_id\":\"07-01\",\"type\":\"medium\",\"name\":\"영화관\"},{\"category_id\":\"07-02\",\"type\":\"medium\",\"name\":\"공연장\"}]},{\"category_id\":\"11\",\"type\":\"large\",\"name\":\"나들이/문화\",\"sub_categories\":[]},{\"category_id\":\"06\",\"type\":\"large\",\"name\":\"숙박\",\"sub_categories\":[{\"category_id\":\"06-05\",\"type\":\"medium\",\"name\":\"호텔\"},{\"category_id\":\"06-04\",\"type\":\"medium\",\"name\":\"펜션\"},{\"category_id\":\"06-07\",\"type\":\"medium\",\"name\":\"콘도\\/리조트\"},{\"category_id\":\"06-08\",\"type\":\"medium\",\"name\":\"캠핑장\"}]},{\"category_id\":\"13\",\"type\":\"large\",\"name\":\"생활서비스\",\"sub_categories\":[{\"category_id\":\"13-01\",\"type\":\"medium\",\"name\":\"백화점\\/면세점\"},{\"category_id\":\"13-02\",\"type\":\"medium\",\"name\":\"목욕\\/찜질방\"},{\"category_id\":\"13-03\",\"type\":\"medium\",\"name\":\"헬스\\/체육시설\"},{\"category_id\":\"13-04\",\"type\":\"medium\",\"name\":\"헤어샵\"},{\"category_id\":\"13-05\",\"type\":\"medium\",\"name\":\"화장품\"},{\"category_id\":\"13-06\",\"type\":\"medium\",\"name\":\"세탁소\"},{\"category_id\":\"13-07\",\"type\":\"medium\",\"name\":\"보육시설\"},{\"category_id\":\"13-08\",\"type\":\"medium\",\"name\":\"미용\"},{\"category_id\":\"13-09\",\"type\":\"medium\",\"name\":\"안경점\"},{\"category_id\":\"13-10\",\"type\":\"medium\",\"name\":\"애견샵\"},{\"category_id\":\"13-11\",\"type\":\"medium\",\"name\":\"자전거\"},{\"category_id\":\"13-12\",\"type\":\"medium\",\"name\":\"문구점\"},{\"category_id\":\"13-13\",\"type\":\"medium\",\"name\":\"꽃집\"},{\"category_id\":\"13-14\",\"type\":\"medium\",\"name\":\"철물점\"}]},{\"category_id\":\"05\",\"type\":\"large\",\"name\":\"마트\\/슈퍼\",\"sub_categories\":[{\"category_id\":\"05-01\",\"type\":\"medium\",\"name\":\"마트\"},{\"category_id\":\"05-05\",\"type\":\"medium\",\"name\":\"편의점\"},{\"category_id\":\"05-04\",\"type\":\"medium\",\"name\":\"재래시장\"},{\"category_id\":\"05-03\",\"type\":\"medium\",\"name\":\"유기농마트\"},{\"category_id\":\"05-02\",\"type\":\"medium\",\"name\":\"슈퍼\"}]},{\"category_id\":\"17\",\"type\":\"large\",\"name\":\"기타\",\"sub_categories\":[]}]}";
		
		final String responseNode = apiHelper.getPickatAuthHttp()
									.setApiName(HttpAPIList.CATEGORYLIST)
									.setHttpMethod(HttpMethod.GET)
									.setResponse(String.class)
									.build();
		
//		System.out.println(responseNode);
		Categories categories =  new Gson().fromJson(responseNode, Categories.class);
		assertNotNull(categories);
		System.out.println(categories.toString());
		
	}

}
