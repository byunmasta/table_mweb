package syruptable.http;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.common.CurrentAOI;
import syruptable.model.common.ReverseGeocodeJSON;
import syruptable.service.SyrupTableService;
import syruptable.util.TableSyrupCallable;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class Thread2Test {
	/**
	 * 1.234.67.217 openmap1.tmap.co.kr
	 */
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SyrupTableService syrupTableService;

	// ExecutorService executor = Executors.newCachedThreadPool();

	DecimalFormat decimalFormat = new DecimalFormat(".######");

	@Test
	public void test() throws InterruptedException, ExecutionException {

		String longitude6 = decimalFormat.format(126.97791682833257);
		String latitude6 = decimalFormat.format(37.56626139533117);

		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("lon", longitude6); // 경도 필수
		variables.put("lat", latitude6); // 위도 필수

		TableSyrupCallable<ReverseGeocodeJSON> callable = new TableSyrupCallable<ReverseGeocodeJSON>(
																	HttpAPIList.REVERSEGEOCODE
																	,variables
																	,syrupTableService);

		TableSyrupCallable<CurrentAOI> callable2 = new TableSyrupCallable<CurrentAOI>(
															HttpAPIList.CURRENTAOI
															,variables
															,syrupTableService);

		
		FutureTask<ReverseGeocodeJSON> reverseFuture = new FutureTask<ReverseGeocodeJSON>(callable);
		FutureTask<CurrentAOI> rbspFuture = new FutureTask<CurrentAOI>(callable2);
		ExecutorService executor = Executors.newFixedThreadPool(2);
		
		executor.execute(reverseFuture);
		executor.execute(rbspFuture);
		
		executor.shutdown();
		

		ReverseGeocodeJSON reverse = reverseFuture.get();
		logger.info(reverse.addressInfo.fullAddress);
		  
		CurrentAOI rbsp = rbspFuture.get();
		logger.info(rbsp.getAoi().getType());
		logger.info(rbsp.getAoi().getName());
		 

	}

	
}
