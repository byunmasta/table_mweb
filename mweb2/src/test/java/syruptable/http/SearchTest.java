package syruptable.http;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.SearchResult;
import syruptable.support.ApiHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class SearchTest {
	
private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ApiHelper apiHelper;

	@Test
	public void test() {
		
		/**
		 * 검색결과 조회
		 */
//		SEARCH = https://api2.pickat.com/v3/search/pois?query={query}&aoi_name={aoi_name}&sort={sort}&has_coupon={has_coupon}&lng={lng}&lat={lat}&ref_lng={ref_lng}&ref_lat={ref_lat}&cursor={cursor}&limit={limit}
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("query", "아이스크림");
		variables.put("aoi_name", "용종동");
		variables.put("sort", "score");
		variables.put("has_coupon", 1);
		variables.put("lng", 127.013115);
		variables.put("lat", 37.490017);
		variables.put("ref_lng", 127.013115);
		variables.put("ref_lat", 37.490017);
		variables.put("cursor", "");
		variables.put("limit", 20);

		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.SEARCH)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		
		logger.debug("SearchResult {}", responseNode);
		SearchResult searchResult =  new Gson().fromJson(responseNode, SearchResult.class);
		
		logger.info(searchResult.toString());
		
	}

}
