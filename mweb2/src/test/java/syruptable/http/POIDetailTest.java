package syruptable.http;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.POIDetail;
import syruptable.support.ApiHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {SyrupTableMobileWebApplication.class })
@WebAppConfiguration
public class POIDetailTest {
	
private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * GPS 좌표에 대한 값을 cache 하기 위한 최소 값
	 *  0.00001° 차이는 우리나라의 남부지방(위도 35°)에서 0.91m 
	 */
	private static final DecimalFormat decimalFormat = new DecimalFormat(".######");

	@Autowired
	private ApiHelper apiHelper;

	@Test
	public void test() {
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("poi_id", "1135269");

		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.POIDETAIL)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		
		logger.info(responseNode);
		POIDetail poiDetail =  new Gson().fromJson(responseNode, POIDetail.class);
		
		logger.info(poiDetail.toString());
		
	}

}
