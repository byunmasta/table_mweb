package syruptable.http;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.exception.UserTokenAPIErrorHandler;
import syruptable.model.UserInfo;
import syruptable.support.ApiHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class UserTokenTest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ApiHelper apiHelper;
	
	@Test
	public void test() {
		final String responseNode = apiHelper.getPickatToken()
				.setApiName(HttpAPIList.USERTOKEN)
				.setHttpMethod(HttpMethod.GET)
				.setErrorHandler(new UserTokenAPIErrorHandler())
				.setResponse(String.class)
				.build();
		
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(responseNode);
		String token = element.getAsJsonObject().get("token").getAsString();
		
		Map<String, Object> variables = new HashMap<String, Object>();
//		variables.put("pitoken", "eyJhbGciOiAiSFMyNTYiLCAidHlwIjogIkpXVCIsICJraWQiOiAiMSJ9.eyJzdWIiOiAiMTAyNDMyNyIsICJpc3MiOiAiMTAwMDA5IiwgImp0aSI6ICJ0a1NtRTdpaWxlQ1ZZQTFBU1RzdUFKWlY5UmhvT0s1M0poNXJtVlJnQTFEZzRxNGV2Rm9rcEpiYTNVeDd3WnpVVGdSWko2N1FkcDQ3TXo1X2RtUzJRdz09IiwgImV4cCI6IDE0MzgzOTY0ODksICJpYXQiOiAxNDM4Mzk2MTg5LCAiYXVkIjogInVybjpwaWNrYXQ6Y2xhaW1zOnBpdG9rZW4ifQ.BojoHZ7_ery-ED_F_j7nIiS8HQfP0eZh6HQU_zqv9A0");
		variables.put("pitoken", token);
		
		final String userToken = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.USERINFO)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setErrorHandler(new UserTokenAPIErrorHandler())
				.setResponse(String.class)
				.build();
		
		UserInfo api =  new Gson().fromJson(userToken, UserInfo.class);
		if( api.getError() == null ) {
			System.out.println("=====>" + api.getUserId());
		} else {
			System.out.println(api.toString());
			System.out.println("error 처리 ");
		}
		
		
		
		
	}

}
