package syruptable.http;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.HashtagDetail;
import syruptable.support.ApiHelper;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class HashtagDetailTest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ApiHelper apiHelper;
	
	
	@Test
	public void test() {
		
		//HASHTAGDETAIL("https://api.pickat.in/hashtags/{hashtag_id}/pois?sort={sort}&lat={lat}&lng={lng}&cursor={cursor}&limit={limit}")
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("hashtag_id", 2340);
		variables.put("sort", "popular");
		variables.put("lng", "127.103779");
		variables.put("lat", "37.402493");
		variables.put("cursor", "");
		variables.put("limit", "20");
		
		final String responseNode = apiHelper.getPickatAuthHttp()
									.setApiName(HttpAPIList.HASHTAGDETAIL)
									.setHttpMethod(HttpMethod.GET)
									.setUriVariableValues(variables)
									.setResponse(String.class)
									.build();
		
		logger.info(responseNode);
		
		HashtagDetail hashtagDetail =  new Gson().fromJson(responseNode, HashtagDetail.class);
		
		logger.info(hashtagDetail.toString());
		
	}

}
