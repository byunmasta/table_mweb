package syruptable.http;

import static org.junit.Assert.*;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.common.ReverseGeocodeJSON;
import syruptable.support.ApiHelper;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class ReverseGeocodingTest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * GPS 좌표에 대한 값을 cache 하기 위한 최소 값
	 *  0.00001° 차이는 우리나라의 남부지방(위도 35°)에서 0.91m 
	 */
	private static final DecimalFormat decimalFormat = new DecimalFormat(".######");
	
	
	@Autowired
	private ApiHelper apiHelper;
	
	@Test
	public void test() {
		
		String latitude6 = decimalFormat.format(37.5627117943);
		String longitude6 = decimalFormat.format(126.9849551207);
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("lon", longitude6);         // 경도 필수
		variables.put("lat", latitude6);			// 위도 필수
		
	/*	variables.put("lat", "37.56626139533117");		 // lat : 지구상의 지점 위치를 나타내는 위도 좌표입니다.
		variables.put("lon", "126.97791682833257");		 // 지구상의 지점 위치를 나타내는 경도 좌표입니다.
*/
	/*	variables.put("lon", "127.201001839");         // 경도 필수
		variables.put("lat", "37.3719356477");			// 위도 필수
*/		
		
		String responseNode = apiHelper.getHttp()
				.setApiName(HttpAPIList.REVERSEGEOCODE)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		
		logger.info("리버스지오코딩 {}", responseNode);
		ReverseGeocodeJSON api =  new Gson().fromJson(responseNode, ReverseGeocodeJSON.class);
		
		
		
		
		
		
		
	}

}
