package syruptable.http;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.TodayLunch;
import syruptable.support.ApiHelper;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class TodayLunchTest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ApiHelper apiHelper;
	
	
	@Test
	public void test() {
		
		//TODAYLUNCH("https://sapi.pickat.in/v3/lunch/places?aoi_id={aoi_id}"),
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("aoi_id", 117300);
		
		final String responseNode = apiHelper.getPickatAuthHttp()
									.setApiName(HttpAPIList.TODAYLUNCH)
									.setHttpMethod(HttpMethod.GET)
									.setUriVariableValues(variables)
									.setResponse(String.class)
									.build();
		
		logger.info(responseNode);
		
		TodayLunch todayLunch =  new Gson().fromJson(responseNode, TodayLunch.class);
		
		logger.info(todayLunch.toString());
		
	}

}
