package syruptable.http;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.common.CurrentAOI;
import syruptable.support.ApiHelper;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class ThemeZoneTest {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * GPS 좌표에 대한 값을 cache 하기 위한 최소 값
	 *  0.00001° 차이는 우리나라의 남부지방(위도 35°)에서 0.91m 
	 */
	private static final DecimalFormat decimalFormat = new DecimalFormat(".######");

	@Autowired
	private ApiHelper apiHelper;

	@Test
	public void test() {
		
		/**
		 * 피켓의 RBSP 조회
		 * 좌표가 포함된 aoi를 조회한다.
		 */
//		RBSPFORPICKAT("https://sapi.pickat.in/v1/aois/current?lng={lon}&lat={lat}&include=nearby"),
		
		String latitude6 = decimalFormat.format(37.5627117943);
		String longitude6 = decimalFormat.format(126.9849551207);
		
		Map<String, Object> variables = new HashMap<String, Object>();
		
		variables.put("lon", longitude6);         // 경도 필수
		variables.put("lat", latitude6);			// 위도 필수

		/*variables.put("lon", "127.201001839");         // 경도 필수
		variables.put("lat", "37.3719356477");			// 위도 필수
*/		
		final String responseNode = apiHelper.getPickatAuthHttp()
								.setApiName(HttpAPIList.CURRENTAOI)
								.setHttpMethod(HttpMethod.GET)
								.setUriVariableValues(variables)
								.setResponse(String.class)
								.build();
		
//		logger.info(responseNode);
		CurrentAOI rbspZone =  new Gson().fromJson(responseNode, CurrentAOI.class);
		
		logger.info(rbspZone.toString());
		System.out.println(rbspZone.getAoi().getAoiId());
		System.out.println(rbspZone.getAoi().getType());
		System.out.println(rbspZone.getAoi().getName());
		
		//ADMIN_DISTRICT
		//THEME
	}

}
