package syruptable.http;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.AOIList;
import syruptable.model.AOIList.AOIDepth;
import syruptable.model.common.BaseAOI;
import syruptable.support.ApiHelper;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class AOIListTest {
	
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ApiHelper apiHelper;

	@Test
	public void test() {
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("type", "theme");

		
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.AOILIST)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		
		logger.info(responseNode);
		AOIList aoiList =  new Gson().fromJson(responseNode, AOIList.class);
		
		AOIDepth[] aois = aoiList.getAois();
		
		for(AOIDepth aoi : aois) {
			BaseAOI[] subAOIs = aoi.getSubAOIs();
			logger.info(aoi.getName());
			for(BaseAOI subAOI : subAOIs) {
				logger.info(subAOI.toString());
			}
		}
	}

}
