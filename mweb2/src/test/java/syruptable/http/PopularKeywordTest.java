package syruptable.http;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.PopularWords;
import syruptable.support.ApiHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class PopularKeywordTest {
	
private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ApiHelper apiHelper;

	@Test
	public void test() {
		
		/**
		 * 검색결과 조회
		 */
//		POPULARWORDS = https://api.pickat.in/v1/search/popular-words

		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.POPULARWORDS)
				.setHttpMethod(HttpMethod.GET)
				.setResponse(String.class)
				.build();
		
		logger.debug("PopularWords {}", responseNode);
		PopularWords popularWords =  new Gson().fromJson(responseNode, PopularWords.class);
		
		logger.info(popularWords.toString());
		
	}

}
