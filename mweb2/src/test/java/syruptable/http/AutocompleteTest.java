package syruptable.http;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.AutocompleteResult;
import syruptable.support.ApiHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class AutocompleteTest {
	
private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ApiHelper apiHelper;

	@Test
	public void test() {
		
		/**
		 * 자동완성 결과 출력
		 */
//		AUTOCOMPLETE = https://api2.pickat.com/v3/search/autocomplete?query={query}
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("query", "아이스크림");

		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.AUTOCOMPLETE)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		
		logger.debug("SearchResult {}", responseNode);
		AutocompleteResult autocompleteResult =  new Gson().fromJson(responseNode, AutocompleteResult.class);
		
		logger.info(autocompleteResult.toString());
		
	}

}
