package syruptable.http;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.MainStories;
import syruptable.support.ApiHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class MainStoriesTest {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ApiHelper apiHelper;

	@Test
	public void test() {
		
//		MAINSTORIES = https://api.pickat.in/v3/main/stories
		
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.MAINSTORIES)
				.setHttpMethod(HttpMethod.GET)
				.setResponse(String.class)
				.build();
		
		logger.info(responseNode);
		MainStories mainStories =  new Gson().fromJson(responseNode, MainStories.class);
		
		logger.info(mainStories.toString());
		
	}

}
