package syruptable.http;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class MessageResourceAccessorTest {
	
	@Autowired
	@Qualifier("messageSourceAccessor")
	private MessageSourceAccessor msAccessor;

	@Test
	public void test() {
		System.out.println(msAccessor.getMessage(HttpAPIList.CATEGORYLIST.name()));
	}

}
