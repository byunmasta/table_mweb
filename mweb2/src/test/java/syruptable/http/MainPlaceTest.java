package syruptable.http;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.MainPlaces;
import syruptable.support.ApiHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class MainPlaceTest {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ApiHelper apiHelper;

	@Test
	public void test() {
		
//		MAINPLACES = https://api.pickat.in/v3/main/places?aoi_id={aoi_id}&set={set}&lat={lat}&lng={lng}&hashtag_id={hashtag_id}&category_id={category_id}&cursor={cursor}&limit={limit}
	
		//명동
		//latitude	37.5627117943	위도	
		//longitude	126.9849551207	경도
		// set or hashtag_id or category_id 중 한개 필수
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("aoi_id", 117982);      		 	// 지역 ID 필수
		variables.put("set", "latest");  				// set={latest,ranking,coupon,sorder}
		variables.put("lat", "37.5627117943");			// 위도
		variables.put("lng", "126.9849551207");         // 경도
		variables.put("hashtag_id", "0");  	 			// 해시태그 ID
		variables.put("category_id", "00-00");  				// 카테고리 ID
		variables.put("cursor", "");      				// 다음 페이지 default  = null
		variables.put("limit", "20");        			// 요청 개수 : default = 20
		
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.MAINPLACES)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		
		logger.info(responseNode);
		MainPlaces mainPlaces =  new Gson().fromJson(responseNode, MainPlaces.class);
		
		logger.info(mainPlaces.toString());
		
		
	}

}
