package syruptable.http;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.google.gson.Gson;

import syruptable.SyrupTableMobileWebApplication;
import syruptable.config.HttpAPIList;
import syruptable.model.Notices;
import syruptable.support.ApiHelper;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SyrupTableMobileWebApplication.class)
@WebAppConfiguration
public class NoticeDetailTest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ApiHelper apiHelper;
	
	
	@Test
	public void test() {
		
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("notice_id", "105");  //상용 발행 DB
		
		final String responseNode = apiHelper.getPickatAuthHttp()
									.setApiName(HttpAPIList.NOTICEDETAIL)
									.setHttpMethod(HttpMethod.GET)
									.setUriVariableValues(variables)
									.setResponse(String.class)
									.build();
		
		logger.info(responseNode);
		
		Notices api =  new Gson().fromJson(responseNode, Notices.class);
		
		System.out.println(api.getNoticeDetail().getCreated());
		
	}

}
