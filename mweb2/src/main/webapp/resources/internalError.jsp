<%@page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
<title>Syrup Table</title>
<link rel="stylesheet" type="text/css" href="/css/story.css" />
</head>
<body>

<div class="ui-wrap-android"> <!-- ios 에서 사용되는 class : .ui-wrap-ios -->

	<div class="error404">
		<div class="inner">
			<strong>일시적인 장애입니다.</strong>
			<span class="txt">이용 중에 불편을 끼쳐드려 진심으로 죄송합니다.<br />
				일시적인 페이지 장애로 인해 원하신 화면으로<br />
				이동하지 못했습니다.<br />
				잠시후 다시 시도해 주세요</span>
			<a href="/main"><span class="ui-hidden">Syrup Table 홈</span></a>
		</div>
	</div>

</div>

</body>
</html>