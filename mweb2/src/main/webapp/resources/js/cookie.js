(function(window){
	window.cookie = function (key, value, max_age) {
		// set cookie
		if (value !== undefined && !/Object/.test(Object.prototype.toString.call(value))) {
			var domain_string = location.hostname ? ("; domain=" + location.hostname) : '' ;
			var maxAge_string = max_age ? "; max-age=" + 60 * 60 * 24 * max_age : '';
			var cookie_string = key + "=" + encodeURIComponent( value ) + "; path=/" +
				maxAge_string + domain_string;
			
			document.cookie = cookie_string; 
		}

		// get cookie
		var name = key + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0; i<ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1);
	        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
	    }
	    return "";
	};

	window.removeCookie = function (key) {
		if (window.cookie(key) !== null) {
			var domain_string = location.hostname ? ("; domain=" + location.hostname) : '' ;
			document.cookie = key + "=; max-age=0; path=/" + domain_string ;
			return true;
		}
		return false;
	};
	
})(window);
