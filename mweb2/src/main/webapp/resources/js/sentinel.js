rake.install({
//  token: "e1f574e31ca2a72e8536d22db32169b54726242",
//  env: rake.LIVE
  token: "692279c35f4dc1c666361cdf1e6044ee93ef3d86",
  env: rake.DEV
});
var shuttle = new SyrupTableSentinelShuttle();

if(window.cookie('userId')){
	var userId = window.cookie('userId');
}else{
	var userId = makeUserId();
	window.cookie('userId', userId, 365);
}
shuttle.setSession_id(userId);
shuttle.setDevice_id(userId);

function rakeTrack(page_id, action_id, callback){
	if(window.rake){
		if(callback){
			setTimeout(callback, 500);
		}
    	var tabelShuttle = shuttle;
    	tabelShuttle.clearBody();
    	tabelShuttle.setPage_id(page_id);
    	tabelShuttle.setAction_id(action_id);
		rake.track(tabelShuttle.get(), callback);
	}else{
		if(callback){
			callback();
		}
	}
}

function makeUserId() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }
  return s4() + s4() + s4() + s4();
}