var XY = {
	toXY: function(arg1, arg2) {
		var res = {x: 0, y: 0};
		if(arg2 != undefined) {
			res = {x: arg1, y: arg2};
		} else if(arg1.x != undefined && arg1.y != undefined){
			res = arg1;
		} else if(arg1.lon != undefined && arg1.lat != undefined){
			res = {x: arg1.lon, y: arg1.lat};
		} else {
			return null;
		}
		return res;
	}
	, floorXY: function(xy){
		return {x: Math.floor(xy.x), y: Math.floor(xy.y)};
	}
	, toLonLat: function(arg1, arg2) {
		var xy = this.toXY(arg1, arg2);
		return new Tmap.LonLat(xy.x, xy.y);
	}
	
	//이벤트 x, y를 이용하여 지도좌표를 구한다.
	, getMapXY: function(e) {
		var xy = this.getEventXY(e);
		var ll = PickatMaps.map.getLonLatFromPixel(new Tmap.Pixel(xy.x, xy.y));
		return {x: ll.lon, y: ll.lat};
	}
	, getEventXY: function(e) {
		var res = {
			x: 0
			, y: 0
		};
		switch(e.type) {
		case 'touchstart':
		case 'touchmove':
			res.x = e.originalEvent.touches[0].pageX;
			res.y = e.originalEvent.touches[0].pageY;
			break;
		case 'touchend':
			res.x = e.originalEvent.changedTouches[0].pageX;
			res.y = e.originalEvent.changedTouches[0].pageY;
			break;
		default:
			res.x = e.pageX;
			res.y = e.pageY; 
		}
		var mcaOffset = $('#mapContent').offset();
		res.x -= mcaOffset.left;
		res.y -= mcaOffset.top;
		return res;
	}
	, dist: function(p1, p2) {
		return Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
	}
	
	// EPSG:3857 좌표계의 두 점의 거리를 구한다.
	, dist3857: function(p1, p2) {
		var line = new Tmap.Geometry.LineString([new Tmap.Geometry.Point(p1.x, p1.y), new Tmap.Geometry.Point(p2.x, p2.y)]);
		return line.getGeodesicLength(new Tmap.Projection("EPSG:3857"));
	}
	
	// EPSG:4326 좌표계의 두 점의 거리를 구한다.
	, dist4326: function(p1, p2) {
		var line = new Tmap.Geometry.LineString([new Tmap.Geometry.Point(p1.x, p1.y), new Tmap.Geometry.Point(p2.x, p2.y)]);
		return line.getGeodesicLength(new Tmap.Projection("EPSG:4326"));
	}
	
	, getScreenXY: function(x, y) {
		var a = NateMaps.map.spec.getBitmapCoordinate(y, x, NateMaps.map.zoomLevel);
		var res  = NateMaps.map.getDivCoordinate(a.x, a.y);
		var offset = NateMaps.map.getCurrentOffset();
		res.x += offset.width;
		res.y += offset.height;
		return res;
	}
	// Katech 좌표계(TM128)를 EPSG:3857 좌표계로 변환한다. 
	, conv128to3857: function(arg1, arg2) {
		var src = new Tmap.Projection("KATECH");
		var dst = new Tmap.Projection("EPSG:3857");
		return this.toXY(this.toLonLat(arg1, arg2).transform(src, dst));
	}
	
	// EPSG:3857 좌표계를 Katech 좌표계(TM128)로 변환한다.
	, conv3857to128: function(arg1, arg2) {
		var src = new Tmap.Projection("EPSG:3857");
		var dst = new Tmap.Projection("KATECH");
		return this.floorXY(this.toXY(this.toLonLat(arg1, arg2).transform(src, dst)));
	}
	
	// Katech 좌표계(TM128)를 EPSG:4326(WGS84) 좌표계로 변환한다.
	, conv128to4326: function(arg1, arg2) {
		var src = new Tmap.Projection("KATECH");
		var dst = new Tmap.Projection("EPSG:4326");
		return this.toXY(this.toLonLat(arg1, arg2).transform(src, dst));
	}
	
	// EPSG:4326(WGS84) 좌표계를 Katech 좌표계(TM128)로 변환한다.
	, conv4326to128: function(arg1, arg2) {
		var src = new Tmap.Projection("EPSG:4326");
		var dst = new Tmap.Projection("KATECH");
		return this.floorXY(this.toXY(this.toLonLat(arg1, arg2).transform(src, dst)));
	}
	
	// EPSG:4326(WGS84) 좌표계(TM128)를 EPSG:3857 좌표계로 변환한다. 
	, conv4326to3857: function(arg1, arg2) {
		var src = new Tmap.Projection("EPSG:4326");
		var dst = new Tmap.Projection("EPSG:3857");
		return this.toXY(this.toLonLat(arg1, arg2).transform(src, dst));
	}
	
	// EPSG:3857 좌표계(TM128)를 EPSG:4326(WGS84) 좌표계로 변환한다.
	, conv3857to4326: function(arg1, arg2) {
		var src = new Tmap.Projection("EPSG:3857");
		var dst = new Tmap.Projection("EPSG:4326");
		return this.toXY(this.toLonLat(arg1, arg2).transform(src, dst));
	}
	
	// EPSG:3857 or Katech(TM128) 좌표계를 EPSG:4326(WGS84) 좌표계로 변환한다.
	, to4326: function(arg1, arg2) {
		var cs = this.getCoordSystem(arg1, arg2);
		if(cs == 128) {
			return this.conv128to4326(arg1, arg2);
		} else if(cs == 3857) {
			return this.conv3857to4326(arg1, arg2);
		} else if(cs == 4326) {
			return this.toXY(arg1, arg2);
		} else {
			return this.toXY(126.9651678, 37.5639791);
		}
	}
	
	// EPSG:4326(WGS84) or Katech(TM128) 좌표계를 EPSG:3857 좌표계로 변환한다.
	, to3857: function(arg1, arg2) {
		var cs = this.getCoordSystem(arg1, arg2);
		if(cs == 128) {
			return this.conv128to3857(arg1, arg2);
		} else if(cs == 4326) {
			return this.conv4326to3857(arg1, arg2);
		} else if(cs == 3857) {
			return this.toXY(arg1, arg2);
		} else {
			return this.toXY(14133697.82601111, 4518012.47934791);
		}
	}
	
	// EPSG:4326(WGS84) or EPSG:3857 좌표계를  Katech(TM128) 좌표계로 변환한다.
	, to128: function(arg1, arg2) {
		var cs = this.getCoordSystem(arg1, arg2);
		if(cs == 3857) {
			return this.conv3857to128(arg1, arg2);
		} else if(cs == 4326) {
			return this.conv4326to128(arg1, arg2);
		} else if(cs == 128) {
			return this.toXY(arg1, arg2);
		} else {
			return this.toXY(308775, 551807);
		}
	}
	
	// 좌표와 경계좌표를 비교하여, 좌표계 여부를 판단한다.
	, isIn: function(extent, arg1, arg2) {
		var xy = this.toXY(arg1, arg2);
		if(xy.x < extent.left || xy.x > extent.right || xy.y < extent.bottom  || xy.y > extent.top) {
			return false;	
		}
		return true;
	}
	
	// 경계좌표를 다른 좌표계로 변환한다. (EPSG:3857 -> EPSG:4326 or TM128)
	, convExtent: function(extent, fnConv) {
		var res = {};
		var xy = fnConv(extent.left, extent.top);		
		res.left = xy.x;
		res.top = xy.y;
		xy = fnConv(extent.right, extent.bottom);		
		res.right = xy.x;
		res.bottom = xy.y;
		return res;
	}
	
	// 좌표의 좌표계 타입(EPSG:3857, EPSG:4326, TM128)을 반환한다.
	// arg1 : Longitude 경도(가로), arg2 : Latitude 위도(세로)
	, getCoordSystem: function(arg1, arg2) {
		var extent3857 = NateMaps.restrictExtent;
		var extent4326 = this.convExtent(NateMaps.restrictExtent, this.conv3857to4326.bind(this));
		var extent128 = this.convExtent(NateMaps.restrictExtent, this.conv3857to128.bind(this));
		if(this.isIn(extent3857, arg1, arg2)) {
			return "3857";
		} else if(this.isIn(extent4326, arg1, arg2)) {
			return "4326";
		} else if(this.isIn(extent128, arg1, arg2)) {
			return "128";
		} else {
			return false;
		}
	}
};