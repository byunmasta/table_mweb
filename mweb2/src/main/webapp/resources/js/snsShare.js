
(function (window, undefined) {
	var snsShare = {};
    window.snsShare = window.snsShare || snsShare;
    
    Kakao.init('4b31d7e20bf84c01d5f9e1e475c6b321');
    
    var app = {
		facebook : {
			execute: function(option){
				var baseUrl = "http://www.facebook.com/sharer/sharer.php?u=";
				location.href = baseUrl+option.url;
			}
		}
    	, kakaotalk : {
    		execute: function(option){
    			Kakao.Link.sendTalkLink({
    		        label: option.label,
    		        image: {
    		        	src: option.image,
    		        	width: 100,
    		       		height: 100,
    		       	},
    		       	webButton: {
    		       		text: 'Syrup 테이블로 보기',
    		       		url: option.url
    		       	}
    			});
    		}
    	}
    	
    }

    snsShare.link = function(name) {
        var link_app = app[name];
        if ( !link_app ) return { send : function() { throw "App is not exists."; }};
        return {
        	send : function (option) {
               link_app.execute(option);
            }
            , app : link_app
        };
    };
	
}(window));
