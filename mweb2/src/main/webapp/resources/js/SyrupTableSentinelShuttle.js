function SyrupTableSentinelShuttle() {

    var log = {
        sentinel_meta: {
            _$schemaId: "5643330d3a0000e20e6ef4a1",
            _$projectId: "",
            _$fieldOrder: {},
            _$encryptionFields: [
      				"device_id",
				"member_id",
				"user_id"
            ]
        }
    };

    var headers = [
    		"base_time",
		"local_time",
		"recv_time",
		"device_id",
		"device_model",
		"manufacturer",
		"os_name",
		"os_version",
		"resolution",
		"screen_width",
		"screen_height",
		"browser_name",
		"browser_version",
		"carrier_name",
		"network_type",
		"referrer",
		"url",
		"document_title",
		"app_version",
		"language_code",
		"rake_lib",
		"rake_lib_version",
		"ip",
		"recv_host",
		"token",
		"log_version",
		"session_id",
		"cookie_id",
		"page_id",
		"action_id",
		"member_id",
		"user_id",
		"poi_id",
		"story_id",
		"story_page_no",
		"review_id",
		"display_order",
		"subtab_name",
		"hashtag_id",
		"hashtag_name",
		"rating",
		"query",
		"category_id",
		"category_name",
		"coupon_id",
		"coupon_type"
    ];

    var bodies = [
    		"certified_type",
		"wish_type",
		"filter_type",
		"sort_type",
		"photofilter_name",
		"search_call_id",
		"searchresult_num",
		"ctm_source",
		"ctm_medium",
		"ctm_campaign_id",
		"ctm_campaign_name",
		"ctm_content_type",
		"ctm_term"
    ];

    var action_key_fields = [
    		"page_id","action_id"
    ];

    var body_layout = {
    		"/ctm:app.inflow":["ctm_source","ctm_medium"],
		"/splash:":[],
		"/main_myloc:":["certified_type"],
		"/main_myloc:top_click.profile":[],
		"/main_myloc:top_click.gnbmylocbtn":[],
		"/main_myloc:top_click.gnbtrendbtn":[],
		"/main_myloc:top_click.searchbtn":[],
		"/main_myloc:top_click.changelocbtn":[],
		"/main_myloc:todaybanner_click.banner":[],
		"/main_myloc:todaybanner_swipe.banner":[],
		"/main_myloc:subtab_click.tab":[],
		"/main_myloc:subtab_swipe.tab":[],
		"/main_myloc:subtab_click.categorybtn":[],
		"/main_myloc:subtab_click.mapbtn":[],
		"/main_myloc:floating_click.review":[],
		"/main_myloc:all_scroll":[],
		"/main_myloc:body_click.banner":[],
		"/main_myloc:body_click.poi":[],
		"/main_myloc:body_click.poiblog":[],
		"/main_myloc:body_click.couponbtn":[],
		"/main_myloc/catogory_list:":[],
		"/main_myloc/catogory_list:top_click.closebtn":[],
		"/main_myloc/catogory_list:click.categorybtn":[],
		"/main_myloc/mapview:":[],
		"/main_myloc/mapview:top_click.backbtn":[],
		"/main_myloc/mapview:subtab_click.tab":[],
		"/main_myloc/mapview:subtab_click.categorybtn":[],
		"/main_myloc/mapview:map_click.poipin":[],
		"/main_myloc/mapview:map_click.poi":[],
		"/main_myloc/mapview:map_swipe.poi":[],
		"/main_myloc/mapview:map_click.locationbtn":[],
		"/main_myloc/mapview:map_click.serchlocbtn":[],
		"/main_myloc/loc_list:":[],
		"/main_myloc/loc_list:top_click.closebtn":[],
		"/main_myloc/loc_list:click.locationbtn":[],
		"/main_myloc/loc_list:click.categorybtn":[],
		"/main_myloc/loc_list:click.subcategorybtn":[],
		"/main_myloc/todaymenu:":["certified_type"],
		"/main_myloc/todaymenu:top_click.backbtn":[],
		"/main_myloc/todaymenu:subtab_click.tab":[],
		"/main_myloc/todaymenu:click.poi":[],
		"/main_myloc/todaymenu:click.wish":[],
		"/main_myloc/todaymenu:subtab_swipe.tab":[],
		"/main_myloc/todaymenu:all_scroll":[],
		"/main_myloc/todaymenu:today_click.todayloc":[],
		"/main_myloc/todaymenu:click.reservebtn":[],
		"/main_trend:":[],
		"/main_trend:top_click.profile":[],
		"/main_trend:top_click.gnbmylocbtn":[],
		"/main_trend:top_click.gnbtrendbtn":[],
		"/main_trend:top_click.searchbtn":[],
		"/main_trend:topbanner_click.banner":[],
		"/main_trend:click.storymorebtn":[],
		"/main_trend:click.story":[],
		"/main_trend:click.ediorbtn":[],
		"/main_trend:click.certificatedmorebtn":[],
		"/main_trend:click.certificatepoi":[],
		"/main_trend:click.keyword":[],
		"/main_trend:click.usermorebtn":[],
		"/main_trend:click.userbtn":[],
		"/main_trend:click.review":[],
		"/main_trend:click.followbtn":[],
		"/main_trend/storylist:":[],
		"/main_trend/storylist:top_click.backbtn":[],
		"/main_trend/storylist:click.story":[],
		"/main_trend/storylist:click.ediorbtn":[],
		"/main_trend/storylist:all_scroll":[],
		"/main_trend/keywordlist:":[],
		"/main_trend/keywordlist:top_click.backbtn":[],
		"/main_trend/keywordlist:click.poi":[],
		"/main_trend/keywordlist:all_scroll":[],
		"/main_trend/list/certificatedlist:":[],
		"/main_trend/list/certificatedlist:top_click.backbtn":[],
		"/main_trend/list/certificatedlist:share_click.kakaotalkbtn":[],
		"/main_trend/list/certificatedlist:share_click.facebookbtn":[],
		"/main_trend/list/certificatedlist:share_click.smsbtn":[],
		"/main_trend/list/certificatedlist:click.infobtn":[],
		"/main_trend/list/certificatedlist:subtab_click.tab":[],
		"/main_trend/list/certificatedlist:click.poi":[],
		"/main_trend/list/certificatedlist:all_scroll":[],
		"/main_trend/list/certificatedlist:swipe.tab":[],
		"/search:":[],
		"/search:top_click.backbtn":[],
		"/search:click.searchinput":[],
		"/search:click.delquerybtn":[],
		"/search:subtab_click.suggest":[],
		"/search:subtab_click.recent":[],
		"/search:click.suggestquery":[],
		"/search:kepad_click.searchbtn":[],
		"/search:all_scroll":[],
		"/search:top_click.cancelbtn":[],
		"/search:click.recentquery":[],
		"/search:click.delrecentquerybtn":[],
		"/search/recentquery:":[],
		"/search/recentquery:top_click.backbtn":[],
		"/search/recentquery:click.delrecectquery":[],
		"/search/recentquery:click.delrecectqueryallbtn":[],
		"/search/autocomplete:":[],
		"/search/autocomplete:top_click.backbtn":[],
		"/search/autocomplete:click.searchinput":[],
		"/search/autocomplete:click.delquerybtn":[],
		"/search/autocomplete:click.qutoquery":[],
		"/search/autocomplete:kepad_click.searchbtn":[],
		"/search/autocomplete:all_scroll":[],
		"/search/autocomplete:top_click.cancel":[],
		"/search/result:":["search_call_id","searchresult_num"],
		"/search/result:top_click.backbtn":[],
		"/search/result:click.searchinput":[],
		"/search/result:click.filter":["filter_type","searchresult_num"],
		"/search/result:click.sort":["sort_type","searchresult_num"],
		"/search/result:click.poi":["search_call_id","searchresult_num"],
		"/search/result:all_scroll":[],
		"/search/result:click.sortbyrank":[],
		"/search/result:click.sortbydist":[],
		"/search/result:click.sortbyrel":[],
		"/detail/poi:":[],
		"/detail/poi:top_click.backbtn":[],
		"/detail/poi:share_click.kakaotalkbtn":[],
		"/detail/poi:share_click.facebookbtn":[],
		"/detail/poi:share_click.smsbtn":[],
		"/detail/poi:top_click.photo":[],
		"/detail/poi:top_swipe.photo":[],
		"/detail/poi:top_click.photocountbtn":[],
		"/detail/poi:top_click.callbtn":[],
		"/detail/poi:top_click.ratingbtn":[],
		"/detail/poi:menu_click.orderbtn":[],
		"/detail/poi:click.reservebtn":[],
		"/detail/poi:subtab_click.moreinfotab":[],
		"/detail/poi:subtab_click.reviewtab":[],
		"/detail/poi:floating_click.wish":["wish_type"],
		"/detail/poi:floating_click.photo":[],
		"/detail/poi:floating_click.review":[],
		"/detail/poi:menu_click.morebtn":[],
		"/detail/poi:coupon_click.morebtn":[],
		"/detail/poi:coupon_click.coupon":[],
		"/detail/poi:coupon_click.coupondownbtn":[],
		"/detail/poi:map_click.map":[],
		"/detail/poi:map_click.tmapbtn":[],
		"/detail/poi:review_click.wishduserbtn":[],
		"/detail/poi:review_click.morebtn":[],
		"/detail/poi:click.userbtn":[],
		"/detail/poi:click.review":[],
		"/detail/poi:click.photo":[],
		"/detail/poi:click.editdelbtn":[],
		"/detail/poi:blogreview_click.morebtn":[],
		"/detail/poi:blogreview_click.blog":[],
		"/detail/poi:rel_click.poi":[],
		"/detail/poi:certificated_click.morebtn":[],
		"/detail/poi:certificated_click.poi":[],
		"/detail/poi:click.deletedpoi":[],
		"/detail/poi:click.editpoi":[],
		"/detail/poi/rate_popup:":[],
		"/detail/poi/rate_popup:click.ratebtn":[],
		"/detail/poi/rate_popup:click.empty":[],
		"/detail/poi/photolist:":[],
		"/detail/poi/photolist:top_click.backbtn":[],
		"/detail/poi/photolist:click.photo":[],
		"/detail/poi/photolist:click.blogphoto":[],
		"/detail/poi/photolist:all_scroll":[],
		"/detail/poi/photolist:photo_click.morebtn":[],
		"/detail/poi/photolist:blogphoto_click.morebtn":[],
		"/detail/poi/menulist:":[],
		"/detail/poi/menulist:top_click.backbtn":[],
		"/detail/poi/menulist:bottom_click.menuedit":[],
		"/detail/poi/menulist:all_scroll":[],
		"/detail/poi/menulist/edit:":[],
		"/detail/poi/menulist/edit:top_click.backbtn":[],
		"/detail/poi/menulist/edit:top_click.confirmbtn":[],
		"/detail/poi/coupon_list:":[],
		"/detail/poi/coupon_list:top_click.backbtn":[],
		"/detail/poi/coupon_list:coupon_click.coupon":[],
		"/detail/poi/coupon_list:coupon_click.coupondownbtn":[],
		"/detail/poi/coupon_list:all_scroll":[],
		"/detail/poi/mapview:":[],
		"/detail/poi/mapview:top_click.backbtn":[],
		"/detail/poi/mapview:map_click.locationbtn":[],
		"/detail/poi/mapview:map_click.poipin":[],
		"/detail/poi/review_list:":[],
		"/detail/poi/review_list:top_click.backbtn":[],
		"/detail/poi/review_list:click.userbtn":[],
		"/detail/poi/review_list:click.editdelbtn":[],
		"/detail/poi/review_list:click.review":[],
		"/detail/poi/review_list:click.photo":[],
		"/detail/poi/review_list:all_scroll":[],
		"/detail/poi/blogreview_list:":[],
		"/detail/poi/blogreview_list:top_click.backbtn":[],
		"/detail/poi/blogreview_list:blogreview_click.blog":[],
		"/detail/poi/blogreview_list:all_scroll":[],
		"/detail/poi/closurepoipopup:":[],
		"/detail/poi/closurepoipopup:click.cancelbtn":[],
		"/detail/poi/closurepoipopup:click.deletereportbtn":[],
		"/detail/poi/editpoi:":[],
		"/detail/poi/editpoi:top_click.backbtn":[],
		"/detail/poi/editpoi:top_click.completebtn":[],
		"/detail/poi/photolist/photo:":[],
		"/detail/poi/photolist/photo:top_click.backbtn":[],
		"/detail/poi/photolist/photo:click.photo":[],
		"/detail/poi/photolist/photo:all_scroll":[],
		"/detail/poi/photolist/blog:":[],
		"/detail/poi/photolist/blog:top_click.backbtn":[],
		"/detail/poi/photolist/blog:click.blogphoto":[],
		"/detail/poi/photolist/blog:all_scroll":[],
		"/profile/my:":["certified_type"],
		"/profile/my:top_click.backbtn":[],
		"/profile/my:top_click.notibtn":[],
		"/profile/my:top_click.settingbtn":[],
		"/profile/my:profileinfo_click.profilephoto":["certified_type"],
		"/profile/my:profileinfo_click.grademark":[],
		"/profile/my:profileinfo_click.following":[],
		"/profile/my:profileinfo_click.follower":[],
		"/profile/my:subtab_click.reveiw":[],
		"/profile/my:subtab_click.place":[],
		"/profile/my:subtab_click.friendfeed":[],
		"/profile/my:subtab_click.coupon":[],
		"/profile/my:floating_click.review":[],
		"/profile/my:all_scroll":[],
		"/profile/my:click.poiname":[],
		"/profile/my:click.user":[],
		"/profile/my:click.review":[],
		"/profile/my:click.photo":[],
		"/profile/my:click.editdelbtn":[],
		"/profile/my:click.username":[],
		"/profile/my:click.filter":[],
		"/profile/my:click.mapview":[],
		"/profile/my:click.poi":[],
		"/profile/my:click.delbtn":[],
		"/profile/my:click.syruporderpaybtn":[],
		"/profile/my:click.reservelistbtn":[],
		"/profile/my:click.reserve":[],
		"/profile/my:click.couponlistbtn":[],
		"/profile/my:click.coupon":[],
		"/profile/my:click.coupondownbtn":[],
		"/profile/my/reservelist:":[],
		"/profile/my/reservelist:top_click.backbtn":[],
		"/profile/my/reservelist:click.reserve":[],
		"/profile/my/reservelist:all_scroll":[],
		"/profile/my/couponlist:top_click.backbtn":[],
		"/profile/my/couponlist:click.coupon":[],
		"/profile/my/couponlist:all_scroll":[],
		"/profile/another:":["certified_type"],
		"/profile/another:top_click.backbtn":[],
		"/profile/another:click.followbtn":[],
		"/profile/another:profileinfo_click.profilephoto":[],
		"/profile/another:profileinfo_click.grademark":[],
		"/profile/another:profileinfo_click.following":[],
		"/profile/another:profileinfo_click.follower":[],
		"/profile/another:subtab_click.reveiw":[],
		"/profile/another:subtab_click.place":[],
		"/profile/another:all_scroll":[],
		"/profile/another:click.poiname":[],
		"/profile/another:click.user":[],
		"/profile/another:click.review":[],
		"/profile/another:click.photo":[],
		"/profile/another:click.username":[],
		"/profile/another:click.filter":[],
		"/profile/another:click.mapview":[],
		"/profile/another:click.poi":[],
		"/profile/filter:":[],
		"/profile/filter:click.sorttype":[],
		"/profile/filter:click.filtertype":[],
		"/profile/filter:click.resetbtn":[],
		"/profile/filter:click.cancelbtn":[],
		"/profile/filter:click.confirmbtn":["filter_type","sort_type"],
		"/profile/mapview:":[],
		"/profile/mapview:top_click.backbtn":[],
		"/profile/mapview:map_click.locationbtn":[],
		"/profile/mapview:map_click.poipin":[],
		"/profile/mapview:map_click.poi":[],
		"/profile/mapview:map_swipe.poi":[],
		"/profile/my/edit:":[],
		"/profile/my/edit:top_click.backbtn":[],
		"/profile/my/edit:top_click.completebtn":[],
		"/profile/my/edit:profile_click.editphoto":[],
		"/profile/my/edit:profile_click.editnicknameinput":[],
		"/profile/my/edit:profile_click.editabourinput":[],
		"/profile/my/edit/inappgallery/photolist:":[],
		"/profile/my/edit/inappgallery/photolist:top_click.backbtn":[],
		"/profile/my/edit/inappgallery/photolist:top_click.folderbtn":[],
		"/profile/my/edit/inappgallery/photolist:top_click.nextbtn":[],
		"/profile/my/edit/inappgallery/photolist:click.camerabtn":[],
		"/profile/my/edit/inappgallery/photolist:click.photo":[],
		"/profile/my/edit/inappgallery/photolist:all_scroll":[],
		"/profile/my/edit/inappgallery/photofilter:":[],
		"/profile/my/edit/inappgallery/photofilter:top_click.backbtn":[],
		"/profile/my/edit/inappgallery/photofilter:top_click.completebtn":[],
		"/profile/my/edit/inappgallery/photofilter:nav_click.filterbtn":[],
		"/profile/my/edit/inappgallery/photofilter:nav_click.cropbtn":[],
		"/profile/my/edit/inappgallery/photofilter:click.filter":["photofilter_name"],
		"/profile/my/edit/inappgallery/photofilter:click_photo":[],
		"/profile/my/inappgallery/photofilter/crop:":[],
		"/profile/my/inappgallery/photofilter/crop:top_click.canclebtn":[],
		"/profile/my/inappgallery/photofilter/crop:top_click.completebtn":[],
		"/profile/my/inappgallery/photofilter/crop:drag.lefttophandler":[],
		"/profile/my/inappgallery/photofilter/crop:drag.righttophandler":[],
		"/profile/my/inappgallery/photofilter/crop:drag.rightbottomhandler":[],
		"/profile/my/inappgallery/photofilter/crop:drag.leftbottomhandler":[],
		"/profile/my/following:":[],
		"/profile/my/following:top_click.backbtn":[],
		"/profile/my/following:click.friendadd":[],
		"/profile/my/following:click.user":[],
		"/profile/my/following:click.followbtn":[],
		"/profile/my/follower:":[],
		"/profile/my/follower:top_click.backbtn":[],
		"/profile/my/follower:click.user":[],
		"/profile/my/follower:click.followbtn":[],
		"/profile/another/following:":[],
		"/profile/another/following:top_click.backbtn":[],
		"/profile/another/following:click.user":[],
		"/profile/another/following:click.followbtn":[],
		"/profile/another/follower:":[],
		"/profile/another/follower:top_click.backbtn":[],
		"/profile/another/follower:click.user":[],
		"/profile/another/follower:click.followbtn":[],
		"/profile/my/friendadd:":[],
		"/profile/my/friendadd:top_click.backbtn":[],
		"/profile/my/friendadd:click.searchinput":[],
		"/profile/my/friendadd:click.kakaofriends":[],
		"/profile/my/friendadd:click.facebookfriends":[],
		"/profile/my/friendadd:click.twitterfriends":[],
		"/profile/my/friendadd:click.user":[],
		"/profile/my/friendadd:click.followbtn":[],
		"/profile/my/friendadd:all_scroll":[],
		"/profile/my/friendadd/facebook:":[],
		"/profile/my/friendadd/facebook:top_click.backbtn":[],
		"/profile/my/friendadd/facebook:click.facebooksearchinput":[],
		"/profile/my/friendadd/facebook:click.user":[],
		"/profile/my/friendadd/facebook:click.invitefriendbtn":[],
		"/profile/my/friendadd/facebook:click.followbtn":[],
		"/profile/my/friendadd/twitter:":[],
		"/profile/my/friendadd/twitter:top_click.backbtn":[],
		"/profile/my/friendadd/twitter:click.twittersearchinput":[],
		"/profile/my/friendadd/twitter:click.user":[],
		"/profile/my/friendadd/twitter:click.invitefriendbtn":[],
		"/profile/my/friendadd/twitter:click.followbtn":[],
		"/profile/my/notification:":[],
		"/profile/my/notification:top_click.backbtn":[],
		"/profile/my/notification:subtab_click.notification":[],
		"/profile/my/notification:subtab_click.notice":[],
		"/profile/my/notification:click.notification":[],
		"/profile/my/notification:all_scroll":[],
		"/profile/my/notification:click.notice":[],
		"/detail/review:":[],
		"/detail/review:top_click.backbtn":[],
		"/detail/review:click.poiname":[],
		"/detail/review:click.user":[],
		"/detail/review:click.photo":[],
		"/detail/review:swipe.photo":[],
		"/detail/review:click.like":[],
		"/detail/review:click.likeuser":[],
		"/detail/review:click.commentuser":[],
		"/detail/review:comment_click.like":[],
		"/detail/review:comment_click.completebtn":[],
		"/detail/review/photo:":[],
		"/detail/review/photo:top_click.backbtn":[],
		"/detail/review/photo:click.photo":[],
		"/detail/review/photo:swipe.photo":[],
		"/detail/coupon:":[],
		"/detail/coupon:top_click.backbtn":[],
		"/detail/coupon:click.coupondownbtn":[],
		"/detail/coupon:click.couponpoi":[],
		"/detail/coupon:click.map":[],
		"/detail/coupon:click.callbtn":[],
		"/detail/coupon:click.tampbtn":[],
		"/detail/coupon/poilist:":[],
		"/detail/coupon/poilist:top_click.backbtn":[],
		"/detail/coupon/poilist:click.poi":[],
		"/detail/coupon/poilist:click.mapview":[],
		"/detail/coupon/mapview:":[],
		"/detail/coupon/mapview:top_click.backbtn":[],
		"/detail/coupon/mapview:map_click.locationbtn":[],
		"/detail/coupon/barcode:":[],
		"/detail/coupon/barcode:top_click.closebtn":[],
		"/detail/story/main:":[],
		"/detail/story/main:top_click.backbtn":[],
		"/detail/story/main:top_click.poibtn":[],
		"/detail/story/main:click.user":[],
		"/detail/story/main:swipe.story":[],
		"/detail/story/main:bottom_click.likebtn":[],
		"/detail/story/main:bottom_click.replybtn":[],
		"/detail/story/main:bottom_click.sharebtn":[],
		"/detail/story/content:":[],
		"/detail/story/content:top_click.backbtn":[],
		"/detail/story/content:top_click.poibtn":[],
		"/detail/story/content:swipe.story":[],
		"/detail/story/content:bottom_click.likebtn":[],
		"/detail/story/content:bottom_click.replybtn":[],
		"/detail/story/content:bottom_click.sharebtn":[],
		"/detail/story/content:body_click.image":[],
		"/detail/story/content:body_click.weblink":[],
		"/detail/story/content:body_click.calllink":[],
		"/detail/story/content:click.poi":[],
		"/detail/story/comment:":[],
		"/detail/story/comment:top_click.backbtn":[],
		"/detail/story/comment:top_click.poibtn":[],
		"/detail/story/comment:body_click.likebtn":[],
		"/detail/story/comment:share_click.kakaotalkbtn":[],
		"/detail/story/comment:share_click.facebookbtn":[],
		"/detail/story/comment:share_click.smsbtn":[],
		"/detail/story/comment:share_click.copylinkbtn":[],
		"/detail/story/comment:body_click.replybtn":[],
		"/detail/story/comment:body_click.replymorebtn":[],
		"/detail/story/comment:click.user":[],
		"/detail/story/comment:swipe.story":[],
		"/detail/story/comment:bottom_click.likebtn":[],
		"/detail/story/comment:bottom_click.replybtn":[],
		"/detail/story/comment:bottom_click.sharebtn":[],
		"/detail/story/suggest:":[],
		"/detail/story/suggest:top_click.backbtn":[],
		"/detail/story/suggest:top_click.poibtn":[],
		"/detail/story/suggest:click.story":[],
		"/detail/story/suggest:swipe.story":[],
		"/detail/story/suggest:bottom_click.likebtn":[],
		"/detail/story/suggest:bottom_click.replybtn":[],
		"/detail/story/suggest:bottom_click.sharebtn":[],
		"/detail/story/commentlist:":[],
		"/detail/story/commentlist:top_click.backbtn":[],
		"/detail/story/commentlist:top_click.closebtn":[],
		"/detail/story/commentlist:body_click.replybtn":[],
		"/detail/story/commentlist:click.user":[],
		"/detail/story/commentlist:click.editdelbtn":[],
		"/detail/story/commentlist:all_scroll":[],
		"/detail/story/poilist:":[],
		"/detail/story/poilist:top_click.backbtn":[],
		"/detail/story/poilist:top_click.closebtn":[],
		"/detail/story/poilist:click.poi":[],
		"/detail/story/poilist:all_scroll":[],
		"/detail/story/share_popup:":[],
		"/detail/story/share_popup:click.empty":[],
		"/detail/story/share_popup:share_click.kakaotalkbtn":[],
		"/detail/story/share_popup:share_click.facebookbtn":[],
		"/detail/story/share_popup:share_click.smsbtn":[],
		"/detail/story/share_popup:share_click.copylinkbtn":[],
		"/detail/story/share_popup:click.canclebtn":[],
		"/review:":[],
		"/review:top_click.backbtn":[],
		"/review:top_click.completebtn":[],
		"/review:click.ratebtn":[],
		"/review:click.deltagbtn":[],
		"/review:click.suggesttagbtn":[],
		"/review:click.photo":[],
		"/review:click.delphotobtn":[],
		"/review:click.inappgalleybtn":[],
		"/review:share_click.facebookbtn":[],
		"/review:share_click.twitterbtn":[],
		"/review/autocomplete:":[],
		"/review/autocomplete:click.autocompletetag":[],
		"/review/poilist:":[],
		"/review/poilist:top_click.backbtn":[],
		"/review/poilist:click.searchinput":[],
		"/review/poilist:subtab_click.tab":[],
		"/review/poilist:click.poi":[],
		"/review/poilist:click.map":[],
		"/review/poilist:all_scroll":[],
		"/review/poilist/mapview:":[],
		"/review/poilist/mapview:top_click.backbtn":[],
		"/review/poilist/mapview:map_click.locationbtn":[],
		"/review/poilist/mapview:map_click.poipin":[],
		"/review/poilist/searchresult:":[],
		"/review/poilist/searchresult:top_click.backbtn":[],
		"/review/poilist/searchresult:click.searchinput":[],
		"/review/poilist/searchresult:click.delquerybtn":[],
		"/review/poilist/searchresult:subtab_click.tab":[],
		"/review/poilist/searchresult:click.poi":[],
		"/review/poilist/searchresult:click.map":[],
		"/review/poilist/searchresult:bottom_click.newpoibtn":[],
		"/review/poilist/searchresult:all_scroll":[],
		"/review/inappgallery/photolist:":[],
		"/review/inappgallery/photolist:top_click.backbtn":[],
		"/review/inappgallery/photolist:top_click.folderbtn":[],
		"/review/inappgallery/photolist:top_click.nextbtn":[],
		"/review/inappgallery/photolist:click.camerabtn":[],
		"/review/inappgallery/photolist:click.photo":[],
		"/review/inappgallery/photolist:all_scroll":[],
		"/review/inappgallery:":[],
		"/review/inappgallery:top_click.backbtn":[],
		"/review/inappgallery:click.folder":[],
		"/review/inappgallery:all_scroll":[],
		"/review/inappgallery/photofilter:":[],
		"/review/inappgallery/photofilter:top_click.backbtn":[],
		"/review/inappgallery/photofilter:top_click.completebtn":[],
		"/review/inappgallery/photofilter:nav_click.filterbtn":[],
		"/review/inappgallery/photofilter:nav_click.cropbtn":[],
		"/review/inappgallery/photofilter:click.photo":[],
		"/review/inappgallery/photofilter:swipe.photo":[],
		"/review/inappgallery/photofilter:click.filter":["photofilter_name"],
		"/review/inappgallery/photofilter:swipe.filter":[],
		"/review/inappgallery/photofilter/crop:":[],
		"/review/inappgallery/photofilter/crop:top_click.canclebtn":[],
		"/review/inappgallery/photofilter/crop:top_click.completebtn":[],
		"/review/complete_popup:":[],
		"/review/complete_popup:click.okbtn":[],
		"/review/newpoi:":[],
		"/review/newpoi:top_click.backbtn":[],
		"/review/newpoi:top_click.completebtn":[],
		"/review/newpoi/findaddress:":[],
		"/review/newpoi/findaddress:top_click.backbtn":[],
		"/review/newpoi/findaddress:top_click.completebtn":[],
		"/review/newpoi/findaddress:subtab_click.streetNo":[],
		"/review/newpoi/findaddress:subtab_click.lotNo":[],
		"/review/newpoi/findaddress:search_click.roadname":[],
		"/review/newpoi/findaddress:search_click.address":[],
		"/setting:":[],
		"/setting:top_click.backbtn":[],
		"/setting:alarm_click.push":[],
		"/setting:alarm_click.expiredcoupon":[],
		"/setting:info_click.ver":[],
		"/setting:info_click.opensource":[],
		"/setting:tnc_click.tnc":[],
		"/setting:faq_click.faq":[],
		"/setting:account_click.importpickat":[],
		"/setting:account_click.accountshare":[],
		"/setting:account_click.logsignout":[],
		"/setting/push:":[],
		"/setting/push:top_click.backbtn":[],
		"/setting/push:content_click.commenton":[],
		"/setting/push:friendcontent_click.onceaday":[],
		"/setting/push:friendcontent_click.realtime":[],
		"/setting/push:friendcontent_click.off":[],
		"/setting/push:today_click.todayloc":[],
		"/setting/push:today_click.todaylocon":[],
		"/setting/push:friend_click.followingon":[],
		"/setting/push:benefit_click.benefiton":[],
		"/setting/push/todayloc:":[],
		"/setting/push/todayloc:top_click.closebtn":[],
		"/setting/push/todayloc:click.todaymenuloc":[],
		"/setting/push/adpushon_popup:":[],
		"/setting/push/adpushon_popup:click.notagree":[],
		"/setting/push/adpushon_popup:click.agree":[],
		"/setting/push/adpushoff_popup:":[],
		"/setting/push/adpushoff_popup:click.confirmbtn":[],
		"/setting/importpickat:":[],
		"/setting/importpickat:top_click.backbtn":[],
		"/setting/importpickat:click.facebook":[],
		"/setting/importpickat:click.twitter":[],
		"/setting/importpickat:click.oneid":[],
		"/setting/logsignout:":[],
		"/setting/logsignout:top_click.backbtn":[],
		"/setting/logsignout:click.logout":[],
		"/walkthrough:":[],
		"/walkthrough:top_swipe.walkthrough":[],
		"/walkthrough:bottom_click.start":[],
		"/signup/tnc:":[],
		"/signup/tnc:top_click.backbtn":[],
		"/signup/tnc:tnc_click.tncallcheck":[],
		"/signup/tnc:tnc_click.servicetnccheck":[],
		"/signup/tnc:tnc_click.servicetnccdetail":[],
		"/signup/tnc:tnc_click.personalinfocheck":[],
		"/signup/tnc:tnc_click.personalinfodetail":[],
		"/signup/tnc:tnc_click.locationinfocheck":[],
		"/signup/tnc:tnc_click.locationinfodetail":[],
		"/signup/tnc:tnc_click.overfourteencheck":[],
		"/signup/tnc:tnc_click.overfourteendetail":[],
		"/signup/tnc:tnc_click.benefitnotificationcheck":[],
		"/signup/tnc:tnc_click.personalcheck":[],
		"/signup/tnc:tnc_click.personalcheckdetail":[],
		"/signup/tnc:all_scroll.all":[],
		"/signup/tnc:click.confirm":[],
		"/syrupauthoverlay:":[],
		"/syrupauthoverlay:click.syruplogin":[],
		"/syrupauthoverlay:click.closebtn":[],
		"/signup_nickname:":[],
		"/signup_nickname:profile_click.editphoto":[],
		"/signup_nickname:profile_click.editnickname":[],
		"/signup_nickname:click.savebtn":[],
		"/signup_nickname/inappgallery/photolist:":[],
		"/signup_nickname/inappgallery/photolist:top_click.backbtn":[],
		"/signup_nickname/inappgallery/photolist:top_click.selectfolder":[],
		"/signup_nickname/inappgallery/photolist:top_click.nextbtn":[],
		"/signup_nickname/inappgallery/photolist:click_camera":[],
		"/signup_nickname/inappgallery/photolist:click_thumnailphoto":[],
		"/signup_nickname/inappgallery/photofilter:":[],
		"/signup_nickname/inappgallery/photofilter:top_click.backbtn":[],
		"/signup_nickname/inappgallery/photofilter:top_click.completebtn":[],
		"/signup_nickname/inappgallery/photofilter:click.photofilter":[],
		"/signup_nickname/inappgallery/photofilter:click_crop":[],
		"/signup_nickname/inappgallery/photofilter:click.photofilter_type":["photofilter_name"],
		"/signup_nickname/inappgallery/photofilter:click_photo":[],
		"/signup_nickname/inappgallery/photofilter/crop:":[],
		"/signup_nickname/inappgallery/photofilter/crop:top_click.cancelbtn":[],
		"/signup_nickname/inappgallery/photofilter/crop:top_click.savebtn":[],
		"/downloadsyrupwallet:":[],
		"/downloadsyrupwallet:top_closebtn":[],
		"/downloadsyrupwallet:download_click.tstore":[],
		"/downloadsyrupwallet:download_click.googleplay":[],
		"/downloadsyrupwallet:download_click.appstore":[],
		"/connectsyruptable:":[],
		"/connectsyruptable:top_click.backbtn":[],
		"/connectsyruptable:syrup_click.authsyrup":[],
		"/gpspopup:":[],
		"/gpspopup:click.cancelbtn":[],
		"/gpspopup:click.gpsonbtn":[],
		"/webview:":[],
		"/webview:top_click.backbtn":[],
		"/webview:top_click.canclebtn":[],
		"/editdelreview_popup:":[],
		"/editdelreview_popup:click.editbtn":[],
		"/editdelreview_popup:click.delbtn":[],
		"/delreivew_confirmpopup:":[],
		"/delreivew_confirmpopup:click.cancelbtn":[],
		"/delreivew_confirmpopup:click.delbtn":[],
		"/delplace_popup:":[],
		"/delplace_popup:click.delbtn":[],
		"/deleteplaceconfirm_popup:":[],
		"/deleteplaceconfirm_popup:click.cancelbtn":[],
		"/deleteplaceconfirm_popup:click.delbtn":[],
		"/reserve:":[],
		"/reserve:top_click.backbtn":[],
		"/reserve:click.poi":[],
		"/reserve:click.reservebtn":[],
		"/reserve_popup:":[],
		"/reserve_popup:click.reservebtn":[],
		"/reserve_popup:click.canclebtn":[],
		"/reserve/detail:":[],
		"/reserve/detail:click.poi":[],
		"/reserve/detail:share_click.kakaotalkbtn":[],
		"/reserve/detail:share_click.facebookbtn":[],
		"/reserve/detail:share_click.smsbtn":[],
		"/reserve/detail:click.reservecanclebtn":[],
		"/reserve/detail:click.reservebtn":[],
		"/reserve/detail:all_scroll":[],
		"/reserve/detail:click.okbtn":[],
		"/reserve/detail:click.nobtn":[]
    };

    var shuttle = {};

    function capitalize(str) {
        str = str === null ? '' : String(str);
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    for (var i = 0, headersLength = headers.length; i < headersLength; i++) {

        log.sentinel_meta._$fieldOrder[headers[i]] = i;
        if(headers[i] === "log_version"){
            log[headers[i]] = "15.09.01:1.5.53:22";
        }else{
            log[headers[i]] = "";
            shuttle['set' + capitalize(headers[i])] = (function (header) {
                return function (value) {
                    log[header] = value;
                    return shuttle;
                };
            })(headers[i]);
        }
    }

    for (var i = 0, bodiesLength = bodies.length; i < bodiesLength; i++) {
        shuttle['set' + capitalize(bodies[i])] = (function (body) {
            return function (value) {
                log._$body[body] = value;
                return shuttle;
            };
        })(bodies[i]);
    }

    log._$body = {};
    log.sentinel_meta._$fieldOrder._$body = headers.length;

    for (var i = 0, headersLength = headers.length; i < headersLength; i++) {
        shuttle['get' + capitalize(headers[i])] = (function (header) {
            return function () {
                return log[header];
            };
        })(headers[i]);
    }

    for (var i = 0, bodiesLength = bodies.length; i < bodiesLength; i++) {
        shuttle['get' + capitalize(bodies[i])] = (function (body) {
            return function () {
                return log._$body[body];
            };
        })(bodies[i]);
    }

    /**
     * @deprecated As of Sentinel-Shuttle 1.5.30, use getImmutableJSONObject() instead
     */
    shuttle.get = function () {
        return log;
    };

    shuttle.getImmutableJSONObject = function () {
        var immutable = JSON.parse(JSON.stringify(log));
        return immutable;
    };

    shuttle.getBodyFields = function () {
       return bodies.slice();
    };

    shuttle.getHeaderFields = function() {
       return headers.slice();
    };

    shuttle.getBodyLayout = function() {
        return JSON.parse(JSON.stringify(body_layout));
    }

    shuttle.getActionKeyFields = function() {
        return action_key_fields.slice();
    }

    shuttle.getFromLog = function (jsonLog) {
        jsonLog["sentinel_meta"] = log.sentinel_meta;
        return jsonLog;
    };

    shuttle.getSentinelMeta = function () {
        return log.sentinel_meta;
    };

    shuttle.toShuttleObject = function(data) {
    	for (var i = 0, headersLength = headers.length; i < headersLength; i++) {
			if(data.hasOwnProperty(headers[i])) {
				log[headers[i]] = data[headers[i]];
			}
			else {
				log[headers[i]] = "";
			}
		}

        log["log_version"] = "15.09.01:1.5.53:22";

		for (var i = 0, bodiesLength = bodies.length; i < bodiesLength; i++) {
			if(data.hasOwnProperty(bodies[i])) {
				log._$body[bodies[i]] = data[bodies[i]];
			}
		}

		return log;
    };

    shuttle.clearBody = function () {
        if (log._$body) {
            log._$body = {};
        }
        return shuttle;
    };

    shuttle.createSetBodyOfFunctionName = function (actionKey) {
      return "setBodyOf" + actionKey.replace(/:/gi, "__").replace(/[\/\.]/gi, "_");
    };

    /**
     * setBodyOf* method example
     *
     * if an action key is "/detail/eventpoint:note_tab.unfoldbtn"
     * (combined action key using ':')
     *
     * and the body layout looks like
     *
     * "/detail/eventpoint:note_tap.unfoldbtn":["geofence_key","advertise_id","mbr_id"]
     *
     * then a setBodyOf* method will be created dynamically using the code described below
     *
     * shuttle.setBodyOf_detail_eventpoint__note_tab_unfoldbtn = function() {
     *    log: this.clearBody();
     *    this.get().page_id = "/detail/eventpoint";
     *    this.get().action_id = "note_tap.unfoldbtn";
     *    this.get()._$body.geofence_key = (geofence_key === 0) ? geofence_key : (geofence_key || '' );
     *    this.get()._$body.advertise_id = (advertise_id === 0) ? advertise_id : (advertise_id || '' );
     *    this.get()._$body.mbr_id = (mbr_id) === 0 ?mbr_id : (mbr_id || '' );
     * };
     */
    var createSetBodyOfFunc = function (shuttle, actionKeyFields, actionKey, requiredBodyFields) {
      var setBodyOfFuncName = shuttle.createSetBodyOfFunctionName(actionKey);

      var temp = [];
      var logVar = "this.get()";
      var bodyVarName = logVar + "._$body.";

      temp.push(
        "this.clearBody(); ");

      for (var index = 0; index < requiredBodyFields.length; index++) {
        var bodyFields = requiredBodyFields[index];

        temp.push(
          bodyVarName + bodyFields +
          " = (" + bodyFields+ " === 0) ? " + bodyFields + " : (" + bodyFields + " || '' ); "
        );
      }

      var actionKeyFieldValues = actionKey.split(":");

      for(var index = 0; index < actionKeyFieldValues.length; index++) {
        var actionKeyValue = actionKeyFieldValues[index];

        temp.push(
          logVar + "." + actionKeyFields[index] + " = \"" + actionKeyValue + "\"; ");
      }

      if (actionKeyFields.length < 0 || actionKeyFields.length > 2)
      throw new Error("Invalid action key fields length");

      temp.push(
        "return this;"
      );

      var funcBody = temp.join("");

      shuttle[setBodyOfFuncName] = new Function(requiredBodyFields, funcBody);
    };

  /* create setBodyOf* dynamically SEN-230 */
  var actionKeys = Object.keys(body_layout);

  for (var i = 0; i < actionKeys.length; i++) {
    var actionKey = actionKeys[i];
    createSetBodyOfFunc(shuttle, action_key_fields, actionKey, body_layout[actionKey]);
  }

  return shuttle;
}


