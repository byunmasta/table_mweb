<%@page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
<title>Syrup Table</title>
<link rel="stylesheet" type="text/css" href="/css/story.css" />
</head>
<body>

<div class="ui-wrap-android"> <!-- ios 에서 사용되는 class : .ui-wrap-ios -->

	<div class="error404">
		<div class="inner">
			<strong>페이지를 찾을 수 없습니다.</strong>
			<span class="txt">페이지의 주소가 잘못 입력 되었거나 요청하신 페이지의<br />
				주소가 변경 혹은 삭제되어 페이지를 찾을 수 없습니다.<br />
				입력하신 주소가 정확한지 한번 더 확인해 주세요.</span>
			<a href="/main"><span class="ui-hidden">Syrup Table 홈</span></a>
		</div>
	</div>

</div>

</body>
</html>