/*
Copyright 2013 SK PLANET
*/

(function (window, undefined) {
	var PLANET = {};
    var doc = document;
    window.PLANET = window.PLANET || PLANET;
    
    //플랫폼 체크
    var userAgent = navigator.userAgent.toLocaleLowerCase();
    if(userAgent.search("android") > -1) {
    	PLANET.os = "android";
    	if (userAgent.search("chrome") > -1) {
    		PLANET.browser = "android+chrome";
        }

    }else if (userAgent.search("iphone") > -1 || userAgent.search("ipod") > -1 || userAgent.search("ipad") > -1) {
    	PLANET.os = "ios";
    }else{
    	PLANET.os = "etc";
    }
    
    //앱정보
    var app = {
		pickat : {
            baseUrl : "pickat2://"
            , package : "com.skt.tmaphot"
            , store : {
            	android : {
            		googleplay : "market://details?id=com.skt.tmaphot"
            		, tstore :	"tstore://PRODUCT_VIEW/0000250761/1/PAYMENT"
            	}
            	, ios : "https://itunes.apple.com/kr/app/pickat-pikaes/id489789094?mt=8"
                , etc : "http://m.pickat.com/main"
            }
        }
    	, pickatsg : {
            baseUrl : "pickatsg://"
            , package : "com.skplanet.pickatsg"
            , store : {
            	android : {
            		googleplay : "market://details?id=com.skplanet.pickatsg"
            		, tstore : ""
            	}
            	, ios : "https://itunes.apple.com/app/pickat-sg/id635816245&mt=8"
                , etc : "http://www.pickat.com.sg"
            }
        }
        , tmap : {
        	baseUrl : "TMAP://"
        	, package : "com.skt.skaf.l001mtm091"
			, store : {
				android : {
            		googleplay : "http://m.tstore.co.kr/mobilepoc/etc/downloadGuide.omp"
            		, tstore : "tstore://PRODUCT_VIEW/0000163382/0"
            	}
				, ios : "https://itunes.apple.com/kr/app/tmap-sk/id431589174&mt=8"
				, etc : "http://www.tmap.co.kr"
			}
        }
        , syruporder : {
        	baseUrl : "syruporder://"
        	, package : "com.skplanet.mbuzzer"
			, store : {
				android : {
            		googleplay : "https://play.google.com/store/apps/details?id=com.skplanet.mbuzzer"
            		, tstore : null
            	}
				, ios : "http://itunes.apple.com/kr/app/syrup-order/id952121334?mt=8"
				, etc : "https://www.facebook.com/Syrup.SKP"
			}
        }
    };

    //플래닛링크 동작
    PLANET.link = function(name) {
        var link_app = app[name];
        if ( !link_app ) return { send : function() { throw "App is not exists."; }};
        return {
        	//앱 실행, 설치 안되어있으면 다운로드 링크
        	send : function (cmd, params) {
                var _app = this.app;
                var linkUrl = _app.baseUrl+cmd+"?"+objectToString(params);
                var goDownloadStore = (function (os){
                    return function () {
                    	if(os == 'android'){
                    		if(_app.store['android']['tstore'] != null){
                    			var iframe = doc.createElement('iframe');
                                iframe.style.visibility = 'hidden';
                                iframe.src = _app.store['android']['tstore'];
                                iframe.onload = function(){ window.location = _app.store['android']['googleplay'];} ;
                                doc.body.appendChild(iframe);
                    		}else{
                    			window.location = _app.store['android']['googleplay'];
                    		}
                    	}else{
                    		window.location = _app.store[os];
                    	}
                    };
                })(this.os);
                
                if (this.os == "ios") {
                    setTimeout(goDownloadStore, 35);
                    window.location = linkUrl;
                    
                } else if (this.os == "android") {
                	if (this.browser == "android+chrome") {
                        window.location = "intent://action#Intent;scheme=syruporder;action=android.intent.action.VIEW;category=android.intent.category.DEFAULT;category=android.intent.category.BROWSABLE;package=com.skplanet.mbuzzer;end;";
                    }else{
	                	var iframe = doc.createElement('iframe');
	                    iframe.style.display = 'none';
	                    iframe.src = linkUrl;
	                    iframe.onload = goDownloadStore;
	                    doc.body.appendChild(iframe);
                    }
                }else{
                	window.location = _app.store['etc'];
                }
            }
	        //바로 다운로드 페이지 연결
	        , downloadApp : function(){
	        	var _app = this.app;
	            if(this.os == 'android'){
	        		var iframe = doc.createElement('iframe');
	        		iframe.style.height = '1px';
	        		iframe.frameBorder = '0';
	        		iframe.setAttribute("frameBorder", "0");
	        		iframe.style.border = '0';
	                iframe.style.visibility = 'hidden';
	                iframe.src = _app.store['android']['tstore'];
	                iframe.onload = function(){ window.location = _app.store['android']['googleplay'];} ;
	                doc.body.appendChild(iframe);
	        	}else{
	        		window.location = _app.store[this.os];
	        	}
	        }
            , app : link_app
            , os : PLANET.os
            , browser : PLANET.browser
        };

        function objectToString(params) {
            var strParam = [];
            for ( var key in params) {
            	strParam.push(key + "=" + encodeURIComponent(params[key]));
            }
            return strParam.join("&");
        }
    };

}(window));