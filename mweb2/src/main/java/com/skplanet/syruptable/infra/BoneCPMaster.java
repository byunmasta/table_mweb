package com.skplanet.syruptable.infra;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix="bonecp.datasource.master")
public class BoneCPMaster extends BoneCPDataSourceProperties {
	
	private static final Logger logger = LoggerFactory.getLogger(BoneCPMaster.class);

	public BoneCPMaster() {
		logger.info("Loading Master Config...");
	}

}
