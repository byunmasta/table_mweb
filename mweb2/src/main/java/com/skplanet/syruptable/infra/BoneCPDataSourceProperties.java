package com.skplanet.syruptable.infra;


public class BoneCPDataSourceProperties  {
	
	private String jdbcUrl;

	private String jdbcUsername;

	private String jdbcPassword;

	private String driverClass;

	private Integer idleMaxAgeInMinutes;

	private Integer idleConnectionTestPeriodInMinutes;

	private Integer maxConnectionsPerPartition;

	private Integer minConnectionsPerPartition;

	private Integer partitionCount;

	private Integer acquireIncrement;

	private Integer statementsCacheSize;

	public String getJdbcUrl() {
		return jdbcUrl;
	}

	public void setJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
	}

	public String getJdbcUsername() {
		return jdbcUsername;
	}

	public void setJdbcUsername(String jdbcUsername) {
		this.jdbcUsername = jdbcUsername;
	}

	public String getJdbcPassword() {
		return jdbcPassword;
	}

	public void setJdbcPassword(String jdbcPassword) {
		this.jdbcPassword = jdbcPassword;
	}

	public String getDriverClass() {
		return driverClass;
	}

	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}

	public Integer getIdleMaxAgeInMinutes() {
		return idleMaxAgeInMinutes;
	}

	public void setIdleMaxAgeInMinutes(Integer idleMaxAgeInMinutes) {
		this.idleMaxAgeInMinutes = idleMaxAgeInMinutes;
	}

	public Integer getIdleConnectionTestPeriodInMinutes() {
		return idleConnectionTestPeriodInMinutes;
	}

	public void setIdleConnectionTestPeriodInMinutes(
			Integer idleConnectionTestPeriodInMinutes) {
		this.idleConnectionTestPeriodInMinutes = idleConnectionTestPeriodInMinutes;
	}

	public Integer getMaxConnectionsPerPartition() {
		return maxConnectionsPerPartition;
	}

	public void setMaxConnectionsPerPartition(Integer maxConnectionsPerPartition) {
		this.maxConnectionsPerPartition = maxConnectionsPerPartition;
	}

	public Integer getMinConnectionsPerPartition() {
		return minConnectionsPerPartition;
	}

	public void setMinConnectionsPerPartition(Integer minConnectionsPerPartition) {
		this.minConnectionsPerPartition = minConnectionsPerPartition;
	}

	public Integer getPartitionCount() {
		return partitionCount;
	}

	public void setPartitionCount(Integer partitionCount) {
		this.partitionCount = partitionCount;
	}

	public Integer getAcquireIncrement() {
		return acquireIncrement;
	}

	public void setAcquireIncrement(Integer acquireIncrement) {
		this.acquireIncrement = acquireIncrement;
	}

	public Integer getStatementsCacheSize() {
		return statementsCacheSize;
	}

	public void setStatementsCacheSize(Integer statementsCacheSize) {
		this.statementsCacheSize = statementsCacheSize;
	}

	@Override
	public String toString() {
		return String
				.format("BoneCPDataSourceProperties [jdbcUrl=%s, jdbcUsername=%s, jdbcPassword=%s, driverClass=%s, idleMaxAgeInMinutes=%s, idleConnectionTestPeriodInMinutes=%s, maxConnectionsPerPartition=%s, minConnectionsPerPartition=%s, partitionCount=%s, acquireIncrement=%s, statementsCacheSize=%s]",
						jdbcUrl, jdbcUsername, jdbcPassword, driverClass,
						idleMaxAgeInMinutes, idleConnectionTestPeriodInMinutes,
						maxConnectionsPerPartition, minConnectionsPerPartition,
						partitionCount, acquireIncrement, statementsCacheSize);
	}
	
	
	
	

}
