package com.skplanet.syruptable.repository.user;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.skplanet.syruptable.entity.user.User_Pois;
import com.skplanet.syruptable.entity.user.User_Pois_PK;

/**
 * UserPoisRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UserPoisRepository extends PagingAndSortingRepository<User_Pois, User_Pois_PK> {
	
	
	@Query(value = 
			"select * "
			+ "from user_pois, sb_pois, pois "
			+ "where user_pois.user_id = :userId "
			+ "and user_pois.poi_id = sb_pois.poi_id "
			+ "and sb_pois.poi_id = pois.id "
			+ "order by user_pois.created_at desc "
			+ "limit :limit offset :cursor", nativeQuery=true)
	public List<User_Pois> getUserPlace(
							@Param("userId") int userId,
							@Param("cursor") int cursor,
							@Param("limit") int limit);
	
	
	@Query(value = 
			"select count(1) "
			+ "from user_pois, sb_pois, pois "
			+ "where user_pois.user_id = :userId "
			+ "and user_pois.poi_id = sb_pois.poi_id "
			+ "and sb_pois.poi_id = pois.id ", nativeQuery=true)
	public int getUserPlaceTotal(@Param("userId") int userId);
	
	
}
