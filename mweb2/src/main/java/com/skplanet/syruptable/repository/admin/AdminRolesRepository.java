package com.skplanet.syruptable.repository.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_Roles;

/**
 * AdminRolesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminRolesRepository extends PagingAndSortingRepository<Admin_Roles, Integer> {
}
