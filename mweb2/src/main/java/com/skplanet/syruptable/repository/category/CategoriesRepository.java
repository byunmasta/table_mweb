package com.skplanet.syruptable.repository.category;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.category.Categories;

/**
 * CategoriesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface CategoriesRepository extends PagingAndSortingRepository<Categories, String> {
}
