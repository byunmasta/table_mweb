package com.skplanet.syruptable.repository.aoi;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.aoi.Aoi_Groups;

/**
 * Aoi_GroupsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AoiGroupsRepository extends PagingAndSortingRepository<Aoi_Groups, Integer> {
	
	List<Aoi_Groups> findAllByParentId(int parentId);

}
