package com.skplanet.syruptable.repository.pick;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.pick.Pick_Images;

/**
 * PickImagesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface PickImagesRepository extends PagingAndSortingRepository<Pick_Images, Integer> {
	
	/**
	 * findFirstBypoiIdOrderByCreatedatDesc
	 * @param poiId
	 * @return
	 */
	Pick_Images findFirstByPoiIdOrderByCreatedatDesc(Integer poiId);
	
	/**
	 * findByPickIdOrderByCreatedatDesc
	 * @param pickId
	 * @return
	 */
	List<Pick_Images> findByPickIdOrderByCreatedatDesc(Integer pickId);
	
	/**
	 * findByPoiIdOrderByCreatedatDesc
	 * @param poiId
	 * @return
	 */
	List<Pick_Images> findByPoiIdOrderByCreatedatDesc(Integer poiId);
}
