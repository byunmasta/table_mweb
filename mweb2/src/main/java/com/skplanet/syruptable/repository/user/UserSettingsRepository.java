package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.User_Settings;

/**
 * UserSettingsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UserSettingsRepository extends PagingAndSortingRepository<User_Settings, Integer> {
}
