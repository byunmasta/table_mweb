package com.skplanet.syruptable.repository.poi;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.poi.Poi_Blog_Ratings;

/**
 * PoiBlogRatingsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface PoiBlogRatingsRepository extends PagingAndSortingRepository<Poi_Blog_Ratings, Integer> {
}
