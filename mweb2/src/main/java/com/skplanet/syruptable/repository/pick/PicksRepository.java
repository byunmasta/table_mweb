package com.skplanet.syruptable.repository.pick;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.skplanet.syruptable.entity.pick.Picks;

/**
 * PicksRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface PicksRepository extends PagingAndSortingRepository<Picks, Integer> {
	/**
	 * findByPublicthemeid
	 * @param publicthemeid
	 * @param Pageable
	 * @return
	 */
	Page<Picks> findByPublicThemeId(Integer publicthemeid, Pageable Pageable);
	
	/**
	 * findByPoiId
	 * @param poiId
	 * @param Pageable
	 * @return
	 */
	Page<Picks> findByPoiId(Integer poiId, Pageable Pageable);
	
	/**
	 * findFirstBypoiIdOrderByUpdatedatDesc
	 * @param poiId
	 * @return
	 */
	Picks findFirstByPoiIdOrderByUpdatedatDesc(Integer poiId);
	
	/**
	 * findFirstByPoiIdAndUserIdOrderByUpdatedatDesc
	 * @param poiId
	 * @param userId
	 * @return
	 */
	Picks findFirstByPoiIdAndUserIdOrderByUpdatedatDesc(Integer poiId, Integer userId);
	
	/**
	 * countByPoiIdAndUserId
	 * @param poiId
	 * @param userId
	 * @return
	 */
	@Query( value="\n"+
			"SELECT count(*) AS CNT \n"+
			"FROM \n"+
			"( \n"+
			"	SELECT poi_id, user_id \n"+
			"	FROM picks \n"+
			"	WHERE poi_id= :poiId \n"+
			"	GROUP BY poi_id, user_id \n"+
			") T \n", nativeQuery=true)
	Integer getCountByPoiId(@Param("poiId") Integer poiId);
	
	
	@Query(value = 
			"select * "
			+ "from picks, sb_picks, pois "
			+ "where picks.user_id = :userId "
			+ "and picks.id = sb_picks.pick_id "
			+ "and picks.poi_id = pois.id "
			+ "order by picks.created_at desc "
			+ "limit :limit offset :cursor", nativeQuery=true)
	List<Picks> findByUserId(@Param("userId") int userId,
							@Param("cursor") int cursor,
							@Param("limit") int limit);
	
}
