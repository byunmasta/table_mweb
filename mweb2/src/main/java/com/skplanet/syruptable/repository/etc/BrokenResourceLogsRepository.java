package com.skplanet.syruptable.repository.etc;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.etc.Broken_Resource_Logs;

/**
 * AgreementsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface BrokenResourceLogsRepository extends PagingAndSortingRepository<Broken_Resource_Logs, Integer> {
}
