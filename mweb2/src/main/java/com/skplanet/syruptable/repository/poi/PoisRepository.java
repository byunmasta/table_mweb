package com.skplanet.syruptable.repository.poi;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.poi.Pois;

public interface PoisRepository extends PagingAndSortingRepository<Pois, Integer> {
//	Page<Pois> findByPoiratingsRating(String rating, Pageable pageable);
}
