package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.User_Score_Logs;

/**
 * UserScoreLogsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UserScoreLogsRepository extends PagingAndSortingRepository<User_Score_Logs, Integer> {
}
