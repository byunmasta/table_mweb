package com.skplanet.syruptable.repository.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_Reserved_Nicknames;

/**
 * AdminReservedNicknamesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminReservedNicknamesRepository extends PagingAndSortingRepository<Admin_Reserved_Nicknames, Integer> {
}
