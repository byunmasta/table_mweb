package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.Syrup_Tokens;
import com.skplanet.syruptable.entity.user.Syrup_Tokens_PK;

/**
 * AuthtokensRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface SyrupTokensRepository extends PagingAndSortingRepository<Syrup_Tokens, Syrup_Tokens_PK> {
}
