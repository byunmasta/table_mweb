package com.skplanet.syruptable.repository.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_Banners;

/**
 * AdminBannersRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminBannersRepository extends PagingAndSortingRepository<Admin_Banners, Integer> {
}
