package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.Auth_Tokens;

/**
 * AuthtokensRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface AuthTokensRepository extends PagingAndSortingRepository<Auth_Tokens, Integer> {
}
