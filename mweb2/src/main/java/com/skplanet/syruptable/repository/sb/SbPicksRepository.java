package com.skplanet.syruptable.repository.sb;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.sb.Sb_Picks;

/**
 * PickLikesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface SbPicksRepository extends PagingAndSortingRepository<Sb_Picks, Integer> {
}
