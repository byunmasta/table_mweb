package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.User_Msgbox_Alerts;

/**
 * UserMsgboxAlertsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UserMsgboxAlertsRepository extends PagingAndSortingRepository<User_Msgbox_Alerts, Integer> {
}
