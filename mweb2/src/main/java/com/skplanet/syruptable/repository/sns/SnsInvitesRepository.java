package com.skplanet.syruptable.repository.sns;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.sns.Sns_Invites;
import com.skplanet.syruptable.entity.sns.Sns_Invites_PK;

/**
 * PickLikesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface SnsInvitesRepository extends PagingAndSortingRepository<Sns_Invites, Sns_Invites_PK> {
}
