package com.skplanet.syruptable.repository.recommended;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.recommended.Recommended_Categories;

/**
 * PickLikesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface RecommendedCategoriesRepository extends PagingAndSortingRepository<Recommended_Categories, Integer> {
}
