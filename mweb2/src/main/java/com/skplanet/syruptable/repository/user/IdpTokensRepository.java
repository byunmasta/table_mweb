package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.Idp_Tokens;
import com.skplanet.syruptable.entity.user.Idp_Tokens_PK;

/**
 * IdpTokensRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface IdpTokensRepository extends PagingAndSortingRepository<Idp_Tokens, Idp_Tokens_PK> {
}
