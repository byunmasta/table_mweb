package com.skplanet.syruptable.repository.etc;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.etc.Popups;

/**
 * AgreementsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface PopupsRepository extends PagingAndSortingRepository<Popups, Integer> {
}
