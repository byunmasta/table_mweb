package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.Users;

/**
 * UsersRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UsersRepository extends PagingAndSortingRepository<Users, Integer> {
}
