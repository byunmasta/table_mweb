package com.skplanet.syruptable.repository.tag;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.tag.Tag_Pois;

/**
 * PickLikesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface TagPoisRepository extends PagingAndSortingRepository<Tag_Pois, Integer> {
}
