package com.skplanet.syruptable.repository.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_Menus;

/**
 * AdminMenusRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminMenusRepository extends PagingAndSortingRepository<Admin_Menus, Integer> {
}
