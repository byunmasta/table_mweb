package com.skplanet.syruptable.repository.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_Users;

/**
 * AdminUsersRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminUsersRepository extends PagingAndSortingRepository<Admin_Users, Integer> {
}
