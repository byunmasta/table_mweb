package com.skplanet.syruptable.repository.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_Push_Report;

/**
 * AdminPushReportRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminPushReportRepository extends PagingAndSortingRepository<Admin_Push_Report, Integer> {
}
