package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.User_Migration_Logs;

/**
 * UserMigrationLogsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UserMigrationLogsRepository extends PagingAndSortingRepository<User_Migration_Logs, Integer> {
}
