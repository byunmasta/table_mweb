package com.skplanet.syruptable.repository.pick;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.pick.Pick_Likes;
import com.skplanet.syruptable.entity.pick.Pick_Likes_PK;

/**
 * PickLikesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface PickLikesRepository extends PagingAndSortingRepository<Pick_Likes, Pick_Likes_PK> {
}
