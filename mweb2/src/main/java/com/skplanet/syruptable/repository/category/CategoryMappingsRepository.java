package com.skplanet.syruptable.repository.category;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.category.Category_Mappings;
import com.skplanet.syruptable.entity.category.Category_Mappings_PK;

/**
 * CategoryMappingsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface CategoryMappingsRepository extends PagingAndSortingRepository<Category_Mappings, Category_Mappings_PK> {
}
