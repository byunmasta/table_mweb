package com.skplanet.syruptable.repository.story;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.story.Story_Contents;
import com.skplanet.syruptable.entity.story.Story_Contents_PK;

/**
 * StoryGroupsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface StoryContentsRepository extends PagingAndSortingRepository<Story_Contents, Story_Contents_PK> {
}