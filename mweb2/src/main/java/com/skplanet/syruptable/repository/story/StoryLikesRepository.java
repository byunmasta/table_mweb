package com.skplanet.syruptable.repository.story;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.story.Story_Like_PK;
import com.skplanet.syruptable.entity.story.Story_Likes;

/**
 * StoryLikesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface StoryLikesRepository extends PagingAndSortingRepository<Story_Likes, Story_Like_PK> {
}