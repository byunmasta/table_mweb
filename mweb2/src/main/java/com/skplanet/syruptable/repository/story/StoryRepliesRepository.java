package com.skplanet.syruptable.repository.story;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.story.Story_Replies;

/**
 * StoryRepliesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface StoryRepliesRepository extends PagingAndSortingRepository<Story_Replies, Integer> {
	
	@Query(value=
			"SELECT * "
			+ "FROM story_replies  "
			+ "where story_id = :storyId "
			+ "order by created_at desc "
			+ "limit :limit offset :cursor ", nativeQuery=true)
	List<Story_Replies> findByStoryId(
			@Param("storyId") int storyId,
			@Param("cursor") int cursor,
			@Param("limit") int limit
			);

	
	@Query(value=
			"select count(*) "
			+ "from story_replies "
			+ "where story_id = :storyId ", nativeQuery=true)
	int countByStoryId(
			@Param("storyId") int storyId
			);
	
	@Query(value=
			"select user_id "
			+ "from story_replies "
			+ "where id = :replyId ", nativeQuery=true)
	int findUserIdByReplyId(
			@Param("replyId") int replyId
			);
	
}