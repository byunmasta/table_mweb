package com.skplanet.syruptable.repository.etc;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.etc.Notices;

/**
 * AgreementsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface NoticesRepository extends PagingAndSortingRepository<Notices, Integer> {
}
