package com.skplanet.syruptable.repository.sorder;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.sorder.Sorder_Main_Banners;

public interface SorderMainBannersRepository extends PagingAndSortingRepository<Sorder_Main_Banners, Integer> {
}
