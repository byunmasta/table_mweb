package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.User_Access_Logs;
import com.skplanet.syruptable.entity.user.User_Access_Logs_PK;

/**
 * UserAccessLogsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UserAccessLogsRepository extends PagingAndSortingRepository<User_Access_Logs, User_Access_Logs_PK> {
}
