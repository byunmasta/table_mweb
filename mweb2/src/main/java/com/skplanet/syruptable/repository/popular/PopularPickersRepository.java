package com.skplanet.syruptable.repository.popular;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.popular.Popular_Pickers;

/**
 * PickLikesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface PopularPickersRepository extends PagingAndSortingRepository<Popular_Pickers, Integer> {
}
