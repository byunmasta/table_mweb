package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.User_Idp_Accounts;
import com.skplanet.syruptable.entity.user.User_Idp_Accounts_PK;

/**
 * UserIdpAccountsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UserIdpAccountsRepository extends PagingAndSortingRepository<User_Idp_Accounts, User_Idp_Accounts_PK> {
}
