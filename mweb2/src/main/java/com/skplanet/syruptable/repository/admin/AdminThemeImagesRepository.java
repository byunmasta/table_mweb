package com.skplanet.syruptable.repository.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_Theme_Images;

/**
 * AdminThemeImagesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminThemeImagesRepository extends PagingAndSortingRepository<Admin_Theme_Images, Integer> {
}
