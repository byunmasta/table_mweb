package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.User_Relations;
import com.skplanet.syruptable.entity.user.User_Relations_PK;

/**
 * UserRelationsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UserRelationsRepository extends PagingAndSortingRepository<User_Relations, User_Relations_PK> {
	
	/**
	 * find Friend Y/N 
	 * @param followerId
	 * @param followeeId
	 * @return
	 */
	User_Relations findFirstByFollowerIdOrFolloweeId(Integer followerId, Integer followeeId);
}
