package com.skplanet.syruptable.repository.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_App_Images;

/**
 * AdminAppImagesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminAppImagesRepository extends PagingAndSortingRepository<Admin_App_Images, Integer> {
}
