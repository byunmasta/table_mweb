package com.skplanet.syruptable.repository.poi;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.poi.Poi_Reports;

/**
 * PoiReportsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface PoiReportsRepository extends PagingAndSortingRepository<Poi_Reports, Integer> {
}
