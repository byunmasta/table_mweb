package com.skplanet.syruptable.repository.etc;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.etc.Stat_Ads_Agreements;

/**
 * AgreementsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface StatAdsAgreementsRepository extends PagingAndSortingRepository<Stat_Ads_Agreements, Integer> {
}
