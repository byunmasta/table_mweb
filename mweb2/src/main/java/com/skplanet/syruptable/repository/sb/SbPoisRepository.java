package com.skplanet.syruptable.repository.sb;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.skplanet.syruptable.entity.sb.Sb_Pois;

/**
 * SbPoisRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface SbPoisRepository extends PagingAndSortingRepository<Sb_Pois, Integer> {
	/**
	 * findByAoiIds
	 * @param aoiid
	 * @param cursor
	 * @param limit
	 * @return
	 */
	@Query(value=
			"select * "
			+ "from sb_pois sbpois "
			+ "where sbpois.aoi_ids @> ARRAY[:aoiid] "
			+ "order by score desc "
			+ "limit :limit offset :cursor ", nativeQuery=true)
	List<Sb_Pois> findByAoiIdsOrderByScoreDesc(
			@Param("aoiid") int aoiid,
			@Param("cursor") int cursor,
			@Param("limit") int limit
			);
	
	/**
	 * Tag Pois findByAoiIds
	 * @param aoiid
	 * @param cursor
	 * @param limit
	 * @return
	 */
	@Query(value=
			"select * "
			+ "from sb_pois sbpois, tag_pois tagpois "
			+ "where sbpois.poi_id=tagpois.poi_id and sbpois.aoi_ids @> ARRAY[:aoiId] "
			+ "order by sbpois.score desc "
			+ "limit :limit offset :cursor ", nativeQuery=true)
	List<Sb_Pois> findByAoiIds(
			@Param("aoiId") int aoiId,
			@Param("cursor") int cursor,
			@Param("limit") int limit
			);
	
	@Query(value=
			"select count(*) "
			+ "from sb_pois sbpois "
			+ "where sbpois.aoi_ids @> ARRAY[:aoiid] ", nativeQuery=true)
	int countByAoiIds(
			@Param("aoiid") int aoiid
			);
	
	/**
	 * AOI 영역에 포함되는 POI들 중 쿠폰이 있는 POI가 존재하는지 검색 
	 * @param aoiid
	 * @return
	 */
	@Query(value = 
			"select 1 "
			+ "from sb_pois sbpois, pois pois "
			+ "where sbpois.aoi_ids @> ARRAY[:aoiid] "
			+ "and sbpois.poi_id = pois.id "
			+ "and pois.has_coupon = 't' "
			+ "limit 1", nativeQuery=true)
	String isCoupon(@Param("aoiid") int aoiid);
	
	
	@Query(value = 
			"select * "
			+ "from sb_pois sbpois, pois pois "
			+ "where sbpois.aoi_ids @> ARRAY[:aoiid] "
			+ "and sbpois.poi_id = pois.id "
			+ "and pois.has_coupon = true "
			+ "order by score desc "
			+ "limit :limit offset :cursor", nativeQuery=true)
	List<Sb_Pois> findByHasCouponForAoiIds(@Param("aoiid") int aoiid,
											@Param("cursor") int cursor,
											@Param("limit") int limit);
	
	@Query(value=
			"select * "
			+ "from sb_pois sbpois "
			+ "where sbpois.repr_hashtag_ids @> ARRAY[12] "
			+ "and sbpois.aoi_ids @> ARRAY[:aoiId] "
			+ "order by sbpois.score desc "
			+ "limit :limit offset :cursor ", nativeQuery=true)
	List<Sb_Pois> findByVerifiedAoiIds(
			@Param("aoiId") int aoiId,
			@Param("cursor") int cursor,
			@Param("limit") int limit
			);
	
	
	@Query(value=
			"select count(1) "
			+ "from sb_pois sbpois "
			+ "where sbpois.repr_hashtag_ids @> ARRAY[12] "
			+ "and sbpois.aoi_ids @> ARRAY[:aoiId] ", nativeQuery=true)
	int findByVerifiedAoiIdsTotal(@Param("aoiId") int aoiId);
	
	@Query( value="\n"+
			"SELECT * \n"+
			"FROM sb_pois sbpois \n"+
			"WHERE sbpois.repr_hashtag_ids @> ARRAY[4857] AND poi_name IS NOT NULL AND poi_name <> '' \n"+
			"ORDER BY sbpois.poi_name ASC \n"+
			"LIMIT :limit OFFSET :cursor \n", nativeQuery=true)
	List<Sb_Pois> findByBookingPois(
			@Param("cursor") Integer cursor, 
			@Param("limit") Integer limit);
	
	@Query( value="\n"+
			"SELECT COUNT(1) \n"+
			"FROM sb_pois sbpois \n"+
			"WHERE sbpois.repr_hashtag_ids @> ARRAY[4857] AND poi_name IS NOT NULL AND poi_name <> '' \n", nativeQuery=true)
	int countByBookingPois();
}
