package com.skplanet.syruptable.repository.sb;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.sb.Sb_Users;

/**
 * PickLikesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface SbUsersRepository extends PagingAndSortingRepository<Sb_Users, Integer> {
}
