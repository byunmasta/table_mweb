package com.skplanet.syruptable.repository.feed;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.feed.My_Feeds;
import com.skplanet.syruptable.entity.feed.My_Feeds_PK;

/**
 * AgreementsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface MyFeedsRepository extends PagingAndSortingRepository<My_Feeds, My_Feeds_PK> {
}
