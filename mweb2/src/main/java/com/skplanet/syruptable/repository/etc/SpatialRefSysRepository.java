package com.skplanet.syruptable.repository.etc;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.etc.Spatial_Ref_Sys;

/**
 * AgreementsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface SpatialRefSysRepository extends PagingAndSortingRepository<Spatial_Ref_Sys, Integer> {
}
