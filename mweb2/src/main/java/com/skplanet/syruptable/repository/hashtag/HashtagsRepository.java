package com.skplanet.syruptable.repository.hashtag;

import java.util.Collection;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.hashtag.Hashtags;

/**
 * AoisRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface HashtagsRepository extends PagingAndSortingRepository<Hashtags, Integer> {
	
	/**
	 * findByIdInAndAliasofFalse
	 * @param hashtagIds
	 * @return
	 */
	List<Hashtags> findByIdInAndBannedFalse(Collection<Integer> hashtagIds);
}
