package com.skplanet.syruptable.repository.etc;

import java.util.LinkedList;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.etc.Suggestions;

/**
 * SuggestionsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface SuggestionsRepository extends PagingAndSortingRepository<Suggestions, Integer> {
	LinkedList<Suggestions> findByAoiIdInOrderByIdDesc(List<Integer> aoiIds);
	
	
	Suggestions findByAoiIdInOrderByIdDesc(Integer aoiId);
}
