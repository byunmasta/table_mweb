package com.skplanet.syruptable.repository.sorder;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.sorder.Sorder_Events;

public interface SorderEventsRepository extends PagingAndSortingRepository<Sorder_Events, Integer> {
}
