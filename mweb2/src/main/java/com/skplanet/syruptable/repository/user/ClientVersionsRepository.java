package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.Client_Versions;

/**
 * AuthtokensRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface ClientVersionsRepository extends PagingAndSortingRepository<Client_Versions, Integer> {
}
