package com.skplanet.syruptable.repository.poi;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.poi.Poi_Repr_Themes;

public interface PoisReprThemesRepository extends PagingAndSortingRepository<Poi_Repr_Themes, Integer> {
}
