package com.skplanet.syruptable.repository.aoi;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.aoi.Aois;
import com.vividsolutions.jts.geom.Point;

/**
 * AoisRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AoisRepository extends PagingAndSortingRepository<Aois, Integer> {
	
	/*
	@Query( value="\n"+
			"SELECT id \n"+
			"FROM aois \n"+
			"WHERE ST_Contains(geom, ST_SetSRID(ST_MakePoint(:longitude, :latitude), 4326))=true AND is_active=true \n"+
			"ORDER BY level ASC \n"+
			"LIMIT 1 \n", nativeQuery=true)
	long getAoiId(
			@Param("longitude") double longitude,
			@Param("latitude")  double latitude);
			*/
	
	/**
	 * Get aoi_id from Point by longitude and latitude
	 * @param point
	 * @return aoi_id
	 */
	@Query( value="\n"+
			"SELECT * \n"+
			"FROM aois \n"+
			"WHERE ST_Contains(geom, :point)=true AND is_active=true \n"+
			"ORDER BY level ASC \n"+
			"LIMIT 1 \n", nativeQuery=true)
	Aois getAoiId(
			@Param("point") Point point);
	
	/**
	 * findByIdInOrderByLevel
	 * @param aoiIds aoiId List
	 * @return List<Aois>
	 */
	List<Aois> findByIdInOrderByLevelAsc(Collection<Integer> aoiIds);
	
	/**
	 * findFirstByIdInOrderByLevelAsc
	 * @param aoiIds
	 * @return
	 */
	Aois findFirstByIdInOrderByLevelAsc(Collection<Integer> aoiIds);
	
	/**
	 * findFirstByIdInOrderByLevelDesc
	 * @param aoiIds
	 * @return
	 */
	Aois findFirstByIdInOrderByLevelDesc(Collection<Integer> aoiIds);
}
