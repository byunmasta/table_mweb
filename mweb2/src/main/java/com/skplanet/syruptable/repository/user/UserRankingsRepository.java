package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.User_Rankings;

/**
 * UserRankingsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UserRankingsRepository extends PagingAndSortingRepository<User_Rankings, Integer> {
}
