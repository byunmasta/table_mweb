package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.Client_Apps;

/**
 * AuthtokensRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface ClientAppsRepository extends PagingAndSortingRepository<Client_Apps, Integer> {
}
