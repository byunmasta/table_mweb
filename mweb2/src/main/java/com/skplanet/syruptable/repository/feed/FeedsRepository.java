package com.skplanet.syruptable.repository.feed;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.skplanet.syruptable.entity.feed.Feeds;

/**
 * AgreementsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface FeedsRepository extends PagingAndSortingRepository<Feeds, Integer> {
	
	/*select * from my_feeds, feeds 
	left outer join pick_comments on feeds.entity_id = pick_comments.id
	where my_feeds.user_id = 1024
	and my_feeds.feed_id = feeds.id
	and feeds.type in (1, 2) 
	ORDER BY feeds.created_at DESC, feeds.id DESC*/
	@Query(value = 
			"select * "
			+ "from my_feeds, feeds left outer join pick_comments on feeds.entity_id = pick_comments.id "
			+ "						left outer join picks on feeds.entity_id = picks.id "
			+ "where my_feeds.user_id = :userId "
			+ "and my_feeds.user_id = feeds.user_id "
			+ "and my_feeds.feed_id = feeds.id "
			+ "and feeds.type in (1, 2) "
			+ "ORDER BY feeds.created_at DESC "
			+ "limit :limit offset :cursor", nativeQuery=true)
	public List<Feeds> getUserReview(
							@Param("userId") int userId,
							@Param("cursor") int cursor,
							@Param("limit") int limit);
	
	
	@Query(value = 
			"select count(1) "
			+ "from my_feeds, feeds left outer join pick_comments on feeds.entity_id = pick_comments.id "
			+ "						left outer join picks on feeds.entity_id = picks.id "
			+ "where my_feeds.user_id = :userId "
			+ "and my_feeds.user_id = feeds.user_id "
			+ "and my_feeds.feed_id = feeds.id "
			+ "and feeds.type in (1, 2) ", nativeQuery=true)
	public int getUserReviewTotal(@Param("userId") int userId);
	
	
	
	@Query(value = 
			"select * "
			+ "from friend_feeds, feeds left outer join pick_comments on feeds.entity_id = pick_comments.id "
			+ "where friend_feeds.user_id = :userId "
			+ "and friend_feeds.feed_id = feeds.id "
			+ "and feeds.type = 1 "
			+ "ORDER BY feeds.created_at DESC "
			+ "limit :limit offset :cursor", nativeQuery=true)
	public List<Feeds> getUserFriendsReview(
							@Param("userId") int userId,
							@Param("cursor") int cursor,
							@Param("limit") int limit);
	
	
	/**
	 * 친구 소식 
	 * @param userId
	 * @return
	 */
	@Query(value = 
			"select count(1) "
			+ "from friend_feeds, feeds left outer join pick_comments on feeds.entity_id = pick_comments.id "
			+ "where friend_feeds.user_id = :userId "
			+ "and friend_feeds.feed_id = feeds.id "
			+ "and feeds.type = 1 ", nativeQuery=true)
	public int getUserFriendsReviewTotal(@Param("userId") int userId);
	
	
	
	
	
	
}
