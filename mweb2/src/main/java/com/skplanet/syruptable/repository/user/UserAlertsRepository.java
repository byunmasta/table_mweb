package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.User_Alerts;
import com.skplanet.syruptable.entity.user.User_Alerts_PK;

/**
 * UserAlertsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UserAlertsRepository extends PagingAndSortingRepository<User_Alerts, User_Alerts_PK> {
}
