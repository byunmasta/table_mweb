package com.skplanet.syruptable.repository.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_Logs;

/**
 * AdminLogsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminLogsRepository extends PagingAndSortingRepository<Admin_Logs, Integer> {
}
