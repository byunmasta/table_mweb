package com.skplanet.syruptable.repository.popular;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.popular.Popluar_Search_Words;

/**
 * PickLikesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface PopularSeachWordsRepository extends PagingAndSortingRepository<Popluar_Search_Words, Integer> {
}
