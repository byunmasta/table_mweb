package com.skplanet.syruptable.repository.sns;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.sns.Sns_Poi_Images;

/**
 * PickLikesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface SnsPoiImagesRepository extends PagingAndSortingRepository<Sns_Poi_Images, String> {
}
