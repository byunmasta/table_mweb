package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.User_Agreements;
import com.skplanet.syruptable.entity.user.User_Agreements_PK;

/**
 * UserAgreementsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UserAgreementsRepository extends PagingAndSortingRepository<User_Agreements, User_Agreements_PK> {
}
