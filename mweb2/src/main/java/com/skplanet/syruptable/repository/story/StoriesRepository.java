package com.skplanet.syruptable.repository.story;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.story.Stories;

/**
 * StoriesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface StoriesRepository extends PagingAndSortingRepository<Stories, Integer> {
	
	/**
	 * findTop100ByIsVisibleOrderByIdDesc
	 * @param isVisible
	 * @return List<Stories>
	 */
	List<Stories> findTop100ByIsVisibleOrderBySeqDesc(Boolean isVisible);
}