package com.skplanet.syruptable.repository.sb;

import java.util.Collection;
import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.sb.Sb_Hashtag_Pois;
import com.skplanet.syruptable.entity.sb.Sb_Hashtag_Pois_PK;

/**
 * SbHashtagPoisRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface SbHashtagPoisRepository extends PagingAndSortingRepository<Sb_Hashtag_Pois, Sb_Hashtag_Pois_PK> {
	
	/**
	 * findByIdInAndAliasofFalse
	 * @param hashtagIds
	 * @return
	 */
	List<Sb_Hashtag_Pois> 
		findByHashtagIdInAndPoiIdAndHashtagBannedFalseOrderByScoreDesc(
				Collection<Integer> hashtagIds, 
				Integer poiId
				);
}