package com.skplanet.syruptable.repository.sorder;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.sorder.Sorder_Event_Pois;
import com.skplanet.syruptable.entity.sorder.Sorder_Event_Pois_PK;

public interface SorderEventPoisRepository extends PagingAndSortingRepository<Sorder_Event_Pois, Sorder_Event_Pois_PK> {
}
