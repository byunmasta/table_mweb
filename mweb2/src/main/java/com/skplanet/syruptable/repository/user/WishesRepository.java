package com.skplanet.syruptable.repository.user;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.Wishes;
import com.skplanet.syruptable.entity.user.Wishes_PK;

/**
 * UserWithdrawlLogsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface WishesRepository extends PagingAndSortingRepository<Wishes, Wishes_PK> {
	
	/**
	 * find wished list
	 * @param poiId
	 * @return
	 */
	List<Wishes> findByPoiIdOrderByUpdatedatDesc(Integer poiId);
}
