package com.skplanet.syruptable.repository.pick;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.pick.Pick_Comments;

/**
 * PickCommentsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface PickCommentsRepository extends PagingAndSortingRepository<Pick_Comments, Integer> {
}
