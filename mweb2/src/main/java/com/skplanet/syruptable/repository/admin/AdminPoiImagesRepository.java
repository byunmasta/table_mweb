package com.skplanet.syruptable.repository.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_Poi_Images;

/**
 * AdminPoiImagesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminPoiImagesRepository extends PagingAndSortingRepository<Admin_Poi_Images, Integer> {
	Admin_Poi_Images findFirstBypoiIdOrderByPoiIdDesc(Integer poiId);
}
