package com.skplanet.syruptable.repository.sns;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.sns.Sns_Poi_Mappings;
import com.skplanet.syruptable.entity.sns.Sns_Poi_Mappings.Snstype;

/**
 * PickLikesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface SnsPoiMappingsRepository extends PagingAndSortingRepository<Sns_Poi_Mappings, String> {
	
	/**
	 * findFirstByPoiIdAndSnsTypeOrderBySnsIdDesc
	 * @param poiId
	 * @param snstype
	 * @return
	 */
	Sns_Poi_Mappings findFirstByPoiIdAndSnsTypeOrderBySnsIdDesc(Integer poiId, Snstype snstype);
	
	/**
	 * findByPoiIdOrderBySnsIdDesc
	 * @param poiId
	 * @param snstype
	 * @return
	 */
	List<Sns_Poi_Mappings> findByPoiIdAndSnsTypeOrderBySnsIdDesc(Integer poiId, Snstype snstype);
}
