package com.skplanet.syruptable.repository.poi;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.poi.Poi_Ratings;
import com.skplanet.syruptable.entity.poi.Poi_Ratings_PK;

/**
 * PoiRatingsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface PoiRatingsRepository extends PagingAndSortingRepository<Poi_Ratings, Poi_Ratings_PK> {
}
