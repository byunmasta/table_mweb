package com.skplanet.syruptable.repository.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_Push_Notice_Target;

/**
 * AdminPushNoticeTargetRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminPushNoticeTargetRepository extends PagingAndSortingRepository<Admin_Push_Notice_Target, Integer> {
}
