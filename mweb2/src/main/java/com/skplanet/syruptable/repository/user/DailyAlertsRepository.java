package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.Daily_Alerts;

/**
 * AuthtokensRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface DailyAlertsRepository extends PagingAndSortingRepository<Daily_Alerts, Integer> {
}
