package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.User_Suspensions;

/**
 * UsersRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UserSuspensionsRepository extends PagingAndSortingRepository<User_Suspensions, Integer> {
}
