package com.skplanet.syruptable.repository.user;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.user.User_Withdrawl_Logs;

/**
 * UserWithdrawlLogsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface UserWithdrawlLogsRepository extends PagingAndSortingRepository<User_Withdrawl_Logs, Integer> {
}
