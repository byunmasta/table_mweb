package com.skplanet.syruptable.repository.sb;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.sb.Sb_Categories;

/**
 * PickLikesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface SbCategoriesRepository extends PagingAndSortingRepository<Sb_Categories, Integer> {
}
