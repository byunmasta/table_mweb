package com.skplanet.syruptable.repository.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_Rules;

/**
 * AdminRulesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminRulesRepository extends PagingAndSortingRepository<Admin_Rules, Integer> {
}
