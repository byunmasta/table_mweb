package com.skplanet.syruptable.repository.admin;

import java.util.Date;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_Trends;

/**
 * AdminStoriesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminTrendsRepository extends PagingAndSortingRepository<Admin_Trends, Integer> {
	Admin_Trends findByShowedat(Date date);
}
