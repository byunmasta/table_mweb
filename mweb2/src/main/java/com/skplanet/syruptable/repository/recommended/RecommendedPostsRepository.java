package com.skplanet.syruptable.repository.recommended;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.recommended.Recommended_Posts;

/**
 * PickLikesRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface RecommendedPostsRepository extends PagingAndSortingRepository<Recommended_Posts, Integer> {
}
