package com.skplanet.syruptable.repository.etc;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.skplanet.syruptable.entity.sb.Coupon_Cache;
import com.skplanet.syruptable.entity.sb.Coupon_Cache_PK;

/**
 * AgreementsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface CouponCacheRepository extends PagingAndSortingRepository<Coupon_Cache, Coupon_Cache_PK> {
	@Query(value=
			"select * "
			+ "from coupon_caches couponcaches "
			+ "where couponcaches.poi_ids @> ARRAY[:poiId] "
			+ "order by updated_at desc "
			+ "limit :limit offset :cursor ", nativeQuery=true)
	List<Coupon_Cache> findByPoiIds(
			@Param("poiId") int poiId,
			@Param("cursor") int cursor,
			@Param("limit") int limit
			);
	
	@Query(value=
			"select count(*) "
			+ "from coupon_caches couponcaches "
			+ "where couponcaches.poi_ids @> ARRAY[:poiId] ", nativeQuery=true)
	int countByPoiId(
			@Param("poiId") int poiId
			);
}
