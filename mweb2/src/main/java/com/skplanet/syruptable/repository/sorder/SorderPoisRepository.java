package com.skplanet.syruptable.repository.sorder;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.sorder.Sorder_Pois;

public interface SorderPoisRepository extends PagingAndSortingRepository<Sorder_Pois, String> {
}
