package com.skplanet.syruptable.repository.feed;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.feed.Friend_Feeds;
import com.skplanet.syruptable.entity.feed.Friend_Feeds_PK;

/**
 * AgreementsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface FriendFeedsRepository extends PagingAndSortingRepository<Friend_Feeds, Friend_Feeds_PK> {
}
