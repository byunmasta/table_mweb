package com.skplanet.syruptable.repository.admin;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.skplanet.syruptable.entity.admin.Admin_Company;

/**
 * AdminCompanyRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
@Repository
public interface AdminCompanyRepository extends PagingAndSortingRepository<Admin_Company, Integer> {
}
