package com.skplanet.syruptable.repository.poi;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.skplanet.syruptable.entity.poi.Poi_Blog_Reviews;

/**
 * PoiBlogReviewsRepository
 * @author mylostland@sk.com Hoseong Ryu
 *
 */
public interface PoiBlogReviewsRepository extends PagingAndSortingRepository<Poi_Blog_Reviews, Integer> {
	
	/**
	 * findFirstBypoiIdOrderByUpdatedatDesc
	 * @param poiId
	 * @return
	 */
	Poi_Blog_Reviews findFirstByPoiIdOrderByUpdatedatDesc(Integer poiId);
	
	/**
	 * findByPoiIdOrderByUpdatedatDesc
	 * @param poiId
	 * @return
	 */
	List<Poi_Blog_Reviews> findByPoiIdOrderByUpdatedatDesc(Integer poiId);
}
