package com.skplanet.syruptable.entity.sorder;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Sorder_Event_Pois_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="event_id", nullable=false)
	private Integer eventId;
	
	@Column(name="mid", nullable=false)
	private String mid;
	
	public Sorder_Event_Pois_PK() {
		
	}
	
	public Sorder_Event_Pois_PK(Integer eventId, String mid) {
		this.eventId = eventId;
		this.mid = mid;
	}
}