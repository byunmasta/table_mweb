package com.skplanet.syruptable.entity.admin;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="admin_top_users")
public class Admin_Top_Users {
	
	@Id
	@NotNull
	@Column(name="user_id")
	private Integer hashtagId;
	
	@Column(name="display_name")
	private String displayName;
	
	@Column(name="profile_image")
	private String profileImage;
	
	@Column(name="is_visible")
	@NotNull
	private Boolean isVisible;

	@Column(name="seq")
	@NotNull
	private Integer seq;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;

	
	public Integer getHashtagId() {
		return hashtagId;
	}

	public void setHashtagId(Integer hashtagId) {
		this.hashtagId = hashtagId;
	}
	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public Boolean isVisible() {
		return isVisible;
	}

	public void setVisible(Boolean isVisible) {
		this.isVisible = isVisible;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	
	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
	
}
