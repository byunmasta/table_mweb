package com.skplanet.syruptable.entity.user;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@Table(schema="public", name="user_score_logs")
public class User_Score_Logs {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="user_score_logs_id_seq")
	@SequenceGenerator(name="user_score_logs_id_seq", sequenceName="user_score_logs_id_seq")
	private Integer id;
	
	@Column(name="user_id")
	@NotNull
	private Integer userId;
	
	public enum ScoreType {
		pick_like,
		followed,
		wish,
		comment,
		share,
		invitation,
		following,
		rating,
		pick;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="type")
	@NotNull
	private ScoreType userType;
	
	@Column(name="score")
	@NotNull
	private Long score;
	
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@Column(name="attrs")
	@NotNull
	private Map<String, String> attrs;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public ScoreType getUserType() {
		return userType;
	}

	public void setUserType(ScoreType userType) {
		this.userType = userType;
	}

	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

	public Map<String, String> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, String> attrs) {
		this.attrs = attrs;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}
