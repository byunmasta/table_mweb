package com.skplanet.syruptable.entity.aoi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="public", name="verified_aois")
public class Verified_Aois {
	
	@Id
	@Column(name="aoi_id")
	private Integer aoi_id;
	
	@Column(name="display_name")
	private String display_name;

	public Integer getAoi_id() {
		return aoi_id;
	}

	public void setAoi_id(Integer aoi_id) {
		this.aoi_id = aoi_id;
	}

	public String getDisplay_name() {
		return display_name;
	}

	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}
}