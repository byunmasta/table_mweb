package com.skplanet.syruptable.entity.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="user_settings")
public class User_Settings {
	
	@Id
	private Integer userId;
	
	@Column(name="alert_comment")
	@NotNull
	private Boolean alertComment;
	
	@Column(name="alert_theme_poi_add")
	@NotNull
	private Boolean alertThemePoiAdd;
	
	@Column(name="alert_friends_posting")
	@NotNull
	private Boolean alertFriendsPosting;
	
	@Column(name="alert_coupon_recommendation")
	@NotNull
	private Boolean alertCouponRecommendation;
	
	@Column(name="alert_new_follower")
	@NotNull
	private Boolean alertNewFollower;
	
	@Column(name="alert_invitee_joined")
	@NotNull
	private Boolean alertInviteeJoined;
	
	@Column(name="allow_ads")
	@NotNull
	private Boolean allowAds;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Boolean getAlertComment() {
		return alertComment;
	}

	public void setAlertComment(Boolean alertComment) {
		this.alertComment = alertComment;
	}

	public Boolean getAlertThemePoiAdd() {
		return alertThemePoiAdd;
	}

	public void setAlertThemePoiAdd(Boolean alertThemePoiAdd) {
		this.alertThemePoiAdd = alertThemePoiAdd;
	}

	public Boolean getAlertFriendsPosting() {
		return alertFriendsPosting;
	}

	public void setAlertFriendsPosting(Boolean alertFriendsPosting) {
		this.alertFriendsPosting = alertFriendsPosting;
	}

	public Boolean getAlertCouponRecommendation() {
		return alertCouponRecommendation;
	}

	public void setAlertCouponRecommendation(Boolean alertCouponRecommendation) {
		this.alertCouponRecommendation = alertCouponRecommendation;
	}

	public Boolean getAlertNewFollower() {
		return alertNewFollower;
	}

	public void setAlertNewFollower(Boolean alertNewFollower) {
		this.alertNewFollower = alertNewFollower;
	}

	public Boolean getAlertInviteeJoined() {
		return alertInviteeJoined;
	}

	public void setAlertInviteeJoined(Boolean alertInviteeJoined) {
		this.alertInviteeJoined = alertInviteeJoined;
	}

	public Boolean getAllowAds() {
		return allowAds;
	}

	public void setAllowAds(Boolean allowAds) {
		this.allowAds = allowAds;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@PrePersist
	protected void onCreate() {
		this.updatedat = new Date();
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}