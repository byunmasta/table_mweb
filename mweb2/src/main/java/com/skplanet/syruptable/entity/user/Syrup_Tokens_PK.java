package com.skplanet.syruptable.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Syrup_Tokens_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="user_id", nullable=false)
	private Integer userId;
	
	@Column(name="ci", nullable=false)
	private String ci;
	
	@Column(name="owp_id", nullable=false)
	private String owpId;
	
	public Syrup_Tokens_PK() {
		
	}
	
	public Syrup_Tokens_PK(Integer userId, String ci, String owpId) {
		this.userId = userId;
		this.ci = ci;
		this.owpId = owpId;
	}
}
