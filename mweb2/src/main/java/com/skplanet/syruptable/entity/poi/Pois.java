package com.skplanet.syruptable.entity.poi;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.skplanet.syruptable.entity.category.Categories;
import com.skplanet.syruptable.entity.sorder.Sorder_Pois;
import com.skplanet.syruptable.entity.tag.Tag_Pois;
import com.vividsolutions.jts.geom.Geometry;

@Entity
@Table(schema="public", name="pois")
public class Pois {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="pois_id_seq")
	@SequenceGenerator(name="pois_id_seq", sequenceName="pois_id_seq")
	private Integer id;
	
	@Column(name="skmc_id")
	private Integer skmcId;
	
	@Column(name="comms_id")
	private Integer commsId;
	
	@Column(name="name")
	private String name;
	
	@Column(name="addr")
	private String addr;
	
	@Type(type="org.hibernate.spatial.GeometryType")
	@Column(name="loc", columnDefinition="Geometry")
	@JsonIgnore
	private Geometry loc;
	
	@Column(name="category")
	private String category;
	
	@Column(name="user_id")
	private Integer userId;
	
	@Column(name="has_coupon")
	@NotNull
	private Boolean hasCoupon;
	
	@Column(name="phone")
	private String phone;
	
	@Column(name="image_url")
	private String imageUrl;
	
	@Column(name="description")
	private String description;
	
	@Column(name="rich_field")
	@NotNull
	private String richField;
	
	public enum Status {
		RMP, 
		XUCP, // 검수 전 UCP
		PUCP, // 검수 완료. (UCP-RMP 머지) pickat 에만 노출.
		SUCP, // 사용 안함. 통합검색에만 노출되는 UCP (RMP)';
		DUCP; // RMP 였으나, 폐점/삭제 처리 되면서, UCP화 된 경우
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	@NotNull
	private Status status;
	
	@Column(name="attrs")
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@NotNull
	private Map<String, String> attrs;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="category", referencedColumnName="id", insertable=false, updatable=false)
	private Categories categories;
	
	@OneToMany(mappedBy="poiId", cascade=CascadeType.PERSIST, fetch=FetchType.EAGER, orphanRemoval=true)
	private List<Sorder_Pois> sOrderPois;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSkmcId() {
		return skmcId;
	}

	public void setSkmcId(Integer skmcId) {
		this.skmcId = skmcId;
	}

	public Integer getCommsId() {
		return commsId;
	}

	public void setCommsId(Integer commsId) {
		this.commsId = commsId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public Geometry getLoc() {
		return loc;
	}

	public void setLoc(Geometry loc) {
		this.loc = loc;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Boolean getHasCoupon() {
		return hasCoupon;
	}

	public void setHasCoupon(Boolean hasCoupon) {
		this.hasCoupon = hasCoupon;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRichField() {
		return richField;
	}

	public void setRichField(String richField) {
		this.richField = richField;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Map<String, String> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, String> attrs) {
		this.attrs = attrs;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	public Categories getCategories() {
		return categories;
	}

	public void setCategories(Categories categories) {
		this.categories = categories;
	}

	public List<Sorder_Pois> getsOrderPois() {
		return sOrderPois;
	}

	public void setsOrderPois(List<Sorder_Pois> sOrderPois) {
		this.sOrderPois = sOrderPois;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}