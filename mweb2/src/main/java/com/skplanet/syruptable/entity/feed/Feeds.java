package com.skplanet.syruptable.entity.feed;

import java.util.Date;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.skplanet.syruptable.entity.pick.Pick_Comments;
import com.skplanet.syruptable.entity.pick.Picks;
import com.skplanet.syruptable.entity.poi.Pois;

@Entity
@Table(schema="public", name="feeds")
public class Feeds {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="feeds_id_seq")
	@SequenceGenerator(name="feeds_id_seq", sequenceName="feeds_id_seq")
	private Integer id;
	
	@Column(name="type")
	@NotNull
	private Integer type;
	
	@Column(name="user_id")
	@NotNull
	private Integer userId;
	
	@Column(name="poi_id")
	private Integer poiId;
	
	@Column(name="aoi_id")
	private Integer aoiId;
	
	@Column(name="entity_id")
	private Integer entityId;
	
	@Column(name="attr")
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	private Map<String, String> attr;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY, optional=false)
	@JoinColumn(name="entity_id", referencedColumnName="id", insertable=false, updatable=false)
	private Pick_Comments pickComments;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY, optional=false)
	@JoinColumn(name="entity_id", referencedColumnName="id", insertable=false, updatable=false)
	private Picks picks;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY, optional=false)
	@JoinColumn(name="poi_id", referencedColumnName="id", insertable=false, updatable=false)
	private Pois pois;
	
	
	
	public Picks getPicks() {
		return picks;
	}

	public void setPicks(Picks picks) {
		this.picks = picks;
	}

	public Pois getPois() {
		return pois;
	}

	public void setPois(Pois pois) {
		this.pois = pois;
	}

	public Integer getPoiId() {
		return poiId;
	}

	public void setPoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public Pick_Comments getPickComments() {
		return pickComments;
	}

	public void setPickComments(Pick_Comments pickComments) {
		this.pickComments = pickComments;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getpoiId() {
		return poiId;
	}

	public void setpoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public Integer getAoiId() {
		return aoiId;
	}

	public void setAoiId(Integer aoiId) {
		this.aoiId = aoiId;
	}

	public Integer getEntityId() {
		return entityId;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public Map<String, String> getAttr() {
		return attr;
	}

	public void setAttr(Map<String, String> attr) {
		this.attr = attr;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}