package com.skplanet.syruptable.entity.feed;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(My_Feeds_PK.class)
@Table(schema="public", name="my_feeds")
public class My_Feeds {
	
	@Id
	@Column(name="user_id")
	private Integer userId;
	
	@Id
	@Column(name="feed_id")
	private Integer feedId;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getFeedId() {
		return feedId;
	}

	public void setFeedId(Integer feedId) {
		this.feedId = feedId;
	}
}