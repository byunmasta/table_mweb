package com.skplanet.syruptable.entity.etc;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="public", name="spatial_ref_sys")
public class Spatial_Ref_Sys {
	
	@Id
	private Integer srid;
	
	@Column(name="auth_name")
	private String authName;
	
	@Column(name="auth_srid")
	private Integer authSrid;
	
	@Column(name="srtext")
	private String srText;
	
	@Column(name="proj4text")
	private String proj4Text;

	public Integer getSrid() {
		return srid;
	}

	public void setSrid(Integer srid) {
		this.srid = srid;
	}

	public String getAuthName() {
		return authName;
	}

	public void setAuthName(String authName) {
		this.authName = authName;
	}

	public Integer getAuthSrid() {
		return authSrid;
	}

	public void setAuthSrid(Integer authSrid) {
		this.authSrid = authSrid;
	}

	public String getSrText() {
		return srText;
	}

	public void setSrText(String srText) {
		this.srText = srText;
	}

	public String getProj4Text() {
		return proj4Text;
	}

	public void setProj4Text(String proj4Text) {
		this.proj4Text = proj4Text;
	}
}