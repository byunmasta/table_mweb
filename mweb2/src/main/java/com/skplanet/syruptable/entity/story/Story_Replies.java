package com.skplanet.syruptable.entity.story;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.skplanet.syruptable.entity.user.Users;

@Entity
@Table(schema="public", name="story_replies")
public class Story_Replies {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="story_replies_id_seq")
	@SequenceGenerator(name="story_replies_id_seq", sequenceName="story_replies_id_seq")
	private Integer id;
	
	@Column(name="story_id")
	@NotNull
	private Integer storyId;
	
	@Column(name="user_id")
	@NotNull
	private Integer userId;
	
	@Column(name="reply")
	@NotNull
	private String reply;
	
	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="user_id", referencedColumnName="id", insertable=false, updatable=false)
	private Users user;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStoryId() {
		return storyId;
	}

	public void setStoryId(Integer storyId) {
		this.storyId = storyId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getReply() {
		return reply;
	}

	public void setReply(String reply) {
		this.reply = reply;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}

	@Override
	public String toString() {
		return "Story_Replies [id=" + id + ", storyId=" + storyId + ", userId=" + userId + ", reply=" + reply
				+ ", user=" + user + ", createdat=" + createdat + "]";
	}
	
	
}