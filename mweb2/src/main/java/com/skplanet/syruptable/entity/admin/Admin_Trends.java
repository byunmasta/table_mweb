package com.skplanet.syruptable.entity.admin;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="admin_trends")
public class Admin_Trends {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="admin_trends_id_seq")
	@SequenceGenerator(name="admin_trends_id_seq", sequenceName="admin_trends_id_seq")
	private Integer id;
	
	@Column(name="title")
	@NotNull
	private String title;
	
	@Column(name="image")
	@NotNull
	private String image;
	
	// 1:Sun 2:Mon 3:tue 4:Wed 5:Thu 6:Fri 7:Sat
	@Column(name="day")
	@NotNull
	private String day;
	
	@Column(name="showed_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date showedat;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public Date getShowedat() {
		return showedat;
	}

	public void setShowedat(Date showedat) {
		this.showedat = showedat;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}

