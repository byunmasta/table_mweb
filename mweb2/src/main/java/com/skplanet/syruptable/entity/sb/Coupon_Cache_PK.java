package com.skplanet.syruptable.entity.sb;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Coupon_Cache_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="partner_code", nullable=false)
	private String partnerCode;
	
	@Column(name="pt_prod_code", nullable=false)
	private String ptProdCode;
	
	public Coupon_Cache_PK() {
		
	}
	
	public Coupon_Cache_PK(String partnerCode, String ptProdCode) {
		this.partnerCode = partnerCode;
		this.ptProdCode = ptProdCode;
	}
}
