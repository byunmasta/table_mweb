package com.skplanet.syruptable.entity.sns;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@IdClass(Sns_Invites_PK.class)
@Table(schema="public", name="sns_invites")
public class Sns_Invites implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer invitorId;
	
	@Id
	private String snsType;
	
	@Id
	private String snsUid;
	
	@Column(name="invitee_id")
	private Integer inviteeId;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	public Integer getInvitorId() {
		return invitorId;
	}

	public void setInvitorId(Integer invitorId) {
		this.invitorId = invitorId;
	}

	public String getSnsType() {
		return snsType;
	}

	public void setSnsType(String snsType) {
		this.snsType = snsType;
	}

	public String getSnsUid() {
		return snsUid;
	}

	public void setSnsUid(String snsUid) {
		this.snsUid = snsUid;
	}

	public Integer getInviteeId() {
		return inviteeId;
	}

	public void setInviteeId(Integer inviteeId) {
		this.inviteeId = inviteeId;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}