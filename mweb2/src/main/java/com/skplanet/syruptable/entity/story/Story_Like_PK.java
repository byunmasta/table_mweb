package com.skplanet.syruptable.entity.story;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Story_Like_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="story_id", nullable=false)
	private Integer storyId;
	
	@Column(name="user_id", nullable=false)
	private Integer userId;
	
	public Story_Like_PK() {
		
	}
	
	public Story_Like_PK(Integer storyId, Integer userId) {
		this.storyId = storyId;
		this.userId = userId;
	}
}