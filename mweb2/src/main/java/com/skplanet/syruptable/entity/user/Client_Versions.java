package com.skplanet.syruptable.entity.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="client_versions")
public class Client_Versions {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="client_versions_id_seq")
	@SequenceGenerator(name="client_versions_id_seq", sequenceName="client_versions_id_seq")
	private Integer id;
	
	@Column(name="seq")
	@NotNull
	private Integer seq;
	
	@Column(name="os")
	private String os;
	
	@Column(name="version")
	@NotNull
	private String version;
	
	@Column(name="is_force_update")
	private Integer isForceUpdate;
	
	@Column(name="admin_user_id")
	private Integer adminUserId;
	
	@Column(name="update_urls_tstore")
	private String updateUrlsTstore;
	
	@Column(name="update_urls_gplay")
	private String updateUrlsGplay;
	
	@Column(name="update_urls_appstore")
	private String updateUrlsAppstore;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getIsForceUpdate() {
		return isForceUpdate;
	}

	public void setIsForceUpdate(Integer isForceUpdate) {
		this.isForceUpdate = isForceUpdate;
	}

	public Integer getAdminUserId() {
		return adminUserId;
	}

	public void setAdminUserId(Integer adminUserId) {
		this.adminUserId = adminUserId;
	}

	public String getUpdateUrlsTstore() {
		return updateUrlsTstore;
	}

	public void setUpdateUrlsTstore(String updateUrlsTstore) {
		this.updateUrlsTstore = updateUrlsTstore;
	}

	public String getUpdateUrlsGplay() {
		return updateUrlsGplay;
	}

	public void setUpdateUrlsGplay(String updateUrlsGplay) {
		this.updateUrlsGplay = updateUrlsGplay;
	}

	public String getUpdateUrlsAppstore() {
		return updateUrlsAppstore;
	}

	public void setUpdateUrlsAppstore(String updateUrlsAppstore) {
		this.updateUrlsAppstore = updateUrlsAppstore;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}
