package com.skplanet.syruptable.entity.story;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Story_Contents_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="story_id", nullable=false)
	private Integer storyId;
	
	@Column(name="page", nullable=false)
	private Integer page;
	
	public Story_Contents_PK() {
		
	}
	
	public Story_Contents_PK(Integer storyId, Integer page) {
		this.storyId = storyId;
		this.page = page;
	}

	public Integer getStoryId() {
		return storyId;
	}

	public void setStoryId(Integer storyId) {
		this.storyId = storyId;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}
}