package com.skplanet.syruptable.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class User_Alerts_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="user_id", nullable=false)
	private Integer userId;
	
	@Column(name="feed_id", nullable=false)
	private Integer feedId;
	
	public User_Alerts_PK() {
		
	}
	
	public User_Alerts_PK(Integer userId, Integer feedId) {
		this.userId = userId;
		this.feedId = feedId;
	}
}
