package com.skplanet.syruptable.entity.admin;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="admin_users")
public class Admin_Users {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="admin_users_id_seq")
	@SequenceGenerator(name="admin_users_id_seq", sequenceName="admin_users_id_seq")
	private Integer id;
	
	@Column(name="role_id")
	@NotNull
	private Integer roleId;
	
	@Column(name="salt")
	private String salt;
	
	@Column(name="password")
	@NotNull
	private String password;
	
	@Column(name="email")
	@NotNull
	private String email;
	
	@Column(name="is_active")
	@NotNull
	private Boolean isActive;
	
	@Column(name="account")
	@NotNull
	private String account;
	
	@Column(name="note")
	private String note;
	
	@Column(name="login_failed")
	@NotNull
	private Integer loginFailed;
	
	@Column(name="ip_address")
	@NotNull
	private String ipAddress;
	
	@Column(name="company_id")
	private Integer companyId;
	
	@Column(name="name")
	private String name;
	
	@Column(name="pw_updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date pwupdatedat;
	
	@Column(name="start_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startat;
	
	@Column(name="end_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endat;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getLoginFailed() {
		return loginFailed;
	}

	public void setLoginFailed(Integer loginFailed) {
		this.loginFailed = loginFailed;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getPwupdatedat() {
		return pwupdatedat;
	}

	public void setPwupdatedat(Date pwupdatedat) {
		this.pwupdatedat = pwupdatedat;
	}

	public Date getStartat() {
		return startat;
	}

	public void setStartat(Date startat) {
		this.startat = startat;
	}

	public Date getEndat() {
		return endat;
	}

	public void setEndat(Date endat) {
		this.endat = endat;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
		this.startat = this.createdat;
		this.endat = this.createdat;
		this.pwupdatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
		this.endat = this.updatedat;
		this.pwupdatedat = this.updatedat;
	}
}
