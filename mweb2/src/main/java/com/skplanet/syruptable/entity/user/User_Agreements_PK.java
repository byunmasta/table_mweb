package com.skplanet.syruptable.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class User_Agreements_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="user_id", nullable=false)
	private Integer userId;
	
	@Column(name="type", nullable=false)
	private String type;
	
	public User_Agreements_PK() {
		
	}
	
	public User_Agreements_PK(Integer userId, String type) {
		this.userId = userId;
		this.type = type;
	}
}
