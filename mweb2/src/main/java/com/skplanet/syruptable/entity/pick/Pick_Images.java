package com.skplanet.syruptable.entity.pick;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.skplanet.syruptable.entity.sb.Sb_Picks;

@Entity
@Table(schema="public", name="pick_images")
public class Pick_Images {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="pick_images_id_seq")
	@SequenceGenerator(name="pick_images_id_seq", sequenceName="pick_images_id_seq")
	private Integer id;
	
	@Column(name="pick_id")
	private Integer pickId;
	
	@Column(name="slot")
	@NotNull
	private Integer slot;
	
	@Column(name="poi_id")
	private Integer poiId;
	
	@Column(name="image")
	@NotNull
	private String image;
	
	/*
	@ManyToOne(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="pick_id", referencedColumnName="pick_id", insertable=false, updatable=false)
	private Sb_Picks sbPicks;
	*/
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPickId() {
		return pickId;
	}

	public void setPickId(Integer pickId) {
		this.pickId = pickId;
	}

	public Integer getSlot() {
		return slot;
	}

	public void setSlot(Integer slot) {
		this.slot = slot;
	}

	public Integer getPoiId() {
		return poiId;
	}

	public void setPoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	/*
	public Sb_Picks getSbPicks() {
		return sbPicks;
	}

	public void setSbPicks(Sb_Picks sbPicks) {
		this.sbPicks = sbPicks;
	}
	*/
	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}