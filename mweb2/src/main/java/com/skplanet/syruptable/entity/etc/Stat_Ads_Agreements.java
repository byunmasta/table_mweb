package com.skplanet.syruptable.entity.etc;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="stat_ads_agreements")
public class Stat_Ads_Agreements {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="stat_ads_agreements_id_seq")
	@SequenceGenerator(name="stat_ads_agreements_id_seq", sequenceName="stat_ads_agreements_id_seq")
	private Integer id;
	
	@Column(name="date")
	@NotNull
	private Date date;
	
	@Column(name="agreement_count")
	private Long agreementCount;
	
	@Column(name="no_agreement_count")
	private Long noAgreementCount;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getAgreementCount() {
		return agreementCount;
	}

	public void setAgreementCount(Long agreementCount) {
		this.agreementCount = agreementCount;
	}

	public Long getNoAgreementCount() {
		return noAgreementCount;
	}

	public void setNoAgreementCount(Long noAgreementCount) {
		this.noAgreementCount = noAgreementCount;
	}
}