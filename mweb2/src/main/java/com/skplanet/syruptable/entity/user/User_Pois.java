package com.skplanet.syruptable.entity.user;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.skplanet.syruptable.entity.sb.Sb_Pois;

@Entity
@IdClass(User_Pois_PK.class)
@Table(schema="public", name="user_pois")
public class User_Pois implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_id")
	private Integer userId;
	
	@Id
	@Column(name="poi_id")
	private Integer poiId;
	
	@Column(name="is_picked")
	private Boolean isPicked;
	
	@Column(name="is_wished")
	private Boolean isWished;
	
	@Column(name="is_rated")
	private Boolean isRated;
	
	@Column(name="latest_pick_id")
	private Integer latestPickId;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.LAZY, optional=false)
	@JoinColumn(name="poi_id", referencedColumnName="poi_id", insertable=false, updatable=false)
	private Sb_Pois sb_pois;

	public Integer getPoiId() {
		return poiId;
	}

	public void setPoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public Sb_Pois getSb_pois() {
		return sb_pois;
	}

	public void setSb_pois(Sb_Pois sb_pois) {
		this.sb_pois = sb_pois;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getpoiId() {
		return poiId;
	}

	public void setpoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public Boolean getIsPicked() {
		return isPicked;
	}

	public void setIsPicked(Boolean isPicked) {
		this.isPicked = isPicked;
	}

	public Boolean getIsWished() {
		return isWished;
	}

	public void setIsWished(Boolean isWished) {
		this.isWished = isWished;
	}

	public Boolean getIsRated() {
		return isRated;
	}

	public void setIsRated(Boolean isRated) {
		this.isRated = isRated;
	}

	public Integer getLatestPickId() {
		return latestPickId;
	}

	public void setLatestPickId(Integer latestPickId) {
		this.latestPickId = latestPickId;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}
