package com.skplanet.syruptable.entity.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="user_migration_logs")
public class User_Migration_Logs {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="user_migration_logs_id_seq")
	@SequenceGenerator(name="user_migration_logs_id_seq", sequenceName="user_migration_logs_id_seq")
	private Integer id;
	
	@Column(name="old_user_id")
	@NotNull
	private Long oldUserId;
	
	@Column(name="new_user_id")
	@NotNull
	private Long newUserId;
	
	@Column(name="old_nickname")
	private String oldNickname;
	
	@Column(name="old_idp_type")
	private String oldIdpType;
	
	@Column(name="old_idp_uid")
	private String oldIdpUid;
	
	@Column(name="success")
	@NotNull
	private Boolean success;
	
	@Column(name="task_id")
	@NotNull
	private String taskId;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getOldUserId() {
		return oldUserId;
	}

	public void setOldUserId(Long oldUserId) {
		this.oldUserId = oldUserId;
	}

	public Long getNewUserId() {
		return newUserId;
	}

	public void setNewUserId(Long newUserId) {
		this.newUserId = newUserId;
	}

	public String getOldnickname() {
		return oldNickname;
	}

	public void setOldNickname(String oldNickname) {
		this.oldNickname = oldNickname;
	}

	public String getOldIdpType() {
		return oldIdpType;
	}

	public void setOldIdpType(String oldIdpType) {
		this.oldIdpType = oldIdpType;
	}

	public String getOldIdpUid() {
		return oldIdpUid;
	}

	public void setOldIdpUid(String oldIdpUid) {
		this.oldIdpUid = oldIdpUid;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}