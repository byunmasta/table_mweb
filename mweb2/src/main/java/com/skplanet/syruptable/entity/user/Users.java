package com.skplanet.syruptable.entity.user;

import java.util.Base64;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import syruptable.util.PickatCryptor;

@Entity
@Table(schema="public", name="users")
public class Users {
	
	@Transient
	private static final String MDN_ENC_KEY = "76Q8MSx53ZbCUKALpXXGB/NDTUP8eAnh90BdgrYkluo="; // etc/common.conf keys.db_mdn_enc_key
	
	@Transient
	private static final String MDN_ENC_IV = "ie3PkzhFPwswZ9dArksTXQ=="; //  etc/common.conf keys.db_mdn_enc_iv
	
	@Transient
	private static final String BIRTHDAY_ENC_KEY = "tpdddA+SyXGVuphlSBhOaAvfNRsP2SY+UXGDFRN8MZQ=";
	
	@Transient
	private static final String GENDER_ENC_KEY = "UfUEz1OI/RXnolz6Ougjzz0u0+eaeHFCuIGrG4hjewo=";
	
	@Transient
	private static final String SIGIL = "\\$E0\\$";
	
	@Transient
	private static final PickatCryptor bdCryptorBirthday = new PickatCryptor(BIRTHDAY_ENC_KEY);
	
	@Transient
	private static final PickatCryptor bdCryptorGender = new PickatCryptor(GENDER_ENC_KEY);
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="users_id_seq")
	@SequenceGenerator(name="users_id_seq", sequenceName="users_id_seq")
	private Integer id;
	
	public enum UserType {
		r, g, n, i;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="type")
	@NotNull
	private UserType userType;
	
	public enum Status {
		a, d, l;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="status")
	@NotNull
	private Status status;
	
	@Column(name="nickname")
	private String nickname;
	
	@Column(name="profile_image")
	private String profileImage;
	
	@Column(name="cover_image")
	private String coverImage;
	
	@Column(name="birthdate")
	private String birthdate;
	
	@Column(name="gender")
	private String gender;
	
	@Column(name="idp_type")
	private String idpType;
	
	@Column(name="idp_uid")
	private String idpUid;
	
	@Column(name="category_ids")
	private String categoryIds;
	
	@Column(name="description")
	private String description;
	
	@Column(name="badge")
	private String badge;
	
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@Column(name="attrs")
	@NotNull
	private Map<String, String> attrs;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public String getCoverImage() {
		return coverImage;
	}

	public void setCoverImage(String coverImage) {
		this.coverImage = coverImage;
	}

	public String getBirthdate() {
		if(this.birthdate!=null && !"".equals(this.birthdate)) {
			String birthday = this.birthdate.replaceAll(SIGIL, "");
			return new String(bdCryptorBirthday.decrypt(Base64.getDecoder().decode(birthday)));
		} else
			return this.birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate=birthdate;
	}

	public String getGender() {
		if(this.gender!=null && !"".equals(this.gender)) {
			String gender = this.gender.replaceAll(SIGIL, "");
			return new String(bdCryptorGender.decrypt(Base64.getDecoder().decode(gender)));
		} else
			return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getIdpType() {
		return idpType;
	}

	public void setIdpType(String idpType) {
		this.idpType = idpType;
	}

	public String getIdpUid() {
		return idpUid;
	}

	public void setIdpUid(String idpUid) {
		this.idpUid = idpUid;
	}

	public String getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(String categoryIds) {
		this.categoryIds = categoryIds;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBadge() {
		return badge;
	}

	public void setBadge(String badge) {
		this.badge = badge;
	}

	public Map<String, String> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, String> attrs) {
		this.attrs = attrs;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}
