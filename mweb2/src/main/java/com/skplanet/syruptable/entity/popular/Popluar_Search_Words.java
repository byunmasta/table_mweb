package com.skplanet.syruptable.entity.popular;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="popular_search_words")
public class Popluar_Search_Words {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="popular_search_words_id_seq")
	@SequenceGenerator(name="popular_search_words_id_seq", sequenceName="popular_search_words_id_seq")
	private Integer id;
	
	@Column(name="search_word")
	@NotNull
	private String searchWord;
	
	@Column(name="admin_user_id")
	private Integer adminUserId;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSearchWord() {
		return searchWord;
	}

	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}

	public Integer getAdminUserId() {
		return adminUserId;
	}

	public void setAdminUserId(Integer adminUserId) {
		this.adminUserId = adminUserId;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}