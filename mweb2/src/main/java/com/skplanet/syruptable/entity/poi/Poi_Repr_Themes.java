package com.skplanet.syruptable.entity.poi;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="poi_repr_themes")
public class Poi_Repr_Themes {
	
	@Id
	private Integer poiId;
	
	@Column(name="public_theme_id")
	@NotNull
	private Integer publicThemeId;
	
	@Column(name="pick_count")
	@NotNull
	private Integer pickCount;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	public Integer getpoiId() {
		return poiId;
	}

	public void setpoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public Integer getPublicThemeId() {
		return publicThemeId;
	}

	public void setPublicThemeId(Integer publicThemeId) {
		this.publicThemeId = publicThemeId;
	}

	public Integer getPickCount() {
		return pickCount;
	}

	public void setPickCount(Integer pickCount) {
		this.pickCount = pickCount;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}