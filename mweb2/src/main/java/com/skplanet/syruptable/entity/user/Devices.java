package com.skplanet.syruptable.entity.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="devices")
public class Devices {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="devices_id_seq")
	@SequenceGenerator(name="devices_id_seq", sequenceName="devices_id_seq")
	private Integer id;
	
	@Column(name="user_id")
	private Integer userId;
	
	@Column(name="device_token")
	private String deviceToken;
	
	public enum Platform {
		A, I;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="platform")
	@NotNull
	private Platform platform;
	
	@Column(name="platform_version")
	private String platformVersion;
	
	@Column(name="dev_model")
	private String devModel;
	
	@Column(name="dev_uuid")
	@NotNull
	private String devUuid;
	
	@Column(name="app_id")
	@NotNull
	private Integer appId;
	
	@Column(name="app_version")
	@NotNull
	private String appVersion;
	
	@Column(name="app_is_devel")
	private Boolean appIsDevel;
	
	public enum Push_Platform {
		A, G;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="push_platform")
	@NotNull
	private Push_Platform pushPlatform;
	
	@Column(name="push_token")
	private String pushToken;
	
	@Column(name="mdn")
	private String mdn;
	
	@Column(name="locale")
	private String locale;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public Platform getPlatform() {
		return platform;
	}

	public void setPlatform(Platform platform) {
		this.platform = platform;
	}

	public String getPlatformVersion() {
		return platformVersion;
	}

	public void setPlatformVersion(String platformVersion) {
		this.platformVersion = platformVersion;
	}

	public String getDevModel() {
		return devModel;
	}

	public void setDevModel(String devModel) {
		this.devModel = devModel;
	}

	public String getDevUuid() {
		return devUuid;
	}

	public void setDevUuid(String devUuid) {
		this.devUuid = devUuid;
	}

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public Boolean getAppIsDevel() {
		return appIsDevel;
	}

	public void setAppIsDevel(Boolean appIsDevel) {
		this.appIsDevel = appIsDevel;
	}

	public Push_Platform getPushPlatform() {
		return pushPlatform;
	}

	public void setPushPlatform(Push_Platform pushPlatform) {
		this.pushPlatform = pushPlatform;
	}

	public String getPushToken() {
		return pushToken;
	}

	public void setPushToken(String pushToken) {
		this.pushToken = pushToken;
	}

	public String getMdn() {
		return mdn;
	}

	public void setMdn(String mdn) {
		this.mdn = mdn;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}
