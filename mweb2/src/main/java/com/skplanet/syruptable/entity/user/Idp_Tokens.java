package com.skplanet.syruptable.entity.user;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@IdClass(Idp_Tokens_PK.class)
@Table(schema="public", name="poi_ratings")
public class Idp_Tokens implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	private String idpType;
	
	@Id
	private String idpUid;
	
	@Column(name="token")
	private String token;
	
	@Column(name="token_secret")
	private String tokenSecret;
	
	@Column(name="refresh_token")
	private String refreshToken;
	
	@Column(name="scopes")
	private String scopes;
	
	@Column(name="created_at")
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="expires_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expires_at;

	public String getIdpType() {
		return idpType;
	}

	public void setIdpType(String idpType) {
		this.idpType = idpType;
	}

	public String getIdpUid() {
		return idpUid;
	}

	public void setIdpUid(String idpUid) {
		this.idpUid = idpUid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTokenSecret() {
		return tokenSecret;
	}

	public void setTokenSecret(String tokenSecret) {
		this.tokenSecret = tokenSecret;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getScopes() {
		return scopes;
	}

	public void setScopes(String scopes) {
		this.scopes = scopes;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getExpires_at() {
		return expires_at;
	}

	public void setExpires_at(Date expires_at) {
		this.expires_at = expires_at;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}
