package com.skplanet.syruptable.entity.sns;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="sns_poi_mappings")
public class Sns_Poi_Mappings {
	
	@Id
	@Column(name="sns_id")
	private String snsId;
	
	public enum Snstype {
		I, F;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="sns_type")
	@NotNull
	private Snstype snsType;
	
	@Column(name="poi_id")
	private Integer poiId;
	
	@Column(name="sns_poi_name")
	private String snsPoiName;
	
	@OneToMany(mappedBy="snspoiId", cascade=CascadeType.ALL, fetch=FetchType.EAGER, orphanRemoval=true)
	@OrderBy(value="created_at DESC")
	private List<Sns_Poi_Images> snsPoiImages;

	public String getSnsId() {
		return snsId;
	}

	public void setSnsId(String snsId) {
		this.snsId = snsId;
	}

	public Snstype getSnsType() {
		return snsType;
	}

	public void setSnsType(Snstype snsType) {
		this.snsType = snsType;
	}

	public Integer getpoiId() {
		return poiId;
	}

	public void setpoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public String getSnsPoiName() {
		return snsPoiName;
	}

	public void setSnsPoiName(String snsPoiName) {
		this.snsPoiName = snsPoiName;
	}

	public List<Sns_Poi_Images> getSnsPoiImages() {
		return snsPoiImages;
	}

	public void setSnsPoiImages(List<Sns_Poi_Images> snsPoiImages) {
		this.snsPoiImages = snsPoiImages;
	}
}