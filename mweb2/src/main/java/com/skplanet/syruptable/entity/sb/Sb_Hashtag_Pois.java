package com.skplanet.syruptable.entity.sb;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.skplanet.syruptable.entity.hashtag.Hashtags;

@Entity
@IdClass(Sb_Hashtag_Pois_PK.class)
@Table(schema="public", name="sb_hashtag_pois")
public class Sb_Hashtag_Pois {
	
	@Id
	@Column(name="hashtag_id")
	private Integer hashtagId;
	
	@Id
	@Column(name="poi_id")
	private Integer poiId;
	
	@Column(name="user_count")
	@NotNull
	private Integer userCount;
	
	@Column(name="buzz_count")
	@NotNull
	private Integer buzzCount;
	
	@Column(name="score")
	@NotNull
	private Integer score;
	
	@Column(name="pick_count")
	@NotNull
	private Integer pickCount;
	
	@Column(name="hashtag_seq")
	private Integer hashtagSeq;
	
	@Column(name="admin_count")
	private Integer adminCount;
	
	@ManyToOne(cascade=CascadeType.PERSIST, fetch=FetchType.LAZY)
	@JoinColumn(name="hashtag_id", referencedColumnName="id", insertable=false, updatable=false)
	private Hashtags hashtag;
	
	public Integer getHashtagId() {
		return hashtagId;
	}

	public void setHashtagId(Integer hashtagId) {
		this.hashtagId = hashtagId;
	}

	public Integer getPoiId() {
		return poiId;
	}

	public void setPoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public Integer getUserCount() {
		return userCount;
	}

	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}

	public Integer getBuzzCount() {
		return buzzCount;
	}

	public void setBuzzCount(Integer buzzCount) {
		this.buzzCount = buzzCount;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Integer getPickCount() {
		return pickCount;
	}

	public void setPickCount(Integer pickCount) {
		this.pickCount = pickCount;
	}

	public Integer getHashtagSeq() {
		return hashtagSeq;
	}

	public void setHashtagSeq(Integer hashtagSeq) {
		this.hashtagSeq = hashtagSeq;
	}

	public Integer getAdminCount() {
		return adminCount;
	}

	public void setAdminCount(Integer adminCount) {
		this.adminCount = adminCount;
	}

	public Hashtags getHashtag() {
		return hashtag;
	}

	public void setHashtag(Hashtags hashtag) {
		this.hashtag = hashtag;
	}
}