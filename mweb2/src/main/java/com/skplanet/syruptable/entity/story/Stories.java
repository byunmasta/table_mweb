package com.skplanet.syruptable.entity.story;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.skplanet.syruptable.entity.user.Users;

@Entity
@Table(schema="public", name="stories")
public class Stories {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="stories_id_seq")
	@SequenceGenerator(name="stories_id_seq", sequenceName="stories_id_seq")
	private Integer id;
	
	@Column(name="title")
	@NotNull
	private String title;
	
	@Column(name="description")
	@NotNull
	private String description;
	
	@Column(name="image")
	private String image;
	
	@Column(name="is_visible")
	@NotNull
	private Boolean isVisible;
	
	@Column(name="seq")
	@NotNull
	private Integer seq;
	
	@Column(name="user_id")
	@NotNull
	private Integer userId;
	
	@Column(name="view_count")
	private Integer viewCount;
	
	@Column(name="share_count")
	private Integer shareCount;

	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	@OneToMany(mappedBy="storyId", cascade=CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=true)
	@OrderBy(value="page ASC")
	private List<Story_Contents> storyContents;
	
	@OneToMany(mappedBy="storyId", cascade=CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=true)
	private List<Story_Likes> storyLikes;
	
	@OneToMany(mappedBy="storyId", cascade=CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=true)
	@OrderBy(value="created_at DESC")
	private List<Story_Replies> storyReplies;
	
	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.LAZY, optional=false)
	@JoinColumn(name="user_id", referencedColumnName="id", insertable=false, updatable=false)
	private Users user;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Boolean getIsVisible() {
		return isVisible;
	}

	public void setIsVisible(Boolean isVisible) {
		this.isVisible = isVisible;
	}

	public Integer getViewCount() {
		return viewCount;
	}

	public void setViewCount(Integer viewCount) {
		this.viewCount = viewCount;
	}

	public Integer getShareCount() {
		return shareCount;
	}

	public void setShareCount(Integer shareCount) {
		this.shareCount = shareCount;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	public List<Story_Likes> getStoryLikes() {
		return storyLikes;
	}

	public void setStoryLikes(List<Story_Likes> storyLikes) {
		this.storyLikes = storyLikes;
	}

	public List<Story_Replies> getStoryReplies() {
		return storyReplies;
	}

	public void setStoryReplies(List<Story_Replies> storyReplies) {
		this.storyReplies = storyReplies;
	}

	public List<Story_Contents> getStoryContents() {
		return storyContents;
	}

	public void setStoryContents(List<Story_Contents> storyContents) {
		this.storyContents = storyContents;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}

	@Override
	public String toString() {
		return "Stories [id=" + id + ", title=" + title + ", description=" + description + ", image=" + image
				+ ", isVisible=" + isVisible + ", seq=" + seq + ", userId=" + userId + ", viewCount=" + viewCount
				+ ", shareCount=" + shareCount + ", createdat=" + createdat + ", updatedat=" + updatedat
				+ ", storyContents=" + storyContents + ", storyLikes=" + storyLikes + ", storyReplies=" + storyReplies
				+ ", user=" + user + "]";
	}

}