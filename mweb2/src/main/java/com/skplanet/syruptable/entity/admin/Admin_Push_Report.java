package com.skplanet.syruptable.entity.admin;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="admin_push_report")
public class Admin_Push_Report {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="admin_push_report_id_seq")
	@SequenceGenerator(name="admin_push_report_id_seq", sequenceName="admin_push_report_id_seq")
	private Integer id;
	
	@Column(name="notice_id")
	@NotNull
	private Integer noticeId;
	
	@Column(name="platform")
	@NotNull
	private String platform;
	
	@Column(name="app_version")
	@NotNull
	private String appVersion;
	
	@Column(name="invalid_device")
	@NotNull
	private Integer invalidDevice;
	
	@Column(name="total_count")
	@NotNull
	private Integer totalCount;
	
	@Column(name="etc_error")
	@NotNull
	private Integer etcError;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(Integer noticeId) {
		this.noticeId = noticeId;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public Integer getInvalidDevice() {
		return invalidDevice;
	}

	public void setInvalidDevice(Integer invalidDevice) {
		this.invalidDevice = invalidDevice;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getEtcError() {
		return etcError;
	}

	public void setEtcError(Integer etcError) {
		this.etcError = etcError;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}
