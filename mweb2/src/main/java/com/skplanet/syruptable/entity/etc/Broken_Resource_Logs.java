package com.skplanet.syruptable.entity.etc;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@Table(schema="public", name="broken_resource_logs")
public class Broken_Resource_Logs {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="broken_resource_logs_id_seq")
	@SequenceGenerator(name="broken_resource_logs_id_seq", sequenceName="broken_resource_logs_id_seq")
	private Integer id;
	
	@Column(name="rscope")
	@NotNull
	private String rScope;
	
	@Column(name="rtype")
	@NotNull
	private String rType;
	
	@Column(name="rid")
	@NotNull
	private String rId;
	
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@Column(name="attrs")
	@NotNull
	private Map<String, String> attrs;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getrScope() {
		return rScope;
	}

	public void setrScope(String rScope) {
		this.rScope = rScope;
	}

	public String getrType() {
		return rType;
	}

	public void setrType(String rType) {
		this.rType = rType;
	}

	public String getrId() {
		return rId;
	}

	public void setrId(String rId) {
		this.rId = rId;
	}

	public Map<String, String> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, String> attrs) {
		this.attrs = attrs;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}