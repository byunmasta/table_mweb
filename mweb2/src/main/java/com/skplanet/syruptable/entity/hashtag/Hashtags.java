package com.skplanet.syruptable.entity.hashtag;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@Table(schema="public", name="hashtags")
public class Hashtags {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="hashtags_id_seq")
	@SequenceGenerator(name="hashtags_id_seq", sequenceName="hashtags_id_seq")
	private Integer id;
	
	@Column(name="name")
	@NotNull
	private String name;
	
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@Column(name="attrs")
	@NotNull
	private Map<String, String> attrs;
	
	@Column(name="attributes")
	private String attributes;
	
	@Type(type="com.skplanet.syruptable.type.ArrayTypeString")
	@Column(name="related_words")
	private int[] relatedWords;
	
	@Column(name="banned")
	@NotNull
	private Boolean banned;
	
	@Column(name="aliasof")
	private Integer aliasof;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, String> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, String> attrs) {
		this.attrs = attrs;
	}

	public String getAttributes() {
		return attributes;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	public int[] getRelatedWords() {
		return relatedWords;
	}

	public void setRelatedWords(int[] relatedWords) {
		this.relatedWords = relatedWords;
	}

	public Boolean getBanned() {
		return banned;
	}

	public void setBanned(Boolean banned) {
		this.banned = banned;
	}

	public Integer getAliasof() {
		return aliasof;
	}

	public void setAliasof(Integer aliasof) {
		this.aliasof = aliasof;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}