package com.skplanet.syruptable.entity.sorder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(Sorder_Event_Pois_PK.class)
@Table(schema="public", name="sorder_event_pois")
public class Sorder_Event_Pois {
	
	@Id
	private Integer eventId;
	
	@Id
	private String mid;

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}
}