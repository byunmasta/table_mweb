package com.skplanet.syruptable.entity.sb;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@IdClass(Sb_Categories_PK.class)
@Table(schema="public", name="sb_categories")
public class Sb_Categories {
	
	@Id
	private Integer aoiId;
	
	@Id
	private String categoryId;
	
	@Column(name="poi_count")
	private Long poiCount;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;

	public Integer getAoiId() {
		return aoiId;
	}

	public void setAoiId(Integer aoiId) {
		this.aoiId = aoiId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public Long getPoiCount() {
		return poiCount;
	}

	public void setPoiCount(Long poiCount) {
		this.poiCount = poiCount;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@PrePersist
	protected void onCreate() {
		this.updatedat = new Date();
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}