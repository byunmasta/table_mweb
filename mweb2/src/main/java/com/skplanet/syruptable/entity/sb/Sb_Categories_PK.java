package com.skplanet.syruptable.entity.sb;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Sb_Categories_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="aoi_id", nullable=false)
	private Integer aoiId;
	
	@Column(name="category_id", nullable=false)
	private String categoryId;
	
	public Sb_Categories_PK() {
		
	}
	
	public Sb_Categories_PK(Integer aoiId, String categoryId) {
		this.aoiId = aoiId;
		this.categoryId = categoryId;
	}
}
