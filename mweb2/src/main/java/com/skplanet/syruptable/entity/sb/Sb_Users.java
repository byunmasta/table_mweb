package com.skplanet.syruptable.entity.sb;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.skplanet.syruptable.entity.user.Users;

@Entity
@Table(schema="public", name="sb_users")
public class Sb_Users {
	
	@Id
	@Column(name="user_id")
	private Integer userId;
	
	@Column(name="theme_count")
	@NotNull
	private Integer themeCount;
	
	@Column(name="follower_count")
	@NotNull
	private Integer followerCount;
	
	@Column(name="followee_count")
	@NotNull
	private Integer followeeCount;
	
	@Column(name="pick_count")
	@NotNull
	private Integer pickCount;
	
	@Column(name="wish_count")
	@NotNull
	private Integer wishCount;
	
	@Column(name="subscribed_themes_count")
	@NotNull
	private Integer subscribedThemesCount;
	
	@Column(name="theme_subscribers_count")
	@NotNull
	private Integer themeSubscribersCount;
	
	@Column(name="rating_count")
	@NotNull
	private Integer ratingCount;
	
	@Column(name="score")
	@NotNull
	private Integer score;

	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	
	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="user_id", referencedColumnName="id", insertable=false, updatable=false)
	private Users users;
	
	
	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getThemeCount() {
		return themeCount;
	}

	public void setThemeCount(Integer themeCount) {
		this.themeCount = themeCount;
	}

	public Integer getFollowerCount() {
		return followerCount;
	}

	public void setFollowerCount(Integer followerCount) {
		this.followerCount = followerCount;
	}

	public Integer getFolloweeCount() {
		return followeeCount;
	}

	public void setFolloweeCount(Integer followeeCount) {
		this.followeeCount = followeeCount;
	}

	public Integer getPickCount() {
		return pickCount;
	}

	public void setPickCount(Integer pickCount) {
		this.pickCount = pickCount;
	}

	public Integer getWishCount() {
		return wishCount;
	}

	public void setWishCount(Integer wishCount) {
		this.wishCount = wishCount;
	}

	public Integer getSubscribedThemesCount() {
		return subscribedThemesCount;
	}

	public void setSubscribedThemesCount(Integer subscribedThemesCount) {
		this.subscribedThemesCount = subscribedThemesCount;
	}

	public Integer getThemeSubscribersCount() {
		return themeSubscribersCount;
	}

	public void setThemeSubscribersCount(Integer themeSubscribersCount) {
		this.themeSubscribersCount = themeSubscribersCount;
	}

	public Integer getRatingCount() {
		return ratingCount;
	}

	public void setRatingCount(Integer ratingCount) {
		this.ratingCount = ratingCount;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}