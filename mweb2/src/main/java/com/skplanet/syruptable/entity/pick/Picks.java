package com.skplanet.syruptable.entity.pick;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.skplanet.syruptable.entity.poi.Pois;
import com.skplanet.syruptable.entity.sb.Sb_Picks;
import com.skplanet.syruptable.entity.user.Users;

@Entity
@Table(schema="public", name="picks")
public class Picks {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="picks_id_seq")
	@SequenceGenerator(name="picks_id_seq", sequenceName="picks_id_seq")
	private Integer id;
	
	@Column(name="public_theme_id")
	@NotNull
	private Integer publicThemeId;
	
	@Column(name="private_theme_id")
	@NotNull
	private Integer privateThemeId;
	
	@Column(name="poi_id")
	private Integer poiId;
	
	@Column(name="user_id")
	private Integer userId;
	
	@Column(name="contents")
	private String contents;
	
	@Column(name="images")
	private String images;
	
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@Column(name="attrs")
	@NotNull
	private Map<String, String> attrs;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="user_id", referencedColumnName="id", insertable=false, updatable=false)
	private Users users;
	
	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="poi_id", referencedColumnName="id", insertable=false, updatable=false)
	private Pois pois;
	
	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="id", referencedColumnName="pick_id", insertable=false, updatable=false)
	private Sb_Picks sbpicks;
	
	@OneToMany(mappedBy="pickId", cascade=CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=true)
	private List<Pick_Images> pickImages;
	
	@OneToMany(mappedBy="pickId", cascade=CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=true)
	@OrderBy(value="updated_at DESC")
	private List<Pick_Comments> pickComments;
	
	@OneToMany(mappedBy="pickId", cascade=CascadeType.ALL, fetch=FetchType.LAZY, orphanRemoval=true)
	private List<Pick_Likes> pickLikes;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPublicThemeId() {
		return publicThemeId;
	}

	public void setPublicThemeId(Integer publicThemeId) {
		this.publicThemeId = publicThemeId;
	}

	public Integer getPrivateThemeId() {
		return privateThemeId;
	}

	public void setPrivateThemeId(Integer privateThemeId) {
		this.privateThemeId = privateThemeId;
	}

	public Integer getPoiId() {
		return poiId;
	}

	public void setPoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public Map<String, String> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, String> attrs) {
		this.attrs = attrs;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	public Sb_Picks getSbpicks() {
		return sbpicks;
	}

	public void setSbpicks(Sb_Picks sbpicks) {
		this.sbpicks = sbpicks;
	}

	public List<Pick_Images> getPickImages() {
		return pickImages;
	}

	public void setPickImages(List<Pick_Images> pickImages) {
		this.pickImages = pickImages;
	}

	public List<Pick_Comments> getPickComments() {
		return pickComments;
	}

	public void setPickComments(List<Pick_Comments> pickComments) {
		this.pickComments = pickComments;
	}

	public Pois getPois() {
		return pois;
	}

	public void setPois(Pois pois) {
		this.pois = pois;
	}

	public List<Pick_Likes> getPickLikes() {
		return pickLikes;
	}

	public void setPickLikes(List<Pick_Likes> pickLikes) {
		this.pickLikes = pickLikes;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}