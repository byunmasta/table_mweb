package com.skplanet.syruptable.entity.pick;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Pick_Likes_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="user_id", nullable=false)
	private Integer userId;
	
	@Column(name="pick_id", nullable=false)
	private Integer pickId;
	
	public Pick_Likes_PK() {
		
	}
	
	public Pick_Likes_PK(Integer userId, Integer pickId) {
		this.userId = userId;
		this.pickId = pickId;
	}
}
