package com.skplanet.syruptable.entity.sb;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.skplanet.syruptable.entity.admin.Admin_Poi_Images;
import com.skplanet.syruptable.entity.pick.Picks;
import com.skplanet.syruptable.entity.poi.Pois;

@Entity
@Table(schema="public", name="sb_pois")
public class Sb_Pois {
	
	@Id
	@Column(name="poi_id")
	private Integer poiId;
	
	@Column(name="poi_name")
	private String poiName;
	
	@Column(name="pick_count")
	@NotNull
	private Integer pickCount;
	
	@Column(name="wish_count")
	@NotNull
	private Integer wishCount;
	
	@Column(name="blog_review_count")
	@NotNull
	private Integer blogReviewCount;
	
	@Column(name="comment_count")
	@NotNull
	private Integer commentCount;
	
	@Column(name="recent_images")
	@JsonFormat
	private String recentImages;
	
	@Column(name="images_count")
	@NotNull
	private Integer imagesCount;
	
	@Column(name="recent_image")
	private String recentImage;
	
	@Column(name="category_id")
	private String categoryId;
	
	@Column(name="category_lid")
	private String categoryLid;
	
	@Column(name="admin_score")
	@NotNull
	private Integer adminScore;
	
	@Column(name="om_total_score")
	@NotNull
	private Integer omTotalScore;
	
	@Column(name="score")
	@NotNull
	private Integer score;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@Column(name="attrs")
	@NotNull
	private Map<String, String> attrs;
	
	@Type(type="com.skplanet.syruptable.type.ArrayTypeInteger")
	@Column(name="aoi_ids")
	private int[] aoiIds;
	
	@Column(name="rater")
	@NotNull
	private Integer rater;
	
	@Column(name="star_rating")
	@NotNull
	private Double starRating;
	
	@Column(name="recent_pick_id")
	@NotNull
	private Integer recentPickId;
	
	@Column(name="has_coupon")
	@NotNull
	private Boolean has_coupon;
	
	@Type(type="com.skplanet.syruptable.type.ArrayTypeInteger")
	@Column(name="repr_hashtag_ids")
	private int[] reprHashtagIds;
	
	@Column(name="picker_count")
	@NotNull
	private Integer pickerCount;
	
	@Column(name="pick_image_count")
	@NotNull
	private Integer pickImageCount;
	
	@Column(name="blog_image_count")
	@NotNull
	private Integer blogImageCount;
	
	@Column(name="sns_image_count")
	@NotNull
	private Integer snsImageCount;
	
	@Column(name="pick_score")
	@NotNull
	private Long pickScore;
	
	@OneToMany(cascade=CascadeType.PERSIST, fetch=FetchType.EAGER, orphanRemoval=true)
	@JoinColumn(name="poi_id", referencedColumnName="poi_id", insertable=false, updatable=false)
	@Where(clause="images IS NOT NULL")
	@OrderBy(value="updated_at DESC")
	private List<Picks> picks;
	
	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.LAZY, optional=false)
	@JoinColumn(name="poi_id", referencedColumnName="id", insertable=false, updatable=false)
	private Pois pois;
	
	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.LAZY, optional=false)
	@JoinColumn(name="poi_id", referencedColumnName="poi_id", insertable=false, updatable=false)
	private Admin_Poi_Images adminPoiImages;
	
	public Integer getRater() {
		return rater;
	}

	public void setRater(Integer rater) {
		this.rater = rater;
	}

	public Double getStarRating() {
		return starRating;
	}

	public void setStarRating(Double starRating) {
		this.starRating = starRating;
	}

	public Integer getRecentPickId() {
		return recentPickId;
	}

	public void setRecentPickId(Integer recentPickId) {
		this.recentPickId = recentPickId;
	}

	public Boolean getHas_coupon() {
		return has_coupon;
	}

	public void setHas_coupon(Boolean has_coupon) {
		this.has_coupon = has_coupon;
	}

	public Integer getPickerCount() {
		return pickerCount;
	}

	public void setPickerCount(Integer pickerCount) {
		this.pickerCount = pickerCount;
	}

	public Integer getPickImageCount() {
		return pickImageCount;
	}

	public void setPickImageCount(Integer pickImageCount) {
		this.pickImageCount = pickImageCount;
	}

	public Integer getBlogImageCount() {
		return blogImageCount;
	}

	public void setBlogImageCount(Integer blogImageCount) {
		this.blogImageCount = blogImageCount;
	}

	public Integer getSnsImageCount() {
		return snsImageCount;
	}

	public void setSnsImageCount(Integer snsImageCount) {
		this.snsImageCount = snsImageCount;
	}

	public Long getPickScore() {
		return pickScore;
	}

	public void setPickScore(Long pickScore) {
		this.pickScore = pickScore;
	}

	public int[] getReprHashtagIds() {
		return reprHashtagIds;
	}

	public void setReprHashtagIds(int[] reprHashtagIds) {
		this.reprHashtagIds = reprHashtagIds;
	}

	public Integer getPoiId() {
		return poiId;
	}

	public void setPoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public String getPoiName() {
		return poiName;
	}

	public void setPoiName(String poiName) {
		this.poiName = poiName;
	}

	public Integer getPickCount() {
		return pickCount;
	}

	public void setPickCount(Integer pickCount) {
		this.pickCount = pickCount;
	}

	public Integer getWishCount() {
		return wishCount;
	}

	public void setWishCount(Integer wishCount) {
		this.wishCount = wishCount;
	}

	public Integer getBlogReviewCount() {
		return blogReviewCount;
	}

	public void setBlogReviewCount(Integer blogReviewCount) {
		this.blogReviewCount = blogReviewCount;
	}

	public Integer getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}

	public String getRecentImages() {
		return recentImages;
	}

	public void setRecentImages(String recentImages) {
		this.recentImages = recentImages;
	}

	public Integer getImagesCount() {
		return imagesCount;
	}

	public void setImagesCount(Integer imagesCount) {
		this.imagesCount = imagesCount;
	}

	public String getRecentImage() {
		return recentImage;
	}

	public void setRecentImage(String recentImage) {
		this.recentImage = recentImage;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryLid() {
		return categoryLid;
	}

	public void setCategoryLid(String categoryLid) {
		this.categoryLid = categoryLid;
	}

	public Integer getAdminScore() {
		return adminScore;
	}

	public void setAdminScore(Integer adminScore) {
		this.adminScore = adminScore;
	}

	public Integer getOmTotalScore() {
		return omTotalScore;
	}

	public void setOmTotalScore(Integer omTotalScore) {
		this.omTotalScore = omTotalScore;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	public Map<String, String> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, String> attrs) {
		this.attrs = attrs;
	}

	public int[] getAoiIds() {
		return aoiIds;
	}

	public void setAoiIds(int[] aoiIds) {
		this.aoiIds = aoiIds;
	}

	public List<Picks> getPicks() {
		return picks;
	}

	public void setPicks(List<Picks> picks) {
		this.picks = picks;
	}

	public Pois getPois() {
		return pois;
	}

	public void setPois(Pois pois) {
		this.pois = pois;
	}

	public Admin_Poi_Images getAdminPoiImages() {
		return adminPoiImages;
	}

	public void setAdminPoiImages(Admin_Poi_Images adminPoiImages) {
		this.adminPoiImages = adminPoiImages;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}