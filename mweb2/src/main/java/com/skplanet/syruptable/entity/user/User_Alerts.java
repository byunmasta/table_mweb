package com.skplanet.syruptable.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@IdClass(User_Alerts_PK.class)
@Table(schema="public", name="user_alerts")
public class User_Alerts implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer userId;
	
	@Id
	private Integer feedId;
	
	@Column(name="marked")
	@NotNull
	private Boolean marked;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getFeedId() {
		return feedId;
	}

	public void setFeedId(Integer feedId) {
		this.feedId = feedId;
	}

	public Boolean getMarked() {
		return marked;
	}

	public void setMarked(Boolean marked) {
		this.marked = marked;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}