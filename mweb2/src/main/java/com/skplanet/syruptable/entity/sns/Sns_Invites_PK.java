package com.skplanet.syruptable.entity.sns;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Sns_Invites_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="invitor_id", nullable=false)
	private Integer invitorId;
	
	@Column(name="sns_type", nullable=false)
	private String snsType;
	
	@Column(name="sns_uid", nullable=false)
	private String snsUid;
	
	public Sns_Invites_PK() {
		
	}
	
	public Sns_Invites_PK(Integer invitorId, String snsType, String snsUid) {
		this.invitorId = invitorId;
		this.snsType = snsType;
		this.snsUid = snsUid;
	}
}
