package com.skplanet.syruptable.entity.aoi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="aoi_groups")
public class Aoi_Groups {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="aoi_groups_id_seq")
	@SequenceGenerator(name="aoi_groups_id_seq", sequenceName="aoi_groups_id_seq")
	private Integer id;
	
	@Column(name="parent_id")
	private Integer parentId;
	
	@Column(name="aoi_id")
	private Integer aoiId;
	
	@Column(name="display_name")
	private String displayName;
	
	@Column(name="seq")
	@NotNull
	private Integer seq;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getAoiId() {
		return aoiId;
	}

	public void setAoiId(Integer aoiId) {
		this.aoiId = aoiId;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}
}