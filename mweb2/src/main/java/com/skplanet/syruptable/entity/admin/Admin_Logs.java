package com.skplanet.syruptable.entity.admin;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@Table(schema="public", name="admin_logs")
public class Admin_Logs {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="admin_logs_id_seq")
	@SequenceGenerator(name="admin_logs_id_seq", sequenceName="admin_logs_id_seq")
	private Integer id;
	
	@Column(name="admin_user_id")
	@NotNull
	private Integer adminUserId;
	
	@Column(name="attrs")
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@NotNull
	private Map<String, String> attrs;
	
	@Column(name="type")
	@NotNull
	private String type;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Map<String, String> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, String> attrs) {
		this.attrs = attrs;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getadminUserId() {
		return adminUserId;
	}

	public void setadminUserId(Integer adminUserId) {
		this.adminUserId = adminUserId;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}