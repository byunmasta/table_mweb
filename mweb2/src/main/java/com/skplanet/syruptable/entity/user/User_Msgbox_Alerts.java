package com.skplanet.syruptable.entity.user;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@Table(schema="public", name="user_msgbox_alerts")
public class User_Msgbox_Alerts {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="user_msgbox_alerts_id_seq")
	@SequenceGenerator(name="user_msgbox_alerts_id_seq", sequenceName="user_msgbox_alerts_id_seq")
	private Integer id;
	
	@Column(name="user_id")
	@NotNull
	private Integer userId;
	
	@Column(name="marked")
	private Boolean marked;
	
	@Column(name="button_type")
	@NotNull
	private String buttonType;
	
	@Column(name="message")
	@NotNull
	private String message;
	
	@Column(name="confirm_label")
	private String confirmLabel;
	
	@Column(name="cancel_label")
	private String cancelLabel;
	
	@Column(name="confirm_link")
	private String confirmLink;
	
	@Column(name="cancel_link")
	private String cancelLink;
	
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@Column(name="attrs")
	@NotNull
	private Map<String, String> attrs;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Boolean getMarked() {
		return marked;
	}

	public void setMarked(Boolean marked) {
		this.marked = marked;
	}

	public String getButtonType() {
		return buttonType;
	}

	public void setButtonType(String buttonType) {
		this.buttonType = buttonType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getConfirmLabel() {
		return confirmLabel;
	}

	public void setConfirmLabel(String confirmLabel) {
		this.confirmLabel = confirmLabel;
	}

	public String getCancelLabel() {
		return cancelLabel;
	}

	public void setCancelLabel(String cancelLabel) {
		this.cancelLabel = cancelLabel;
	}

	public String getConfirmLink() {
		return confirmLink;
	}

	public void setConfirmLink(String confirmLink) {
		this.confirmLink = confirmLink;
	}

	public String getCancelLink() {
		return cancelLink;
	}

	public void setCancelLink(String cancelLink) {
		this.cancelLink = cancelLink;
	}

	public Map<String, String> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, String> attrs) {
		this.attrs = attrs;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}