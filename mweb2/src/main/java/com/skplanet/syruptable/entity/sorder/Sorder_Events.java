package com.skplanet.syruptable.entity.sorder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="sorder_events")
public class Sorder_Events {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="sorder_events_id_seq")
	@SequenceGenerator(name="sorder_events_id_seq", sequenceName="sorder_events_id_seq")
	private Integer id;
	
	@Column(name="name")
	@NotNull
	private String name;
	
	@Column(name="active")
	@NotNull
	private Boolean active;
	
	@Column(name="main_images")
	@NotNull
	private String mainImages;
	
	@Column(name="desc_images")
	@NotNull
	private String descImages;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getMainImages() {
		return mainImages;
	}

	public void setMainImages(String mainImages) {
		this.mainImages = mainImages;
	}

	public String getDescImages() {
		return descImages;
	}

	public void setDescImages(String descImages) {
		this.descImages = descImages;
	}
}