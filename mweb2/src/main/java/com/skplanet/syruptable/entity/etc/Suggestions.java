package com.skplanet.syruptable.entity.etc;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@Table(schema="public", name="suggestions")
public class Suggestions {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="suggestions_id_seq")
	@SequenceGenerator(name="suggestions_id_seq", sequenceName="suggestions_id_seq")
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="params")
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@NotNull
	private Map<String, String> params;
	
	@Column(name="aoi_id")
	private Integer aoiId;
	
	@Column(name="sort")
	private Integer sort;
	
	@Column(name="admin_user_id")
	private Integer adminUserId;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

	public Integer getAoiId() {
		return aoiId;
	}

	public void setAoiId(Integer aoiId) {
		this.aoiId = aoiId;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getAdminUserId() {
		return adminUserId;
	}

	public void setAdminUserId(Integer adminUserId) {
		this.adminUserId = adminUserId;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	@Override
	public String toString() {
		return String
				.format("Suggestions [id=%s, name=%s, params=%s, aoiId=%s, sort=%s, adminUserId=%s, createdat=%s]",
						id, name, params, aoiId, sort, adminUserId, createdat);
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}
