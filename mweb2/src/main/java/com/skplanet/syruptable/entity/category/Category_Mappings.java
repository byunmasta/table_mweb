package com.skplanet.syruptable.entity.category;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@IdClass(Category_Mappings_PK.class)
@Table(schema="public", name="category_mappings")
public class Category_Mappings implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer providerId;
	
	@Id
	private String code;
	
	@Column(name="name")
	@NotNull
	private String name;
	
	@Column(name="display_name")
	private String displayName;
	
	@Column(name="seq")
	private Integer seq;
	
	@Column(name="pickat_id")
	private String pickatId;

	public Integer getProviderId() {
		return providerId;
	}

	public void setProviderId(Integer providerId) {
		this.providerId = providerId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getPickatId() {
		return pickatId;
	}

	public void setPickatId(String pickatId) {
		this.pickatId = pickatId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}