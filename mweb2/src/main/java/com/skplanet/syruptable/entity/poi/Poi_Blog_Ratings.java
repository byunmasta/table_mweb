package com.skplanet.syruptable.entity.poi;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@Table(schema="public", name="poi_blog_ratings")
public class Poi_Blog_Ratings {
	
	@Id
	private Integer poiId;
	
	@Column(name="comms_id")
	private Long commsId;
	
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@Column(name="ratings")
	@NotNull
	private Map<String, String> ratings;

	public Integer getpoiId() {
		return poiId;
	}

	public void setpoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public Long getCommsId() {
		return commsId;
	}

	public void setCommsId(Long commsId) {
		this.commsId = commsId;
	}

	public Map<String, String> getRatings() {
		return ratings;
	}

	public void setRatings(Map<String, String> ratings) {
		this.ratings = ratings;
	}
}