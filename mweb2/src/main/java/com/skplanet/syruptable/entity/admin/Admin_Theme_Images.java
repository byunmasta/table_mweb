package com.skplanet.syruptable.entity.admin;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="admin_theme_images")
public class Admin_Theme_Images {
	
	@Id
	@Column(name="public_theme_id")
	private Integer publicThemeId;
	
	@Column(name="admin_user_id")
	private Integer adminUserId;
	
	@Column(name="image")
	@NotNull
	private String image;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;

	public Integer getPublicThemeId() {
		return publicThemeId;
	}

	public void setPublicThemeId(Integer publicThemeId) {
		this.publicThemeId = publicThemeId;
	}

	public Integer getAdminUserId() {
		return adminUserId;
	}

	public void setAdminUserId(Integer adminUserId) {
		this.adminUserId = adminUserId;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}
