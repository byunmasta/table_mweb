package com.skplanet.syruptable.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Idp_Tokens_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="idp_type", nullable=false)
	private String idpType;
	
	@Column(name="idp_uid", nullable=false)
	private String idpUid;
	
	public Idp_Tokens_PK() {
		
	}
	
	public Idp_Tokens_PK(String idpType, String idpUid) {
		this.idpType = idpType;
		this.idpUid = idpUid;
	}
}
