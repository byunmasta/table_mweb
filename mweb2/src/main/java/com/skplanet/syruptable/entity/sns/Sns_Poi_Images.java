package com.skplanet.syruptable.entity.sns;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="sns_poi_images")
public class Sns_Poi_Images {
	
	@Id
	private String id;
	
	@Column(name="sns_poi_id")
	@NotNull
	private String snspoiId;
	
	@Column(name="image_url")
	private String imageUrl;
	
	@Column(name="user_id")
	private String userId;
	
	@Column(name="visible")
	@NotNull
	private Boolean visible;
	
	@Column(name="user_name")
	private String userName;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSnspoiId() {
		return snspoiId;
	}

	public void setSnspoiId(String snspoiId) {
		this.snspoiId = snspoiId;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}
