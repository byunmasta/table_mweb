package com.skplanet.syruptable.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Wishes_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="user_id", nullable=false)
	private Integer userId;
	
	@Column(name="poi_id", nullable=false)
	private Integer poiId;
	
	public Wishes_PK() {
		
	}
	
	public Wishes_PK(Integer userId, Integer poiId) {
		this.userId = userId;
		this.poiId = poiId;
	}
}
