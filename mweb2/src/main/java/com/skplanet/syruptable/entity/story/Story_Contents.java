package com.skplanet.syruptable.entity.story;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@IdClass(Story_Contents_PK.class)
@Table(schema="public", name="story_contents")
public class Story_Contents {
	
	@Id
	private Integer storyId;
	
	@Id
	private Integer page;
	
	public enum Stroytype {
		MDTE, TEXT, MEDI, POTE, IMTE, IMGE
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="type")
	@NotNull
	private Stroytype type;

	@Type(type="com.skplanet.syruptable.type.ArrayTypeInteger")
	@Column(name="poi_ids")
	private int[] poiIds;
	
	@Column(name="media")
	private String media;
	
	@Type(type="com.skplanet.syruptable.type.ArrayTypeString")
	@Column(name="contents")
	private String[] contents;

	public Integer getStoryId() {
		return storyId;
	}

	public void setStoryId(Integer storyId) {
		this.storyId = storyId;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Stroytype getType() {
		return type;
	}

	public void setType(Stroytype type) {
		this.type = type;
	}

	public int[] getPoiIds() {
		return poiIds;
	}

	public void setPoiIds(int[] poiIds) {
		this.poiIds = poiIds;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public String[] getContents() {
		return contents;
	}

	public void setContents(String[] contents) {
		this.contents = contents;
	}
}