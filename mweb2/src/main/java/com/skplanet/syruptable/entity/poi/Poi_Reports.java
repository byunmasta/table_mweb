package com.skplanet.syruptable.entity.poi;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(schema="public", name="poi_reports")
public class Poi_Reports {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="poi_reports_id_seq")
	@SequenceGenerator(name="poi_reports_id_seq", sequenceName="poi_reports_id_seq")
	private Integer id;
	
	@Column(name="poi_id")
	@NotNull
	private Integer poiId;
	
	@Column(name="user_id")
	private Integer userId;
	
	public enum Type {
		incorrect_detail,
		incorrect_store_info,
		missing_poi,
		incorrect_location,
		incorrect_phone,
		incorrect_price_menu,
		incorrect_res_park,
		incorrect_etc1,
		incorrect_etc2,
		incorrect_etc3,
		incorrect_etc4;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="type")
	private Type type;
	
	@Column(name="phone")
	private String phone;
	
	@Column(name="addr")
	private String addr;
	
	@Column(name="lat")
	private double lat;
	
	@Column(name="lng")
	private double lng;
	
	@Column(name="detail")
	private String detail;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getpoiId() {
		return poiId;
	}

	public void setpoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}

	public void setCreated_at(Date createdat) {
		this.createdat = createdat;
	}
}
