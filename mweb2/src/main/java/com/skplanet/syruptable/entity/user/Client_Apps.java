package com.skplanet.syruptable.entity.user;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@Table(schema="public", name="client_apps")
public class Client_Apps {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="client_apps_id_seq")
	@SequenceGenerator(name="client_apps_id_seq", sequenceName="client_apps_id_seq")
	private Integer id;
	
	@Column(name="name")
	@NotNull
	private String name;
	
	@Column(name="description")
	private String description;
	
	@Column(name="oauth_consumer_key")
	private String oauthConsumerKey;
	
	@Column(name="oauth_consumer_secret")
	private String oauthConsumerSecret;
	
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@Column(name="attrs")
	private Map<String, String> attrs;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOauthConsumerKey() {
		return oauthConsumerKey;
	}

	public void setOauthConsumerKey(String oauthConsumerKey) {
		this.oauthConsumerKey = oauthConsumerKey;
	}

	public String getOauthConsumerSecret() {
		return oauthConsumerSecret;
	}

	public void setOauthConsumerSecret(String oauthConsumerSecret) {
		this.oauthConsumerSecret = oauthConsumerSecret;
	}

	public Map<String, String> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, String> attrs) {
		this.attrs = attrs;
	}
}
