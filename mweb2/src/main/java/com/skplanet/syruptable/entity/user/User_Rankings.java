package com.skplanet.syruptable.entity.user;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@Table(schema="public", name="user_rankings")
public class User_Rankings {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="user_rankings_id_seq")
	@SequenceGenerator(name="user_rankings_id_seq", sequenceName="user_rankings_id_seq")
	private Integer id;
	
	public enum Stats_Type {
		WEEKLY, MONTHLY, ALL;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="stats_type")
	@NotNull
	private Stats_Type statsType;
	
	public enum Rangking_Type {
		PICK_COUNT, FOLLOWER_COUNT;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="ranking_type")
	@NotNull
	private Rangking_Type rankingType;
	
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@Column(name="attrs")
	@NotNull
	private Map<String, String> attrs;
	
	@Column(name="start_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startat;
	
	@Column(name="end_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endat;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Stats_Type getStatsType() {
		return statsType;
	}

	public void setStatsType(Stats_Type statsType) {
		this.statsType = statsType;
	}

	public Rangking_Type getRankingType() {
		return rankingType;
	}

	public void setRankingType(Rangking_Type rankingType) {
		this.rankingType = rankingType;
	}

	public Map<String, String> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, String> attrs) {
		this.attrs = attrs;
	}

	public Date getStartat() {
		return startat;
	}

	public void setStartat(Date startat) {
		this.startat = startat;
	}

	public Date getEndat() {
		return endat;
	}

	public void setEndat(Date endat) {
		this.endat = endat;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}
