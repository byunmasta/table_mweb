package com.skplanet.syruptable.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class User_Relations_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="follower_id", nullable=false)
	private Integer followerId;
	
	@Column(name="followee_id", nullable=false)
	private Integer followeeId;
	
	public User_Relations_PK() {
		
	}
	
	public User_Relations_PK(Integer followerId, Integer followeeId) {
		this.followerId = followerId;
		this.followeeId = followeeId;
	}
}
