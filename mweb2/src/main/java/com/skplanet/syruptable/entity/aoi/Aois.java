package com.skplanet.syruptable.entity.aoi;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vividsolutions.jts.geom.Geometry;

@Entity
@Table(schema="public", name="aois")
public class Aois {
	
	@Id
	private Integer id;
	
	@Column(name="parent_id")
	private Long parentId;
	
	@Column(name="is_active")
	@NotNull
	private Boolean isActive;
	
	@Column(name="name")
	private String name;
	
	public enum Category {
		SKM_ZTHEMEA,
		SIGUNP_B,
		SIDOP_B,
		BDONGP
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name="category")
	@NotNull
	private Category category;
	
	@Column(name="level")
	private String level;
	
	@Column(name="country")
	private String country;
	
	@Type(type="org.hibernate.spatial.GeometryType")
	@Column(name="geom", columnDefinition="Geometry")
	@JsonIgnore
	private Geometry geom;
	
	@Type(type="org.hibernate.spatial.GeometryType")
	@Column(name="raw_geom", columnDefinition="Geometry")
	@JsonIgnore
	private Geometry rawGeom;
	
	@Column(name="description")
	private String description;
	
	@Column(name="rbsp_common_attrs")
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@NotNull
	private Map<String, String> rbspCommonAttrs;
	
	@Column(name="rbsp_extra_attrs")
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	@NotNull
	private Map<String, String> rbspExtraAttrs;
	
	@Column(name="attrs")
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	private Map<String, String> attrs;
	
	@Column(name="nearby_aois")
	private String nearbyAois;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Geometry getGeom() {
		return geom;
	}

	public void setGeom(Geometry geom) {
		this.geom = geom;
	}

	public Geometry getRawGeom() {
		return rawGeom;
	}

	public void setRawGeom(Geometry rawGeom) {
		this.rawGeom = rawGeom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Map<String, String> getRbspCommonAttrs() {
		return rbspCommonAttrs;
	}

	public void setRbspCommonAttrs(Map<String, String> rbspCommonAttrs) {
		this.rbspCommonAttrs = rbspCommonAttrs;
	}

	public Map<String, String> getRbspExtraAttrs() {
		return rbspExtraAttrs;
	}

	public void setRbspExtraAttrs(Map<String, String> rbspExtraAttrs) {
		this.rbspExtraAttrs = rbspExtraAttrs;
	}
	
	public Map<String, String> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, String> attrs) {
		this.attrs = attrs;
	}

	public String getNearbyAois() {
		return nearbyAois;
	}

	public void setNearbyAois(String nearbyAois) {
		this.nearbyAois = nearbyAois;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@Override
	public String toString() {
		return String
				.format("Aois [id=%s, parentId=%s, isActive=%s, name=%s, category=%s, level=%s, country=%s, geom=%s, rawGeom=%s, description=%s, rbspCommonAttrs=%s, rbspExtraAttrs=%s, nearbyAois=%s, createdat=%s, updatedat=%s]",
						id, parentId, isActive, name, category, level, country,
						geom, rawGeom, description, rbspCommonAttrs,
						rbspExtraAttrs, nearbyAois, createdat, updatedat);
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}