package com.skplanet.syruptable.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class User_Idp_Accounts_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="user_id", nullable=false)
	private Integer userId;
	
	@Column(name="idp_type", nullable=false)
	private String idpType;
	
	public User_Idp_Accounts_PK() {
		
	}
	
	public User_Idp_Accounts_PK(Integer userId, String idpType) {
		this.userId = userId;
		this.idpType = idpType;
	}
}
