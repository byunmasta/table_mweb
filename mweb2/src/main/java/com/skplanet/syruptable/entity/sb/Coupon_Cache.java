package com.skplanet.syruptable.entity.sb;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@IdClass(Coupon_Cache_PK.class)
@Table(schema="public", name="coupon_caches")
public class Coupon_Cache {
	
	@Id
	@Column(name="partner_code")
	private String partnerCode;
	
	@Id
	@Column(name="pt_prod_code")
	private String ptProdCode;
	
	@Column(name="cp_name")
	@NotNull
	private String cpName;
	
	@Column(name="data")
	@NotNull
	private String data;
	
	@Type(type="com.skplanet.syruptable.type.ArrayTypeInteger")
	@Column(name="poi_ids")
	@NotNull
	private int[] poiIds;
	
	@Type(type="com.skplanet.syruptable.type.ArrayTypeInteger")
	@Column(name="skmc_ids")
	@NotNull
	private int[] skmcIds;
	
	@Column(name="issue_period")
	@NotNull
	private String issuePeriod;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;
	
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getPtProdCode() {
		return ptProdCode;
	}

	public void setPtProdCode(String ptProdCode) {
		this.ptProdCode = ptProdCode;
	}

	public String getCpName() {
		return cpName;
	}

	public void setCpName(String cpName) {
		this.cpName = cpName;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int[] getPoiIds() {
		return poiIds;
	}

	public void setPoiIds(int[] poiIds) {
		this.poiIds = poiIds;
	}

	public int[] getSkmcIds() {
		return skmcIds;
	}

	public void setSkmcIds(int[] skmcIds) {
		this.skmcIds = skmcIds;
	}

	public String getIssuePeriod() {
		return issuePeriod;
	}

	public void setIssuePeriod(String issuePeriod) {
		this.issuePeriod = issuePeriod;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}