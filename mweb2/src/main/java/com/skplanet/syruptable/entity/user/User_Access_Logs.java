package com.skplanet.syruptable.entity.user;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@IdClass(User_Access_Logs_PK.class)
@Table(schema="public", name="user_access_logs")
public class User_Access_Logs implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer userId;
	
	@Id
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	
	@Column(name="count")
	@NotNull
	private Integer count;
	
	@Column(name="last_accessed")
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastAccessed;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Date getLastAccessed() {
		return lastAccessed;
	}

	public void setLastAccessed(Date lastAccessed) {
		this.lastAccessed = lastAccessed;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}