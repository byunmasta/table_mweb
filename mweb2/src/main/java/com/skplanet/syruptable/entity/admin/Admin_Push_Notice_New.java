package com.skplanet.syruptable.entity.admin;

import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@Table(schema="public", name="admin_push_notice_new")
public class Admin_Push_Notice_New {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="admin_push_notice_new_id_seq")
	@SequenceGenerator(name="admin_push_notice_new_id_seq", sequenceName="admin_push_notice_new_id_seq")
	private Integer id;
	
	@Column(name="type")
	@NotNull
	private String type;
	
	@Column(name="message")
	@NotNull
	private String message;
	
	@Column(name="admin_user_id")
	@NotNull
	private Integer adminUserId;
	
	@Column(name="android_status")
	private String androidStatus;
	
	@Column(name="ios_status")
	private String iosStatus;
	
	@Column(name="send_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendat;
	
	@Column(name="total_count")
	private Integer totalCount;
	
	@Column(name="data")
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	private Map<String, String> data;
	
	@Column(name="attrs")
	@Type(type="com.skplanet.syruptable.type.HstoreUserType")
	private Map<String, String> attrs;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@Column(name="updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedat;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getAdminUserId() {
		return adminUserId;
	}

	public void setAdminUserId(Integer adminUserId) {
		this.adminUserId = adminUserId;
	}

	public String getAndroidStatus() {
		return androidStatus;
	}

	public void setAndroidStatus(String androidStatus) {
		this.androidStatus = androidStatus;
	}

	public String getIosStatus() {
		return iosStatus;
	}

	public void setIosStatus(String iosStatus) {
		this.iosStatus = iosStatus;
	}

	public Date getSendat() {
		return sendat;
	}

	public void setSendat(Date sendat) {
		this.sendat = sendat;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Map<String, String> getData() {
		return data;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}

	public Map<String, String> getAttrs() {
		return attrs;
	}

	public void setAttrs(Map<String, String> attrs) {
		this.attrs = attrs;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Date getUpdatedat() {
		return updatedat;
	}

	public void setUpdatedat(Date updatedat) {
		this.updatedat = updatedat;
	}
	
	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
		this.updatedat = this.createdat;
	}
	
	@PreUpdate
	protected void onUpdate() {
		this.updatedat = new Date();
	}
}