package com.skplanet.syruptable.entity.poi;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Poi_Ratings_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="user_id", nullable=false)
	private Integer userId;
	
	@Column(name="poi_id", nullable=false)
	private Integer poiId;
	
	public Poi_Ratings_PK() {
		
	}
	
	public Poi_Ratings_PK(Integer userId, Integer poiId) {
		this.userId = userId;
		this.poiId = poiId;
	}
}
