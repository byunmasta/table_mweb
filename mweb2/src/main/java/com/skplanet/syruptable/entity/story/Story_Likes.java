package com.skplanet.syruptable.entity.story;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.skplanet.syruptable.entity.user.Users;

@Entity
@IdClass(Story_Like_PK.class)
@Table(schema="public", name="story_likes")
public class Story_Likes {
	
	@Id
	private Integer storyId;
	
	@Id
	private Integer userId;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;
	
	@OneToOne(cascade=CascadeType.PERSIST, fetch=FetchType.LAZY, optional=true)
	@JoinColumn(name="user_id", referencedColumnName="id", insertable=false, updatable=false)
	private Users user;

	public Integer getStoryId() {
		return storyId;
	}

	public void setStoryId(Integer storyId) {
		this.storyId = storyId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}