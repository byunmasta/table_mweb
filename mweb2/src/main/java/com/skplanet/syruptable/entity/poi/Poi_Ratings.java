package com.skplanet.syruptable.entity.poi;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@IdClass(Poi_Ratings_PK.class)
@Table(schema="public", name="poi_ratings")
public class Poi_Ratings implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_id")
	private Integer userId;
	
	@Id
	@Column(name="poi_id")
	private Integer poiId;
	
	@Column(name="rating")
	private String rating;
	
	@Column(name="star_rating")
	@NotNull
	private Double starRating;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getPoiId() {
		return poiId;
	}

	public void setPoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public Double getStarRating() {
		return starRating;
	}

	public void setStarRating(Double starRating) {
		this.starRating = starRating;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}

	public void setCreated_at(Date createdat) {
		this.createdat = createdat;
	}
}
