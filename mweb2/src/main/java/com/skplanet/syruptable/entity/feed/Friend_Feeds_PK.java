package com.skplanet.syruptable.entity.feed;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Friend_Feeds_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="user_id", nullable=false)
	private Integer userId;
	
	@Column(name="feed_id", nullable=false)
	private Integer feedId;
	
	public Friend_Feeds_PK() {
		
	}
	
	public Friend_Feeds_PK(Integer userId, Integer feedId) {
		this.userId = userId;
		this.feedId = feedId;
	}
}
