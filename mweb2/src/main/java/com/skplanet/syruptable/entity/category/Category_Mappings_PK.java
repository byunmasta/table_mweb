package com.skplanet.syruptable.entity.category;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Category_Mappings_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="provider_id", nullable=false)
	private Integer providerId;
	
	@Column(name="code", nullable=false)
	private String code;
	
	public Category_Mappings_PK() {
		
	}
	
	public Category_Mappings_PK(Integer providerId, String code) {
		this.providerId = providerId;
		this.code = code;
	}
}
