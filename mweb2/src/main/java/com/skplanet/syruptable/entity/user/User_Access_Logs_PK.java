package com.skplanet.syruptable.entity.user;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class User_Access_Logs_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="user_id", nullable=false)
	private Integer userId;
	
	@Column(name="date", nullable=false)
	private Date date;
	
	public User_Access_Logs_PK() {
		
	}
	
	public User_Access_Logs_PK(Integer userId, Date date) {
		this.userId = userId;
		this.date = date;
	}
}
