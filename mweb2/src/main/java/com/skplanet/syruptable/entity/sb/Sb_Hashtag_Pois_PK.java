package com.skplanet.syruptable.entity.sb;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Sb_Hashtag_Pois_PK implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name="hashtag_id", nullable=false)
	private Integer hashtagId;
	
	@Column(name="poi_id", nullable=false)
	private Integer poiId;
	
	public Sb_Hashtag_Pois_PK() {
		
	}
	
	public Sb_Hashtag_Pois_PK(Integer hashtagId, Integer poiId) {
		this.hashtagId = hashtagId;
		this.poiId = poiId;
	}
}
