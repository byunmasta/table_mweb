package com.skplanet.syruptable.entity.user;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@IdClass(Syrup_Tokens_PK.class)
@Table(schema="public", name="syrup_tokens")
public class Syrup_Tokens implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	private Integer userId;
	
	@Id
	private String ci;
	
	@Id
	private String owpId;
	
	@Column(name="created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdat;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getCi() {
		return ci;
	}

	public void setCi(String ci) {
		this.ci = ci;
	}

	public String getOwpId() {
		return owpId;
	}

	public void setOwpId(String owpId) {
		this.owpId = owpId;
	}

	public Date getCreatedat() {
		return createdat;
	}

	public void setCreatedat(Date createdat) {
		this.createdat = createdat;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@PrePersist
	protected void onCreate() {
		this.createdat = new Date();
	}
}
