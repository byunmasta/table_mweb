package com.skplanet.syruptable.type;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.StringUtils;

public class HstoreHelper {
	
	private static final String S_SEPARATOR = "\", \"";
	private static final String K_V_SEPARATOR = "=>";

    public static String toString(Map<String, String> m) {
        if (m.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        int n = m.size();
        for (String key : m.keySet()) {
            sb.append("\"" + key + "\"" + K_V_SEPARATOR + "\"" + m.get(key) + "\"");
            if (n > 1) {
                sb.append(", ");
                n--;
            }
        }
        return sb.toString();
    }

    public static Map<String, String> toMap(String s) {
		Map<String, String> m = new HashMap<String, String>();
		if (!StringUtils.hasText(s)) {
			return m;
		}
		String[] tokens1 = s.split(S_SEPARATOR);

		// Original Code has the Bug
		/*
		String[] tokens = s.split(", ");
		for (String token : tokens) {
			System.out.println("token : " + token);
			String[] kv = token.split(K_V_SEPARATOR);
			String k = kv[0];
			System.out.println("kkkkkkkkkkkkkkk000000 : " + k);
			k = k.trim().substring(1, k.length() - 1);
			System.out.println("kkkkkkkkkkkkkkk111111 : " + k);
			String v = kv[1];
			v = v.trim().substring(1, v.length() - 1);
			m.put(k, v);
		}
		*/
		
		// New Code 
		for (String token : tokens1) {
			String[] kv = token.split(K_V_SEPARATOR);
			String k = kv[0].replaceAll("\"", "").trim();
			String v = kv[1].replaceAll("\"", "").trim();
			m.put(k, v);
		}
		
		return m;
    }

}
