package com.skplanet.syruptable.response.user;

public class UserProfile {
	
	private Integer id;
	
	private String nickname;

	private String description;
	
	private String profileImage;
	
	private String gender;
	
	private String badge;
	
	private Boolean isFollowing;
	
	private Integer alerts;
	
	private UserScoreboard scoreboard;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBadge() {
		return badge;
	}

	public void setBadge(String badge) {
		this.badge = badge;
	}

	

	public Integer getAlerts() {
		return alerts;
	}

	public void setAlerts(Integer alerts) {
		this.alerts = alerts;
	}

	public UserScoreboard getScoreboard() {
		return scoreboard;
	}


	public void setScoreboard(UserScoreboard scoreboard) {
		this.scoreboard = scoreboard;
	}

	
	public static final class UserScoreboard {
		
		private Integer followers;
		
		private Integer followees;

		public Integer getFollowers() {
			return followers;
		}

		public void setFollowers(Integer followers) {
			this.followers = followers;
		}

		public Integer getFollowees() {
			return followees;
		}

		public void setFollowees(Integer followees) {
			this.followees = followees;
		}

		@Override
		public String toString() {
			return String.format("UserScoreboard [followers=%s, followees=%s]",
					followers, followees);
		}

		
	}


	public Boolean getIsFollowing() {
		return isFollowing;
	}

	public void setIsFollowing(Boolean isFollowing) {
		this.isFollowing = isFollowing;
	}


	
	
	

}
