package com.skplanet.syruptable.response.pick;

import java.util.List;

import com.skplanet.syruptable.response.BaseTableAPI;


public class PickList extends BaseTableAPI {

	private List<Pick> picks;

	public List<Pick> getPicks() {
		return picks;
	}

	public void setPicks(List<Pick> picks) {
		this.picks = picks;
	}
}
