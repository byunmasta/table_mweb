package com.skplanet.syruptable.response;

import com.skplanet.syruptable.response.image.MainImage;


public class Hashtag {
	
	private Integer hashtagId;
	
	private String name;
	
	private MainImage mainImage;
	
	public Hashtag() {
		
	}
	
	public Hashtag(Integer hashtagId, String name) {
		this.hashtagId = hashtagId;
		this.name = name;
	}

	public Integer getHashtagId() {
		return hashtagId;
	}

	public void setHashtagId(Integer hashtagId) {
		this.hashtagId = hashtagId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MainImage getMainImage() {
		return mainImage;
	}

	public void setMainImage(MainImage mainImage) {
		this.mainImage = mainImage;
	}

	@Override
	public String toString() {
		return "Hashtag [hashtagId=" + hashtagId + ", name=" + name + ", mainImage=" + mainImage + "]";
	}
	
}
