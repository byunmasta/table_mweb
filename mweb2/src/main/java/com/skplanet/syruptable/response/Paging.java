package com.skplanet.syruptable.response;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Base64.Encoder;

public class Paging {
	
	private Encoder encoder = Base64.getEncoder().withoutPadding();

	private String prev = "";
	
	private String next = "";
	
	public void setPrev(String prev) {
		this.prev = prev;
	}

	public void setNext(String next) {
		this.next = next;
	}
	
	/**
	 * @return the next
	 */
	public String getPrev() {
		try {			
	        return encoder.encodeToString(this.prev.getBytes("UTF-8"));
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return prev;
	}
	
	public String getNext() {
		
		try {
			return encoder.encodeToString(this.next.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return next;
	}

	@Override
	public String toString() {
		return String.format("Paging [prev=%s, next=%s]", prev, next);
	}
}
