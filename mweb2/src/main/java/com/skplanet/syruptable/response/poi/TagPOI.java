package com.skplanet.syruptable.response.poi;

import java.util.Arrays;
import java.util.List;

public class TagPOI {
	
	private Boolean isAdsense = false;
	
	private Boolean isVerified = false;
	
	private Boolean isOrder = false;
	
	private Boolean isCoupon = false;
	
	/**
	 * Tab_pois 테이블의 결과 값, 오더, 쿠폰 
	 * @param tags Tab_pois
	 * @param order
	 * @param coupon
	 */
	public TagPOI(String[] tags, int order, int coupon) {
		
		if(tags != null) {
			List<String> tagList = Arrays.asList(tags);
			
			if(tagList.contains("ads")) {
				this.isAdsense = true;
			}
			
			if(tagList.contains("verified")) {
				this.isVerified = true;
			}
		
		}
		
		if(order > 0) {
			this.isOrder = true;
		}
		
		if (coupon > 0) {
			this.isCoupon = true;
		}
		
	}



	public Boolean getIsAdsense() {
		return isAdsense;
	}



	public Boolean getIsVerified() {
		return isVerified;
	}



	public Boolean getIsOrder() {
		return isOrder;
	}



	public Boolean getIsCoupon() {
		return isCoupon;
	}



	@Override
	public String toString() {
		return String
				.format("TagPOIs [isAdsense=%s, isVerified=%s, isOrder=%s, isCoupon=%s]",
						isAdsense, isVerified, isOrder, isCoupon);
	}
	
}
