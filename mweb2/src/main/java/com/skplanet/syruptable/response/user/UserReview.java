package com.skplanet.syruptable.response.user;

import java.util.Date;
import java.util.List;

import com.skplanet.syruptable.response.BaseTableAPI;
import com.skplanet.syruptable.response.Hashtag;
import com.skplanet.syruptable.response.image.Image;


public class UserReview extends BaseTableAPI {

	private List<Feed> feeds;

	public List<Feed> getFeeds() {
		return feeds;
	}

	public void setFeeds(List<Feed> feeds) {
		this.feeds = feeds;
	}

	public static final class Feed {

		private int type;

		private Integer feedId;

		private PickPOI poi;

		public int getType() {
			return type;
		}

		public void setType(int type) {
			this.type = type;
		}

		public Integer getFeedId() {
			return feedId;
		}

		public void setFeedId(Integer feedId) {
			this.feedId = feedId;
		}

		public PickPOI getPoi() {
			return poi;
		}

		public void setPoi(PickPOI poi) {
			this.poi = poi;
		}

		public static final class PickPOI {

			private Integer poiId;

			private String name;

			private String abbrAddress;

			private Pick pick;
			
			private Comment comment;
			
			public Comment getComment() {
				return comment;
			}

			public void setComment(Comment comment) {
				this.comment = comment;
			}

			public Integer getPoiId() {
				return poiId;
			}

			public void setPoiId(Integer poiId) {
				this.poiId = poiId;
			}

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			public String getAbbrAddress() {
				return abbrAddress;
			}

			public void setAbbrAddress(String abbrAddress) {
				this.abbrAddress = abbrAddress;
			}

			public Pick getPick() {
				return pick;
			}

			public void setPick(Pick pick) {
				this.pick = pick;
			}

		}

		public static final class Comment {

			private Date createdAt;

			private String contents;

			private User editor;
			
			public User getEditor() {
				return editor;
			}

			public void setEditor(User editor) {
				this.editor = editor;
			}

			public Date getCreatedAt() {
				return createdAt;
			}

			public void setCreatedAt(Date createdAt) {
				this.createdAt = createdAt;
			}

			public String getContents() {
				return contents;
			}

			public void setContents(String contents) {
				this.contents = contents;
			}

			
		}

	}

	public static final class Pick {

		private Date createdAt;
		
		private int pickId;

		private User editor;

		private PickScoreboard scoreboard;

		private String contents;

		private List<Image> images;

		private List<Hashtag> hashtags;
		
		public int getPickId() {
			return pickId;
		}

		public void setPickId(int pickId) {
			this.pickId = pickId;
		}

		public List<Hashtag> getHashtags() {
			return hashtags;
		}

		public void setHashtags(List<Hashtag> hashtags) {
			this.hashtags = hashtags;
		}

		public Date getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(Date createdAt) {
			this.createdAt = createdAt;
		}

		public User getEditor() {
			return editor;
		}

		public void setEditor(User editor) {
			this.editor = editor;
		}

		public PickScoreboard getScoreboard() {
			return scoreboard;
		}

		public void setScoreboard(PickScoreboard scoreboard) {
			this.scoreboard = scoreboard;
		}

		public String getContents() {
			return contents;
		}

		public void setContents(String contents) {
			this.contents = contents;
		}

		public List<Image> getImages() {
			return images;
		}

		public void setImages(List<Image> images) {
			this.images = images;
		}
		
		public static final class PickScoreboard {

			private int likes;

			private int comments;

			private int images;

			private Double rating = 0d;

			public int getLikes() {
				return likes;
			}

			public void setLikes(int likes) {
				this.likes = likes;
			}

			public int getComments() {
				return comments;
			}

			public void setComments(int comments) {
				this.comments = comments;
			}

			public int getImages() {
				return images;
			}

			public void setImages(int images) {
				this.images = images;
			}

			public Double getRating() {
				return rating;
			}

			public void setRating(Double rating) {
				this.rating = rating;
			}

			@Override
			public String toString() {
				return String
						.format("PickScoreboard [likes=%s, comments=%s, images=%s, rating=%s]",
								likes, comments, images, rating);
			}

		}

	}

	@Override
	public String toString() {
		return String.format(
				"UserPick [feeds=%s, getTotalCount()=%s, getPaging()=%s]",
				feeds, getTotalCount(), getPaging());
	}

}
