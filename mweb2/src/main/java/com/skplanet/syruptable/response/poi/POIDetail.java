package com.skplanet.syruptable.response.poi;

import java.util.Date;
import java.util.List;

import com.skplanet.syruptable.response.Hashtag;
import com.skplanet.syruptable.response.image.MainImage;
import com.skplanet.syruptable.response.pick.Pick;
import com.skplanet.syruptable.response.user.User;

/**
 * picks 리뷰
 * BlogReview 블로그 리뷰
 * POI Poi 정보 - 쿠폰
 * SuggestionPOI 지역 추천
 * VerifiedPOI 인증 맛집
 * @author mylostland@sk.com
 *
 */
public class POIDetail {
	
	private Integer pickCount;
	
	private List<Pick> picks;
	
	private Integer wishCount;
	
	private List<User> wishes; 
	
	private Integer blogReviewCount;
	
	private List<BlogReview> blogReviews; 
	
	private POI poi;
	
	private My my;
	
	private List<SuggestionPOI> suggestionPOI;
	
	private String verifiedAoiName;
	
	private List<VerifiedPOI> verifiedPOI;
	
	public String getVerifiedAoiName() {
		return verifiedAoiName;
	}

	public void setVerifiedAoiName(String verifiedAoiName) {
		this.verifiedAoiName = verifiedAoiName;
	}

	public Integer getBlogReviewCount() {
		return blogReviewCount;
	}

	public void setBlogReviewCount(Integer blogReviewCount) {
		this.blogReviewCount = blogReviewCount;
	}

	public Integer getWishCount() {
		return wishCount;
	}

	public void setWishCount(Integer wishCount) {
		this.wishCount = wishCount;
	}

	public List<User> getWishes() {
		return wishes;
	}

	public void setWishes(List<User> wishes) {
		this.wishes = wishes;
	}

	public List<SuggestionPOI> getSuggestionPOI() {
		return suggestionPOI;
	}

	public void setSuggestionPOI(List<SuggestionPOI> suggestionPOI) {
		this.suggestionPOI = suggestionPOI;
	}

	public List<VerifiedPOI> getVerifiedPOI() {
		return verifiedPOI;
	}

	public void setVerifiedPOI(List<VerifiedPOI> verifiedPOI) {
		this.verifiedPOI = verifiedPOI;
	}

	public Integer getPickCount() {
		return pickCount;
	}

	public void setPickCount(Integer pickCount) {
		this.pickCount = pickCount;
	}

	public List<Pick> getPicks() {
		return picks;
	}

	public void setPicks(List<Pick> picks) {
		this.picks = picks;
	}

	public List<BlogReview> getBlogReviews() {
		return blogReviews;
	}

	public void setBlogReviews(List<BlogReview> blogReviews) {
		this.blogReviews = blogReviews;
	}

	public POI getPoi() {
		return poi;
	}

	public void setPoi(POI poi) {
		this.poi = poi;
	}
	
	public My getMy() {
		return my;
	}

	public void setMy(My my) {
		this.my = my;
	}
	
	public static final class My {
		
		private Double rating;
		
		private Boolean isWished;

		public Double getRating() {
			return rating;
		}

		public void setRating(Double rating) {
			this.rating = rating;
		}

		public Boolean getIsWished() {
			return isWished;
		}

		public void setIsWished(Boolean isWished) {
			this.isWished = isWished;
		}
	}

	public static final class BlogReview {
		
		private Integer blogId;
		
		private String originUrl;
		
		private String title;
		
		private String author;
		
		private String froms;
		
		private String image;
		
		private String summary;
		
		private Date createdAt;

		public Integer getBlogId() {
			return blogId;
		}

		public void setBlogId(Integer blogId) {
			this.blogId = blogId;
		}

		public String getOriginUrl() {
			return originUrl;
		}

		public void setOriginUrl(String originUrl) {
			this.originUrl = originUrl;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getAuthor() {
			return author;
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		public String getFroms() {
			return froms;
		}

		public void setFroms(String froms) {
			this.froms = froms;
		}

		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}

		public String getSummary() {
			return summary;
		}

		public void setSummary(String summary) {
			this.summary = summary;
		}

		public Date getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(Date createdAt) {
			this.createdAt = createdAt;
		}
	}
	
	public static final class SuggestionPOI {
		
		private Integer rater;
		
		private Double rating;
		
		private Integer reviewCount;
		
		private Integer poiId;
		
		private String name;
		
		private MainImage mainImage;
		
		private Integer imageCount;
		
		private List<Hashtag> hashtags;
		
		private TagPOI tagPOI;

		public Integer getImageCount() {
			return imageCount;
		}

		public void setImageCount(Integer imageCount) {
			this.imageCount = imageCount;
		}

		public Integer getRater() {
			return rater;
		}

		public void setRater(Integer rater) {
			this.rater = rater;
		}

		public Double getRating() {
			return rating;
		}

		public void setRating(Double rating) {
			this.rating = rating;
		}

		public Integer getReviewCount() {
			return reviewCount;
		}

		public void setReviewCount(Integer reviewCount) {
			this.reviewCount = reviewCount;
		}

		public Integer getPoiId() {
			return poiId;
		}

		public void setPoiId(Integer poiId) {
			this.poiId = poiId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public MainImage getMainImage() {
			return mainImage;
		}

		public void setMainImage(MainImage mainImage) {
			this.mainImage = mainImage;
		}

		public List<Hashtag> getHashtags() {
			return hashtags;
		}

		public void setHashtags(List<Hashtag> hashtags) {
			this.hashtags = hashtags;
		}

		public TagPOI getTagPOI() {
			return tagPOI;
		}

		public void setTagPOI(TagPOI tagPOI) {
			this.tagPOI = tagPOI;
		}
	}
	
	public static final class VerifiedPOI {
		
		private Integer rater;
		
		private Double rating;
		
		private Integer reviewCount;
		
		private Integer poiId;
		
		private String name;
		
		private String address;
		
		private MainImage mainImage;
		
		public Integer getRater() {
			return rater;
		}

		public void setRater(Integer rater) {
			this.rater = rater;
		}

		public Double getRating() {
			return rating;
		}

		public void setRating(Double rating) {
			this.rating = rating;
		}

		public Integer getReviewCount() {
			return reviewCount;
		}

		public void setReviewCount(Integer reviewCount) {
			this.reviewCount = reviewCount;
		}

		public Integer getPoiId() {
			return poiId;
		}

		public void setPoiId(Integer poiId) {
			this.poiId = poiId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public MainImage getMainImage() {
			return mainImage;
		}

		public void setMainImage(MainImage mainImage) {
			this.mainImage = mainImage;
		}
	}
}