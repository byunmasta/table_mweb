package com.skplanet.syruptable.response.image;

import java.util.List;

import com.skplanet.syruptable.response.BaseTableAPI;

public class Images extends BaseTableAPI{
	
	private List<Image> images;

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}
}
