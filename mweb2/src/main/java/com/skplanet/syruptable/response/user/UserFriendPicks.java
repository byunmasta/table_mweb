package com.skplanet.syruptable.response.user;

import java.util.Date;
import java.util.List;

import com.skplanet.syruptable.response.BaseTableAPI;
import com.skplanet.syruptable.response.Hashtag;
import com.skplanet.syruptable.response.image.Image;

public class UserFriendPicks extends BaseTableAPI {
	
	private List<FriendsFeed> feeds;
	
	public List<FriendsFeed> getFeeds() {
		return feeds;
	}

	public void setFeeds(List<FriendsFeed> feeds) {
		this.feeds = feeds;
	}

	public static final class FriendsFeed {
		
		private int type;
		
		private Integer feedId;
		
		private Pick pick;
		
		
		public int getType() {
			return type;
		}

		public void setType(int type) {
			this.type = type;
		}

		public Integer getFeedId() {
			return feedId;
		}

		public void setFeedId(Integer feedId) {
			this.feedId = feedId;
		}

		public Pick getPick() {
			return pick;
		}

		public void setPick(Pick pick) {
			this.pick = pick;
		}

		
		public static final class Pick {
			
			private Date createdAt;
			
			private Integer poiId;
			
			private String name;
			
			private String abbrAddress;
			
			private User editor;
			
			private PickScoreboard scoreboard;
			
			private String contents;
			
			private List<Hashtag> hashtags;
			
			public List<Hashtag> getHashtags() {
				return hashtags;
			}

			public void setHashtags(List<Hashtag> hashtags) {
				this.hashtags = hashtags;
			}


			private List<Image> image;
			
			
			public Integer getPoiId() {
				return poiId;
			}


			public void setPoiId(Integer poiId) {
				this.poiId = poiId;
			}

			public Date getCreatedAt() {
				return createdAt;
			}

			public void setCreatedAt(Date createdAt) {
				this.createdAt = createdAt;
			}

			public String getName() {
				return name;
			}


			public void setName(String name) {
				this.name = name;
			}


			public String getAbbrAddress() {
				return abbrAddress;
			}


			public void setAbbrAddress(String abbrAddress) {
				this.abbrAddress = abbrAddress;
			}


			public User getEditor() {
				return editor;
			}


			public void setEditor(User editor) {
				this.editor = editor;
			}


			public PickScoreboard getScoreboard() {
				return scoreboard;
			}


			public void setScoreboard(PickScoreboard scoreboard) {
				this.scoreboard = scoreboard;
			}


			public String getContents() {
				return contents;
			}


			public void setContents(String contents) {
				this.contents = contents;
			}


			public List<Image> getImage() {
				return image;
			}


			public void setImage(List<Image> image) {
				this.image = image;
			}


			
			
			public static final class PickScoreboard {
				
				private int likes;
				
				private int comments;
				
				private int images;
				
				private Double rating = 0d;

				public int getLikes() {
					return likes;
				}

				public void setLikes(int likes) {
					this.likes = likes;
				}

				public int getComments() {
					return comments;
				}

				public void setComments(int comments) {
					this.comments = comments;
				}
				
				public int getImages() {
					return images;
				}

				public void setImages(int images) {
					this.images = images;
				}

				public Double getRating() {
					return rating;
				}

				public void setRating(Double rating) {
					this.rating = rating;
				}

				@Override
				public String toString() {
					return String
							.format("PickScoreboard [likes=%s, comments=%s, images=%s, rating=%s]",
									likes, comments, images, rating);
				}
				
			}


			@Override
			public String toString() {
				return String
						.format("Pick [createdat=%s, poiId=%s, name=%s, abbrAddress=%s, editor=%s, scoreboard=%s, contents=%s, image=%s]",
								createdAt, poiId, name, abbrAddress, editor,
								scoreboard, contents, image);
			}
			
			
		}

		@Override
		public String toString() {
			return String.format("Feed [type=%s, feedId=%s, pick=%s]", type,
					feedId, pick);
		}

	
		
		
	}

	@Override
	public String toString() {
		return String.format(
				"UserPick [feeds=%s, getTotalCount()=%s, getPaging()=%s]",
				feeds, getTotalCount(), getPaging());
	}
	

}
