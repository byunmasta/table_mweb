package com.skplanet.syruptable.response.pick;

import java.util.Date;

import com.skplanet.syruptable.response.user.User;

public class Comment {
	
	private String contents;
	
	private User editor;
	
	private Date createdAt;

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public User getEditor() {
		return editor;
	}

	public void setEditor(User editor) {
		this.editor = editor;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
}
