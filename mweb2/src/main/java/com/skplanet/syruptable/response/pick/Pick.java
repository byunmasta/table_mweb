package com.skplanet.syruptable.response.pick;

import java.util.List;

import com.skplanet.syruptable.response.Hashtag;
import com.skplanet.syruptable.response.image.Image;
import com.skplanet.syruptable.response.poi.POI;
import com.skplanet.syruptable.response.user.User;


public class Pick extends BasePick {

	private POI poi;
	
	private List<Hashtag> hashtags;
	
	private List<Image> images;
	
	private List<Comment> comments;
	
	private List<User> wishes;

	public POI getPoi() {
		return poi;
	}

	public void setPoi(POI poi) {
		this.poi = poi;
	}

	public List<Hashtag> getHashtags() {
		return hashtags;
	}

	public void setHashtags(List<Hashtag> hashtags) {
		this.hashtags = hashtags;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<User> getWishes() {
		return wishes;
	}

	public void setWishes(List<User> wishes) {
		this.wishes = wishes;
	}
}
