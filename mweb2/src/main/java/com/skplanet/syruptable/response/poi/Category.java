package com.skplanet.syruptable.response.poi;


public class Category {
	
	private String categoryId;
	
	private String type;
	
	private String name;
	
	public Category() {
	}
	
	public Category(String categoryId, String type, String name) {
		this.categoryId = categoryId;
		this.type = type;
		this.name = name;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("Category [categoryId=%s, type=%s, name=%s]",
				categoryId, type, name);
	}
}