package com.skplanet.syruptable.response;

public class AOI extends BaseAOI {

	public AOI() {
		
	}

	private Center center;
	
	private Polygon polygon;

	public Center getCenter() {
		return center;
	}

	public void setCenter(Center center) {
		this.center = center;
	}

	public Polygon getPolygon() {
		return polygon;
	}

	public void setPolygon(Polygon polygon) {
		this.polygon = polygon;
	}

}
