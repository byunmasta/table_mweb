package com.skplanet.syruptable.response;

public class ViewSuggestion {
	
	public String category_id;
	
	public int keyword_id;
	
	public String name;
	
	public int coupon;
	
	public String sort;
	
	public int order;

	@Override
	public String toString() {
		return String
				.format("ViewSuggestion [category_id=%s, keyword_id=%s, name=%s, coupon=%s, sort=%s, order=%s]",
						category_id, keyword_id, name, coupon, sort, order);
	}

	
	
	

}
