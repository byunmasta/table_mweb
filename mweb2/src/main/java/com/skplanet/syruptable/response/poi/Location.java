package com.skplanet.syruptable.response.poi;

import java.util.Arrays;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

public class Location {
	
	private String type;
	
	private double[] coordinates;
	
	
	public Location(String type, double[] coordinates) {
		this.type = type;
		this.coordinates = coordinates;
	}
	
	public Location(final Geometry geometry) {
		Point point = (Point) geometry;
		this.type = point.getGeometryType();
		
		double[] coordinate = {point.getX(), point.getY()};
		this.coordinates =  coordinate;
		
	}
	

	public void setType(String type) {
		this.type = type;
	}

	public void setCoordinates(double[] coordinates) {
		this.coordinates = coordinates;
	}

	public String getType() {
		return type;
	}

	public double[] getCoordinates() {
		return coordinates;
	}

	@Override
	public String toString() {
		return String.format("Location [type=%s, coordinates=%s]", type,
				Arrays.toString(coordinates));
	}
	
}
