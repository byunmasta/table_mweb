package com.skplanet.syruptable.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.skplanet.syruptable.entity.aoi.Aois;

public class BaseMain extends BaseTableAPI {
	
	private AOI aoi;
	
	private TodayLunch[] todayLunch;
	
	private Banner banner;
	
	private Suggestion[] suggestion;
	

	public AOI getAoi() {
		return aoi;
	}

	public void setAoi(Aois aois) {
		this.aoi = new AOI();
		aoi.setAddress(aois.getName());
		aoi.setAoiId(aois.getId());
		aoi.setType(aois.getCategory().name());
	}


	public TodayLunch[] getTodayLunch() {
		return todayLunch;
	}


	public void setTodayLunch(TodayLunch[] todayLunch) {
		this.todayLunch = todayLunch;
	}


	public Banner getBanner() {
		return banner;
	}


	public void setBanner(Banner banner) {
		this.banner = banner;
	}


	public Suggestion[] getSuggestions() {
		return suggestion;
	}


	public void setSuggestion(Suggestion[] suggestion) {
		this.suggestion = suggestion;
	}


	public static final class AOI {
		
		private String address;
		
		private String type;
		
		private int aoiId;

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public int getAoiId() {
			return aoiId;
		}

		public void setAoiId(int aoiId) {
			this.aoiId = aoiId;
		}

		@Override
		public String toString() {
			return String.format("AOI [address=%s, type=%s, aoiId=%s]",
					address, type, aoiId);
		}
		
	}
	
	
	public static final class TodayLunch {
		
		private String description;

		private String menuTitle;
		
		private LunchPOI[] lunchPOI;
		
		private String backgroundImage;
		
		private String menuImage;
		
		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getMenuTitle() {
			return menuTitle;
		}



		public void setMenuTitle(String menuTitle) {
			this.menuTitle = menuTitle;
		}



		public LunchPOI[] getLunchPOI() {
			return lunchPOI;
		}



		public void setLunchPOI(LunchPOI[] lunchPOI) {
			this.lunchPOI = lunchPOI;
		}

		public String getBackgroundImage() {
			return backgroundImage;
		}

		public void setBackgroundImage(String backgroundImage) {
			this.backgroundImage = backgroundImage;
		}

		public String getMenuImage() {
			return menuImage;
		}

		public void setMenuImage(String menuImage) {
			this.menuImage = menuImage;
		}




		public static final class LunchPOI {
			
			
			
			private int poiId;
			
			private String name;
			
			public LunchPOI(int poiId, String name) {
				this.poiId = poiId;
				this.name = name;
			}

			public int getpoiId() {
				return poiId;
			}

			public void setpoiId(int poiId) {
				this.poiId = poiId;
			}

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			@Override
			public String toString() {
				return String.format("LunchPOI [poiId=%s, name=%s]", poiId,
						name);
			}
			
			
			
		}



		
		
	}
	
	

	/**
	 * 메뉴       :  최근인기, 랭킹 
	 * 쿠폰       :  ? , 
	 * 오더       : 바로 주문 
	 * 지역 키워드 : 키워드 or 카테고리 어성
	 * @author webmadeup
	 *
	 */
	public static final class Suggestion {
		
		private String name;  // 최근인기, 랭킹, 쿠폰, 바로주문, 바삭치킨, 디저트/카페 
		
		private String sort = "";
		
		private int isCoupon;
		
		private int order;
		
		@JsonProperty("keyword_id")
		private int keywordId;
		
		@JsonProperty("category_id")
		private String categoryId = "";

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getSort() {
			return sort;
		}

		public void setSort(String sort) {
			this.sort = sort;
		}

		public int getIsCoupon() {
			return isCoupon;
		}

		public void setIsCoupon(int isCoupon) {
			this.isCoupon = isCoupon;
		}

		public int getOrder() {
			return order;
		}

		public void setOrder(int order) {
			this.order = order;
		}

		public int getKeywordId() {
			return keywordId;
		}

		public void setKeywordId(int keywordId) {
			this.keywordId = keywordId;
		}

		public String getCategoryId() {
			return categoryId;
		}

		public void setCategoryId(String categoryId) {
			this.categoryId = categoryId;
		}

		@Override
		public String toString() {
			return String
					.format("Suggestion [name=%s, sort=%s, isCoupon=%s, order=%s, keywordId=%s, categoryId=%s]",
							name, sort, isCoupon, order, keywordId, categoryId);
		}
		
		
		
	}

}
