package com.skplanet.syruptable.response.user;

import java.util.Date;
import java.util.List;

import com.skplanet.syruptable.response.BaseTableAPI;
import com.skplanet.syruptable.response.Hashtag;
import com.skplanet.syruptable.response.image.Image;

public class UserPicks extends BaseTableAPI {
	
	
	private List<Feed> feeds;
	
	public List<Feed> getFeeds() {
		return feeds;
	}

	public void setFeeds(List<Feed> feeds) {
		this.feeds = feeds;
	}

	public static final class Feed {
		
		private int type;
		
		private Integer feedId;
		
		private Pick pick;
		
		private Comment comment;
		
		
		public int getType() {
			return type;
		}

		public void setType(int type) {
			this.type = type;
		}

		public Integer getFeedId() {
			return feedId;
		}

		public void setFeedId(Integer feedId) {
			this.feedId = feedId;
		}

		public Pick getPick() {
			return pick;
		}

		public void setPick(Pick pick) {
			this.pick = pick;
		}

		public Comment getComment() {
			return comment;
		}

		public void setComment(Comment comment) {
			this.comment = comment;
		}

		public static final class Comment {
			
			private Date createdAt;
			
			private CommentPoi poi;
			
			private Integer pickId;
			
			private PickUser editor;
			
			private String contents;
			
			public CommentPoi getPoi() {
				return poi;
			}

			public void setPoi(CommentPoi poi) {
				this.poi = poi;
			}

			public Date getCreatedAt() {
				return createdAt;
			}

			public void setCreatedAt(Date createdAt) {
				this.createdAt = createdAt;
			}


			public Integer getPickId() {
				return pickId;
			}



			public void setPickId(Integer pickId) {
				this.pickId = pickId;
			}


			public PickUser getEditor() {
				return editor;
			}



			public void setEditor(PickUser editor) {
				this.editor = editor;
			}



			public String getContents() {
				return contents;
			}



			public void setContents(String contents) {
				this.contents = contents;
			}

			
			public static final class CommentPoi {
				
				private Integer poiId;
				
				private String name;
				
				private String abbrAddress;
				
				private CommentUser editor;
				

				public CommentUser getEditor() {
					return editor;
				}

				public void setEditor(CommentUser editor) {
					this.editor = editor;
				}

				public Integer getPoiId() {
					return poiId;
				}

				public void setPoiId(Integer poiId) {
					this.poiId = poiId;
				}

				public String getName() {
					return name;
				}

				public void setName(String name) {
					this.name = name;
				}

				public String getAbbrAddress() {
					return abbrAddress;
				}

				public void setAbbrAddress(String abbrAddress) {
					this.abbrAddress = abbrAddress;
				}
				
				
				public static final class CommentUser {
					
					private int userId;
					
					private String gender;
					
					private String profileImage;
					
					private String nickname;

					public int getUserId() {
						return userId;
					}

					public void setUserId(int userId) {
						this.userId = userId;
					}

					public String getGender() {
						return gender;
					}

					public void setGender(String gender) {
						this.gender = gender;
					}

					public String getProfileImage() {
						return profileImage;
					}

					public void setProfileImage(String profileImage) {
						this.profileImage = profileImage;
					}


					public String getNickname() {
						return nickname;
					}

					public void setNickname(String nickname) {
						this.nickname = nickname;
					}
					
				}
				
				
			}


			public static final class PickUser {
				
				private int userId;
				
				private String description;
				
				private String badge;
				
				private String gender;
				
				private String profileImage;
				
				private Boolean isFollowing = false;
				
				private String nickname;

				public int getUserId() {
					return userId;
				}

				public void setUserId(int userId) {
					this.userId = userId;
				}

				public String getDescription() {
					return description;
				}

				public void setDescription(String description) {
					this.description = description;
				}

				public String getBadge() {
					return badge;
				}

				public void setBadge(String badge) {
					this.badge = badge;
				}

				public String getGender() {
					return gender;
				}

				public void setGender(String gender) {
					this.gender = gender;
				}

				public String getProfileImage() {
					return profileImage;
				}

				public void setProfileImage(String profileImage) {
					this.profileImage = profileImage;
				}

				public Boolean getIsFollowing() {
					return isFollowing;
				}

				public void setIsFollowing(Boolean isFollowing) {
					this.isFollowing = isFollowing;
				}

				public String getNickname() {
					return nickname;
				}

				public void setNickname(String nickname) {
					this.nickname = nickname;
				}

				@Override
				public String toString() {
					return String
							.format("User [userId=%s, description=%s, badge=%s, gender=%s, profileImage=%s, isFollowing=%s, nickname=%s]",
									userId, description, badge, gender,
									profileImage, isFollowing, nickname);
				}
				
			}



		
			
			
			
		}
		
		public static final class Pick {
			
			private Date createdAt;
			
			private PickPoi poi;
			
			private User editor;
			
			private PickScoreboard scoreboard;
			
			private String contents;
			
			public PickPoi getPoi() {
				return poi;
			}

			public void setPoi(PickPoi poi) {
				this.poi = poi;
			}


			private List<Hashtag> hashtags;
			
			public List<Hashtag> getHashtags() {
				return hashtags;
			}

			public void setHashtags(List<Hashtag> hashtags) {
				this.hashtags = hashtags;
			}


			private List<Image> image;

			
			public Date getCreatedAt() {
				return createdAt;
			}

			public void setCreatedAt(Date createdAt) {
				this.createdAt = createdAt;
			}

			


			public User getEditor() {
				return editor;
			}


			public void setEditor(User editor) {
				this.editor = editor;
			}


			public PickScoreboard getScoreboard() {
				return scoreboard;
			}


			public void setScoreboard(PickScoreboard scoreboard) {
				this.scoreboard = scoreboard;
			}


			public String getContents() {
				return contents;
			}


			public void setContents(String contents) {
				this.contents = contents;
			}


			public List<Image> getImage() {
				return image;
			}


			public void setImage(List<Image> image) {
				this.image = image;
			}
			
			public static final class PickPoi {
				
				
				private Integer poiId;
				
				private String name;
				
				private String abbrAddress;
				
				public String getName() {
					return name;
				}


				public void setName(String name) {
					this.name = name;
				}


				public String getAbbrAddress() {
					return abbrAddress;
				}


				public void setAbbrAddress(String abbrAddress) {
					this.abbrAddress = abbrAddress;
				}
				
				public Integer getPoiId() {
					return poiId;
				}


				public void setPoiId(Integer poiId) {
					this.poiId = poiId;
				}
				
			}


			
			
			public static final class PickScoreboard {
				
				private int likes;
				
				private int comments;
				
				private int images;
				
				private Double rating = 0d;

				public int getLikes() {
					return likes;
				}

				public void setLikes(int likes) {
					this.likes = likes;
				}

				public int getComments() {
					return comments;
				}

				public void setComments(int comments) {
					this.comments = comments;
				}
				
				public int getImages() {
					return images;
				}

				public void setImages(int images) {
					this.images = images;
				}

				public Double getRating() {
					return rating;
				}

				public void setRating(Double rating) {
					this.rating = rating;
				}

				@Override
				public String toString() {
					return String
							.format("PickScoreboard [likes=%s, comments=%s, images=%s, rating=%s]",
									likes, comments, images, rating);
				}
				
			}


			
			
			
		}

		@Override
		public String toString() {
			return String.format(
					"Feed [type=%s, feedId=%s, review=%s, comment=%s]", type,
					feedId, pick, comment);
		}

		
		
		
		
	}

	@Override
	public String toString() {
		return String.format(
				"UserPick [feeds=%s, getTotalCount()=%s, getPaging()=%s]",
				feeds, getTotalCount(), getPaging());
	}
	
	
	
	
	

	

}
