package com.skplanet.syruptable.response.image;

import com.skplanet.syruptable.response.pick.PickScoreboard;

public class Image {
	
	
		private String image;
		
		private String type;
		
		private PickScoreboard scoreboard;
		
		public Image(String image, String type) {
			
			this.image = image;
			this.type = type;
		}
		
		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public PickScoreboard getScoreboard() {
			return scoreboard;
		}

		public void setScoreboard(PickScoreboard scoreboard) {
			this.scoreboard = scoreboard;
		}
}
