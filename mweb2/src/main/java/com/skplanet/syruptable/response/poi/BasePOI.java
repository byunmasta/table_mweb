package com.skplanet.syruptable.response.poi;

import java.util.List;

import com.skplanet.syruptable.response.BaseAOI;
import com.skplanet.syruptable.response.Hashtag;
import com.skplanet.syruptable.response.image.MainImage;

public class BasePOI {
	
	private Category category;
	
	private Integer poiId;
	
	private String name;
	
	private Location location;
	
	private String phone;
	
	private BaseAOI aoi;
	
	private MainImage mainImage;
	
	private List<Hashtag> hashtags;
	
	private POIScoreboard scoreboard;
	
	private String addressNo;
	
	private String abbrAddress;
	
	private TagPOI tagPOI;

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Integer getPoiId() {
		return poiId;
	}

	public void setPoiId(Integer poiId) {
		this.poiId = poiId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public BaseAOI getAoi() {
		return aoi;
	}

	public void setAoi(BaseAOI aoi) {
		this.aoi = aoi;
	}

	public MainImage getMainImage() {
		return mainImage;
	}

	public void setMainImage(MainImage mainImage) {
		this.mainImage = mainImage;
	}

	public List<Hashtag> getHashtags() {
		return hashtags;
	}

	public void setHashtags(List<Hashtag> hashtags) {
		this.hashtags = hashtags;
	}

	public POIScoreboard getScoreboard() {
		return scoreboard;
	}

	public void setScoreboard(POIScoreboard scoreboard) {
		this.scoreboard = scoreboard;
	}

	public String getAddressNo() {
		return addressNo;
	}

	public void setAddressNo(String addressNo) {
		this.addressNo = addressNo;
	}

	public String getAbbrAddress() {
		return abbrAddress;
	}

	public void setAbbrAddress(String abbrAddress) {
		this.abbrAddress = abbrAddress;
	}

	public TagPOI getTagPOI() {
		return tagPOI;
	}

	public void setTagPOI(TagPOI tagPOI) {
		this.tagPOI = tagPOI;
	}

	@Override
	public String toString() {
		return "BasePOI [category=" + category + ", poiId=" + poiId + ", name=" + name + ", location=" + location
				+ ", phone=" + phone + ", aoi=" + aoi + ", mainImage=" + mainImage + ", hashtags=" + hashtags
				+ ", scoreboard=" + scoreboard + ", addressNo=" + addressNo + ", abbrAddress=" + abbrAddress
				+ ", tagPOI=" + tagPOI + "]";
	}
	
}