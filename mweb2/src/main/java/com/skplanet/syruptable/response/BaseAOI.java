package com.skplanet.syruptable.response;

public class BaseAOI {
	
	private String type;
	
	private String name;
	
	private Integer aoiId;
	
	public BaseAOI() {
		
	}
	
	public BaseAOI(String name, Integer aoiId, String type) {
		this.name = name;
		this.aoiId = aoiId;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAoiId() {
		return aoiId;
	}

	public void setAoiId(Integer aoiId) {
		this.aoiId = aoiId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "BaseAOI [type=" + type + ", name=" + name + ", aoiId=" + aoiId + "]";
	}

}
