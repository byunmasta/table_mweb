package com.skplanet.syruptable.response;


public class Banner {
	
	private String url;
	
	private int type;
	
	private String image;
	
	private String link;
	
	private String createdAt;

	public int getType() {
		return type;
	}

	public String getImage() {
		return image;
	}

	public String getLink() {
		return link;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public String getUrl() {
		Banners banner = new Banners();
		return banner.getLinkURL(this.type, this.link);
	}


	@Override
	public String toString() {
		return String.format(
				"Banner [url=%s, type=%s, image=%s, link=%s, createdAt=%s]",
				url, type, image, link, createdAt);
	}
}
