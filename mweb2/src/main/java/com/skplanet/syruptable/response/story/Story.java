package com.skplanet.syruptable.response.story;

import com.google.gson.annotations.SerializedName;
import com.skplanet.syruptable.response.image.Image;
import com.skplanet.syruptable.response.user.User;

public class Story {
	
	private Integer storyId;
	
	private String title;

	private String description;
	
	private Image image;
	
	private Integer likeCount;

	private Integer replyCount;
	
	private Integer viewCount;
	
	private Integer poiCount;
	
	private User user;

	public Integer getStoryId() {
		return storyId;
	}

	public void setStoryId(Integer storyId) {
		this.storyId = storyId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public Integer getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(Integer likeCount) {
		this.likeCount = likeCount;
	}

	public Integer getReplyCount() {
		return replyCount;
	}

	public void setReplyCount(Integer replyCount) {
		this.replyCount = replyCount;
	}

	public Integer getViewCount() {
		return viewCount;
	}

	public void setViewCount(Integer viewCount) {
		this.viewCount = viewCount;
	}

	public Integer getPoiCount() {
		return poiCount;
	}

	public void setPoiCount(Integer poiCount) {
		this.poiCount = poiCount;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Story [storyId=" + storyId + ", title=" + title + ", description=" + description + ", image=" + image
				+ ", likeCount=" + likeCount + ", replyCount=" + replyCount + ", viewCount=" + viewCount + ", poiCount="
				+ poiCount + ", user=" + user + "]";
	}

}