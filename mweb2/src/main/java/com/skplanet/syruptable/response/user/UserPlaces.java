package com.skplanet.syruptable.response.user;

import java.util.List;

import com.skplanet.syruptable.response.BaseTableAPI;
import com.skplanet.syruptable.response.Hashtag;
import com.skplanet.syruptable.response.image.MainImage;
import com.skplanet.syruptable.response.poi.Location;
import com.skplanet.syruptable.response.poi.TagPOI;

public class UserPlaces extends BaseTableAPI {
	
	
	private List<UserPoi> pois;
	
	public List<UserPoi> getPois() {
		return pois;
	}

	public void setPois(List<UserPoi> pois) {
		this.pois = pois;
	}


	public static final class UserPoi {
		
		private Integer poiId;
		
		private MainImage mainImage;

		private String name;
		
		private Location location;
		
		private String abbrAddress;

		private POIScoreboard scoreboard;

		private List<Hashtag> hashtags;
		
		private TagPOI tagPOI;
		
		
		
		public Integer getPoiId() {
			return poiId;
		}

		public void setPoiId(Integer poiId) {
			this.poiId = poiId;
		}

		public MainImage getMainImage() {
			return mainImage;
		}

		public void setMainImage(MainImage mainImage) {
			this.mainImage = mainImage;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Location getLocation() {
			return location;
		}

		public void setLocation(Location location) {
			this.location = location;
		}

		public String getAbbrAddress() {
			return abbrAddress;
		}

		public void setAbbrAddress(String abbrAddress) {
			this.abbrAddress = abbrAddress;
		}

		public POIScoreboard getScoreboard() {
			return scoreboard;
		}

		public void setScoreboard(POIScoreboard scoreboard) {
			this.scoreboard = scoreboard;
		}

		public List<Hashtag> getHashtags() {
			return hashtags;
		}

		public void setHashtags(List<Hashtag> hashtags) {
			this.hashtags = hashtags;
		}

		public TagPOI getTagPOI() {
			return tagPOI;
		}

		public void setTagPOI(TagPOI tagPOI) {
			this.tagPOI = tagPOI;
		}

		
		
		public static final class POIScoreboard {
			
			/**
			 * 총 평가 평균 
			 */
			private Double rating;
			
			/**
			 * 총 평가자 
			 */
			private Integer rater;
			
			/**
			 * 리뷰 수
			 */
			private Integer picks;
			
			/**
			 * 사진 수
			 */
			private Integer images;
			
			/**
			 * 블로그 리뷰 수
			 */
			private Integer blogReviews;
			
			
			public Integer getPicks() {
				return picks;
			}

			public void setPicks(Integer picks) {
				this.picks = picks;
			}

			public Integer getImages() {
				return images;
			}

			public void setImages(Integer images) {
				this.images = images;
			}

			public Integer getBlogReviews() {
				return blogReviews;
			}

			public void setBlogReviews(Integer blogReviews) {
				this.blogReviews = blogReviews;
			}

			public Double getRating() {
				return rating;
			}

			public void setRating(Double rating) {
				this.rating = rating;
			}

			public Integer getRater() {
				return rater;
			}

			public void setRater(Integer rater) {
				this.rater = rater;
			}

			@Override
			public String toString() {
				return String
						.format("POIScoreboard [rating=%s, rater=%s, picks=%s, images=%s, blogReviews=%s]",
								rating, rater, picks, images, blogReviews);
			}
			
			
		}

		@Override
		public String toString() {
			return String
					.format("UserPoi [poiId=%s, mainImage=%s, name=%s, location=%s, abbrAddress=%s, scoreboard=%s, hashtags=%s, tagPOI=%s]",
							poiId, mainImage, name, location, abbrAddress,
							scoreboard, hashtags, tagPOI);
		}
		
	}


	@Override
	public String toString() {
		return String.format("UserPlaces [pois=%s]", pois);
	}
	

}
