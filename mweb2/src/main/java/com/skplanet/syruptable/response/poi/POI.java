package com.skplanet.syruptable.response.poi;

import java.util.List;

import com.skplanet.syruptable.response.image.MainImage;
import com.skplanet.syruptable.response.pick.Pick;

public class POI extends BasePOI{
	
	private List<String> price;
	
	private List<RichInfo> richInfo;
	
	private List<MainImage> mainImages;
	
	private Integer couponCount;
	
	private Coupon coupon;
	
	private List<Coupon> coupons;
	
	private Boolean isClosed;
	
	private Boolean isUcp;
	
	private Pick pick;
	
	private List<Pick> picks;

	public List<String> getPrice() {
		return price;
	}

	public void setPrice(List<String> price) {
		this.price = price;
	}

	public List<RichInfo> getRichInfo() {
		return richInfo;
	}

	public void setRichInfo(List<RichInfo> richInfo) {
		this.richInfo = richInfo;
	}

	public List<MainImage> getMainImages() {
		return mainImages;
	}

	public void setMainImages(List<MainImage> mainImages) {
		this.mainImages = mainImages;
	}

	public Integer getCouponCount() {
		return couponCount;
	}

	public void setCouponCount(Integer couponCount) {
		this.couponCount = couponCount;
	}

	public Coupon getCoupon() {
		return coupon;
	}

	public void setCoupon(Coupon coupon) {
		this.coupon = coupon;
	}

	public List<Coupon> getCoupons() {
		return coupons;
	}

	public void setCoupons(List<Coupon> coupons) {
		this.coupons = coupons;
	}

	public Boolean getIsClosed() {
		return isClosed;
	}

	public void setIsClosed(Boolean isClosed) {
		this.isClosed = isClosed;
	}

	public Boolean getIsUcp() {
		return isUcp;
	}

	public void setIsUcp(Boolean isUcp) {
		this.isUcp = isUcp;
	}

	public Pick getPick() {
		return pick;
	}

	public void setPick(Pick pick) {
		this.pick = pick;
	}

	public List<Pick> getPicks() {
		return picks;
	}

	public void setPicks(List<Pick> picks) {
		this.picks = picks;
	}
	
}