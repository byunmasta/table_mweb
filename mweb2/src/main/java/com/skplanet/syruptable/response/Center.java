package com.skplanet.syruptable.response;

public class Center {

	private String type;
	
	private Double[] cooredinates;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double[] getCooredinates() {
		return cooredinates;
	}

	public void setCooredinates(Double[] cooredinates) {
		this.cooredinates = cooredinates;
	}
}
