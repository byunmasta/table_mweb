package com.skplanet.syruptable.response.main;

import com.skplanet.syruptable.response.BaseMain;
import com.skplanet.syruptable.response.poi.POI;


/**
 * AOI 이름 
 * 내주변 메인 
 * 오늘의 추천 맛집
 * 이벤트
 * 서브 카테고리
 * 최근리뷰 (최근인기 : 일짜별 가중치 적용 인기)
 * 	- POI 사진 ( 리뷰 )  unnest
 * @author webmadeup
 *
 */

public class MainAround extends BaseMain {
	
	private POI[] pois;
	
	public POI[] getPois() {
		return pois;
	}

	public void setPois(POI[] pois) {
		this.pois = pois;
	}
}