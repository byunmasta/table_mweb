package com.skplanet.syruptable.response.pick;

import java.util.Date;

import com.skplanet.syruptable.response.user.User;

public class BasePick {

	private Integer pickId;
	
	private String contents;
	
	private Date createdAt;
	
	private PickScoreboard scoreboard;
	
	private User editor;

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public Integer getPickId() {
		return pickId;
	}

	public void setPickId(Integer pickId) {
		this.pickId = pickId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public PickScoreboard getScoreboard() {
		return scoreboard;
	}

	public void setScoreboard(PickScoreboard scoreboard) {
		this.scoreboard = scoreboard;
	}

	public User getEditor() {
		return editor;
	}

	public void setEditor(User editor) {
		this.editor = editor;
	}
}
