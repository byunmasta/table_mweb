package com.skplanet.syruptable.response.story;

import java.util.List;

import com.skplanet.syruptable.response.BaseTableAPI;
import com.skplanet.syruptable.response.story.StoryDetail.StoryReply;

public class StoryReplies extends BaseTableAPI {
	
	
	private List<StoryReply> storyReplies;
	

	public List<StoryReply> getStoryReplies() {
		return storyReplies;
	}

	public void setStoryReplies(List<StoryReply> storyReplies) {
		this.storyReplies = storyReplies;
	}

	@Override
	public String toString() {
		return "StoryReplies [storyReplies=" + storyReplies + "]";
	}

}