package com.skplanet.syruptable.response.poi;

public class POIScoreboard {
	
	/**
	 * 리뷰 수
	 */
	private Integer picks;
	
	/**
	 * 사진 수
	 */
	private Integer images;
	
	/**
	 * 블로그 리뷰 수
	 */
	private Integer blogReviews;
	
	private Double rating;
	
	private Integer rater;
	
	private Integer comments;
	
	private Integer wishes;
	
	public Integer getWishes() {
		return wishes;
	}

	public void setWishes(Integer wishes) {
		this.wishes = wishes;
	}

	private Integer score;

	public Integer getPicks() {
		return picks;
	}

	public void setPicks(Integer picks) {
		this.picks = picks;
	}

	public Integer getImages() {
		return images;
	}

	public void setImages(Integer images) {
		this.images = images;
	}

	public Integer getBlogReviews() {
		return blogReviews;
	}

	public void setBlogReviews(Integer blogReviews) {
		this.blogReviews = blogReviews;
	}

	public Integer getComments() {
		return comments;
	}

	public void setComments(Integer comments) {
		this.comments = comments;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public Integer getRater() {
		return rater;
	}

	public void setRater(Integer rater) {
		this.rater = rater;
	}

	@Override
	public String toString() {
		return "POIScoreboard [picks=" + picks + ", images=" + images + ", blogReviews=" + blogReviews + ", rating="
				+ rating + ", rater=" + rater + ", comments=" + comments + ", wishes=" + wishes + ", score=" + score
				+ "]";
	}
	
}
