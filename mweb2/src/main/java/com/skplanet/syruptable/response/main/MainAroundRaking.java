package com.skplanet.syruptable.response.main;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.time.FastDateFormat;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;

import com.skplanet.syruptable.response.BaseMain;
import com.skplanet.syruptable.response.image.MainImage;
import com.skplanet.syruptable.response.poi.Location;
import com.skplanet.syruptable.response.poi.TagPOI;

public class MainAroundRaking extends BaseMain {
	
	
	private List<RakingPOI> pois; 

	public List<RakingPOI> getPois() {
		return pois;
	}
	
	public void setPois(List<RakingPOI> pois) {
		this.pois = pois;
	}

	public static final class RakingPOI {
		
		private int raking;
		
		private MainImage mainImage;

		private String name;
		
		private Location location;

		private POIScoreboard scoreboard;

		private List<POIHashTag> hashtags;
		
		private TagPOI tagPOI;
		
		
		private Coupon coupon;

		public Coupon getCoupon() {
			return coupon;
		}

		public void setCoupon(Coupon coupon) {
			this.coupon = coupon;
		}

		public int getRaking() {
			return raking;
		}

		public void setRaking(int raking) {
			this.raking = raking;
		}

		public MainImage getMainImage() {
			return mainImage;
		}

		public void setMainImage(MainImage mainImage) {
			this.mainImage = mainImage;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Location getLocation() {
			return location;
		}

		public void setLocation(Location location) {
			this.location = location;
		}

		
		public TagPOI getTagPOI() {
			return tagPOI;
		}

		public void setTagPOI(TagPOI tagPOI) {
			this.tagPOI = tagPOI;
		}

		public List<POIHashTag> getHashtags() {
			return hashtags;
		}

		public void setHashtags(List<POIHashTag> hashtags) {
			this.hashtags = hashtags;
		}

		


		public POIScoreboard getScoreboard() {
			return scoreboard;
		}

		public void setScoreboard(POIScoreboard scoreboard) {
			this.scoreboard = scoreboard;
		}


		public static final class POIHashTag {
			
			private Integer hashtagId;
			
			private String name;
			
			public POIHashTag(Integer hashtagId, String name) {
				this.hashtagId = hashtagId;
				this.name = name;
			}

			public Integer getHashtagId() {
				return hashtagId;
			}

			public void setHashtagId(Integer hashtagId) {
				this.hashtagId = hashtagId;
			}

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

		}
		
		public static final class POIScoreboard {
			
			/**
			 * 총 평가 평균 
			 */
			private Double rating;
			
			/**
			 * 총 평가자 
			 */
			private Integer rater;
			
			/**
			 * 리뷰 수
			 */
			private Integer picks;
			
			/**
			 * 사진 수
			 */
			private Integer images;
			
			/**
			 * 블로그 리뷰 수
			 */
			private Integer blogReviews;
			
			
			public Integer getPicks() {
				return picks;
			}

			public void setPicks(Integer picks) {
				this.picks = picks;
			}

			public Integer getImages() {
				return images;
			}

			public void setImages(Integer images) {
				this.images = images;
			}

			public Integer getBlogReviews() {
				return blogReviews;
			}

			public void setBlogReviews(Integer blogReviews) {
				this.blogReviews = blogReviews;
			}

			public Double getRating() {
				return rating;
			}

			public void setRating(Double rating) {
				this.rating = rating;
			}

			public Integer getRater() {
				return rater;
			}

			public void setRater(Integer rater) {
				this.rater = rater;
			}
			
			
		}
		
		
		public static final class Coupon {
			
			private String name;
			
			private String image;
			
			private String validateDate;
			
			public String getImage() {
				return image;
			}

			public String getName() {
				return name;
			}

			public String getValidateDate() {
				return validateDate;
			}

			/**
			 * 
			 * @param image
			 * @param name
			 * @param startDate
			 * @param endDate
			 */
			public Coupon( String name, String image, String startDate, String endDate) {
				this.name = name;
				this.image = image;
				this.validateDate = this.getCouponTerm(startDate, endDate);
			}
			
			final FastDateFormat updateDate = FastDateFormat.getInstance("yyyy.MM.dd", Locale.KOREA);
			
			final DateTimeParser[] parsers = { 
		        	DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ssZ").getParser(),
		        	DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ").getParser() 
		        };
		
			final DateTimeFormatter formatter = new DateTimeFormatterBuilder().append( null, parsers ).toFormatter();
			
			/**
			 * 쿠폰 유효기간을 노출하는 정책적용 
			 * @param startDate
			 * @param endDate
			 * @return
			 */
			private String getCouponTerm(final String startDate, final String endDate) {
				
				String sDate = startDate;
				String eDate = endDate;
				
				try {
					sDate = updateDate.format(formatter.parseDateTime(startDate).toDate());
					eDate = updateDate.format(formatter.parseDateTime(endDate).toDate());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				return String.format("%s ~ %s", sDate, eDate);
			}
			
			
		}
		

		
		
	}

	@Override
	public String toString() {
		return String
				.format("MainAroundRaking [pois=%s, getAoi()=%s, getTodayLunch()=%s, getBanner()=%s, getSuggestions()=%s, getTotalCount()=%s, getPaging()=%s]",
						pois, getAoi(), Arrays.toString(getTodayLunch()),
						getBanner(), Arrays.toString(getSuggestions()),
						getTotalCount(), getPaging());
	}

}
