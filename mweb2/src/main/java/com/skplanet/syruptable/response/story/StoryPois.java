package com.skplanet.syruptable.response.story;

import java.util.List;

import com.skplanet.syruptable.response.BaseTableAPI;
import com.skplanet.syruptable.response.poi.BasePOI;

public class StoryPois extends BaseTableAPI {
	
	
	private List<BasePOI> pois;

	public List<BasePOI> getPois() {
		return pois;
	}

	public void setPois(List<BasePOI> pois) {
		this.pois = pois;
	}

	@Override
	public String toString() {
		return "StoryPois [pois=" + pois + "]";
	}
	
}