package com.skplanet.syruptable.response.main;

import java.util.Arrays;
import java.util.List;

import com.skplanet.syruptable.response.image.Image;
import com.skplanet.syruptable.response.image.MainImage;
import com.skplanet.syruptable.response.story.Story;
import com.skplanet.syruptable.response.user.User;


/**
 * Trend 트렌드 
 * Story 스토리
 * Syrup 인증 맛집
 * @author mylostland@sk.com
 *
 */
public class MainTrend {
	
	private Trend trend;
	
	private Integer storyCount;
	
	private String storyTitle;
	
	private Story[] stories;
	
	private VerifiedPoi[] verifiedPoi;
	
	private Integer hashtagCount;
	
	private Hashtag[] hashtags;
	
	private Integer topuserCount;
	
	private Topuser[] topUsers;
	
	public MainTrend() {
		
	}
	
	public Trend getTrend() {
		return trend;
	}

	public void setTrend(Trend trend) {
		this.trend = trend;
		this.trend.setTitle(trend.getTitle());
		this.trend.setDate(trend.getDate());
	}

	public Integer getStoryCount() {
		return storyCount;
	}

	public void setStoryCount(Integer storyCount) {
		this.storyCount = storyCount;
	}

	public String getStoryTitle() {
		return storyTitle;
	}

	public void setStoryTitle(String storyTitle) {
		this.storyTitle = storyTitle;
	}

	public Story[] getStories() {
		return stories;
	}

	public void setStories(Story[] stories) {
		this.stories = stories;
	}

	public VerifiedPoi[] getVerifiedPoi() {
		return verifiedPoi;
	}

	public void setVerifiedPoi(VerifiedPoi[] verifiedPoi) {
		this.verifiedPoi = verifiedPoi;
	}

	public Integer getHashtagCount() {
		return hashtagCount;
	}

	public void setHashtagCount(Integer hashtagCount) {
		this.hashtagCount = hashtagCount;
	}

	public Hashtag[] getHashtags() {
		return hashtags;
	}

	public void setHashtags(Hashtag[] hashtags) {
		this.hashtags = hashtags;
	}

	public Integer getTopuserCount() {
		return topuserCount;
	}

	public void setTopuserCount(Integer topuserCount) {
		this.topuserCount = topuserCount;
	}

	public Topuser[] getTopUsers() {
		return topUsers;
	}

	public void setTopUsers(Topuser[] topUsers) {
		this.topUsers = topUsers;
	}


	public static final class Trend {
		
		private String title;
		
		private String date;
		
		private Image image;
		
		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public Image getImage() {
			return image;
		}

		public void setImage(Image image) {
			this.image = image;
		}

		@Override
		public String toString() {
			return String.format("Trend [title=%s, date=%s]",
					title, date);
		}
		
	}
	
	public static final class VerifiedPoi {
		
		private String name;
		
		private int poiId;
		
		private String address;
		
		private StoryScoreboard storyScoreboard;
		
		private MainImage mainImage;
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
		public int getPoiId() {
			return poiId;
		}

		public void setPoiId(int poiId) {
			this.poiId = poiId;
		}

		public String getAddress() {
			return address;
		}
		
		public void setAddress(String address) {
			this.address = address;
		}
		
		public StoryScoreboard getStoryScoreboard() {
			return storyScoreboard;
		}

		public void setStoryScoreboard(StoryScoreboard storyScoreboard) {
			this.storyScoreboard = storyScoreboard;
		}
		
		

		public MainImage getMainImage() {
			return mainImage;
		}

		public void setMainImage(MainImage mainImage) {
			this.mainImage = mainImage;
		}



		public static final class StoryScoreboard {
			
			private Integer rating;
			
			private Integer rater;

			public Integer getRating() {
				return rating;
			}

			public void setRating(Integer rating) {
				this.rating = rating;
			}

			public Integer getRater() {
				return rater;
			}

			public void setRater(Integer rater) {
				this.rater = rater;
			}
		}
	}
	
	
	public static final class Hashtag {
		
		private Integer hashtagId;
		
		private String hashtag;
		
		private List<Image> images;

		public Integer getHashtagId() {
			return hashtagId;
		}

		public void setHashtagId(Integer hashtagId) {
			this.hashtagId = hashtagId;
		}

		public String getHashtag() {
			return hashtag;
		}

		public void setHashtag(String hashtag) {
			this.hashtag = hashtag;
		}

		public List<Image> getImages() {
			return images;
		}

		public void setImages(List<Image> images) {
			this.images = images;
		}
	}
	
	public static final class Topuser {
		
		private User user;
		
		private List<Image> images;
		
		private Boolean isFollow;

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		public List<Image> getImages() {
			return images;
		}

		public void setImages(List<Image> images) {
			this.images = images;
		}

		public Boolean getIsFollow() {
			return isFollow;
		}

		public void setIsFollow(Boolean isFollow) {
			this.isFollow = isFollow;
		}

		@Override
		public String toString() {
			return String.format("Topuser [user=%s, images=%s, isFollow=%s]",
					user, images, isFollow);
		}
	}

	@Override
	public String toString() {
		return String
				.format("MainTrend [trend=%s, storyCount=%s, storyTitle=%s, stories=%s, verifiedPoi=%s, hashtagCount=%s, hashtags=%s, topuserCount=%s, topUsers=%s]",
						trend, storyCount, storyTitle, stories,
						Arrays.toString(verifiedPoi), hashtagCount,
						Arrays.toString(hashtags), topuserCount,
						Arrays.toString(topUsers));
	}

}
