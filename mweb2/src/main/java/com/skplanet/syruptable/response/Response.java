package com.skplanet.syruptable.response;


/**
 * @author mylostland@sk.com
 */
public class Response {

	private String rcode = "RET0000";

	private String rmsg = "SUCCESS";

	private Object object;

	public Response() {

	}

	public Response(Object object) {
		this.object = object;
	}

	public String getRcode() {
		return rcode;
	}

	public void setRcode(String rcode) {
		this.rcode = rcode;
	}

	public String getRmsg() {
		return rmsg;
	}

	public void setRmsg(String rmsg) {
		this.rmsg = rmsg;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
}