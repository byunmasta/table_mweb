package com.skplanet.syruptable.response.verified;

import java.util.List;

import com.skplanet.syruptable.response.BaseTableAPI;
import com.skplanet.syruptable.response.Hashtag;
import com.skplanet.syruptable.response.image.MainImage;
import com.skplanet.syruptable.response.poi.Location;
import com.skplanet.syruptable.response.poi.POIScoreboard;
import com.skplanet.syruptable.response.poi.TagPOI;

public class VerifiedAOIPlaces {
	
	private List<VerifiedPoi> verifieds;

	public List<VerifiedPoi> getVerifieds() {
		return verifieds;
	}

	public void setVerifieds(List<VerifiedPoi> verifieds) {
		this.verifieds = verifieds;
	}

	public static final class VerifiedPoi extends BaseTableAPI {
		
		private int aoiId;
		
		private String name;

		private List<POI> pois;

		public int getAoiId() {
			return aoiId;
		}

		public void setAoiId(int aoiId) {
			this.aoiId = aoiId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public List<POI> getPois() {
			return pois;
		}

		public void setPois(List<POI> pois) {
			this.pois = pois;
		}
		
		public static final class POI {
			
			private Integer poiId;
			
			private MainImage mainImage;

			private String name;
			
			private Location location;
			
			private String abbrAddress;

			private POIScoreboard scoreboard;

			private List<Hashtag> hashtags;
			
			private TagPOI tagPOI;
			
			
			
			public TagPOI getTagPOI() {
				return tagPOI;
			}

			public void setTagPOI(TagPOI tagPOI) {
				this.tagPOI = tagPOI;
			}

			public Integer getPoiId() {
				return poiId;
			}

			public void setPoiId(Integer poiId) {
				this.poiId = poiId;
			}

			public MainImage getMainImage() {
				return mainImage;
			}

			public void setMainImage(MainImage mainImage) {
				this.mainImage = mainImage;
			}

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			public Location getLocation() {
				return location;
			}

			public void setLocation(Location location) {
				this.location = location;
			}

			public String getAbbrAddress() {
				return abbrAddress;
			}

			public void setAbbrAddress(String abbrAddress) {
				this.abbrAddress = abbrAddress;
			}

			public POIScoreboard getScoreboard() {
				return scoreboard;
			}

			public void setScoreboard(POIScoreboard scoreboard) {
				this.scoreboard = scoreboard;
			}

			public List<Hashtag> getHashtags() {
				return hashtags;
			}

			public void setHashtags(List<Hashtag> hashtags) {
				this.hashtags = hashtags;
			}

			@Override
			public String toString() {
				return String
						.format("POI [poiId=%s, mainImage=%s, name=%s, location=%s, abbrAddress=%s, scoreboard=%s, hashtags=%s, tagPOI=%s]",
								poiId, mainImage, name, location, abbrAddress,
								scoreboard, hashtags, tagPOI);
			}
			
		}

		@Override
		public String toString() {
			return String.format("VerifiedPoi [aoiId=%s, name=%s, pois=%s]",
					aoiId, name, pois);
		}
		
		
		
	}

	@Override
	public String toString() {
		return String.format("VerifiedPlaces [verifieds=%s]", verifieds);
	}
	
}
