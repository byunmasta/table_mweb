package com.skplanet.syruptable.response.pick;

public class PickScoreboard {
	
	private Double rating;
	
	private Integer images;
	
	private Integer likes;
	
	private Integer comments;
	
	public PickScoreboard() {
		
	}
	
	public PickScoreboard(Double rating, Integer images, Integer likes, Integer comments) {
		this.rating = rating;
		this.images = images;
		this.likes = likes;
		this.comments = comments;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public Integer getImages() {
		return images;
	}

	public void setImages(Integer images) {
		this.images = images;
	}

	public Integer getLikes() {
		return likes;
	}

	public void setLikes(Integer likes) {
		this.likes = likes;
	}

	public Integer getComments() {
		return comments;
	}

	public void setComments(Integer comments) {
		this.comments = comments;
	}
}
