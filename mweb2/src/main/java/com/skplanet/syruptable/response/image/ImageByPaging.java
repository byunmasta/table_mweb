package com.skplanet.syruptable.response.image;


public class ImageByPaging {
	
		private Integer rownum;
		
		private Image image;

		public Integer getRownum() {
			return rownum;
		}

		public void setRownum(Integer rownum) {
			this.rownum = rownum;
		}

		public Image getImage() {
			return image;
		}

		public void setImage(Image image) {
			this.image = image;
		}
}