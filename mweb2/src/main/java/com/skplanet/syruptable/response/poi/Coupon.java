package com.skplanet.syruptable.response.poi;

public class Coupon {
	
	private String partnerCode;
	
	private String ptProdCode;
	
	private String cpName;
	
	private String image;
	
	private String validSdate;
	
	private String validEdate;

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getPtProdCode() {
		return ptProdCode;
	}

	public void setPtProdCode(String ptProdCode) {
		this.ptProdCode = ptProdCode;
	}

	public String getCpName() {
		return cpName;
	}

	public void setCpName(String cpName) {
		this.cpName = cpName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getValidSdate() {
		return validSdate;
	}

	public void setValidSdate(String validSdate) {
		this.validSdate = validSdate;
	}

	public String getValidEdate() {
		return validEdate;
	}

	public void setValidEdate(String validEdate) {
		this.validEdate = validEdate;
	}
}