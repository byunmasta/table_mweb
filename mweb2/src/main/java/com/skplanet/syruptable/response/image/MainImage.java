package com.skplanet.syruptable.response.image;

public class MainImage {

	public MainImage(String image, String type) {
		this.image = image;
		this.type = type;
	}

	private String image;

	private String type;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return String.format("MainImage [image=%s, type=%s]", image, type);
	}
}
