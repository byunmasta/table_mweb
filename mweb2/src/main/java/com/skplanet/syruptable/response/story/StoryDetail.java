package com.skplanet.syruptable.response.story;

import java.util.Date;
import java.util.List;

import com.skplanet.syruptable.response.Hashtag;
import com.skplanet.syruptable.response.poi.BasePOI;
import com.skplanet.syruptable.response.poi.TagPOI;
import com.skplanet.syruptable.response.user.User;

public class StoryDetail extends Story {
	
	private Integer shareCount;
	
	private List<Integer> pois;
	
	private List<StoryContent> storyContents;
	
	private List<StoryReply> storyReplies;
	
	private List<StoryLike> storyLikes;
	
	private List<Story> anotherStories;

	public List<Integer> getPois() {
		return pois;
	}

	public void setPois(List<Integer> pois) {
		this.pois = pois;
	}

	public Integer getShareCount() {
		return shareCount;
	}

	public void setShareCount(Integer shareCount) {
		this.shareCount = shareCount;
	}

	public List<StoryContent> getStoryContents() {
		return storyContents;
	}

	public void setStoryContents(List<StoryContent> storyContents) {
		this.storyContents = storyContents;
	}

	public List<StoryReply> getStoryReplies() {
		return storyReplies;
	}

	public void setStoryReplies(List<StoryReply> storyReplies) {
		this.storyReplies = storyReplies;
	}

	public List<StoryLike> getStoryLikes() {
		return storyLikes;
	}

	public void setStoryLikes(List<StoryLike> storyLikes) {
		this.storyLikes = storyLikes;
	}

	public List<Story> getAnotherStories() {
		return anotherStories;
	}

	public void setAnotherStories(List<Story> anotherStories) {
		this.anotherStories = anotherStories;
	}

	public static final class StoryContent {
		
		private Integer storyId;
		
		private String type;
		
		private Integer page;
		
		private String media;
		
		private List<Content> contents;

		public Integer getStoryId() {
			return storyId;
		}

		public void setStoryId(Integer storyId) {
			this.storyId = storyId;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public Integer getPage() {
			return page;
		}

		public void setPage(Integer page) {
			this.page = page;
		}

		public String getMedia() {
			return media;
		}

		public void setMedia(String media) {
			this.media = media;
		}

		public List<Content> getContents() {
			return contents;
		}

		public void setContents(List<Content> contents) {
			this.contents = contents;
		}

		@Override
		public String toString() {
			return "StoryContent [storyId=" + storyId + ", type=" + type + ", page=" + page + ", media=" + media
					+ ", contents=" + contents + "]";
		}
		
	}
	
	public static final class Content {
		
		private Integer poiId;
		
		private String contents;
		
		private BasePOI poiDetail;
		
		public BasePOI getPoiDetail() {
			return poiDetail;
		}

		public void setPoiDetail(BasePOI poiDetail) {
			this.poiDetail = poiDetail;
		}

		public Integer getPoiId() {
			return poiId;
		}

		public void setPoiId(Integer poiId) {
			this.poiId = poiId;
		}

		public String getContents() {
			return contents;
		}

		public void setContents(String contents) {
			this.contents = contents;
		}

		@Override
		public String toString() {
			return "Content [poiId=" + poiId + ", contents=" + contents + ", poiDetail=" + poiDetail + "]";
		}
		
	}
	
	public static final class Poi {
		
		private Integer imageCount;
		
		private Integer pickCount;
		
		private Integer raterCount;
		
		private Double rating;
		
		private String address;
		
		private String name;
		
		private String image;
		
		private List<Hashtag> hashtags;
		
		private TagPOI tagPoi;

		public Integer getImageCount() {
			return imageCount;
		}

		public void setImageCount(Integer imageCount) {
			this.imageCount = imageCount;
		}

		public Integer getPickCount() {
			return pickCount;
		}

		public void setPickCount(Integer pickCount) {
			this.pickCount = pickCount;
		}

		public Integer getRaterCount() {
			return raterCount;
		}

		public void setRaterCount(Integer raterCount) {
			this.raterCount = raterCount;
		}

		public Double getRating() {
			return rating;
		}

		public void setRating(Double rating) {
			this.rating = rating;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}

		public List<Hashtag> getHashtags() {
			return hashtags;
		}

		public void setHashtags(List<Hashtag> hashtags) {
			this.hashtags = hashtags;
		}

		public TagPOI getTagPoi() {
			return tagPoi;
		}

		public void setTagPoi(TagPOI tagPoi) {
			this.tagPoi = tagPoi;
		}

		@Override
		public String toString() {
			return "Poi [imageCount=" + imageCount + ", pickCount=" + pickCount + ", raterCount=" + raterCount
					+ ", rating=" + rating + ", address=" + address + ", name=" + name + ", image=" + image
					+ ", hashtags=" + hashtags + ", tagPoi=" + tagPoi + "]";
		}
		
	}
	
	public static final class StoryReply {
		
		private Integer id;
		
		private Integer storyId;
		
		private String reply;
		
		private User user;
		
		private Date createdat;
		
		private boolean owner;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getStoryId() {
			return storyId;
		}

		public void setStoryId(Integer storyId) {
			this.storyId = storyId;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		public String getReply() {
			return reply;
		}

		public void setReply(String reply) {
			this.reply = reply;
		}

		public Date getCreatedat() {
			return createdat;
		}

		public void setCreatedat(Date createdat) {
			this.createdat = createdat;
		}

		public boolean isOwner() {
			return owner;
		}

		public void setOwner(boolean owner) {
			this.owner = owner;
		}

		@Override
		public String toString() {
			return "StoryReply [id=" + id + ", storyId=" + storyId + ", reply=" + reply + ", user=" + user
					+ ", createdat=" + createdat + ", owner=" + owner + "]";
		}

	}
	
	public static final class StoryLike {
		
		private Integer storyId;
		
		private User user;

		public Integer getStoryId() {
			return storyId;
		}

		public void setStoryId(Integer storyId) {
			this.storyId = storyId;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		@Override
		public String toString() {
			return "StoryLike [storyId=" + storyId + ", user=" + user + "]";
		}
		
	}

	@Override
	public String toString() {
		return "StoryDetail [shareCount=" + shareCount + ", pois=" + pois + ", storyContents=" + storyContents
				+ ", storyReplies=" + storyReplies + ", storyLikes=" + storyLikes + ", anotherStories=" + anotherStories
				+ "]";
	}
	
	
}