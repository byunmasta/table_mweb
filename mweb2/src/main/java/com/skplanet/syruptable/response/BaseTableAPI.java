package com.skplanet.syruptable.response;


/**
 * response에서 공통으로 가지는 properties
 * @author webmadeup
 *
 */
public class BaseTableAPI {

	private int totalCount;
	
	/**
	 * @return the totalCount
	 */
	public int getTotalCount() {
		return totalCount;
	}
	

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public void setPaging(String prev, String next ) {
		this.paging = new Paging();
		this.paging.setPrev(prev);
		this.paging.setNext(next);
		
	}

	private Paging paging;
	
	public Paging getPaging() {
		return paging;
	}


	@Override
	public String toString() {
		return String.format("BaseTableAPI [totalCount=%s, paging=%s]",
				totalCount, paging);
	}
	
	

}
