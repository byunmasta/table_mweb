package com.skplanet.syruptable.response.user;

public class User {
	
	private Integer userId;
	
	private String nickname;
	
	private String profileImage;
	
	private String gender;
	
	private String badge;
	
	private String description;
	
	private Boolean isFollowing;
	
	private Double rating;
	
	public User() {
		
	}
	
	public User(Integer userId, String nickname, String profileImage) {
		this.userId = userId;
		this.nickname = nickname;
		this.profileImage = profileImage;
	}
	
	public User(Integer userId, String nickname, String profileImage, String gender
			, String badge, String description, Boolean isFollowing, Double rating) {
		this.userId = userId;
		this.nickname = nickname;
		this.profileImage = profileImage;
		this.gender = gender;
		this.badge = badge;
		this.description = description;
		this.isFollowing = isFollowing;
		this.rating = rating;
	}

	public Boolean getIsFollowing() {
		return isFollowing;
	}

	public void setIsFollowing(Boolean isFollowing) {
		this.isFollowing = isFollowing;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBadge() {
		return badge;
	}

	public void setBadge(String badge) {
		this.badge = badge;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", nickname=" + nickname + ", profileImage=" + profileImage + ", gender="
				+ gender + ", badge=" + badge + ", description=" + description + ", isFollowing=" + isFollowing
				+ ", rating=" + rating + "]";
	}
	
}