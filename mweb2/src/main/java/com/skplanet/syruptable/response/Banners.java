package com.skplanet.syruptable.response;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;


public class Banners {

	private String link;
	private String image;
	private int type;
	
	JSONArray bannerArray;
	private List<JSONArray> jsonArrary;
	/**
		url("url")                          //  A : web page
		,poi("poi")							// 8 : POI 상세
		,notice("notice")                   // 안씀 5 : 공지사항  안씀
		,public_theme("public_theme")		// 6 : 테마속 장소
		,private_theme("private_theme")		// 7 : 테마속 장소
		,search("search")					// 9 : 통합검색  안씀
		,user("user")						// 안씀  D : 사용자 사용자 탭('N': 나의 소식, 'M': 내 장소, 'T': 나의 테마, 'F': 친구)
		,coupon("coupon")					// 안씀 C : 쿠폰 상세  c=(coupon_src)&d=(coupon_code)
		,coupon_detail("coupon_detail")		// 안씀 C : 쿠폰 상세  c=(coupon_src)&d=(coupon_code)
		,keyword("keyword");				// K : 키워드 상세
		,popup_store						// P: 팝업스토어
	*/
	public Banners(final String link, final String image, final int type) {
		this.link = link;
		this.image = image;
		this.type = type;
	}
		
	public Banners(final JSONObject banner) {
		this.link = banner.getString("link");
		this.image = banner.getString("image");
		this.type = banner.getInt("type");
	}
	
	public Banners(final JSONArray banners) {
		jsonArrary = Arrays.asList(banners);
		
	}
	
	public Banners() {
	}

	final public JSONArray getBanner() {
		JSONArray bannerArray = jsonArrary.get(0);
		JSONArray bannersInfo  = new JSONArray();
		
		for(int i =0; i < bannerArray.length(); i++) {
			Banners banner = new Banners(bannerArray.getJSONObject(i));
			Map<String, String> bannerInfo = new HashMap<String, String>();
			try {
				bannerInfo.put("url", banner.getLinkURL(banner.type, banner.link));
				bannerInfo.put("image", banner.image);
		
			} catch (Exception e) {
				//특정 raw데이타 변환시 에러가 발생한 경우 해당 배너는 전시 하지 않음
				continue;
			}
			bannersInfo.put(bannerInfo);
		}
		return bannersInfo;
	}
	
	
	public String getLinkURL(final int type, final String link) {
		
		String[] rawLink = link.split("&");
		if(rawLink.length == 0) {
			return "";
		}
		String bannerInfo = rawLink[1].replaceFirst("b=", "");
		String siteLink;
		
		switch(type) {
		
			case 1:  // URL 
				siteLink = bannerInfo;
				break;
			case 2:  // POI
				siteLink = String.format("/poi/%s", bannerInfo);
				break;
			case 3:  // Notice
				siteLink = "/notices";
				break;
			case 4:  // public theme
				siteLink = String.format("/themes/%s/public", bannerInfo);
				break;
			case 5:  // private theme
				siteLink = String.format("/themes/%s/private", bannerInfo);
				break;
			case 10: // keyword
				siteLink = String.format("/keyword/%s", bannerInfo);
				break;
			case 12:  // URL 
				siteLink = bannerInfo;
				break;
			default :
				siteLink = "";
				break;
		}
		
		return siteLink;
		
	}
	
	/*public static void main(String[] args) {
		JSONArray banners = new JSONArray();
		
		//webpage 
		JSONObject url = new JSONObject();
		url.put("link", "a=A&b=웹사이트주소");
		url.put("image", "http:\\/\\/file.pickat.kiwiple.net\\/files\\/0\\/0\\/20140425\\/3c8ca78dcdb7832db3f8a8ea9305d4ac57cbb733");
		url.put("type", 1);
		
		banners.put(url);
		
		//POI 상세
		JSONObject poi = new JSONObject();
		poi.put("link", "a=8&b=POI_ID");
		poi.put("image", "http:\\/\\/file.pickat.kiwiple.net\\/files\\/0\\/0\\/20140425\\/3c8ca78dcdb7832db3f8a8ea9305d4ac57cbb733");
		poi.put("type", 2);
		
		banners.put(poi);
		
		//공지사항 
		// 안씀
		JSONObject notice = new JSONObject();
		notice.put("link", "a=5&b=notice_id");
		notice.put("image", "http:\\/\\/file.pickat.kiwiple.net\\/files\\/0\\/0\\/20140425\\/3c8ca78dcdb7832db3f8a8ea9305d4ac57cbb733");
		notice.put("type", 3);
		
		banners.put(notice);
		
		//public 테마속 장소
		JSONObject public_theme = new JSONObject();
		public_theme.put("link", "a=6&b=public_theme_id&c=local여부");
		public_theme.put("image", "http:\\/\\/file.pickat.kiwiple.net\\/files\\/0\\/0\\/20140425\\/3c8ca78dcdb7832db3f8a8ea9305d4ac57cbb733");
		public_theme.put("type", 4);
		
		banners.put(public_theme);
		
		//private 테마속 장소
		JSONObject private_theme = new JSONObject();
		private_theme.put("link", "a=7&b=private_theme_id&c=local여부");
		private_theme.put("image", "http:\\/\\/file.pickat.kiwiple.net\\/files\\/0\\/0\\/20140425\\/3c8ca78dcdb7832db3f8a8ea9305d4ac57cbb733");
		private_theme.put("type", 5);
		
		banners.put(private_theme);
		
		//통합검색
		// 안씀
		JSONObject search = new JSONObject();
		search.put("link", "a=9&b=검색어");
		search.put("image", "http:\\/\\/file.pickat.kiwiple.net\\/files\\/0\\/0\\/20140425\\/3c8ca78dcdb7832db3f8a8ea9305d4ac57cbb733");
		search.put("type", 6);
		
		banners.put(search);
		
		// 안씀
		JSONObject user = new JSONObject();
		user.put("link", "a=D&b=user_id&c=사용자탭");  // N 나의소식 , M : 내 장소, T : 나의 테마, F : 친구
		user.put("image", "http:\\/\\/file.pickat.kiwiple.net\\/files\\/0\\/0\\/20140425\\/3c8ca78dcdb7832db3f8a8ea9305d4ac57cbb733");
		user.put("type", 7);
		
		banners.put(public_theme);
		
		// 안씀
		JSONObject coupon = new JSONObject();
		coupon.put("link", "a=C&b=poi_id&c=coupon_src&d=coupon_code");
		coupon.put("image", "http:\\/\\/file.pickat.kiwiple.net\\/files\\/0\\/0\\/20140425\\/3c8ca78dcdb7832db3f8a8ea9305d4ac57cbb733");
		coupon.put("type", 8);
		
		banners.put(coupon);
		
		// 안씀
		JSONObject coupon_detail = new JSONObject();
		coupon_detail.put("link", "a=C&b=poi_id&c=coupon_src&d=coupon_code");
		coupon_detail.put("image", "http:\\/\\/file.pickat.kiwiple.net\\/files\\/0\\/0\\/20140425\\/3c8ca78dcdb7832db3f8a8ea9305d4ac57cbb733");
		coupon_detail.put("type", 9);
		
		banners.put(coupon_detail);
		
		//키워드 상세  
		JSONObject keyword = new JSONObject();
		keyword.put("link", "a=K&b=keyword_id");
		keyword.put("image", "http:\\/\\/file.pickat.kiwiple.net\\/files\\/0\\/0\\/20140425\\/3c8ca78dcdb7832db3f8a8ea9305d4ac57cbb733");
		keyword.put("type", 10);
		
		banners.put(keyword);
		
		
		Banners banner = new Banners(banners);
		banner.getBanner();
		
		for(int i =0; i < banners.length(); i++) {
			
			Banners banner1 = new Banners(banners.getJSONObject(i));
			
			System.out.println(banner1.type);
			System.out.println(banner1.getLinkURL(banner1.type, banner1.link));
		}
		
		
		List<JSONArray> jsonArrary = Arrays.asList(banners);
		JSONArray bannerArray = jsonArrary.get(0);
		
		
		
		Banners banner = new Banners((JSONObject) a.get(0));
		System.out.println(banner.type);
		
		*//**
		url("url")                          //  A : web page
		,poi("poi")							// 8 : POI 상세
		,notice("notice")                   // 안씀 5 : 공지사항  안씀
		,public_theme("public_theme")		// 6 : 테마속 장소
		,private_theme("private_theme")		// 7 : 테마속 장소
		,search("search")					// 9 : 통합검색  안씀
		,user("user")						// 안씀  D : 사용자 사용자 탭('N': 나의 소식, 'M': 내 장소, 'T': 나의 테마, 'F': 친구)
		,coupon("coupon")					// 안씀 C : 쿠폰 상세  c=(coupon_src)&d=(coupon_code)
		,coupon_detail("coupon_detail")		// 안씀 C : 쿠폰 상세  c=(coupon_src)&d=(coupon_code)
		,keyword("keyword");				// K : 키워드 상세
		 *//*
		
		
//		Banners banners = new Banners("a=A&b=http:\\/\\/pickat.com", "http:\\/\\/file.pickat.kiwiple.net\\/files\\/0\\/0\\/20140425\\/3c8ca78dcdb7832db3f8a8ea9305d4ac57cbb733", 1);
		
	}*/
}
