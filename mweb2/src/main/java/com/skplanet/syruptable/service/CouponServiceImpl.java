package com.skplanet.syruptable.service;

import java.util.LinkedList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skplanet.syruptable.entity.sb.Coupon_Cache;
import com.skplanet.syruptable.repository.etc.CouponCacheRepository;
import com.skplanet.syruptable.response.poi.Coupon;

@Service
public class CouponServiceImpl {
	
	@Autowired
	private CouponCacheRepository couponCacheRepository;
	
	public List<Coupon> getCoupons(Integer poiId) {
		List<Coupon> coupons = null;
		List<Coupon_Cache> couponCaches = couponCacheRepository.findByPoiIds(poiId, 0, 20);
		try {
			if(couponCaches!=null) {
				coupons = new LinkedList<Coupon>();
				for(Coupon_Cache c : couponCaches) {
					JSONObject jsonObject = new JSONObject(c.getData());
					Coupon coupon = new Coupon();
					coupon.setCpName(c.getCpName());
					coupon.setPtProdCode(c.getPtProdCode());
					coupon.setPartnerCode(c.getPartnerCode());
					coupon.setImage(jsonObject.getString("cpImage"));
					coupon.setValidSdate(jsonObject.getString("validSdate"));
					coupon.setValidEdate(jsonObject.getString("validEdate"));
					coupons.add(coupon);
				}
			} 
			
			if(coupons.size()<1)
				coupons = null;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return coupons;
	}
}