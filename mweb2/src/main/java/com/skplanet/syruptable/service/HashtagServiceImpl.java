package com.skplanet.syruptable.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skplanet.syruptable.entity.poi.Pois;
import com.skplanet.syruptable.entity.sb.Sb_Hashtag_Pois;
import com.skplanet.syruptable.entity.sb.Sb_Pois;
import com.skplanet.syruptable.repository.hashtag.HashtagsRepository;
import com.skplanet.syruptable.repository.sb.SbHashtagPoisRepository;
import com.skplanet.syruptable.repository.sb.SbPoisRepository;
import com.skplanet.syruptable.response.Hashtag;

@Service
public class HashtagServiceImpl {
	
	@Autowired
	private SbPoisRepository sbPoisRepository;
	
	@Autowired
	private HashtagsRepository hashtagsRepository;
	
	@Autowired
	private SbHashtagPoisRepository sbHashtagPoisRepository;
	
	public List<Hashtag> getHashtags(Pois poi) {
		List<Integer> hashtagIds = new LinkedList<Integer>();
		List<Hashtag> hashTags = new LinkedList<Hashtag>();
		if(poi!=null) {
			Sb_Pois sbPois = sbPoisRepository.findOne(poi.getId());
			if(sbPois!=null) {
				for(int hashtagId : sbPois.getReprHashtagIds())
					hashtagIds.add(hashtagId);
				
				List<Sb_Hashtag_Pois> sbHashtagPois = 
						sbHashtagPoisRepository.findByHashtagIdInAndPoiIdAndHashtagBannedFalseOrderByScoreDesc(
								hashtagIds, 
								sbPois.getPoiId());
				for(Sb_Hashtag_Pois shp : sbHashtagPois) {
					hashTags.add(new Hashtag(shp.getHashtag().getId(), shp.getHashtag().getName()));
				}
			}
		}
		
		return hashTags;
	}
}