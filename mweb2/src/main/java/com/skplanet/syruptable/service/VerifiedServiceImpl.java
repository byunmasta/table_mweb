package com.skplanet.syruptable.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.skplanet.syruptable.entity.aoi.Aoi_Groups;
import com.skplanet.syruptable.entity.poi.Pois;
import com.skplanet.syruptable.entity.sb.Sb_Pois;
import com.skplanet.syruptable.entity.sorder.Sorder_Pois;
import com.skplanet.syruptable.repository.aoi.AoiGroupsRepository;
import com.skplanet.syruptable.repository.sb.SbPoisRepository;
import com.skplanet.syruptable.response.poi.Location;
import com.skplanet.syruptable.response.poi.POIScoreboard;
import com.skplanet.syruptable.response.poi.TagPOI;
import com.skplanet.syruptable.response.verified.VerifiedAOIPlaces;
import com.skplanet.syruptable.response.verified.VerifiedAOIPlaces.VerifiedPoi;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

@Service
public class VerifiedServiceImpl {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AoiGroupsRepository aoiGroupsRepository;
	
	@Autowired
	private SbPoisRepository sbPoisRepository;
	
	@Autowired
	private ImageServiceImpl imageServiceImpl;
	
	@Autowired
	private HashtagServiceImpl hashtagServiceImpl;
	
	// 광고 hashtag
	private static final int ads = 11; 
	
	// 인증 hashtag
	private static final int verified = 12; 
	
	@Cacheable("verifiedAOIs")
	public List<Aoi_Groups> getVerifiedAOIs() {
		return (List<Aoi_Groups>) aoiGroupsRepository.findAllByParentId(1353);
	}
	
	public Map<Integer, String> getAllVerifiedAOIsName() {
		
		Map<Integer, String> aoiNames = new HashMap<Integer, String>();
		
		List<Aoi_Groups> verifiedAois = this.getVerifiedAOIs();
		for(Aoi_Groups aois : verifiedAois) {
			aoiNames.put(aois.getAoiId(), aois.getDisplayName());
		}
		return aoiNames;
		
	}
	
	/**
	 * 추천맛집 전체 리스트 가져오기 
	 * @param aoiId
	 * @return
	 */
	@Cacheable("allVerifiedPOIs")
	public VerifiedAOIPlaces getAllVerifiedAOIs() {

		VerifiedAOIPlaces verifiedPlace = new VerifiedAOIPlaces();

		//추천맛집 지역리스트(ex 서울,부산 등등)
		List<Aoi_Groups> verifiedAois = this.getVerifiedAOIs();

		//aoi아이디로 poi리스트 가져오기
		List<VerifiedAOIPlaces.VerifiedPoi> verifieds = new LinkedList<VerifiedAOIPlaces.VerifiedPoi>();
		
		for(Aoi_Groups aois : verifiedAois) {
			VerifiedAOIPlaces.VerifiedPoi verifiedPOI = this.getVerifiedAOI(aois.getAoiId(), 0, 20);
			verifieds.add(verifiedPOI);
		}
		
		verifiedPlace.setVerifieds(verifieds);
		return verifiedPlace;
		
	}
	
	@Cacheable("verifiedPOI")
	public VerifiedPoi getVerifiedAOI(final int aoiId, final int cursor, final int limit) {

		Map<Integer, String> aoisName = this.getAllVerifiedAOIsName();
		int totalCount = this.getVerifiedAoiForPOIsTotalCount(aoiId);
		List<Sb_Pois> verified_sb_pois = sbPoisRepository.findByVerifiedAoiIds(aoiId, cursor, limit);
		
		VerifiedAOIPlaces.VerifiedPoi verifiedPOI;
		VerifiedAOIPlaces.VerifiedPoi.POI poi;
		POIScoreboard poiScoreboard;
		Pois pois;
		
		List<VerifiedAOIPlaces.VerifiedPoi.POI> verifiedPois = new LinkedList<VerifiedAOIPlaces.VerifiedPoi.POI>();
		
		verifiedPOI = new VerifiedAOIPlaces.VerifiedPoi();
		verifiedPOI.setAoiId(aoiId);
		verifiedPOI.setName(aoisName.get(aoiId));
		verifiedPOI.setTotalCount(totalCount);
		
		if(totalCount > cursor + limit) {
			verifiedPOI.setPaging(null, Integer.toString(cursor+limit));
		}
		
		for( Sb_Pois sb_pois : verified_sb_pois ) {
			pois = sb_pois.getPois();
			if(pois != null) {
				
				poi = new VerifiedAOIPlaces.VerifiedPoi.POI();
				
				poi.setPoiId(pois.getId());
				poi.setName(pois.getName());
				
				poi.setMainImage(imageServiceImpl.getMainImage(pois.getId()));
				poi.setHashtags(hashtagServiceImpl.getHashtags(pois));
				
				if(pois.getAttrs() != null && pois.getAttrs().get("l_area_nm")!=null) {
					poi.setAbbrAddress(pois.getAttrs().get("l_area_nm").toString());
				}
				
				Geometry geo = pois.getLoc();
				Point loc = (Point) geo;
				
				double[] coordinates = {loc.getX(), loc.getY()};
				Location location = new Location(loc.getGeometryType(), coordinates);
				poi.setLocation(location);
				
				poiScoreboard = new POIScoreboard();
				poiScoreboard.setImages(sb_pois.getImagesCount());
				poiScoreboard.setPicks(sb_pois.getPickCount());
				poiScoreboard.setBlogReviews(sb_pois.getBlogReviewCount());
				poiScoreboard.setRating(sb_pois.getStarRating());
				poiScoreboard.setRater(sb_pois.getRater());
				
				poi.setScoreboard(poiScoreboard);
				
				// set tag
				String[] tagPoi = new String[2];
				List<Integer> hashtagIds = new LinkedList<Integer>();
				for(Integer i : sb_pois.getReprHashtagIds())
					hashtagIds.add(i);
				if(hashtagIds.contains(ads)) { tagPoi[0] = new String(); tagPoi[0] = "ads"; }
				if(hashtagIds.contains(verified)) { tagPoi[1] = new String(); tagPoi[1] = "verified"; }
				
				
				List<Sorder_Pois> orderPOIs = pois.getsOrderPois();
				
				TagPOI tagPOI = new TagPOI(tagPoi
											,orderPOIs.isEmpty() ? 0: 1
											,pois.getHasCoupon() ? 1 :0);
				
				poi.setTagPOI(tagPOI);
				verifiedPois.add(poi);
			}
			
		}
		verifiedPOI.setPois(verifiedPois);
		
		
		return verifiedPOI;
		
	}
	
	/**
	 * 요청된 AOI에 속하는 인증맛집 POI 수 
	 * @param aoiId
	 * @return
	 */
	private final int getVerifiedAoiForPOIsTotalCount(final int aoiId) {
		return sbPoisRepository.findByVerifiedAoiIdsTotal(aoiId);
	}
	
	
	
}
