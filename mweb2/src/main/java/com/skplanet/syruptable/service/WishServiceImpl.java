package com.skplanet.syruptable.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skplanet.syruptable.entity.user.Users;
import com.skplanet.syruptable.entity.user.Wishes;
import com.skplanet.syruptable.entity.user.Wishes_PK;
import com.skplanet.syruptable.repository.user.UsersRepository;
import com.skplanet.syruptable.repository.user.WishesRepository;
import com.skplanet.syruptable.response.user.User;

@Service
public class WishServiceImpl {
	
	@Autowired
	private WishesRepository wishesRepository;
	
	@Autowired
	private UsersRepository usersRepository;
	
	@Autowired
	private UserServiceImpl userServiceImpl;
	
	/**
	 * getWishes
	 * @param poiId
	 * @param userId
	 * @return List<Wish>
	 */
	public List<User> getWishes(Integer poiId, Integer userId) {
		List<User> wishList = null;
		List<Wishes> wishes = wishesRepository.findByPoiIdOrderByUpdatedatDesc(poiId);
		Map<Integer, String> checkWish = new HashMap<Integer, String>();
		try {
			if(wishes!=null) {
				wishList = new LinkedList<User>();
				for(Wishes w : wishes) {
					if(userServiceImpl.isFriend(userId, w.getUserId())) {
						User wish = new User();
						Users u = usersRepository.findOne(w.getUserId());
						wish.setUserId(u.getId());
						wish.setProfileImage(u.getProfileImage());
						wish.setIsFollowing(userServiceImpl.isFriend(userId, w.getUserId()));
						wishList.add(wish);
						checkWish.put(w.getUserId(), "");
					}
					if(wishList.size()>3)
						break;
				}
				
				if(wishList.size()<4)
					for(Wishes w : wishes) {
						if(checkWish.get(w.getUserId())==null) {
							User wish = new User();
							Users u = usersRepository.findOne(w.getUserId());
							wish.setUserId(u.getId());
							wish.setProfileImage(u.getProfileImage());
							wish.setIsFollowing(false);
							wishList.add(wish);
							checkWish.put(w.getUserId(), "");
						}
						if(wishList.size()>3)
							break;
					}
				if(wishList.size()<1)
					wishList=null;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return wishList;
	}
	
	/**
	 * getWishes Count
	 * @param poiId
	 * @return Integer
	 */
	public Integer getWishesCount(Integer poiId) {
		int count = 0;
		List<Wishes> wishes = wishesRepository.findByPoiIdOrderByUpdatedatDesc(poiId);
		if(wishes!=null)
			count = wishes.size();
		return count;
	}
	
	/**
	 * getIsWished
	 * @param poiId
	 * @param userId
	 * @return
	 */
	public Boolean getIsWished(Integer poiId, Integer userId) {
		Wishes wishes = wishesRepository.findOne(new Wishes_PK(userId, poiId));
		if(wishes!=null)
			return true;
		else
			return false;
	}
}