package com.skplanet.syruptable.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skplanet.syruptable.entity.category.Categories;
import com.skplanet.syruptable.entity.poi.Pois;
import com.skplanet.syruptable.repository.category.CategoriesRepository;
import com.skplanet.syruptable.response.poi.Category;

@Service
public class CategoryServiceImpl {
	
	@Autowired
	private CategoriesRepository categoriesRepository;
	
	private final String categoryL = "large";
	private final String categoryM = "medium";
	
	public Category getCategory(Pois poi) {
		if(poi!=null) {
			Categories categories = poi.getCategories();
			if(categories!=null) {
				if(categories.getmId()!=null) {
					Category category = new Category(categories.getId(), categoryM, categories.getmName()); 
					return category;
				} else {
					Category category = new Category(categories.getId(), categoryL, categories.getlName()); 
					return category;
				}
			} else
				return null;
		} else
			return null;
	}
}