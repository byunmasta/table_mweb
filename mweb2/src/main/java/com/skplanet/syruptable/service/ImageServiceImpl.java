package com.skplanet.syruptable.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skplanet.syruptable.entity.admin.Admin_Poi_Images;
import com.skplanet.syruptable.entity.pick.Pick_Images;
import com.skplanet.syruptable.entity.poi.Poi_Blog_Reviews;
import com.skplanet.syruptable.entity.sb.Sb_Picks;
import com.skplanet.syruptable.entity.sns.Sns_Poi_Images;
import com.skplanet.syruptable.entity.sns.Sns_Poi_Mappings;
import com.skplanet.syruptable.entity.sns.Sns_Poi_Mappings.Snstype;
import com.skplanet.syruptable.repository.admin.AdminPoiImagesRepository;
import com.skplanet.syruptable.repository.pick.PickImagesRepository;
import com.skplanet.syruptable.repository.poi.PoiBlogReviewsRepository;
import com.skplanet.syruptable.repository.sb.SbPicksRepository;
import com.skplanet.syruptable.repository.sns.SnsPoiMappingsRepository;
import com.skplanet.syruptable.response.image.Image;
import com.skplanet.syruptable.response.image.ImageByPaging;
import com.skplanet.syruptable.response.image.MainImage;
import com.skplanet.syruptable.response.pick.PickScoreboard;

@Service
public class ImageServiceImpl {
	
	@Autowired
	private AdminPoiImagesRepository adminPoiImagesRepository;
	
	@Autowired
	private SbPicksRepository sbPicksRepository;
	
	@Autowired
	private PickImagesRepository pickImagesRepository;
	
	@Autowired
	private SnsPoiMappingsRepository snsPoiMappingsRepository;
	
	@Autowired
	private PoiBlogReviewsRepository PoiBlogReviewsRepository;
	
	private final int mainImageSize = 10;
	
	private final String admin = "admin";
	private final String pick = "pick";
	private final String instagram = "instagram";
	private final String foursquare = "foursquare";
	private final String blog = "blog";
	
	/**
	 * getFullImages By Paging
	 * @param poiId
	 * @param cursor
	 * @param limit
	 * @return List<Image>
	 */
	public List<Image> getFullImages(Integer poiId, String cursor, Integer limit) {
		List<Image> images = this.getFullImages(poiId);
		if(images!=null && images.size()>0) {
			List<ImageByPaging> imageByPagings = new LinkedList<ImageByPaging>();
			for(int i=0; i<images.size(); i++) {
				ImageByPaging imageByPaging = new ImageByPaging();
				imageByPaging.setRownum(i);
				imageByPaging.setImage(images.get(i));
				
				imageByPagings.add(imageByPaging);
			}
			
			images = new LinkedList<Image>();
			for(ImageByPaging ibp : imageByPagings) {
				if(ibp.getRownum()>=Integer.parseInt(cursor)) {
					Image i = ibp.getImage();
					images.add(i);
				}
				
				if(images.size()==limit)
					break;
			}
		}
		return images;
	}
	
	/**
	 * getFullImages
	 * @param poiId
	 * @return List<Image>
	 */
	public List<Image> getFullImages(Integer poiId) {
		
		List<Image> images = new LinkedList<Image>();
		
		try {
			/*
				set images S
				이미지 노출 순서 : 
				1순위_어드민에서 설정한 이미지  
				2순위_시럽테이블 사용자가 업로드한 이미지(최신순)  
				3순위_인스타그램 이미지(최신순)  
				4순위_포스퀘어 이미지(최신순)  
				5순위 네이버 블로그 사진 [다음 페이지]
			*/
			Admin_Poi_Images adminPoiImage = adminPoiImagesRepository.findFirstBypoiIdOrderByPoiIdDesc(poiId);
			List<Pick_Images> pickImages = pickImagesRepository.findByPoiIdOrderByCreatedatDesc(poiId);
			List<Sns_Poi_Mappings> snspoimappingI = 
					snsPoiMappingsRepository.findByPoiIdAndSnsTypeOrderBySnsIdDesc(poiId, Snstype.I);
			List<Sns_Poi_Mappings> snspoimappingF = 
					snsPoiMappingsRepository.findByPoiIdAndSnsTypeOrderBySnsIdDesc(poiId, Snstype.F);
			List<Poi_Blog_Reviews> poiblogreviews = PoiBlogReviewsRepository.findByPoiIdOrderByUpdatedatDesc(poiId);
			
			if(adminPoiImage!=null) {
				Image i = new Image(adminPoiImage.getImage(), admin);
				images.add(i);
			} 
			
			if(pickImages!=null) {
				for(Pick_Images pickImage : pickImages) {
					Image i = new Image(pickImage.getImage(), pick);
					Sb_Picks sbPick = sbPicksRepository.findOne(pickImage.getPickId());
					int likeCount = 0, commentCount = 0;
					if(sbPick!=null) {
						likeCount = sbPick.getLikeCount();
						commentCount = sbPick.getCommentCount();
					}
					PickScoreboard scoreboard = new PickScoreboard(
							3.5, 
							null, 
							likeCount, 
							commentCount);
					i.setScoreboard(scoreboard);
					images.add(i);
				}
			}
			
			if(snspoimappingI!=null) {
				for(Sns_Poi_Mappings snspoimapping : snspoimappingI) {
					for(Sns_Poi_Images snsPoiImage :  snspoimapping.getSnsPoiImages()) {
						if (snsPoiImage.getVisible()) {
							Image i = new Image(snsPoiImage.getImageUrl(), instagram);
							images.add(i);
						}
					}
				}
			}
			
			if(snspoimappingF!=null) {
				for(Sns_Poi_Mappings snspoimapping : snspoimappingF) {
					for(Sns_Poi_Images snsPoiImage :  snspoimapping.getSnsPoiImages()) {
						if (snsPoiImage.getVisible()) {
							Image i = new Image(snsPoiImage.getImageUrl(), foursquare);
							images.add(i);
						}
					}
				}
			}
			
			if(poiblogreviews!=null) {
				for(Poi_Blog_Reviews poiBlogReview : poiblogreviews) {
					Image i = new Image(poiBlogReview.getImage(), blog);
					images.add(i);
				}
			}
			// set images E
				
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return images;
	}
	
	/**
	 * getMainImages
	 * @param poiId
	 * @return List<MainImage>
	 */
	public List<MainImage> getMainImages(Integer poiId) {
		
		List<MainImage> mainImages = new LinkedList<MainImage>();
		
		try {
			/*
				set images S
				이미지 노출 순서 : 
				1순위_어드민에서 설정한 이미지  
				2순위_시럽테이블 사용자가 업로드한 이미지(최신순)  
				3순위_인스타그램 이미지(최신순)  
				4순위_포스퀘어 이미지(최신순)  
				5순위 네이버 블로그 사진 [다음 페이지]
			*/
			Admin_Poi_Images adminPoiImage = adminPoiImagesRepository.findFirstBypoiIdOrderByPoiIdDesc(poiId);
			if(adminPoiImage!=null) {
				MainImage image = new MainImage(adminPoiImage.getImage(), admin);
				mainImages.add(image);
			} 
			
			if(mainImages.size()<mainImageSize) {
				List<Pick_Images> pickImages = pickImagesRepository.findByPoiIdOrderByCreatedatDesc(poiId);
				if(pickImages!=null) {
					for(Pick_Images pickImage : pickImages) {
						MainImage image = new MainImage(pickImage.getImage(), pick);
						mainImages.add(image);
						if(mainImages.size()>=mainImageSize) break;
					}
				}
			}
			
			if(mainImages.size()<mainImageSize) {
				List<Sns_Poi_Mappings> snspoimappingI = 
						snsPoiMappingsRepository.findByPoiIdAndSnsTypeOrderBySnsIdDesc(poiId, Snstype.I);
				if(snspoimappingI!=null) {
					for(Sns_Poi_Mappings snspoimapping : snspoimappingI) {
						for(Sns_Poi_Images snsPoiImage :  snspoimapping.getSnsPoiImages()) {
							MainImage image = new MainImage(snsPoiImage.getImageUrl(), instagram);
							mainImages.add(image);
							if(mainImages.size()>=mainImageSize) break;
						}
					}
				}
			}
			
			if(mainImages.size()<mainImageSize) {
				List<Sns_Poi_Mappings> snspoimappingF = 
						snsPoiMappingsRepository.findByPoiIdAndSnsTypeOrderBySnsIdDesc(poiId, Snstype.F);
				if(snspoimappingF!=null) {
					for(Sns_Poi_Mappings snspoimapping : snspoimappingF) {
						for(Sns_Poi_Images snsPoiImage :  snspoimapping.getSnsPoiImages()) {
							MainImage image = new MainImage(snsPoiImage.getImageUrl(), foursquare);
							mainImages.add(image);
							if(mainImages.size()>=mainImageSize) break;
						}
					}
				}
			}
			
			if(mainImages.size()==0) {
				List<Poi_Blog_Reviews> poiblogreviews = PoiBlogReviewsRepository.findByPoiIdOrderByUpdatedatDesc(poiId);
				if(poiblogreviews!=null) {
					for(Poi_Blog_Reviews poiBlogReview : poiblogreviews) {
						MainImage image = new MainImage(poiBlogReview.getImage(), blog);
						mainImages.add(image);
						if(mainImages.size()>2) break;
					}
				}
			}
			// set images E
				
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return mainImages;
	}
	
	/**
	 * getMainImage
	 * @param poiId
	 * @return MainImage
	 */
	public MainImage getMainImage(Integer poiId) {
		List<MainImage> mainImages = new LinkedList<MainImage>();
		
		try {
			/*
				set images S
				이미지 노출 순서 : 
				1순위_어드민에서 설정한 이미지  
				2순위_시럽테이블 사용자가 업로드한 이미지(최신순)  
				3순위_인스타그램 이미지(최신순)  
				4순위_포스퀘어 이미지(최신순)  
				5순위 네이버 블로그 사진 [다음 페이지]
			*/
			Admin_Poi_Images adminPoiImage = adminPoiImagesRepository.findFirstBypoiIdOrderByPoiIdDesc(poiId);
			if(adminPoiImage!=null) {
				MainImage image = new MainImage(adminPoiImage.getImage(), admin);
				mainImages.add(image);
				if(mainImages.size()>0)
					return mainImages.get(0);
			} 
			
			if(mainImages.size()<1) {
				List<Pick_Images> pickImages = pickImagesRepository.findByPoiIdOrderByCreatedatDesc(poiId);
				if(pickImages!=null) {
					for(Pick_Images pickImage : pickImages) {
						MainImage image = new MainImage(pickImage.getImage(), pick);
						mainImages.add(image);
						if(mainImages.size()>0) return mainImages.get(0);
					}
				}
			}
			
			if(mainImages.size()<mainImageSize) {
				List<Sns_Poi_Mappings> snspoimappingI = 
						snsPoiMappingsRepository.findByPoiIdAndSnsTypeOrderBySnsIdDesc(poiId, Snstype.I);
				if(snspoimappingI!=null) {
					for(Sns_Poi_Mappings snspoimapping : snspoimappingI) {
						for(Sns_Poi_Images snsPoiImage :  snspoimapping.getSnsPoiImages()) {
							if(snsPoiImage.getVisible()){
								MainImage image = new MainImage(snsPoiImage.getImageUrl(), instagram);
								mainImages.add(image);
							}
							if(mainImages.size()>0) return mainImages.get(0);
						}
					}
				}
			}
			
			if(mainImages.size()<mainImageSize) {
				List<Sns_Poi_Mappings> snspoimappingF = 
						snsPoiMappingsRepository.findByPoiIdAndSnsTypeOrderBySnsIdDesc(poiId, Snstype.F);
				if(snspoimappingF!=null) {
					for(Sns_Poi_Mappings snspoimapping : snspoimappingF) {
						for(Sns_Poi_Images snsPoiImage :  snspoimapping.getSnsPoiImages()) {
							if(snsPoiImage.getVisible()){
								MainImage image = new MainImage(snsPoiImage.getImageUrl(), foursquare);
								mainImages.add(image);
							}
							if(mainImages.size()>0) return mainImages.get(0);
						}
					}
				}
			}
			
			if(mainImages.size()==0) {
				List<Poi_Blog_Reviews> poiblogreviews = PoiBlogReviewsRepository.findByPoiIdOrderByUpdatedatDesc(poiId);
				if(poiblogreviews!=null) {
					for(Poi_Blog_Reviews poiBlogReview : poiblogreviews) {
						MainImage image = new MainImage(poiBlogReview.getImage(), blog);
						mainImages.add(image);
						if(mainImages.size()>0) return mainImages.get(0);
					}
				}
			}
			// set images E
				
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
