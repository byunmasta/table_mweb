package com.skplanet.syruptable.service;

import java.text.Normalizer;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.skplanet.syruptable.entity.story.Stories;
import com.skplanet.syruptable.entity.story.Story_Contents;
import com.skplanet.syruptable.entity.story.Story_Like_PK;
import com.skplanet.syruptable.entity.story.Story_Likes;
import com.skplanet.syruptable.entity.story.Story_Replies;
import com.skplanet.syruptable.entity.user.Users;
import com.skplanet.syruptable.repository.story.StoriesRepository;
import com.skplanet.syruptable.repository.story.StoryLikesRepository;
import com.skplanet.syruptable.repository.story.StoryRepliesRepository;
import com.skplanet.syruptable.response.image.Image;
import com.skplanet.syruptable.response.poi.BasePOI;
import com.skplanet.syruptable.response.story.Story;
import com.skplanet.syruptable.response.story.StoryDetail;
import com.skplanet.syruptable.response.story.StoryDetail.Content;
import com.skplanet.syruptable.response.story.StoryDetail.StoryContent;
import com.skplanet.syruptable.response.story.StoryDetail.StoryLike;
import com.skplanet.syruptable.response.story.StoryDetail.StoryReply;
import com.skplanet.syruptable.response.story.StoryPois;
import com.skplanet.syruptable.response.story.StoryReplies;
import com.skplanet.syruptable.response.user.User;

@Service
public class StoryServiceImpl {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private StoriesRepository storiesRepository;
	
	@Autowired
	private StoryLikesRepository storyLikesRepository;
	
	@Autowired
	private StoryRepliesRepository storyRepliesRepository;
	
	@Autowired
	private POIServiceImpl poiServiceImpl;
	
	@Autowired
	private UserServiceImpl userServiceImpl;

	
	/**
	 * getStories 
	 * @return List<StoryDetail>
	 */
	public List<StoryDetail> getStories() {
		
		List<StoryDetail> returnStories = null;
		try {
			List<Stories> stories = storiesRepository.findTop100ByIsVisibleOrderBySeqDesc(true);
			if(stories!=null) {
				returnStories = new LinkedList<StoryDetail>();
				for(Stories s : stories) {
					if(s.getIsVisible()) {
						StoryDetail storyDetail = this.getStoryContents(s.getId());
						returnStories.add(storyDetail);
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return returnStories;
	}
	
	/**
	 * getStoryContents
	 * @param storyId
	 * @return StoryDetail
	 */
	public StoryDetail getStoryContents(final Integer storyId) {
		StoryDetail returnStory = null;
		
		List<StoryLike> returnLikes = new LinkedList<StoryLike>();
		List<StoryReply> returnReplies = new LinkedList<StoryReply>();
		List<StoryContent> returnContents = new LinkedList<StoryContent>();
		
		try {
			Stories s = storiesRepository.findOne(storyId);
			if(s!=null) {
				returnStory = new StoryDetail();
				returnStory.setStoryId(storyId);
				
				User user = new User();
				
				// set user info
				Users users = s.getUser();
				List<Story_Likes> storyLikes = s.getStoryLikes();
				List<Story_Replies> storyReplies = s.getStoryReplies();
				List<Story_Contents> storyContents = s.getStoryContents();
				
				returnStory.setTitle(Normalizer.normalize(s.getTitle(), Normalizer.Form.NFC));
				returnStory.setDescription(s.getDescription());
				returnStory.setViewCount(s.getViewCount());
				returnStory.setShareCount(s.getShareCount());
				returnStory.setImage(new Image(s.getImage(), "story"));
				
				user.setGender(users.getGender());
				user.setUserId(users.getId());
				user.setNickname(users.getNickname());
				user.setProfileImage(users.getProfileImage());
				returnStory.setUser(user);
				
				if(storyLikes!=null)
					returnStory.setLikeCount(storyLikes.size());
				if(storyReplies!=null)
					returnStory.setReplyCount(storyReplies.size());
				if(storyContents!=null)
					returnStory.setLikeCount(storyLikes.size());
				
				// set like
				for(Story_Likes sl : storyLikes) {
					StoryLike storyLike = new StoryLike();
					storyLike.setStoryId(sl.getStoryId());
					storyLike.setUser(userServiceImpl.getUser(sl.getUser()));
					
					returnLikes.add(storyLike);
				}
				returnStory.setStoryLikes(returnLikes);
				
				// set replies
				for(Story_Replies sr : storyReplies) {
					StoryReply storyReply = new StoryReply();
					storyReply.setId(sr.getId());
					storyReply.setStoryId(sr.getStoryId());
					storyReply.setUser(userServiceImpl.getUser(sr.getUser()));
					storyReply.setReply(sr.getReply());
					storyReply.setCreatedat(sr.getCreatedat());
					
					returnReplies.add(storyReply);
				}
				returnStory.setStoryReplies(returnReplies);
				
				// set story contents
				Map<Integer, String> poiMap = new HashMap<Integer, String>();
				List<Integer> pois = new LinkedList<Integer>();
				for(Story_Contents sc : storyContents) {
					StoryContent storyContent = new StoryContent();
					storyContent.setMedia(sc.getMedia());
					storyContent.setPage(sc.getPage());
					storyContent.setStoryId(sc.getStoryId());
					storyContent.setType(sc.getType().toString());
					
					List<Content> contents = new LinkedList<StoryDetail.Content>();
					int[] poiA = sc.getPoiIds();
					String[] contentA = sc.getContents();
					int poiLength = 0;
					int contentLength = 0;
					if(poiA!=null)
						poiLength = poiA.length;
					if(contentA!=null)
						contentLength = contentA.length;
					if(sc.getType().toString().equals(Story_Contents.Stroytype.POTE.toString())) {
						if(poiLength!=0) {
							for(int i=0; i<poiLength; i++) {
								if(poiA[i]!=0) {
									if(poiMap.get(poiA[i])==null) {
										poiMap.put(poiA[i], "");
										pois.add(poiA[i]);
									}
									
									Content content = new Content();
									content.setPoiId(poiA[i]);
									if(i<contentA.length)
										content.setContents(contentA[i]);
									contents.add(content);
									
									BasePOI poiSummary = poiServiceImpl.getBasePOI(poiA[i]);
									if(poiSummary!=null) {
										content.setPoiDetail(poiSummary);
									}
									
									storyContent.setContents(contents);
								}
							}
							returnStory.setPois(pois);
						}
					} else if(sc.getType().toString().equals(Story_Contents.Stroytype.TEXT.toString()) 
							|| sc.getType().toString().equals(Story_Contents.Stroytype.IMTE.toString())
							|| sc.getType().toString().equals(Story_Contents.Stroytype.MDTE.toString())) {
						if(contentLength!=0) {
							for(int i=0; i<contentLength; i++) {
								Content content = new Content();
								content.setContents(contentA[i]);
								contents.add(content);
							}
							storyContent.setContents(contents);
						}
					}
					
					returnContents.add(storyContent);
				}
				
				// get another stories
				List<Story> anothers = null;
				List<Stories> stories = storiesRepository.findTop100ByIsVisibleOrderBySeqDesc(true);
				if(stories!=null) {
					anothers = new LinkedList<Story>();
					Collections.shuffle(stories);
					
					for(Stories st : stories) {
						if(!returnStory.getStoryId().equals(st.getId())) {
							Story story = new Story();
							story.setStoryId(st.getId());
							story.setTitle(st.getTitle());
							story.setUser(userServiceImpl.getUser(st.getUser()));
							story.setImage(new Image(st.getImage(), "story"));;
							
							anothers.add(story);
						}
						
						if(anothers.size()>1)
							break;
					}
					returnStory.setAnotherStories(anothers);
				}
				
				returnStory.setPoiCount(poiMap.size());
				returnStory.setStoryContents(returnContents);
				
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return returnStory;
	}
	
	/**
	 * getStoryReplies
	 * @param storyId
	 * @return List<StoryReply>
	 */
	public StoryReplies getStoryReplies(final Integer storyId, final Integer userId, final int cursor, final int limit) {
		StoryReplies storyReplies = new StoryReplies();
		List<StoryReply> storyReplyList = new LinkedList<StoryReply>();
		
		int totalCount = storyRepliesRepository.countByStoryId(storyId);
		
		try {
			List<Story_Replies> story_replies = storyRepliesRepository.findByStoryId(storyId, cursor, limit);
			
			if(story_replies.size() > 0) {
				// set replies
				for(Story_Replies sr : story_replies) {
					StoryReply storyReply = new StoryReply();
					storyReply.setId(sr.getId());
					storyReply.setStoryId(sr.getStoryId());
					storyReply.setUser(userServiceImpl.getUser(sr.getUser()));
					storyReply.setReply(sr.getReply());
					storyReply.setCreatedat(sr.getCreatedat());
					storyReply.setOwner(sr.getUser().getId().equals(userId));
					
					storyReplyList.add(storyReply);
				}
				
				//next값이 있는경우 
				if(totalCount > cursor + limit) {
					storyReplies.setPaging(null, Integer.toString(cursor+limit));
				}
				
				storyReplies.setTotalCount(totalCount);
				storyReplies.setStoryReplies(storyReplyList);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return storyReplies;
	}
	
	/**
	 * getStoryPois
	 * @param storyId
	 * @return StoryDetail
	 */
	@Cacheable("storyPois")
	public StoryPois getStoryPois(final Integer storyId, final int cursor, final int limit) {
		StoryPois storyPois = new StoryPois();
		
		try {
			Stories s = storiesRepository.findOne(storyId);
			Set<Integer> poiIds = new HashSet<Integer>();
			List<BasePOI> pois = new LinkedList<BasePOI>();
			
			if(s!=null) {	
				for(Story_Contents sc : s.getStoryContents()) {
					if(sc.getType().toString().equals(Story_Contents.Stroytype.POTE.toString())) {
						for(int id : sc.getPoiIds()){
							poiIds.add(id);
						}
					}
				}
				for(int i=0; i<limit+1; i++){
					if(cursor+i < poiIds.size()){
						BasePOI poiSummary = poiServiceImpl.getBasePOI((int)poiIds.toArray()[cursor+i]);
						if(poiSummary!=null) {
							pois.add(poiSummary);
						}
					}
				}
				//next값이 있는경우 
				if(pois.size()>limit){
					pois.remove(pois.size()-1);
					storyPois.setPaging("", String.valueOf(cursor+limit));
				}else{
					storyPois.setPaging("", "");
				}
				storyPois.setTotalCount(pois.size());
				storyPois.setPois(pois);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return storyPois;
	}
	
	
	/**
	 * isLike
	 * @param storyId
	 * @param userId
	 * @return boolean
	 */
	public boolean isLiked(Integer storyId, Integer userId) {
		Story_Likes storyLike = null;
		try {
			storyLike = storyLikesRepository.findOne(new Story_Like_PK(storyId, userId));
			if(storyLike != null) {
				return true;
			} else {
				return false;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * likeAction
	 * @param storyId
	 * @param userId
	 * @return Story_Likes
	 */
	@Transactional
	public Story_Likes likeAction(Integer storyId, Integer userId) {
		Story_Likes storyLike = null;
		try {
			storyLike = storyLikesRepository.findOne(new Story_Like_PK(storyId, userId));
			
			if(storyLike!=null) {
				storyLikesRepository.delete(storyLike);
			} else {
				storyLike = new Story_Likes();
				storyLike.setStoryId(storyId);
				storyLike.setUserId(userId);
				storyLike = storyLikesRepository.save(storyLike);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return storyLike;
	}
	
	/**
	 * viewAction
	 * @param storyId
	 * @return Stories
	 */
	@Transactional
	public Stories viewAction(Integer storyId) {
		
		Stories stories = null;
		try {
			stories = storiesRepository.findOne(storyId);
			
			if(stories!=null) {
				stories.setViewCount(stories.getViewCount()+1);
				stories = storiesRepository.save(stories);
			} 
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stories;
	}
	
	/**
	 * shareAction
	 * @param storyId
	 * @return Stories
	 */
	@Transactional
	public Stories shareAction(Integer storyId) {
		
		Stories stories = null;
		try {
			stories = storiesRepository.findOne(storyId);
			
			if(stories!=null) {
				stories.setShareCount(stories.getShareCount()+1);
				stories = storiesRepository.save(stories);
			} 
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stories;
	}
	
	//댓글 id있으면 수정 없으면 등록
	public Story_Replies setReply(
			Integer storyId,
			Integer userId,
			Integer replyId,
			String reply) {
		
		Story_Replies storyReplies = null;
		if(replyId == null){ //insert
			storyReplies = new Story_Replies();
			storyReplies.setStoryId(storyId);
			storyReplies.setUserId(userId);
			storyReplies.setReply(reply);
			
		}else{	//update
			int replierId = storyRepliesRepository.findUserIdByReplyId(replyId);
			if(replierId != userId){
				return null;
			}
			storyReplies = storyRepliesRepository.findOne(replyId);
			if(storyReplies != null){
				storyReplies.setReply(reply);
			}
		}
		
		try {
			storyRepliesRepository.save(storyReplies);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return storyReplies;
	}
	
	//댓글쓰기
	public Story_Replies setReplyWrite(
			Integer storyId,
			Integer userId,
			String reply) {
		
		Story_Replies storyReplies = new Story_Replies();
		storyReplies.setStoryId(storyId);
		storyReplies.setUserId(userId);
		storyReplies.setReply(reply);
		try {
			storyRepliesRepository.save(storyReplies);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return storyReplies;
	}
	
	//댓글수정
	public Story_Replies setReplyModify(
			Integer replyId,
			Integer userId,
			String reply) {
		
		int replierId = storyRepliesRepository.findUserIdByReplyId(replyId);
		if(replierId != userId){
			return null;
		}
		Story_Replies storyReply = storyRepliesRepository.findOne(replyId);
		if(storyReply != null){
			storyReply.setReply(reply);
			try {
				storyRepliesRepository.save(storyReply);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return storyReply;
	}
	
	//댓글삭제
	public Story_Replies setReplyDelete(
			Integer replyId,
			Integer userId) {
		
		int replierId = storyRepliesRepository.findUserIdByReplyId(replyId);
		if(replierId != userId){
			return null;
		}
		Story_Replies storyReplies = storyRepliesRepository.findOne(replyId);
		try {
			if(storyReplies!=null)
				storyRepliesRepository.delete(storyReplies);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return storyReplies;
	}
	
	
	// 스토리 대표이미지 가져오기
	public String getStoryImage(final Integer storyId) {
		try {
			Stories s = storiesRepository.findOne(storyId);
			if (s != null) {
				return s.getImage();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean isValidStory(int storyId){
		if(storiesRepository.findOne(storyId)==null){
			return false;
		}
		return true;
	}
}
