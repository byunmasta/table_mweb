package com.skplanet.syruptable.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.skplanet.syruptable.entity.aoi.Aois;
import com.skplanet.syruptable.entity.etc.Suggestions;
import com.skplanet.syruptable.entity.pick.Pick_Images;
import com.skplanet.syruptable.entity.pick.Picks;
import com.skplanet.syruptable.entity.poi.Poi_Ratings;
import com.skplanet.syruptable.entity.poi.Poi_Ratings_PK;
import com.skplanet.syruptable.entity.sb.Coupon_Cache;
import com.skplanet.syruptable.entity.sb.Sb_Pois;
import com.skplanet.syruptable.entity.sorder.Sorder_Pois;
import com.skplanet.syruptable.entity.user.Users;
import com.skplanet.syruptable.repository.aoi.AoisRepository;
import com.skplanet.syruptable.repository.etc.CouponCacheRepository;
import com.skplanet.syruptable.repository.etc.SuggestionsRepository;
import com.skplanet.syruptable.repository.pick.PickImagesRepository;
import com.skplanet.syruptable.repository.pick.PicksRepository;
import com.skplanet.syruptable.repository.poi.PoiRatingsRepository;
import com.skplanet.syruptable.repository.sb.SbPoisRepository;
import com.skplanet.syruptable.response.BaseMain;
import com.skplanet.syruptable.response.BaseMain.Suggestion;
import com.skplanet.syruptable.response.BaseMain.TodayLunch;
import com.skplanet.syruptable.response.BaseMain.TodayLunch.LunchPOI;
import com.skplanet.syruptable.response.image.Image;
import com.skplanet.syruptable.response.image.MainImage;
import com.skplanet.syruptable.response.main.MainAround;
import com.skplanet.syruptable.response.main.MainAroundRaking;
import com.skplanet.syruptable.response.main.MainAroundRaking.RakingPOI;
import com.skplanet.syruptable.response.main.MainAroundRaking.RakingPOI.POIHashTag;
import com.skplanet.syruptable.response.pick.Pick;
import com.skplanet.syruptable.response.pick.PickScoreboard;
import com.skplanet.syruptable.response.poi.Location;
import com.skplanet.syruptable.response.poi.POI;
import com.skplanet.syruptable.response.poi.POIScoreboard;
import com.skplanet.syruptable.response.poi.TagPOI;
import com.skplanet.syruptable.response.user.User;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

import syruptable.model.common.SyrupTable;

@Service
public class APIServiceImpl {

	
	@Autowired
	private AoisRepository readAoisRepository;
	
	@Autowired
	private SbPoisRepository sbPoisRepository;
	
	@Autowired
	private SbPoisRepository readSbPoisRepository;
	
	@Autowired
	private PicksRepository readPicksRepository;
	
	@Autowired
	private PoiRatingsRepository readPoiRatingsRepository;
	
	@Autowired
	private PickImagesRepository readPickImagesRepository;
	
	@Autowired
	private SuggestionsRepository readSuggestionsRepository;
	
	@Autowired
	private CouponCacheRepository couponCacheRepository;
	
	@Autowired
	private HashtagServiceImpl hashtagServiceImpl;
	
	@Autowired
	private ImageServiceImpl imageServiceImpl;
	
	// 광고 hashtag
	private static final int ads = 11; 
	
	// 인증 hashtag
	private static final int verified = 12; 
	
	public Aois getRBSPZone(Point point) {
		
		return readAoisRepository.getAoiId(point);
	}
	
	public TodayLunch[] getTodayLunch() {
		
		TodayLunch[] todayLunch = new TodayLunch[3];

		todayLunch[0] = new TodayLunch();
		todayLunch[0].setDescription("매콤매콤 스트레스 날리자!");
		todayLunch[0].setMenuTitle("매운낙지");

		LunchPOI[] lunchPOI = new LunchPOI[3];
		lunchPOI[0] = new LunchPOI(3876995, "무교동유정낙지 서초점");
		lunchPOI[1] = new LunchPOI(1343822, "뱃고동 강남역점");
		lunchPOI[2] = new LunchPOI(10801921, "난공불낙");

		todayLunch[0].setLunchPOI(lunchPOI);
		todayLunch[0].setBackgroundImage("http://rol-dev.skplanet.com/table/etc/v3_bg_food_woodong@2x.png");
		todayLunch[0].setMenuImage("http://rol-dev.skplanet.com/table/etc/v3_bg_text_woodong@2x.png");
		
		
		/*todayLunch[0]
				.setImage(new Image(
						480,
						320,
						"http://rol-dev.skplanet.com/table",
						"/pick/0/3/20121121/16850657668bb6305fdad8b6db7dc11b89e46b9e.png",
						"pick"));*/

		todayLunch[1] = new TodayLunch();
		todayLunch[1].setDescription("뽀오얀 국물 한그릇 뚝딱!");
		todayLunch[1].setMenuTitle("든든한 설렁탕 ");

		LunchPOI[] lunchPOI2 = new LunchPOI[3];
		lunchPOI2[0] = new LunchPOI(5641375, "우리집만두");
		lunchPOI2[1] = new LunchPOI(1004003402, "방스만두");
		lunchPOI2[2] = new LunchPOI(1004452358, "북촌손만두 킵유어포크점");

		todayLunch[1].setLunchPOI(lunchPOI2);
		todayLunch[1].setBackgroundImage("http://rol-dev.skplanet.com/table/etc/v3_bg_food_woodong@2x.png");
		todayLunch[1].setMenuImage("http://rol-dev.skplanet.com/table/etc/v3_bg_text_woodong@2x.png");
		/*todayLunch[1]
				.setImage(new Image(
						480,
						320,
						"http://rol-dev.skplanet.com/table",
						"/pick/0/3/20121121/24ff3a28bfd4aa81b700114a1708ba5b7b9df542.png",
						"pick"));*/

		todayLunch[2] = new TodayLunch();
		todayLunch[2].setDescription("오늘 점심");
		todayLunch[2].setMenuTitle("강남교자");

		LunchPOI[] lunchPOI3 = new LunchPOI[3];
		lunchPOI3[0] = new LunchPOI(1045997, "강남교자");
		lunchPOI3[1] = new LunchPOI(6570985, "음악국수집");
		lunchPOI3[2] = new LunchPOI(5724784, "들름집");

		todayLunch[2].setLunchPOI(lunchPOI3);
		todayLunch[2].setBackgroundImage("http://rol-dev.skplanet.com/table/etc/v3_bg_food_woodong@2x.png");
		todayLunch[2].setMenuImage("http://rol-dev.skplanet.com/table/etc/v3_bg_text_woodong@2x.png");
//		todayLunch[2]
//				.setImage(new Image(
//						480,
//						360,
//						"http://rol-dev.skplanet.com/table",
//						"/pick/0/3/20121121/116be54d67fc3dc3c6a7036c1c73f1ee96b5c178.png",
//						"pick"));

		
		return todayLunch;
	}
	
	public List<Suggestion> getSuggestion(Integer aoiId) {
		
		List<Integer> aoiids = new ArrayList<Integer>();
		aoiids.add(0);
		aoiids.add(118101);  // 서브 카테고리가 있는 샘플직역임 
//		aoiids.add(aoiId);
		
		/**
		 * 전달받은 aoi
		 */
		int isCoupon = sbPoisRepository.isCoupon(24823) != null ? 1 : 0;
		
		List<Suggestions> list = readSuggestionsRepository.findByAoiIdInOrderByIdDesc(aoiids);
		
		Map<String, String> poiViewType = new LinkedHashMap<String, String>();
		poiViewType.put("LATEST", "최근리뷰");
		poiViewType.put("RAKING", "랭킹");

		/**
		 * 바로주문은 어떻게 가져올까..대박..
		 */
		poiViewType.put("ORDER", "바로주문");

		Map<Integer, String> keywords = new LinkedHashMap<Integer, String>();
		keywords.put(51, "샌드위치");
		keywords.put(515, "화끈한매운맛");
		keywords.put(78, "분식");
		keywords.put(566, "TV맛집");
		keywords.put(557, "달콤디저트");
		keywords.put(572, "일본식라멘");

		Suggestion[] poiType = new Suggestion[4];
		poiType[0] = new Suggestion();
		poiType[0].setSort("latest");
		poiType[0].setName("최근인기");

		poiType[1] = new Suggestion();
		poiType[1].setSort("raking");
		poiType[1].setName("랭킹");

		poiType[2] = new Suggestion();
		poiType[2].setOrder(1);
		poiType[2].setName("바로주문");

		poiType[3] = new Suggestion();
		poiType[3].setIsCoupon(1);
		poiType[3].setName("쿠폰");

		List<Suggestion> suggestionList = new LinkedList<Suggestion>();
		suggestionList.add(poiType[0]);
		suggestionList.add(poiType[1]);
		suggestionList.add(poiType[2]);
		
		if(isCoupon > 0) {
			suggestionList.add(poiType[3]);
		}
		
		ObjectMapper mapper = new ObjectMapper();
		
		for(Suggestions suggestion : list) {
		
			Suggestion view = mapper.convertValue(suggestion.getParams(), Suggestion.class);
			view.setName(suggestion.getName());
			if(view.getKeywordId() > 0) {
				view.setName(keywords.get(view.getKeywordId()));
			}
			
			suggestionList.add(view);
			
		}
		
		return suggestionList;
	}
	
	
	private <T> BaseMain getBaseMain(final SyrupTable syrupTable, final T baseMain) {
		
		final BaseMain base = (BaseMain) baseMain;
		
		base.setAoi(this.getRBSPZone(syrupTable.getPoint()));
		base.setTodayLunch(this.getTodayLunch());
		
		List<Suggestion> suggestionList = this.getSuggestion(base.getAoi().getAoiId());
		base.setSuggestion(suggestionList.toArray(new Suggestion[suggestionList.size()]));
		
		return base;
	}
	
	
	public MainAround getMainPlace(final SyrupTable syrupTable) {
		
		final MainAround mainAround = (MainAround) this.getBaseMain(syrupTable, new MainAround());
				
		int aoi_id = 24872;
						
		List<Sb_Pois> pageSbpois = readSbPoisRepository.findByAoiIdsOrderByScoreDesc(aoi_id, 0, syrupTable.getLimit());
		mainAround.setTotalCount(readSbPoisRepository.countByAoiIds(aoi_id));
		mainAround.setPaging("", "khSA");
		
//		System.out.println("+++++++" + pageSbpois.size());
		
		POI[] poi = new POI[pageSbpois.size()];
		
		for(int i = 0; i < pageSbpois.size(); i++) {	
		
			Sb_Pois sbPoi = pageSbpois.get(i);
			poi[i] = new POI();
			
			poi[i].setName(sbPoi.getPoiName());
			poi[i].setPoiId(sbPoi.getPoiId());
			
			Geometry geo = sbPoi.getPois().getLoc();
			Point loc = (Point) geo;
			
			double[] coordinates = {loc.getX(), loc.getY()};
			Location location = new Location(loc.getGeometryType(), coordinates);
			
			poi[i].setLocation(location);
			
			poi[i].setMainImage(imageServiceImpl.getMainImage(sbPoi.getPoiId()));
			
			POIScoreboard poiScoreboard = new POIScoreboard();
			poiScoreboard.setImages(sbPoi.getImagesCount());
			poiScoreboard.setPicks(sbPoi.getPickCount());
			poiScoreboard.setBlogReviews(sbPoi.getBlogReviewCount());
			poi[i].setScoreboard(poiScoreboard);
			Picks picks = readPicksRepository.findFirstByPoiIdOrderByUpdatedatDesc(sbPoi.getPoiId());
			
			Pick pick = null;
			if(picks != null) {
			
			
				pick = new Pick();
				Users users = picks.getUsers();
				
				User editor = new User();
				editor.setNickname(users.getNickname());
				editor.setProfileImage("http://file.pickat.com/files/0/1/20140728/e607eb8a9a52fa8da483edfcba236941b4d6196b");
				editor.setUserId(users.getId());
				editor.setBadge(users.getBadge());
				editor.setGender(users.getGender());
				
				pick.setEditor(editor);
				pick.setContents(picks.getContents());
				
				List<Pick_Images> pickImages = picks.getPickImages();
	//					if(pickImages.size() > 0) {
					List<Image> images = new LinkedList<Image>();
					images.add(new Image("http://rol-dev.skplanet.com/table/pick/0/3/20121121/16850657668bb6305fdad8b6db7dc11b89e46b9e.png", "pick"));
					images.add(new Image("http://rol-dev.skplanet.com/table/pick/0/3/20121121/24ff3a28bfd4aa81b700114a1708ba5b7b9df542.png", "pick"));
					images.add(new Image("http://rol-dev.skplanet.com/table/pick/0/3/20121121/99d1a6590f2f07b599abcd68d79c9b238a544019.png", "pick"));
					images.add(new Image("/http://rol-dev.skplanet.com/tablepick/0/3/20121121/16850657668bb6305fdad8b6db7dc11b89e46b9e.png", "pick"));
					pick.setImages(images);
	//					}
				
				
				PickScoreboard pickScoreboard = new PickScoreboard();
				pickScoreboard.setImages(pickImages.size());
				
				Poi_Ratings poiRations = readPoiRatingsRepository.findOne(new Poi_Ratings_PK(picks.getUserId(), picks.getPoiId()));
				if(poiRations != null) {
					pickScoreboard.setRating(3.0);
					
				//	System.out.println("평가 :  " + poiRations.getRating());
				} 
				pick.setScoreboard(pickScoreboard);
				
				poi[i].setPick(pick);
				poi[i].setHashtags(hashtagServiceImpl.getHashtags(sbPoi.getPois()));
			}
	
		}
	
		mainAround.setPois(poi);
			
	return mainAround;
	
	}
	
	
	public MainAroundRaking getMainRaking(final SyrupTable syrupTable) {
		
		final MainAroundRaking aroundRaking = (MainAroundRaking) this.getBaseMain(syrupTable, new MainAroundRaking());
		
		int aoi_id = 24872;
		
		List<MainImage> mainImage = new LinkedList<MainImage>();
 		
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/16850657668bb6305fdad8b6db7dc11b89e46b9e","pick"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/116be54d67fc3dc3c6a7036c1c73f1ee96b5c178","instagram"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/9b2c72315b1a5b2f5a88a85214b0680baf04cd8a","foursquare"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/ae0f7ebcf4ee90ffc8a6ed7d734060fbf25155a7","pick"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/4bc06b8a51e7cfa7959fb99461952f44c0e05805","pick"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/35a3e1bf42c2889f13b7bd66da8b3487f115a2e8","pick"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/7b0e95b3faac595f3a82c01d209e1ff67ed84546","instagram"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/952321de8d9ea7a15d15ee61f05006404727a58e","pick"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/99d1a6590f2f07b599abcd68d79c9b238a544019","foursquare"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/16850657668bb6305fdad8b6db7dc11b89e46b9e","pick"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/16850657668bb6305fdad8b6db7dc11b89e46b9e","pick"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/116be54d67fc3dc3c6a7036c1c73f1ee96b5c178","instagram"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/9b2c72315b1a5b2f5a88a85214b0680baf04cd8a","foursquare"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/ae0f7ebcf4ee90ffc8a6ed7d734060fbf25155a7","pick"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/4bc06b8a51e7cfa7959fb99461952f44c0e05805","pick"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/35a3e1bf42c2889f13b7bd66da8b3487f115a2e8","pick"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/7b0e95b3faac595f3a82c01d209e1ff67ed84546","instagram"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/952321de8d9ea7a15d15ee61f05006404727a58e","pick"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/99d1a6590f2f07b599abcd68d79c9b238a544019","foursquare"));
		mainImage.add(new MainImage("http://file.pickat.com/files/0/3/20121121/16850657668bb6305fdad8b6db7dc11b89e46b9e","pick"));
	
		List<POIHashTag> hashtags = new LinkedList<POIHashTag>();
		
		hashtags.add(new POIHashTag(1, "걍먹어"));
		hashtags.add(new POIHashTag(2, "처먹어"));
		hashtags.add(new POIHashTag(3, "닥치고먹어"));
		
		
		List<Sb_Pois> pageSbpois = null;
		
		if(syrupTable.getHasCoupon() > 0) {
			pageSbpois = readSbPoisRepository.findByHasCouponForAoiIds(aoi_id, 0, syrupTable.getLimit());
		}
		
		if("".equals(syrupTable.getCategoryId())) {
			
		}
		
		if(syrupTable.getHashtagId() > 0) {
			
		}
		
		if(pageSbpois == null) {
			pageSbpois = readSbPoisRepository.findByAoiIdsOrderByScoreDesc(aoi_id, 0, syrupTable.getLimit());
		}
		
		aroundRaking.setTotalCount(readSbPoisRepository.countByAoiIds(aoi_id));
		aroundRaking.setPaging("khSA", "");
		
//		RakingPOI[] pois = new RakingPOI[pageSbpois.size()]; 
		
		List<RakingPOI> pois = new LinkedList<RakingPOI>();
		
		int raking = 1;
		for(int i = 0; i < pageSbpois.size(); i++) {	
			Sb_Pois sbPoi = pageSbpois.get(i);
			RakingPOI poi = new RakingPOI();
			poi.setName(sbPoi.getPoiName());
			poi.setMainImage(mainImage.get(i));
			poi.setRaking(raking++);
			
			MainAroundRaking.RakingPOI.POIScoreboard  poiScoreboard = new MainAroundRaking.RakingPOI.POIScoreboard();
			
			poiScoreboard.setImages(sbPoi.getImagesCount());
			poiScoreboard.setPicks(sbPoi.getPickCount());
			poiScoreboard.setBlogReviews(sbPoi.getBlogReviewCount());
			poiScoreboard.setRating(4.0);
			poiScoreboard.setRater(sbPoi.getWishCount());
			poi.setScoreboard(poiScoreboard);
			
			
			
			Location location = new Location(sbPoi.getPois().getLoc());
			poi.setLocation(location);
			
			
			// set tag
			String[] tagPoi = new String[2];;
			List<Integer> hashtagIds = new LinkedList<Integer>();
			for(Integer h : sbPoi.getReprHashtagIds())
				hashtagIds.add(h);
			if(hashtagIds.contains(ads)) { tagPoi[0] = new String(); tagPoi[0] = "ads"; }
			if(hashtagIds.contains(verified)) { tagPoi[1] = new String(); tagPoi[1] = "verified"; }

			List<Coupon_Cache> coupon = new LinkedList<Coupon_Cache>();
			if(syrupTable.getHasCoupon() > 0 && sbPoi.getPois().getHasCoupon()) {
				//쿠폰 상세 정보 
				coupon = couponCacheRepository.findByPoiIds(sbPoi.getPoiId(), 0, 1);
//				System.out.println("나 쿠폰 있쑝 " + coupon.get(0).getData());
				
				
				JSONObject jsonObject = new JSONObject(coupon.get(0).getData());
				MainAroundRaking.RakingPOI.Coupon poiCoupon 
						= new MainAroundRaking.RakingPOI.Coupon(
											coupon.get(0).getCpName()
											,jsonObject.getString("cpImage")
											,jsonObject.getString("validSdate")
											,jsonObject.getString("validEdate"));
				
				
				poi.setCoupon(poiCoupon);
				
				
			}
//			System.out.println("쿠폰 : " + coupon.isEmpty());
			
			
			List<Sorder_Pois> orderPOIs = sbPoi.getPois().getsOrderPois();
//			System.out.println("order Info " + orderPOIs.isEmpty());
			
			TagPOI tagPOI = new TagPOI(tagPoi
										,orderPOIs.isEmpty() ? 0: 1
										,coupon.isEmpty() ? 0 : 1);
			
			
			poi.setTagPOI(tagPOI);
			
			poi.setHashtags(hashtags);
			
			pois.add(poi);
			
			
		}
		
		aroundRaking.setPois(pois);
		
		
		
		
		return aroundRaking;
	}
}
