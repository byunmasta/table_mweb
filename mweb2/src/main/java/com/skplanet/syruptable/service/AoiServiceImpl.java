package com.skplanet.syruptable.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skplanet.syruptable.entity.aoi.Aois;
import com.skplanet.syruptable.entity.sb.Sb_Pois;
import com.skplanet.syruptable.repository.aoi.AoisRepository;
import com.skplanet.syruptable.response.AOI;
import com.skplanet.syruptable.response.BaseAOI;
import com.skplanet.syruptable.response.Center;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

@Service
public class AoiServiceImpl {
	
	@Autowired
	private AoisRepository aoisRepository;
	
	public BaseAOI getAOI(Point point) {
		Aois aois = aoisRepository.getAoiId((Point) point);
		if(aois!=null) {
			BaseAOI aoi = new BaseAOI(aois.getName(), aois.getId(), aois.getCategory().toString());
			return aoi;
		} else
			return null;
	}
	
	/**
	 * getAOI Detail
	 * @param aoiId
	 * @return AOI
	 */
	public AOI getAOI(Integer aoiId) {
		AOI aoi = null;
		
		if(aoiId!=null) {
			Aois aois = aoisRepository.findOne(aoiId);
			if(aois!=null) {
				Center center = this.getCenterLocation(aois);
				com.skplanet.syruptable.response.Polygon polygon = this.getPolygonLocations(aois);
			
				aoi = new AOI();
				aoi.setAoiId(aois.getId());
				aoi.setType(aois.getCategory().toString());
				aoi.setName(aois.getName());
				aoi.setCenter(center);
				aoi.setPolygon(polygon);
			}
		}
		
		return aoi;
	}
	
	public BaseAOI getAOI(Sb_Pois sbPois) {
		//set aoiIds
		List<Integer> aoiIds = new LinkedList<Integer>();
		for(int i : sbPois.getAoiIds()) aoiIds.add(i);
		
		if(aoiIds.size()>0) {
			Aois aois = aoisRepository.findFirstByIdInOrderByLevelAsc(aoiIds);
			if(aois!=null) {
				BaseAOI aoi = new BaseAOI(aois.getName(), aois.getId(), aois.getCategory().toString());
				return aoi;
			} else
				return null;
		} else
			return null;
	}
	
	public BaseAOI getVerifiedAOI(Sb_Pois sbPois) {
		//set aoiIds
		List<Integer> aoiIds = new LinkedList<Integer>();
		for(int i : sbPois.getAoiIds()) aoiIds.add(i);
		
		if(aoiIds.size()>0) {
			Aois aois = aoisRepository.findFirstByIdInOrderByLevelDesc(aoiIds);
			if(aois!=null) {
				BaseAOI aoi = new BaseAOI(aois.getName(), aois.getId(), aois.getCategory().toString());
				return aoi;
			} else
				return null;
		} else
			return null;
	}
	
	/**
	 * getCenterLocation
	 * @param aoiId
	 * @return Double[]
	 */
	public Center getCenterLocation(Aois aoi) {
		Center center = null;
		try {
			if(aoi!=null) {
				Geometry geo = aoi.getGeom();
				if(geo!=null) {
					Point point = ((Polygon) geo).getCentroid();
					Double[] location = new Double[2];
					location[0] = point.getX();
					location[1] = point.getY();
					
					center = new Center();
					center.setType(point.getGeometryType());
					center.setCooredinates(location);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return center;
	}
	
	/**
	 * getPolygonLocations
	 * @param aoiId
	 * @return List<Double[]>
	 */
	public com.skplanet.syruptable.response.Polygon getPolygonLocations(Aois aoi) {
		com.skplanet.syruptable.response.Polygon polygon = null;
		try {
			if(aoi!=null) {
				Geometry geo = aoi.getGeom();
				if(geo!=null) {
					List<List<Double[]>> geolocations = new LinkedList<List<Double[]>>();
					List<Double[]> locations = new LinkedList<Double[]>();
					Coordinate[] coordinates = ((Polygon) geo).getCoordinates();
					for(Coordinate c : coordinates) {
						Double[] location = new Double[2];
						location[0] = c.x;
						location[1] = c.y;
						locations.add(location);
					}
					geolocations.add(locations);
					
					polygon = new com.skplanet.syruptable.response.Polygon();
					polygon.setType(geo.getGeometryType());
					polygon.setCoordinates(geolocations);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return polygon;
	}
}