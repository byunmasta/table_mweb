package com.skplanet.syruptable.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skplanet.syruptable.entity.sb.Sb_Users;
import com.skplanet.syruptable.entity.user.Users;
import com.skplanet.syruptable.repository.sb.SbUsersRepository;
import com.skplanet.syruptable.response.user.UserProfile;

@Service
public class ProfileServiceImpl {

	@Autowired
	private SbUsersRepository sbUsersRepository;
	
	/**
	 * 프로필 > 사용자 기본 정보 
	 * badge : None, Badge.CURATOR, Badge.TRAVELLER, Badge.EXPERT
	 */
	public UserProfile getProfile(Integer userId) {
		
		UserProfile userProfile = new UserProfile();

		Sb_Users sbUser = sbUsersRepository.findOne(userId);
		if( sbUser != null) {
			
			Users users = sbUser.getUsers();
			if( users != null) {
				userProfile.setId(users.getId());
				userProfile.setProfileImage(users.getProfileImage());
				if( 1024 == users.getId()) {
					userProfile.setProfileImage("http://file.pickat.com/files/0/0/20131126/f5633392a00358c4b92eb4893d6c745b13e6beaf");
				}
				userProfile.setNickname(users.getNickname());
				userProfile.setDescription(users.getDescription());
				
				userProfile.setIsFollowing(false);
				userProfile.setAlerts(999);
				userProfile.setBadge("curator");
				userProfile.setGender("1");
				
				UserProfile.UserScoreboard scoreboard = new UserProfile.UserScoreboard();
				
				scoreboard.setFollowers(sbUser.getFollowerCount());
				scoreboard.setFollowees(sbUser.getFolloweeCount());
				userProfile.setScoreboard(scoreboard);
			}
		}
		
		return userProfile;
	}
	
	
	
}
