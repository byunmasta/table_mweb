package com.skplanet.syruptable.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.skplanet.syruptable.entity.pick.Pick_Comments;
import com.skplanet.syruptable.entity.pick.Pick_Images;
import com.skplanet.syruptable.entity.pick.Pick_Likes;
import com.skplanet.syruptable.entity.pick.Picks;
import com.skplanet.syruptable.entity.sb.Sb_Picks;
import com.skplanet.syruptable.entity.user.Users;
import com.skplanet.syruptable.repository.pick.PicksRepository;
import com.skplanet.syruptable.repository.user.UsersRepository;
import com.skplanet.syruptable.response.Hashtag;
import com.skplanet.syruptable.response.image.Image;
import com.skplanet.syruptable.response.pick.Comment;
import com.skplanet.syruptable.response.pick.Pick;
import com.skplanet.syruptable.response.pick.PickScoreboard;
import com.skplanet.syruptable.response.poi.BasePOI;
import com.skplanet.syruptable.response.poi.POI;
import com.skplanet.syruptable.response.user.User;

import syruptable.model.common.SyrupTable;

@Service
public class PickServiceImpl {
	
	@Autowired
	private PicksRepository picksRepository;
	
	@Autowired
	private UsersRepository usersRepository;
	
	@Autowired
	private POIServiceImpl poiServiceImpl;
	
	@Autowired
	private UserServiceImpl userServiceImpl;
	
	private final String pickType = "pick";
	
	public enum Type {
		poiDetail,
		pickList
	}
	
	/**
	 * getPickList
	 * @param sbPois
	 * @param userId
	 * @param type
	 * @return List<Pick>
	 */
	public List<Pick> getPickList(
			final SyrupTable syrupTable, 
			Type type) {
		List<Pick> reviews = null;
		
		//remove after
		Integer userId=1;
		//remove after
		
		String cursor = "0";
		if(syrupTable.getCursor()!=null && !"".equals(syrupTable.getCursor()))
			cursor = syrupTable.getCursor();
		
		int limit = 20;
		if(syrupTable.getLimit()!=0)
			limit = syrupTable.getLimit();
		
		try {
			Page<Picks> picks = picksRepository.findByPoiId(
					syrupTable.getPoiId(), 
					new PageRequest(Integer.parseInt(cursor), limit, 
							new Sort(Direction.DESC, "createdat")));
			if(picks!=null) {
				reviews = new LinkedList<Pick>();
				for(Picks pick : picks) {
					
					// set editor
					User editor = userServiceImpl.getUser(pick);
					editor.setIsFollowing(userServiceImpl.isFriend(userId, editor.getUserId()));
					
					// set images
					List<Image> reviewImages = new LinkedList<Image>();
					List<Pick_Images> pi = pick.getPickImages();
					if(pi!=null) {
						for(Pick_Images i : pick.getPickImages()) {
							reviewImages.add(new Image(i.getImage(), pickType));
							if(reviewImages.size()>2 && Type.poiDetail.equals(type))
								break;
						}
					}
					
					// set scoreboard
					Sb_Picks sbPicks = pick.getSbpicks();
					PickScoreboard scoreboard = new PickScoreboard(
							3.5,
							sbPicks.getImageCount(), 
							sbPicks.getLikeCount(),
							sbPicks.getCommentCount());
					
					// set comments
					List<Comment> comments = null;
					List<Comment> tempComments = this.getComments(pick);
					if(tempComments!=null && tempComments.size()>1 && Type.poiDetail.equals(type)) {
						comments = new LinkedList<Comment>();
						comments.add(tempComments.get(0));
						comments.add(tempComments.get(1));
					} else
						comments = tempComments;
					
					// set hashtags
					List<Hashtag> hashtags = new LinkedList<Hashtag>();
					hashtags.add(new Hashtag(1, "걍먹어"));
					hashtags.add(new Hashtag(2, "처먹어"));
					hashtags.add(new Hashtag(3, "막먹어"));
					
					Pick pickDetail = new Pick();
					pickDetail.setPickId(pick.getId());
					pickDetail.setContents(pick.getContents());
					pickDetail.setCreatedAt(pick.getUpdatedat());
					pickDetail.setScoreboard(scoreboard);
					pickDetail.setHashtags(hashtags);
					pickDetail.setEditor(editor);
					pickDetail.setImages(reviewImages);
					pickDetail.setComments(comments);
					
					reviews.add(pickDetail);
					
					if(reviews.size()>3 && Type.poiDetail.equals(type))
						break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return reviews;
	}
	
	/**
	 * getPickDetail
	 * @param pickId
	 * @param userId
	 * @return Pick
	 */
	public Pick getPickDetail(Integer pickId, Integer userId) {
		Pick pickDetail = null;
		
		List<Image> images = null;
		List<Comment> comments = null;
		List<User> wishes = null;
		
		try {
			Picks pick = picksRepository.findOne(pickId);
			if(pick!=null) {
				pickDetail = new Pick();
				
				List<Pick_Images> pickImages = pick.getPickImages();
				List<Pick_Likes> pickLikes = pick.getPickLikes();
				List<Pick_Comments> pickComments = pick.getPickComments();
				Sb_Picks sbPick = pick.getSbpicks();
				
				pickDetail.setPickId(pickId);
				pickDetail.setContents(pick.getContents());
				pickDetail.setCreatedAt(pick.getUpdatedat());
				
				// set POI
				POI poi = new POI();
				BasePOI poiSummary = poiServiceImpl.getBasePOI(pick.getPoiId());
				poi.setAbbrAddress(poiSummary.getAbbrAddress());
				poi.setName(poiSummary.getName());
				pickDetail.setPoi(poi);
				
				// set editor
				User editor = userServiceImpl.getUser(pick);
				pickDetail.setEditor(editor);
				
				// set images
				if(pickImages!=null && pickImages.size()>0) {
					images = new LinkedList<Image>();
					for(Pick_Images pi : pickImages) {
						Image i = new Image(pi.getImage(), pickType);
						images.add(i);
					}
				}
				pickDetail.setImages(images);
				
				// set wishes
				if(pickLikes!=null && pickLikes.size()>0) {
					wishes = new LinkedList<User>();
					for(Pick_Likes pl : pickLikes) {
						User wish = new User();
						Users u = pl.getUser();
						wish.setProfileImage(u.getProfileImage());
						wish.setUserId(u.getId());
						wish.setIsFollowing(userServiceImpl.isFriend(userId, u.getId()));
						
						wishes.add(wish);
					}
				}
				pickDetail.setWishes(wishes);
				
				// set comments
				if(pickComments!=null && pickComments.size()>0) {
					comments = new LinkedList<Comment>();
					for(Pick_Comments pc : pickComments) {
						Comment c = new Comment();
						c.setContents(pc.getContents());
						c.setCreatedAt(pc.getUpdatedat());
						
						Users us = usersRepository.findOne(pc.getUserId());
						User u = new User();
						u.setUserId(us.getId());
						u.setNickname(us.getNickname());
						u.setProfileImage(us.getProfileImage());
						
						c.setEditor(u);
						comments.add(c);
					}
				}
				pickDetail.setComments(comments);
				
				// set scoreboard
				PickScoreboard pb = new PickScoreboard();
				pb.setComments(sbPick.getCommentCount());
				pb.setImages(sbPick.getImageCount());
				pb.setLikes(sbPick.getLikeCount());
				pb.setRating(4.5);
				pickDetail.setScoreboard(pb);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pickDetail;
	}
	
	/**
	 * getComments
	 * @param pick
	 * @return List<Comment> 
	 */
	public List<Comment> getComments(Picks pick) {
		List<Comment> comments = null;
		List<Pick_Comments> pickComments = pick.getPickComments();
		if(pickComments!=null && pickComments.size()>0) {
			comments = new LinkedList<Comment>();
			for(Pick_Comments pc : pickComments) {
				Comment comment = new Comment();
				User u = userServiceImpl.getUser(pick);
//				u.setIsFollowing(userServiceImpl.isFriend(userId, editor.getUserId()));
				
				comment.setContents(pc.getContents());
				comment.setEditor(u);
				
				comments.add(comment);
			}
		}
		return comments;
	}
}