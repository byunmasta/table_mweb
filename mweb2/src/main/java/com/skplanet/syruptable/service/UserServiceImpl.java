package com.skplanet.syruptable.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.skplanet.syruptable.entity.pick.Picks;
import com.skplanet.syruptable.entity.user.User_Relations;
import com.skplanet.syruptable.entity.user.User_Relations_PK;
import com.skplanet.syruptable.entity.user.Users;
import com.skplanet.syruptable.repository.user.UserRelationsRepository;
import com.skplanet.syruptable.repository.user.UsersRepository;
import com.skplanet.syruptable.repository.user.WishesRepository;
import com.skplanet.syruptable.response.user.User;

@Service
public class UserServiceImpl {
	
	@Autowired
	private WishesRepository wishesRepository;
	
	@Autowired
	private UsersRepository usersRepository;
	
	@Autowired
	private UserRelationsRepository userRelationsRepository;
	
	/**
	 * isFriend
	 * @param followerId userId
	 * @param followeeId target userId
	 * @return Boolean
	 */
	public Boolean isFriend(Integer followerId, Integer followeeId) {
		User_Relations ur = userRelationsRepository.findOne(new User_Relations_PK(followerId, followeeId));
		if(ur!=null)
			return true;
		else
			return false;
	}
	
	public User getUser(Picks pick) {
		Users user = pick.getUsers();
		User editor = new User(user.getId(), user.getNickname(), user.getProfileImage(), 
				user.getGender(), user.getBadge(), user.getDescription(), 
				null, 3.5);
		return editor;
	}
	
	public User getUser(Users user) {
		User editor = null;
		if(user!=null)
			editor = new User(user.getId(), user.getNickname(), user.getProfileImage(), 
					user.getGender(), user.getBadge(), user.getDescription(), 
					null, 3.5);
		return editor;
	}
}