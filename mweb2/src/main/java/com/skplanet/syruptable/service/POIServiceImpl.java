package com.skplanet.syruptable.service;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.skplanet.syruptable.entity.aoi.Aois;
import com.skplanet.syruptable.entity.poi.Poi_Blog_Reviews;
import com.skplanet.syruptable.entity.poi.Poi_Ratings;
import com.skplanet.syruptable.entity.poi.Poi_Ratings_PK;
import com.skplanet.syruptable.entity.poi.Pois;
import com.skplanet.syruptable.entity.sb.Sb_Pois;
import com.skplanet.syruptable.entity.sorder.Sorder_Pois;
import com.skplanet.syruptable.repository.aoi.AoisRepository;
import com.skplanet.syruptable.repository.poi.PoiBlogReviewsRepository;
import com.skplanet.syruptable.repository.poi.PoiRatingsRepository;
import com.skplanet.syruptable.repository.sb.SbPoisRepository;
import com.skplanet.syruptable.response.BaseAOI;
import com.skplanet.syruptable.response.pick.Pick;
import com.skplanet.syruptable.response.poi.BasePOI;
import com.skplanet.syruptable.response.poi.Category;
import com.skplanet.syruptable.response.poi.Location;
import com.skplanet.syruptable.response.poi.POI;
import com.skplanet.syruptable.response.poi.POIDetail;
import com.skplanet.syruptable.response.poi.POIDetail.BlogReview;
import com.skplanet.syruptable.response.poi.POIDetail.My;
import com.skplanet.syruptable.response.poi.POIDetail.SuggestionPOI;
import com.skplanet.syruptable.response.poi.POIDetail.VerifiedPOI;
import com.skplanet.syruptable.response.poi.POIScoreboard;
import com.skplanet.syruptable.response.poi.RichInfo;
import com.skplanet.syruptable.response.poi.TagPOI;
import com.skplanet.syruptable.response.user.User;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

import syruptable.model.common.SyrupTable;
import syruptable.util.Util;

@Service
public class POIServiceImpl {
	
	@Autowired
	private AoisRepository aoisRepository;
	
	@Autowired
	private PoiRatingsRepository poiRatingsRepository;
	
	@Autowired
	private SbPoisRepository sbPoisRepository;
	
//	@Autowired
//	private HashtagsRepository hashtagsRepository;
	
	@Autowired
	private PoiBlogReviewsRepository poiBlogReviewsRepository;
	
	@Autowired
	private ImageServiceImpl imageServiceImpl;
	
	@Autowired
	private AoiServiceImpl aoiServiceImpl;
	
	@Autowired
	private CategoryServiceImpl categoryServiceImpl;
	
	@Autowired
	private HashtagServiceImpl hashtagServiceImpl;
	
	@Autowired
	private PickServiceImpl pickServiceImpl;
	
//	@Autowired
//	private CouponServiceImpl couponServiceImpl;
	
	@Autowired
	private WishServiceImpl wishServiceImpl;
	
	// 광고 hashtag
	private static final int ads = 11; 
	
	// 인증 hashtag
	private static final int verified = 12; 
	
	/**
	 * getPois
	 * @param poiId
	 * @return PoiDetail
	 */
	public POIDetail getPois(final SyrupTable syrupTable) {
		
		POIDetail poiDetail = new POIDetail();
		Integer poiId = syrupTable.getPoiId();
		
		try {
			Sb_Pois sbPois = sbPoisRepository.findOne(poiId);
			
			poiId = sbPois.getPoiId();
			int userId = 1;
			
			// exist result
			if(sbPois!=null) {
				
				// set pois
				Pois pois = sbPois.getPois();
				
				//set aoiIds
				BaseAOI aoi = aoiServiceImpl.getAOI(sbPois);
				BaseAOI verifiedAoi = aoiServiceImpl.getVerifiedAOI(sbPois);
				
				// set review S
				List<Pick> reviews = pickServiceImpl.getPickList(syrupTable, PickServiceImpl.Type.poiDetail);
				
				poiDetail.setPickCount(sbPois.getPickCount());
				poiDetail.setPicks(reviews);
				// set review E
				
				// set My S
				My my = new My();
				my.setRating(this.myRating(userId, poiId));
				my.setIsWished(wishServiceImpl.getIsWished(poiId, userId));
				poiDetail.setMy(my);
				// set My E
				
				// set POI S
				POI poi = new POI();
				BasePOI poiSummary = this.getBasePOI(poiId);
				poi.setPoiId(poiId);
				poi.setName(poiSummary.getName());
				poi.setHashtags(poiSummary.getHashtags());
				poi.setAbbrAddress(poiSummary.getAbbrAddress());
				poi.setAddressNo(poiSummary.getAddressNo());
				poi.setAoi(aoi);
				
				// set menu, richField S
				List<RichInfo> richField = this.getRichInfo(pois);
				if(richField!=null) {
					List<String> price = this.getPriceInfo(richField);
					poi.setPrice(price);
				}
				poi.setRichInfo(richField);
				// set menu, richField E
				
				// set scoreboard
				POIScoreboard scoreboard = new POIScoreboard();
				scoreboard.setBlogReviews(sbPois.getBlogReviewCount());
				scoreboard.setComments(sbPois.getCommentCount());
				scoreboard.setImages(sbPois.getImagesCount());
				scoreboard.setPicks(sbPois.getPickCount());
				scoreboard.setRater(54);
				scoreboard.setRating(4.5);
				scoreboard.setScore(sbPois.getScore());
				scoreboard.setWishes(sbPois.getWishCount());
				
				poi.setScoreboard(scoreboard);
				
				// set phone
				poi.setPhone(pois.getPhone());
				
				// set isClosed, isUcp
				if(pois.getStatus().equals(Pois.Status.DUCP))
					poi.setIsClosed(true);
				else
					poi.setIsClosed(false);
				
				if(!pois.getStatus().equals(Pois.Status.RMP))
					poi.setIsUcp(true);
				else
					poi.setIsUcp(false);
				
				// set category
				Category category = categoryServiceImpl.getCategory(pois);
				poi.setCategory(category);
				
				// set location
				Geometry geo = pois.getLoc();
				Point loc = (Point) geo;
				
				double[] coordinates = {loc.getX(), loc.getY()};
				Location location = new Location(loc.getGeometryType(), coordinates);
				poi.setLocation(location);
				
				// set tag
//				Tag_Pois tagPois = pois.getTagPois();
//				if(tagPois != null) {
//					tagPoi = tagPois.getTags();
//				}
				
				// set tag
				String[] tagPoi = new String[2];
				List<Integer> hashtagIds = new LinkedList<Integer>();
				for(Integer i : sbPois.getReprHashtagIds())
					hashtagIds.add(i);
				if(hashtagIds.contains(ads)) { tagPoi[0] = new String(); tagPoi[0] = "ads"; }
				if(hashtagIds.contains(verified)) { tagPoi[1] = new String(); tagPoi[1] = "verified"; }

				List<Sorder_Pois> orderPOIs = pois.getsOrderPois();
				
				// set wished list
				List<User> wishList = wishServiceImpl.getWishes(poiId, userId);
				poiDetail.setWishes(wishList);
				poiDetail.setWishCount(wishServiceImpl.getWishesCount(poiId));
				
				// set coupon
//				List<Coupon> coupons = couponServiceImpl.getCoupons(poiId);
//				poi.setCoupons(coupons);
//				if(coupons!=null)
//					poi.setCouponCount(coupons.size());
				
				TagPOI tagPOI = new TagPOI(tagPoi, 
						(orderPOIs!=null && orderPOIs.isEmpty())? 0: 1 , 
								pois.getHasCoupon() ? 1 :0);
				poi.setTagPOI(tagPOI);
				
				poiDetail.setPoi(poi);
				// set POI E
				
				// set blog review S
				List<Poi_Blog_Reviews> poiblogreviews = poiBlogReviewsRepository.findByPoiIdOrderByUpdatedatDesc(poiId);
				List<BlogReview> blogReviews = null;
				if(poiblogreviews!=null) {
					blogReviews = new LinkedList<BlogReview>();
					for(Poi_Blog_Reviews pbr : poiblogreviews) {
						BlogReview blogReview = new BlogReview();
						blogReview.setBlogId(pbr.getId());
						blogReview.setAuthor(pbr.getAuthor());
						blogReview.setCreatedAt(pbr.getCreatedat());
						blogReview.setFroms(pbr.getFroms());
						blogReview.setImage(pbr.getImage());
						blogReview.setOriginUrl(pbr.getOriginUrl());
						blogReview.setSummary(pbr.getSummary());
						blogReview.setTitle(pbr.getTitle());
						
						blogReviews.add(blogReview);
						
						if(blogReviews.size()>2)
							break;
					}
					
					if(blogReviews.size()<1)
						blogReviews = null;
				}
				poiDetail.setBlogReviews(blogReviews);
				poiDetail.setBlogReviewCount(poiblogreviews.size());
				// set blog review E
				
				// set suggestion verified poi S
				List<Sb_Pois> suggestions = sbPoisRepository.findByAoiIdsOrderByScoreDesc(aoi.getAoiId(), 0, 3);
				List<SuggestionPOI> suggestionPOI = null;
				if(suggestions!=null) {
					suggestionPOI = new LinkedList<SuggestionPOI>();
					for(Sb_Pois s : suggestions) {
						if(!poiId.equals(s.getPoiId())) {
							SuggestionPOI suggestion = new SuggestionPOI();
							Pois p = s.getPois();
							BasePOI ps = this.getBasePOI(p.getId());
							suggestion.setMainImage(ps.getMainImage());
							suggestion.setName(ps.getName());
							suggestion.setPoiId(ps.getPoiId());
							suggestion.setRater(ps.getScoreboard().getRater());
							suggestion.setRating(ps.getScoreboard().getRating());
							suggestion.setReviewCount(ps.getScoreboard().getPicks());
							suggestion.setImageCount(ps.getScoreboard().getImages());
							suggestion.setHashtags(ps.getHashtags());
							suggestion.setTagPOI(ps.getTagPOI());
							
							suggestionPOI.add(suggestion);
							
							if(suggestionPOI.size()>1)
								break;
						}
					}
				}
				poiDetail.setSuggestionPOI(suggestionPOI);
				
				List<Sb_Pois> verifies = sbPoisRepository.findByAoiIds(verifiedAoi.getAoiId(), 0, 100);
				
				// random shuffle
				List<VerifiedPOI> verifiedPOI = null;
				if(verifies!=null) {
					Collections.shuffle(verifies);
					verifiedPOI = new LinkedList<VerifiedPOI>();
					poiDetail.setVerifiedAoiName(verifiedAoi.getName());
					for(Sb_Pois s : verifies) {
						if(!poiId.equals(s.getPoiId())) {
							VerifiedPOI verified = new VerifiedPOI();
							Pois p = s.getPois();
							BasePOI ps = this.getBasePOI(p.getId());
							if(ps.getTagPOI().getIsVerified()) {
								verified.setAddress(ps.getAbbrAddress());
								verified.setMainImage(ps.getMainImage());
								verified.setName(ps.getName());
								verified.setPoiId(ps.getPoiId());
								verified.setRater(ps.getScoreboard().getRater());
								verified.setRating(ps.getScoreboard().getRating());
								verified.setReviewCount(ps.getScoreboard().getPicks());
								
								verifiedPOI.add(verified);
								
								if(verifiedPOI.size()>1)
									break;
							}
						}
					}
				}
				poiDetail.setVerifiedPOI(verifiedPOI);
				// set suggestion verified poi E
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return poiDetail;
	}
	
	/**
	 * getBasePOI
	 * @param poiId
	 * @return POISummary
	 */
	public BasePOI getBasePOI(final Integer poiId) {
		
		BasePOI poiSummary = new BasePOI();
		
		try{
			Sb_Pois sbPois = sbPoisRepository.findOne(poiId);
			if(sbPois!=null) {
				poiSummary.setPoiId(sbPois.getPoiId());
				poiSummary.setName(sbPois.getPoiName());
				
				Pois pois = sbPois.getPois();
				if(pois!=null){
					
					// set category
					poiSummary.setCategory(categoryServiceImpl.getCategory(pois));
					
					// set aoi
					poiSummary.setAoi(aoiServiceImpl.getAOI((Point) pois.getLoc()));
					
					// set address
					Map<String, String> attrs = pois.getAttrs();
                    if(attrs!=null) {
                        if(attrs.get("road_nm")!=null 
                        		&& poiSummary.getAoi()!=null
                        		&& attrs.get("bld_no")!=null)
                            poiSummary.setAbbrAddress(poiSummary.getAoi().getName()
                                    +" "+attrs.get("road_nm").toString()
                                    +" "+attrs.get("bld_no").toString());
                        else if(attrs.get("l_area_nm")!=null)
                            poiSummary.setAbbrAddress(attrs.get("l_area_nm").toString());
                        else
                        	poiSummary.setAbbrAddress("");
                        
                        if(attrs.get("addr_no")!=null) {
                            List<Integer> aoiIds = new LinkedList<Integer>();
                            for(int aoiId : sbPois.getAoiIds())
                                aoiIds.add(aoiId);
                            Aois aoi = aoisRepository.findFirstByIdInOrderByLevelDesc(aoiIds);
                            if(aoi!=null)
                            	poiSummary.setAddressNo(aoi.getName()+" "+attrs.get("addr_no").toString());
                            else
                            	poiSummary.setAddressNo(attrs.get("addr_no").toString());
                        }
                    }
                    
					// set mainImage
					poiSummary.setMainImage(imageServiceImpl.getMainImage(poiId));
					
					// set tag
					String[] tagPoi = new String[2];
					List<Integer> hashtagIds = new LinkedList<Integer>();
					for(Integer i : sbPois.getReprHashtagIds())
						hashtagIds.add(i);
					if(hashtagIds.contains(ads)) { tagPoi[0] = new String(); tagPoi[0] = "ads"; }
					if(hashtagIds.contains(verified)) { tagPoi[1] = new String(); tagPoi[1] = "verified"; }

					List<Sorder_Pois> orderPOIs = pois.getsOrderPois();
					
					TagPOI tagPOI = new TagPOI(tagPoi, 
							(orderPOIs!=null && orderPOIs.isEmpty())? 0: 1 , 
									pois.getHasCoupon() ? 1 :0);
					poiSummary.setTagPOI(tagPOI);
					
					// set hashtag
					poiSummary.setHashtags(hashtagServiceImpl.getHashtags(pois));
					
					// set location
					Geometry geo = pois.getLoc();
					Point loc = (Point) geo;
					
					double[] coordinates = {loc.getX(), loc.getY()};
					Location location = new Location(loc.getGeometryType(), coordinates);
					poiSummary.setLocation(location);
				}
				
				// set scoreboard
				POIScoreboard scoreboard = new POIScoreboard();
				scoreboard.setBlogReviews(sbPois.getBlogReviewCount());
				scoreboard.setComments(sbPois.getCommentCount());
				scoreboard.setImages(sbPois.getImagesCount());
				scoreboard.setPicks(sbPois.getPickCount());
				scoreboard.setRater(sbPois.getRater());
				scoreboard.setRating(sbPois.getStarRating());
				scoreboard.setScore(sbPois.getScore());
				scoreboard.setWishes(sbPois.getWishCount());
				
				// set scoreboard
				poiSummary.setScoreboard(scoreboard);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return poiSummary;
	}
	
	/**
	 * getRichInfo
	 * @param sbPois
	 * @return List<RichInfo>
	 */
	public List<RichInfo> getRichInfo(Pois pois) {
		
		List<RichInfo> richField = null;
		
		// Rich Filed delimeter
		String delimeterS = ":";
		
		// Rich Filed delimeter
		String delimeter = "\\|";
		
		try{
			if(pois!=null) {
				String sRichField = pois.getRichField();
				
				if(!"".equals(sRichField)) {
					richField = new LinkedList<RichInfo>();
					String[] sRichFields = sRichField.split(delimeter);
					if(sRichFields.length>0) {
						for(String r : sRichFields) {
							String[] s = r.split(delimeterS);
							Map<String, String> richTitle = Util.getRichTitle();
							String name = richTitle.get(s[0].toLowerCase()).toString();
							RichInfo richInfo = new RichInfo();
							richInfo.setCode(s[0]);
							richInfo.setName(name);
							richInfo.setValue(s[1]);
							richField.add(richInfo);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return richField;
	}
	
	/**
	 * getPriceInfo
	 * @param sbPois
	 * @return List<RichInfo>
	 */
	public List<String> getPriceInfo(List<RichInfo> richField) {
		List<String> price = null;
		
		// Price delimeter
		String delimeterP = "/";
		
		try{
			if(richField!=null) {
				price = new LinkedList<String>();
				for(RichInfo ri : richField) {
					if("Price".equals(ri.getCode())) {
						String[] p =  ri.getValue().split(delimeterP);
						for(String m : p)
							price.add(m.trim());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return price;
	}
	
	/**
	 * myRating
	 * @param userId
	 * @param poiId
	 * @return
	 */
	public Double myRating(Integer userId, Integer poiId) {
		Poi_Ratings pr = poiRatingsRepository.findOne(new Poi_Ratings_PK(userId, poiId));
		if(pr!=null)
			return pr.getStarRating();
		else
			return (double) 0;
	}
	
	
	/**
	 * 예약 POI 전체 개수 가져오기
	 * @return
	 */
	@Cacheable("bookingPoisCount")
	public int countByBookingPois() {
		return sbPoisRepository.countByBookingPois();
	}
	/**
	 * 예약 POI 목록 가져오기
	 * @return
	 */
	@Cacheable("bookingPois")
	public List<Sb_Pois> getBookingPois(int cursor, int limit) {
		List<Sb_Pois> pois = sbPoisRepository.findByBookingPois(cursor, limit);
		return pois;
	}
}
