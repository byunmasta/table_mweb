package com.skplanet.syruptable.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.jolbox.bonecp.BoneCPDataSource;
import com.skplanet.syruptable.infra.BoneCPDataSourceProperties;
import com.skplanet.syruptable.infra.BoneCPMaster;

@Configuration
public class BoneCPDatabaseConfig {
	
	@Autowired
	private BoneCPMaster boneCPMaster;

	
	@Bean(name="dataSourceMaster")
//	@Primary
	public DataSource dataSourceMaster() {
		return createDataSource(boneCPMaster);
	}

	private DataSource createDataSource(final BoneCPDataSourceProperties properties) {
		final BoneCPDataSource dataSource = new BoneCPDataSource();
		dataSource.setDriverClass(properties.getDriverClass());
		dataSource.setJdbcUrl(properties.getJdbcUrl());
		dataSource.setUsername(properties.getJdbcUsername());
		dataSource.setPassword(properties.getJdbcPassword());
		dataSource.setIdleConnectionTestPeriodInMinutes(properties.getIdleConnectionTestPeriodInMinutes());
		dataSource.setIdleMaxAgeInMinutes(properties.getIdleMaxAgeInMinutes());
		dataSource.setMaxConnectionsPerPartition(properties.getMaxConnectionsPerPartition());
		dataSource.setMinConnectionsPerPartition(properties.getMinConnectionsPerPartition());
		dataSource.setPartitionCount(properties.getPartitionCount());
		dataSource.setAcquireIncrement(properties.getAcquireIncrement());
		dataSource.setStatementsCacheSize(properties.getStatementsCacheSize());
		return dataSource;
	}
}
