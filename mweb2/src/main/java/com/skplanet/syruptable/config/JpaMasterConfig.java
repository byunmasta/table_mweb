package com.skplanet.syruptable.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(basePackages = "com.skplanet.syruptable.repository", 
						entityManagerFactoryRef = "sqlMaster",
						transactionManagerRef = "transactionManager")
public class JpaMasterConfig {
	
	@Autowired
	private DataSource boneCPMaster;


	@Bean(name = "sqlMaster")
//	@Primary
	public LocalContainerEntityManagerFactoryBean masterEntityManagerFactory(
							EntityManagerFactoryBuilder builder) {
		
			return builder
					.dataSource(boneCPMaster)
					.packages("com.skplanet.syruptable.entity") // package where are the @Entity classes are located, ususaly your domain package
					.persistenceUnit("sqlMaster")
					.build();
	}
	
	
	@Bean(name = "transactionManager")
	public PlatformTransactionManager masterTransactionManager() {
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		jpaTransactionManager.setDataSource(boneCPMaster);
		jpaTransactionManager.setPersistenceUnitName("sqlMaster");
		return jpaTransactionManager;
	}

}
