package syruptable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;

import com.google.common.cache.CacheBuilder;

//@SpringBootApplication
@Configuration
@ComponentScan({"com.skplanet.syruptable", "syruptable"})
@EnableCaching
@EnableAutoConfiguration
public class SyrupTableMobileWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SyrupTableMobileWebApplication.class, args);
	}

	@Autowired
	private Environment environment;

	@Bean(name = "messageSource")
	public MessageSource messageSource() {
		List<String> fileList = new ArrayList<String>();
		fileList.add("classpath:application");

		String[] files = (String[]) fileList.toArray(new String[fileList.size()]);

		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasenames(files);
		messageSource.setDefaultEncoding("UTF-8");
		if (environment.acceptsProfiles("loc")) {
			messageSource.setCacheSeconds(0);
		}
		return messageSource;
	}

	@Bean(name = "messageSourceAccessor")
	public MessageSourceAccessor messageSourceAccessor() {
		return new MessageSourceAccessor(messageSource());
	}

	@Bean(name="cacheManager")
	public CacheManager cacheManager() {
		SimpleCacheManager simpleCacheManager = new SimpleCacheManager();
		GuavaCache category = new GuavaCache("category", CacheBuilder.newBuilder()
				.maximumSize(1)
				.expireAfterWrite(30, TimeUnit.MINUTES)
				.build());
		
		GuavaCache notices = new GuavaCache("notices", CacheBuilder.newBuilder()
				.maximumSize(1)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache mainPlaces = new GuavaCache("mainPlaces", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache mainStories = new GuavaCache("mainStories", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache todayLunch = new GuavaCache("todayLunch", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache storyList = new GuavaCache("storyList", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache aoiList = new GuavaCache("aoiList", CacheBuilder.newBuilder()
				.maximumSize(1)
				.expireAfterWrite(30, TimeUnit.MINUTES)
				.build());
		
		GuavaCache geofenceZoneAOI = new GuavaCache("geofenceZoneAOI", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache reverseGeocode = new GuavaCache("reverseGeocode", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache currentAOI = new GuavaCache("currentAOI", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(60, TimeUnit.MINUTES)
				.build());
		
		GuavaCache poiDetail = new GuavaCache("poiDetail", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache poiImageList = new GuavaCache("poiImageList", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache pickDetail = new GuavaCache("pickDetail", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache bookingPois = new GuavaCache("bookingPois", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache bookingPoisCount = new GuavaCache("bookingPoisCount", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache storyPois = new GuavaCache("storyPois", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache allVerifiedPOIs = new GuavaCache("allVerifiedPOIs", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		GuavaCache verifiedAOIs = new GuavaCache("verifiedAOIs", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		GuavaCache verifiedPOI = new GuavaCache("verifiedPOI", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(1, TimeUnit.DAYS)
				.build());
		
		GuavaCache hashtagDetail = new GuavaCache("hashtagDetail", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache searchResult = new GuavaCache("searchResult", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache autocompleteResult = new GuavaCache("autocompleteResult", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		GuavaCache popularWords = new GuavaCache("popularWords", CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(10, TimeUnit.MINUTES)
				.build());
		
		
		
		simpleCacheManager.setCaches(Arrays.asList(category, notices, mainPlaces, mainStories, todayLunch
													, aoiList, geofenceZoneAOI, reverseGeocode ,currentAOI
													, poiDetail, poiImageList, pickDetail, hashtagDetail, bookingPoisCount, bookingPois
													, storyList, storyPois, verifiedAOIs, allVerifiedPOIs, verifiedPOI
													, searchResult, autocompleteResult, popularWords));
		
		return simpleCacheManager;
	}

}
