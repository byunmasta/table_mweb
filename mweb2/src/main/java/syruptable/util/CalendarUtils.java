package syruptable.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.lang.time.FastDateFormat;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;

public class CalendarUtils {
	
	private final static String yyyyMMdd = "yyyyMMdd";

	private final static String yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";
	
	private final static String yyyyMMdd_SMS = "yyyy년 MM월 dd일";

			
	public static Date parse(String source) {
		try {
			return new SimpleDateFormat(yyyyMMdd).parse(source);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	public static Date parseTime(String source) {
		try {
			return new SimpleDateFormat(yyyyMMddHHmmss).parse(source);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	
	final DateTimeParser[] parsers = { 
        	DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSSSSSZ").getParser()
        	,DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ssZ").getParser()
        	,DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ").getParser() 
        };

	final DateTimeFormatter formatter = new DateTimeFormatterBuilder().append( null, parsers ).toFormatter();
	
	final FastDateFormat updateDate = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.KOREA);
	final FastDateFormat UTCupdateDate = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss'Z'", TimeZone.getTimeZone("UTC"));
	
	public static DateFormat getISO8601Format() {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		df.setTimeZone(tz);
		
		return df;
	}

	public static Date getNow() {
		return new Date();
	}
	
	public static String after(Date date, int amount) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		
		c.add(Calendar.DAY_OF_MONTH, amount);
		
		return new SimpleDateFormat(yyyyMMdd_SMS).format(c.getTime());
	}
}
