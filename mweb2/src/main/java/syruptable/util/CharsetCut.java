package syruptable.util;

/**
 * 특정 문장에 대해 요청한 사이즈에 맞게 글자를 생략처리 한다.
 * @author webmadeup
 *
 */
public class CharsetCut {

	private static final int DEFAULT_DBCSWIDTH = 12;
	
	private static final int DEFAULT_DBCSBOLDWIDTH = 13;
	
	private static final int ASCIIWIDTH[] = { 0, 6, 6, 6, 6, 6, 6, 6, 6, 0, 0, 0, 0, 0, 6, 6, 6,
			6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 4, 3, 5, 7, 7, 11, 8,
			4, 5, 5, 6, 6, 4, 6, 4, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 4, 4, 8,
			6, 8, 6, 10, 8, 8, 9, 8, 8, 7, 9, 8, 3, 6, 7, 7, 11, 8, 9, 8, 9, 8,
			8, 7, 8, 8, 10, 8, 8, 8, 6, 11, 6, 6, 6, 4, 7, 7, 7, 7, 7, 3, 7, 7,
			3, 3, 6, 3, 9, 7, 7, 7, 7, 4, 7, 3, 7, 6, 10, 6, 6, 7, 6, 6, 6, 9 };

	private static final  int ASCIIBOLDWIDTH[] = { 0, 7, 7, 7, 7, 7, 7, 7, 7, 0, 0, 0, 0, 0, 7, 7,
			7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 5, 4, 6, 8, 8, 12,
			9, 5, 6, 6, 7, 7, 5, 7, 5, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 5, 5,
			9, 7, 9, 7, 11, 9, 9, 10, 9, 9, 8, 10, 9, 4, 7, 8, 8, 12, 9, 10, 9,
			10, 9, 9, 8, 9, 9, 11, 9, 9, 9, 7, 12, 7, 7, 7, 5, 8, 8, 8, 8, 8,
			4, 8, 8, 4, 4, 7, 4, 10, 8, 8, 8, 8, 5, 8, 4, 8, 7, 11, 7, 7, 8, 7,
			7, 7, 10 };

	private static int normalPixelSize = DEFAULT_DBCSWIDTH;

	private static int boldPixelSize = DEFAULT_DBCSBOLDWIDTH;

	/**
	 * "동해물과 백두산이 마르고 닳도록"을 일반 폰트로 150pixel 안에 출력할 경우
	 * 예) obj.GetPixelSizedText("동해물과 백두산이 마르고 닳도록", 150, false)
	 * 반환) 동해물과 백두산이 마르...
	 * 
	 * @param text
	 *            출력 텍스트
	 * @param width
	 *            가로사이즈
	 * @param bold
	 *            볼드체 유무
	 * @return
	 */
	public static String getPixelSizedText(String text, int width, boolean bold) {
		if(text == null) return "";
		StringBuffer result = new StringBuffer();
		int len = 0;
		for (int i=0; i<text.length(); i++) {
			int charLen = getCharPixelWidth(text.charAt(i), bold);
			if (len + charLen > width)
				break;
			len = len + charLen;
			result = result.append(text.charAt(i));
		}
		if (result.length() != text.length()) {
			if (len + (bold ? 10 : 12) > width) {
				int temp = 0;
				int resultLen = result.length();
				int i;
				for (i=0; i<resultLen; i++) {
					temp += getCharPixelWidth(result.charAt(resultLen - 1 - i), bold);
					if (len + (bold ? 10 : 12) - temp <= width)
						break;
				}
				try {
					int tempLen = resultLen - i - 1;
					if (tempLen < 0)
						tempLen = 0;
					result = result.append(result.substring(0, tempLen)).append(bold ? ".." : "...");
				} catch (StringIndexOutOfBoundsException e) {
				}
			} else
				result = result.append(bold ? ".." : "...");
		}

		return result.toString();
	}

	/**
	 * 해당 글자의 가로폭 사이즈를 반환
	 * 
	 * @param chr
	 *            문자
	 * @param bold
	 *            볼드체 유무
	 * @return
	 */
	public static  int getCharPixelWidth(char chr, boolean bold) {
		try {
			// if (nCode >= '\uAC00' && nCode <= '\uD7A3')
			if ((int) chr > 255)
				return (bold ? boldPixelSize : normalPixelSize);
			return (bold ? ASCIIBOLDWIDTH[chr] : ASCIIWIDTH[chr]);
		} catch (Exception e) {
			return 0;
		}
	}
	
	/*public static void main(String[] args) {
		System.out.println(CharsetCut.getPixelSizedText("동해물과 백두산이 마르고 닳도록", 150, false));
	}*/
}
