package syruptable.util;



/**
 * @author webmadeup
 * 1. Converting coordinates type between WGS84 and KTM(TM128).
 * 2. Calulating Distance using by WGS84 or KTM coordinates.
 * 
 * * GPS 경위도를 우리나라 경위도로 바꾸는 방법
	①	GPS 경위도를 WGS84 타원체 지구 중심 좌표계로 변환
	②	WGS84 구심 좌표계를 Bessel 타원체 지구 중심 좌표계로 변환
	③	Bessel 구심 좌표계를 Bessel 타원체 경위도로 변환
	④	Bessel 타원체 경위도를 Bessel 타원체 직교 좌표로 변환후 거리 계산
	(또는 Bessel 타원체 경위도를 UTM(또는 TM)좌표계로 변환후 거리계산)
 */
public class WGS2SKReg  {
	
	private static final double BESSEL_A = 6377397.155;
	private static final double BESSEL_RF = 299.1528128;
	private static final double BESSEL_B = (BESSEL_A - BESSEL_A / BESSEL_RF);
	private static final double BESSEL_EE = (0.006674372231315);  // (a*a - b*b)/(a*a)
	
	private static final double W2B_DELTAX = 128;
	private static final double W2B_DELTAY = -481;
	private static final double W2B_DELTAZ = -664;
	private static final double W2B_DELTAA = -739.845;
	private static final double W2B_DELTAF = -0.000010037483;
	
	private static final double WGS84_A = 6378137.0;
	private static final double WGS84_F = (1.0 / 298.257223563);
	private static final double WGS84_RF = 298.257223563;
	private static final double WGS84_B = (WGS84_A - WGS84_A / WGS84_RF);
	private static final double WGS84_EE = (2.0 * WGS84_F - WGS84_F * WGS84_F);
	
	
	/**
	 * Gets the single instance of CoordsUtil.
	 *
	 * @return single instance of CoordsUtil
	 */
	

	/**
	 * @param wx : 경도
	 * @param wy : 위도 
	 * 
	 * @return points[0] : 경도, points[1] : 위도
	 * @throws Exception
	 */
    public static int[] conversion_WGS_to_Bessel(double wx, double wy)
    {
       double rn, rm, d_pi, d_lamda;
       double h;
       
       h = 0.0;

       double[] tempCoord = {wx,wy};
       int[] retCoord = {0,0};
       
       wx *= (Math.PI/180.0);
   	   wy *= (Math.PI/180.0);
       
       
       rn = WGS84_A / Math.sqrt(1 - WGS84_EE*Math.pow(Math.sin(wy), 2.));
       rm = WGS84_A*(1-WGS84_EE) / Math.pow(Math.sqrt(1 - WGS84_EE * Math.pow(Math.sin(wy), 2.)), 3.);

       d_pi = (-W2B_DELTAX * Math.sin(wy) * Math.cos(wx) -
          W2B_DELTAY * Math.sin(wy) * Math.sin(wx) +
          W2B_DELTAZ * Math.cos(wy) +
          W2B_DELTAA * (rn * WGS84_EE * Math.sin(wy) * Math.cos(wy)) / WGS84_A +
          W2B_DELTAF * (rm * WGS84_A / WGS84_B + rn * WGS84_B / WGS84_A) * Math.sin(wy) * Math.cos(wy)) /
          ((rm + h) * Math.sin(Math.PI/180. * 1/3600.));
       d_lamda = (-W2B_DELTAX * Math.sin(wx) + W2B_DELTAY * Math.cos(wx))
          / ((rn + h) * Math.cos(wy) * Math.sin(Math.PI/180. * 1/ 3600.));

        	
       tempCoord[0] += d_lamda / 3600.0;
       tempCoord[1] += d_pi / 3600.0;
       
       //WGS84->BESEEL한후 36000을 곱한다.
       retCoord[0] = (int)(Math.floor(tempCoord[0] * 36000));
       retCoord[1] = (int)(Math.floor(tempCoord[1] * 36000));
       
       return retCoord;
    }
    
    /*public static void main(String[] args){
    	int[] ret = WGS2SKReg.conversion_WGS_to_Bessel(126.985098, 37.566385);
    	
    	System.out.println("ret : " + ret[0] );
    	System.out.println("ret : " + ret[1] );
    }*/

}

