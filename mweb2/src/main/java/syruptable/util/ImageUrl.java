package syruptable.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author byunMasta
 * 이미지URL에 대해 피캣서버의 이미지일 경우 섬네일 이미지URL로 변환한다.
 * 이미지가 없을경우 디폴트 이미지 지정가능
 * 사이즈에는 반드시 지정된 사이즈만 넣어야 함, 지정되지 않은 사이즈 입력 시 원본이미지 리턴
 */
public class ImageUrl{
	
	private static Map<String, String> sizeMap = new HashMap<String, String>(){{
		put("180x180","180x180");
		put("480","480");
	}};
	
	/** 
	 * @param image			: 원본 이미지 URL
	 * @param size			: 썸네일사이즈 ex) 180x180, 480
	 * @param defaultImage	: URL이 잘못되었을 경우 디폴트 이미지
	 */
	public static String getUrl(String image, String size){
		if((image.startsWith("http://file.pickat.com") || image.startsWith("http://file.pickat.kiwiple.net/")) && sizeMap.containsKey(size) ){
			return image + "_" + size;
		}else if(image.startsWith("http://") || image.startsWith("https://")){
			return image;
		}else{
			return "";
		}
	}
	
}
