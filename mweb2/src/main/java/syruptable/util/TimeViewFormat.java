	package syruptable.util;

import java.util.Locale;

import org.apache.commons.lang.time.FastDateFormat;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.LocalDate;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Period;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;
import org.springframework.stereotype.Component;

/**
 *  30일 이후 : 2014.03.23
   30일 이내 : 29일 전
   몇일 전 : 1일 전
   60분 이후 : 1시간 59분 전
   60분 이내 : 59분 전
   60초 이내 : 1분 전
 * @author webmadeup
 *
 */
@Component
public class TimeViewFormat {
	
	private static final FastDateFormat updateDate = FastDateFormat.getInstance("yyyy.MM.dd", Locale.KOREA);
//	private static final DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ");
	
	private static final DateTimeParser[] parsers = { 
            DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSSSSSZ").getParser()
            ,DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSSSS").getParser()
            ,DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSSSSS").getParser()
            ,DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ssZ").getParser()
            ,DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ").getParser() 
        };

	private static final DateTimeFormatter formatter = new DateTimeFormatterBuilder().append( null, parsers ).toFormatter();
	
	private DateTime now;
	private LocalDate nowDate;
	
	/**
	 * 생성자
	 */
	public TimeViewFormat() {
		 this.now = new DateTime();
		 this.nowDate = new LocalDate();
	}
	
	/**
	 * 시간 계산을 할때 마다 새로운 시간을 구하는 것이 아니라
	 * Controller 의 메소드 단위로 시간을 sync한다.
	 */
	public void setNowDate() {
		 this.now = new DateTime();
		 this.nowDate = new LocalDate();
		 
	}
	/**
	 * 컨텐츠가 생성된 시간을 보여준다.
	 * @param startDate
	 * @return
	 */
	public final String getPeriod(final String startDate) {
		DateTime inputDateTime;
		
		String viewDate = "";
		Period period;
		
		try {
			/**
			 * 자정시간 기준으로 어제와 오늘을 구분하여 Date를 한번 연산함
			 * 두 날짜를 모두 자정으로 변경후 어제, 오늘 구분
			 */
			final DateTime startDateTime = formatter.parseDateTime(startDate); //new DateTime(formatter.parseDateTime(startDate), DateTimeZone.forID("Asia/Seoul")); //

			LocalDate inputDate = new LocalDate(startDateTime);
			period = new Period(inputDate, nowDate);
			
			/**
			 * 자정기준 계산시 하루가 차이가 나면 입력된 시간을 00:00:00으로 변경함	
			 */
			if(period.getDays() > 0 ) {
				inputDateTime = new DateTime(inputDate.toDate());
			} else {
				inputDateTime = startDateTime;
				period = new Period(inputDateTime, now);
			}
			
			/*inputDateTime = new DateTime(startDate);
			period = new Period(inputDateTime, now);*/
			
			int seconds = Seconds.secondsBetween(inputDateTime, now).getSeconds();
			int minutes = Minutes.minutesBetween(inputDateTime, now).getMinutes();
			int hours = Hours.hoursBetween(inputDateTime, now).getHours();
			int days = Days.daysBetween(inputDateTime, now).getDays();  
			int months = Months.monthsBetween(inputDateTime, now).getMonths();  
	//        int years = Years.yearsBetween(poiCreate, now).getYears(); 

			/**
			 * 현재 시간을 기준으로 60초 이내의 등록된 글
			 */
			if(seconds <= 60) {
				return viewDate = "1분 전";
			}
		
			/**
			 * 현재 시간을 기준으로 60분 이내 등록된 글
			 */
			if(minutes <= 60) {
				return viewDate = String.format("%s분 전", minutes);
			}
			
			/**
			 * 60분 이후 등록된 글
			 */
			if(hours < 24) {
				return viewDate = String.format("%s시간 %s분 전", period.getHours(), period.getMinutes());
			}
		
			/**
			 * 어제부터 30일 이내 등록된 글
			 */
			if(days <= 30) {
				return viewDate = String.format("%s일 전", days );
			}
		
			/**
			 * 30일 이후에 등록된 글
			 */
			if(months > 0) {
				return viewDate = updateDate.format(startDateTime.toDate());
			}
			
			
			
			/*if(months > 0) {
				viewDate = updateDate.format(new DateTime(startDate).toDate());
			}
			
			if(days < 30) {
				viewDate = String.format("%s일 전", days);
			}
			
			if(hours < 24) {
				viewDate = String.format("%s시간 %s분 전", period.getHours(), period.getMinutes());
			}
		
			if(minutes < 60) {
				viewDate = String.format("%s분 전", period.getMinutes());
			}
		
			if(seconds < 60) {
				viewDate = "1분 전";
			}*/
		
		} catch(Exception ignore) {
			ignore.printStackTrace();
			/***
			 * 시간 노출 정책 계산 에러 발생시는 빈값을 리턴한다.
			 */
		}
        
		return viewDate;
		
	}

}
