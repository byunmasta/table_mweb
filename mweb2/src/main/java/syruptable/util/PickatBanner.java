package syruptable.util;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Pickat 어드민에서 등록된 배너에 대한 정보
 * @author webmadeup
 *
 */
public class PickatBanner {
	
	/**
	 * 배너에 사용될 링크 정보
	 */
	private String link;
	
	/**
	 * 배너에 사용될 이미지 URL
	 */
	private String image;
	
	/**
	 * 배너의 다양한 타입에 대한 정의
		url("url")                          // A : web page
		,poi("poi")							// 8 : POI 상세
		,notice("notice")                   // 안씀 5 : 공지사항  안씀
		,public_theme("public_theme")		// 6 : 테마속 장소
		,private_theme("private_theme")		// 7 : 테마속 장소
		,search("search")					// 9 : 통합검색  안씀
		,user("user")						// D : 사용자 사용자 탭('N': 나의 소식, 'M': 내 장소, 'T': 나의 테마, 'F': 친구)
		,coupon("coupon")					// C : 쿠폰 상세  c=(coupon_src)&d=(coupon_code)
		,coupon_detail("coupon_detail")		// C : 쿠폰 상세  c=(coupon_src)&d=(coupon_code)
		,keyword("keyword");				// K : 키워드 상세
	 */
	private int type;
	
	/**
	 * 배너에 사용될 이미지 URL
	 */
	public String getImage() {
		return image;
	}
	
	public PickatBanner() {
		
	}

	/**
	 * JSON 객체로 전달 받은 것은 데이타를 Banner로 캐스팅 
	 * @param banner
	 */
	public PickatBanner(final JSONObject banner) {
		this.link = banner.getString("link");
		this.image = banner.getString("image");
		this.type = banner.getInt("type");
	}
	
	
	/**
	 * 배너정보에 대해 화면에서 사용할 수있는 
	 * image, url 으로 매핑
	 * @return
	 */
	public final static JSONArray getBanner(final JSONArray banners) {
		JSONArray bannersInfo  = new JSONArray();
		
		for(int i =0; i < banners.length(); i++) {
			PickatBanner banner = new PickatBanner(banners.getJSONObject(i));
			Map<String, String> bannerInfo = new HashMap<String, String>();
			try {
				bannerInfo.put("url", banner.getLinkURL(banner.type, banner.link));
				bannerInfo.put("image", banner.image);
			} catch (Exception e) {
				//특정 raw데이타 변환시 에러가 발생한 경우 해당 배너는 전시 하지 않음
				continue;
			}
			bannersInfo.put(bannerInfo);
		}
		return bannersInfo;
	}
	
	
	/**
	 * 피켓 API 의 결과값에서 URL 정보를 다시 조합한다.
	 * @param type
	 * @param link
	 * @return
	 * @throws Exception
	 */
	public final String getLinkURL(final int type, final String link) throws Exception {
		String siteLink = "";
			String[] rawLink = link.split("&");
			if(rawLink.length == 0) {
				return "";
			}
			String bannerInfo = rawLink[1].replaceFirst("b=", "");
			switch(type) {
			
				case 1:		// URL 
					siteLink = bannerInfo;
					break;
				case 2:		// POI
					siteLink = String.format("/poi/%s", bannerInfo);
					break;
				case 3:		// Notice
					siteLink = String.format("/notice");
					break;
				case 4:		// public theme
					siteLink = String.format("/theme/%s/public", bannerInfo);
					break;
				case 5:		// private theme
					siteLink = String.format("/theme/%s/private", bannerInfo);
					break;
				case 6:		// search
					siteLink = String.format("/search/%s", bannerInfo);
					break;
				case 10:	// keyword
					siteLink = String.format("/keyword/%s", bannerInfo);
					break;
				case 12:	// popup store
					siteLink = URLDecoder.decode(URLDecoder.decode(bannerInfo, "UTF-8"), "UTF-8");
					break;
				case 14:	// hashtag
					siteLink = String.format("/hashtag/%s", bannerInfo);
					break;
				default :
					siteLink = bannerInfo;
//				    throw new Exception();
					break;
			}
			
		return siteLink;
	}
	
	/**
	 * 피켓 API 의 결과값에서 URL 정보를 다시 조합한다. type없는 경우
	 * @param link
	 * @return
	 * @throws Exception
	 */
	public final String getLinkURL(final String link) throws Exception {
		String siteLink = "";
			String[] rawLink = link.split("&");
			if(rawLink.length == 0) {
				return "/";
			}
			String[] bannerInfo = new String[rawLink.length];
			for(int i=0; i<rawLink.length; i++){
				bannerInfo[i] = rawLink[i].split("=")[1];
			}

			switch(bannerInfo[0]) {
			
				case "A":		// URL 
					siteLink = bannerInfo[1];
					break;
				case "8":		// POI
					siteLink = String.format("/poi/%s", bannerInfo[1]);
					break;
				case "5":		// Notice
					siteLink = String.format("/notice");
					break;
				case "9":		// search
					siteLink = String.format("/search/%s", bannerInfo[1]);
					break;
				case "P":	// popup store
					siteLink = URLDecoder.decode(URLDecoder.decode(bannerInfo[1], "UTF-8"), "UTF-8");
					break;
				case "H":	// hashtag
					siteLink = String.format("/hashtag/%s", bannerInfo[1]);
					break;
				case "J":	// Today Lunch
					siteLink = String.format("/today-lunch");
					break;
				default :
					siteLink = bannerInfo[1];
//				    throw new Exception();
					break;
			}
			
		return siteLink;
	}

}
