package syruptable.util;

import java.security.SecureRandom;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.Transient;


public class PickatCryptor {

    // Constants
    public final static String CIPHER_SPEC = "AES/CBC/PKCS5Padding";
    public final static String CIPHER_ALGO = "AES"; 
    public final static String STR_ENCODING = "utf-8";
    
    private SecretKeySpec key;
    private byte[] staticIv;
    private SecureRandom csprng;
    
    public PickatCryptor(byte[] rawKey, byte[] rawStaticIv) {
        init(rawKey, rawStaticIv);
    }
    
    public PickatCryptor(byte[] rawKey) {
        init(rawKey, null);
    }
    
    public PickatCryptor(String b64KeyStr, String b64StaticIvStr) {
        byte[] rawKey = Base64.getDecoder().decode(b64KeyStr);
        byte[] rawStaticIv = null;
        
        if (b64StaticIvStr != null && !b64StaticIvStr.isEmpty()) {
            rawStaticIv = Base64.getDecoder().decode(b64StaticIvStr);
        }
        
        init(rawKey, rawStaticIv);
    }
    
    public PickatCryptor(String b64KeyStr) {
        byte[] rawKey = Base64.getDecoder().decode(b64KeyStr);
        init(rawKey, null);
    }
    
    private void init(byte[] rawKey, byte[] rawStaticIv) {
        try {
			Cipher.getInstance(CIPHER_SPEC);
		} catch (Exception e) {
            throw new RuntimeException("Unable to initialize cipher", e);
		} 
        
        key = new SecretKeySpec(rawKey, CIPHER_ALGO);
        staticIv = rawStaticIv;
        
        if(rawStaticIv == null) {
            csprng = new SecureRandom();
        }
        
    }
    
    public byte[] encrypt(byte[] payload) {
        if(staticIv != null) {
            return encryptWithStaticIV(payload);
        } else {
            return encryptWithRandomIV(payload);
        }
    }
    
    public byte[] decrypt(byte[] payload) {
        if(staticIv != null) {
            return decryptWithStaticIV(payload);
        } else {
            return decryptWithRandomIV(payload);
        }
    }
    
    private byte[] encryptWithStaticIV(byte[] plainText) {

        try {
            Cipher cipher = Cipher.getInstance(CIPHER_SPEC);
//            int blockSize = cipher.getBlockSize();
            
            IvParameterSpec ivSpec = new IvParameterSpec(staticIv);

            cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);

            return cipher.doFinal(plainText);
        } catch(Exception e) {
            throw new RuntimeException("Encryption failed", e);
        }
    }
    
    private byte[] encryptWithRandomIV(byte[] plainText) {

        try {
            Cipher cipher = Cipher.getInstance(CIPHER_SPEC);
            int blockSize = cipher.getBlockSize();
            
            byte[] ivBytes = new byte[blockSize];
            csprng.nextBytes(ivBytes);
            IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);

            cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);

            int outputLen = cipher.getOutputSize(plainText.length);
            byte[] cipherText = new byte[blockSize + outputLen];
            System.arraycopy(ivBytes, 0, cipherText, 0, ivBytes.length);
            cipher.doFinal(plainText, 0, plainText.length, cipherText, blockSize);

            return cipherText;
        } catch(Exception e) {
            throw new RuntimeException("Encryption failed", e);
        }
    }
    
    
    private byte[] decryptWithStaticIV(byte[] cipherText) {

        try {
            Cipher cipher = Cipher.getInstance(CIPHER_SPEC);
//            int blockSize = cipher.getBlockSize();
            
            IvParameterSpec ivSpec = new IvParameterSpec(staticIv);

            cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);

            return cipher.doFinal(cipherText);
        } catch(Exception e) {
            throw new RuntimeException("Encryption failed", e);
        }
    }
    
    private byte[] decryptWithRandomIV(byte[] cipherText) {

        try {
            Cipher cipher = Cipher.getInstance(CIPHER_SPEC);
            int blockSize = cipher.getBlockSize();
            
            IvParameterSpec ivSpec = new IvParameterSpec(cipherText, 0, blockSize);

            cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);

            return cipher.doFinal(cipherText, blockSize, cipherText.length - blockSize);

        } catch(Exception e) {
            throw new RuntimeException("Decryption failed", e);
        }
    }
    
    /*public static void main(String[] args) {
        byte[] result;
        String b64Result;
        
        String MDN_SAMPLE = "01029274551";
        
    	String BIRTHDAY_ENC_KEY = "tpdddA+SyXGVuphlSBhOaAvfNRsP2SY+UXGDFRN8MZQ=";
    	
    	String GENDER_ENC_KEY = "UfUEz1OI/RXnolz6Ougjzz0u0+eaeHFCuIGrG4hjewo=";
    	
    	String MDN_ENC_KEY = "76Q8MSx53ZbCUKALpXXGB/NDTUP8eAnh90BdgrYkluo="; // etc/common.conf keys.db_mdn_enc_key
    	
    	String MDN_ENC_IV = "ie3PkzhFPwswZ9dArksTXQ=="; //  etc/common.conf keys.db_mdn_enc_iv
    	
        PickatCryptor mdnCryptor = new PickatCryptor(MDN_ENC_KEY, MDN_ENC_IV);

        result = mdnCryptor.encrypt(MDN_SAMPLE.getBytes());
        b64Result = Base64.getEncoder().encodeToString(result);
        System.out.println("MDN Encrypt result: " + b64Result);
        
        result = mdnCryptor.decrypt(result);
        System.out.println("MDN Decrypt result: " + new String(result));        
        
        String BIRTHDAY_PLAINTEXT_SAMPLE = "1989-04-18";
        String BIRTHDAY_CIPHERTEXT_SAMPLE = "ew15kMWAXI+ymw102D+/aONbWxSArPhVimVECTou8Jk=";
        PickatCryptor bdCryptor = new PickatCryptor(BIRTHDAY_ENC_KEY);
        
        result = bdCryptor.encrypt(BIRTHDAY_PLAINTEXT_SAMPLE.getBytes());
        b64Result = Base64.getEncoder().encodeToString(result);
        System.out.println("Birthday Encrypt result: " + b64Result);
        
        result = bdCryptor.decrypt(Base64.getDecoder().decode(BIRTHDAY_CIPHERTEXT_SAMPLE));
        System.out.println("Birthday Decrypt result: " + new String(result));
        

    }*/

}
