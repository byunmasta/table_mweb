package syruptable.util;

import java.text.DecimalFormat;

/**
 * http://stackoverflow.com/questions/3126830/query-to-get-records-based-on-radius-in-sqlite
 * http://www.movable-type.co.uk/scripts/latlong.html
 * http://sd.ngii.go.kr/sub/gps/gps_outl.jsp?serv_cd=5&mmenu=1&smenu=1
 * http://www.biz-gis.com/index.php?mid=pds&page=3
 * http://www.ga.gov.au/earth-monitoring/geodesy/geodetic-techniques/calculation-methods.html
 * @author webmadeup
	
	φ is latitude, λ is longitude, 
	R is earth’s radius (mean radius = 6376.5km)
	d is distance
	Haversine(구면기하 삼각함수) : a = sin²(Δφ/2) + cos(φ1).cos(φ2).sin²(Δλ/2)
	formula (최종 면적산출식)    : c = 2.atan2(√a, √(1−a))
								 d = R.c 
	
	평면 상의 두 점의 거리는 sqrt( (x2 - x1)^2 + (y2 - y1)^2 ) 
	구면 상의 두 점의 거리는 구면기하 삼각함수를 이용한 Haversine 공식
	
	자전 및 공전을 하는 지구가 완전한 구체는 아니라서(WGS-84 ellipsoid 모델에서 적도 반지름은 6378km, 극 반지름은 6357km) 
	Haversine식은 0.3% 가량의 오차를 가진다(즉, 1km 거리에서 3m 가량의 오차).
	그래서 국토지리원에서 배포한 대한민국 기준 지도 반경 값을 참고하여 6376.5로 사용함 
	
	오차를 줄이려면 Vincenty (http://www.movable-type.co.uk/scripts/latlong-vincenty.html)공식을 사용하면 되지만(오차가 무려 1mm), 
	좌표간 거리가 대단한 정밀도를 가질 필요가 없고 계산량도 많아 Haversine 공식을 사용하기로 했다
	*
 */
public class EPSG4326 {
	
	private static final DecimalFormat decimalFormat = new DecimalFormat(".#");
	/**
	 * request는 기본형태인 String으로 받는다.
     * longitude : 경도(가로)
	 * latitude  : 위도(세로)
     * @param  longitude , latitude
     * @return
     */
    public final static String getDistance(final double longitude, final double latitude, final double POILongitude, final double POILatitude, final String km) {
    	
    	final double earthRadius = 6376.5;
		double lat1 = degreesToRadian(latitude);
		double lon1 = degreesToRadian(longitude);
		double lat2 = degreesToRadian(POILatitude);
		double lon2 = degreesToRadian(POILongitude);

		double distnaceLongitude = lon2 - lon1;
		double distnaceLatitude = lat2 - lat1;

		/**
		 * Haversine : a = sin²(Δφ/2) + cos(φ1).cos(φ2).sin²(Δλ/2)
			formula  : c = 2.atan2(√a, √(1−a))
				       d = R.c 
		 */
		double a = Math.pow(Math.sin(distnaceLatitude / 2.0), 2) + 
							Math.cos(lat1) * Math.cos(lat2) *
							Math.pow(Math.sin(distnaceLongitude / 2.0), 2);
		double dist = (earthRadius * 2.0 * Math.atan2(Math.sqrt(a), Math.sqrt(1.0 - a))) * 1000;
		
		int miter = (int) Math.round(dist);
		String distance = null;
		try {
			if("km".equals(km) && dist >= 1000.0) {
				//%d 십진정수, %f 부동소수점, %x 16진수, %c 문자
				dist = dist / 1000;
				distance = String.format("%.2f", dist); //소수점 둘째자리에서 반올림
				
				/*
				 * 사업부 최종 의사결정 함(한 3번 변경되었음 말할때 녹음해야 함)
				 */
				distance = decimalFormat.format(Double.parseDouble(distance)); //소수점 첫째자리까지만 표시
				return distance + "km";
			}
		} catch(Exception e) {
			
		}
		 
		return distance = miter + "m";
	}
    
    /**
     * 각도 값(degree)을 radian 각도값으로 변환
     * @param degree
     * @return
     */
    private final static double degreesToRadian (final double degree) {
		return degree * Math.PI / 180.0;
	}
    /**
	 * 삼화타워 
	 * 126.9846308
	 * 37.5669234
	 * @param args
	 *//*
	public static void main(String[] args) {
		System.out.println(getDistance(126.96828501574961, 37.55861572818036, 127.97064535968546, 37.5586837687227, "km"));   
		System.out.println(getDistance(126.96828501574961, 37.55861572818036, 126.97064535968546, 37.5586837687227, "km"));
		System.out.println(getDistance(126.924128, 37.525756, 126.931280, 37.525756, "km"));
		
		
	}*/
	
	
}
