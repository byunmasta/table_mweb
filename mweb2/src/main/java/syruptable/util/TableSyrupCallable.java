package syruptable.util;

import java.util.Map;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import syruptable.config.HttpAPIList;
import syruptable.model.common.SyrupTable;
import syruptable.service.SyrupTableService;

/**
 * Syrup Table 에서 여려 API를 조회해야 하는경우
 * TableSyrupCallable 에 call()에 추가 하여 FutureTask를 통해 동시에 자료를 요청 하게 한다.
 * @author webmadeup
 *
 * @param <V>
 */
public class TableSyrupCallable<V> implements Callable<V> {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private SyrupTableService syrupTableService;
	
	private HttpAPIList httpAPIList;
	private Map<String, Object> variables;
	private SyrupTable syrupTable;
	
	@Deprecated
	public TableSyrupCallable(HttpAPIList httpAPIList, Map<String, Object> variables, SyrupTableService syrupTableService) {
		this.httpAPIList = httpAPIList;
		this.variables = variables;
		this.syrupTableService = syrupTableService;
	}
	
	/**
	 * Syrup Table 에서 동시에 많은 양의데이타를 조회할때,
	 * Thread로 해당 데이타를 가져오도록 한다.
	 * @param httpAPIList
	 * @param syrupTable
	 * @param syrupTableService
	 */
	public TableSyrupCallable(final HttpAPIList httpAPIList, final SyrupTable syrupTable, final SyrupTableService syrupTableService) {
		this.httpAPIList = httpAPIList;
		this.syrupTable = syrupTable;
		this.syrupTableService = syrupTableService;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public V call() throws Exception {
		
		V v = null;
		logger.info("=============== Thread {}================", httpAPIList.name());
//		logger.info(this.variables.toString());
		switch(httpAPIList) {
			case MAINPLACES :
				v = (V) syrupTableService.getMainPlaces(syrupTable.getAoiId()
														, syrupTable.getSet()
														, syrupTable.getLatitude6()
														, syrupTable.getLongitude6()
														, syrupTable.getHashtagId()
														, syrupTable.getCategoryId()
														, syrupTable.getCursor()
														, syrupTable.getLimit());
				break;
			case REVERSEGEOCODE :
				v = (V) syrupTableService.getReverseGeocode(syrupTable.getLongitude6(), syrupTable.getLatitude6());
				break;
				
			case CURRENTAOI :
				v = (V) syrupTableService.getCurrentAOI(syrupTable.getLongitude6(), syrupTable.getLatitude6());
				break;
			
			case POIIMAGEFORPICK :
			case POIIMAGEFORBLOG :
				v = (V) syrupTableService.getPOIImageList(syrupTable.getImageType(), syrupTable.getPoiId(), syrupTable.getCursor(), syrupTable.getLimit());
				break;
				
			default: 
				logger.error("정의되지 않은 TableSyrupCallable {}", httpAPIList.name());
				break;
		}
		return v;
	}
}
