package syruptable.util;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 캐시삭제할때 1,2,3번서버에 각각 찌르자
 * @author byunmasta
 *
 * @param <V>
 */
public class CacheEvictCallable<V> implements Callable<V> {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private String targetIp="";
	private String cacheName="";
	private String token="";

	public CacheEvictCallable(String targetIp, String cacheName, String token) {
		this.targetIp = targetIp;
		this.cacheName = cacheName;
		this.token = token;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public V call() throws Exception {
		Boolean v = new Boolean(true);
		
		if(targetIp.equals("") || targetIp.equals(null)){
			return null;
		}
		
		String url = "http://"+targetIp+"/cache/evict/!/"+cacheName+"/"+token;
		logger.info("=============== Cache Evict Thread {}================", url);
		
		HttpURLConnection con = null;
		try{
			URL obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
	 
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "SK Planet (SyrupTable 2.0) made by Yongho Byun/87.05.13");
//			con.setConnectTimeout(3000);
			
			con.getResponseCode();
			/*int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
	 
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	 
			//print result
			System.out.println(response.toString());*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			v = false;
		} finally {
			con.disconnect();
		}
		
		return (V)v;
	}
}
