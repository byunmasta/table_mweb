package syruptable.util;

import java.util.Arrays;
import java.util.List;

public class MobileUtil {

	private static List<String> mobileList = Arrays.asList("android", "iphone", "ipod");
	
	public static boolean isMobile(String userAgent){
		for(String mobile : mobileList){
			if(userAgent.indexOf(mobile) > -1){
				return true;
			}
		}
		return false;
		
	}
}
