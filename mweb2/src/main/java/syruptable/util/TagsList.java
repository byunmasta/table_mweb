package syruptable.util;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;

/**
 * POI에 대한 출처중 제휴, 추천에 해당 하는 출처에 대해
 * 아래와 같은 우선순위로 노출이 된다.
 * @author webmadeup
 *
 */
public class TagsList{
	
	private static final Map<String, Integer> ORDER = new HashMap<String, Integer>();
	static {
		ORDER.put("partner", 0);
		ORDER.put("recommend", 1);
	}

	/**
	 * order 에 정의된 순서로 데이타 정렬
	 */
	private static final Comparator<String> tagsComparator = new Comparator<String>() {
		/**
		 * ORDER 에 정의된 순서로 재정렬 구현
		 */
		public int compare(String tags1, String tags2) {
			if(ORDER.containsKey(tags1) && ORDER.containsKey(tags2)) {
				return ORDER.get(tags1).compareTo(ORDER.get(tags2));
			} else {
				return 1;
			}
		}
	};
	
	/**
	 * POI의 출처중 우선순위기 적용된 한영역만 가져오기
	 * @param tags
	 * @return
	 */
	public static String getDisplay(final JSONArray tags) {
		if(tags.length() == 0) {
			return "";
		}
		/**
		 * JSONArray를 String array 로 변경
		 */
		String[] tag = tags.join(",").split(",");
		try {
			/**
			 * Arrays.sort를 이용함
			 */
			Arrays.sort(tag, tagsComparator);
		} catch(NullPointerException e) {
			return "";
		}
//		return new StringBuilder().append("<em class=\"ribbon ").append(tags[0]).append("\">").append(tags[0]).append("</em>").toString();
		return tag[0].replaceAll("\"", "");
	}

}
