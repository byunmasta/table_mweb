package syruptable.model;

import java.util.Arrays;

import syruptable.model.common.BaseTableAPI;

public class PopularWords extends BaseTableAPI {

	private String[] words;

	public String[] getWords() {
		return words;
	}

	@Override
	public String toString() {
		return "PopularWords [words=" + Arrays.toString(words) + "]";
	}
}
