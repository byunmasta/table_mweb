package syruptable.model;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

/**
 * Syrup Table 카테고리 전체 목록
 * @author Yongho Byun
 *
 */
public class Categories {
	

	private Category[] categories;
	
	public Category[] getCaregories() {
		return categories;
	}

	public class Category {

		@SerializedName ("category_id")
		private String categoryId;
		
		private String type;
		
		private String name;
		
		@SerializedName ("sub_categories")
		private SubCategory[] subCategories;

		public String getCategoryId() {
			return this.categoryId;
		}
		
		public String getType() {
			return type;
		}

		public String getName() {
			return name;
		}

		public SubCategory[] getSubCategories() {
			return subCategories;
		}
		
		@Override
		public String toString() {
			return "Category [categoryId=" + categoryId + ", type=" + type + ", name=" + name + ", subCategories="
					+ Arrays.toString(subCategories) + "]";
		}
	}
	
	/**
	 * 중분류 카테고리
	 * @author Yongho Byun
	 *
	 */
	public class SubCategory {

		@SerializedName ("category_id")
		private String categoryId;
		
		private String type;
		
		private String name;

		public String getCategoryId() {
			return categoryId;
		}

		public String getType() {
			return type;
		}

		public String getName() {
			return name;
		}
		
		@Override
		public String toString() {
			return String.format(
					"categoryId=%s, type=%s, name=%s",
					categoryId, type, name);
		}
	}


	
	@Override
	public String toString() {
		return "Categories [caregories=" + Arrays.toString(categories) + "]";
	}

}
