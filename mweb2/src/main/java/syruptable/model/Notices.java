package syruptable.model;

import java.util.Arrays;
import java.util.Locale;

import org.apache.commons.lang.time.FastDateFormat;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.annotations.SerializedName;

import syruptable.model.common.Paging;

/**
 * Syrup Table 공지사항
 * @author webmadeup
 *
 */
public class Notices {
	
	private static final DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ");
	private static final FastDateFormat updateDate = FastDateFormat.getInstance("yyyy.MM.dd HH:mm:ss", Locale.KOREA);
	
	@SerializedName ("notices")
	private Notice[] notice;
	
	public Notice[] getNotice() {
		return notice;
	}
	
	@SerializedName ("notice")
	private Notice noticeDetail;
	
	public Notice getNoticeDetail() {
		return noticeDetail;
	}

	/**
	 * 공지사항 내용
	 * @author webmadeup
	 *
	 */
	public class Notice {
		
		@SerializedName ("created_at")
		private String created;
		
		public String getCreated() {
			DateTime startDateTime = formatter.parseDateTime(this.created); 
			return updateDate.format(startDateTime.toDate());
		}
		
		@SerializedName ("notice_id")
		private int noticeId;
		
		@SerializedName ("contents")
		private String contents;
		
		@SerializedName ("title")
		private String title;
		
		
		/**
		 * @return the noticeId
		 */
		public int getNoticeId() {
			return noticeId;
		}

		/**
		 * @return the contents
		 */
		public String getContents() {
			return contents;
		}

		/**
		 * @return the title
		 */
		public String getTitle() {
			return title;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return String
					.format("notices [created=%s, noticeId=%s, contents=%s, title=%s]",
							created, noticeId, contents, title);
		}
	}
	
	@SerializedName ("total_count")
	private int totalCount;
	
	/**
	 * @return the totalCount
	 */
	public int getTotalCount() {
		return totalCount;
	}

	@SerializedName ("paging")
	private Paging paging;
	
	public Paging getPaging() {
		return paging;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"[notices=%s, tatalCount=%s, paging=%s]",
				Arrays.toString(notice), totalCount, paging.getNext());
	}

	
}
