package syruptable.model;

import java.util.Arrays;

import syruptable.model.common.BaseTableAPI;
import syruptable.model.common.Hashtag;
import syruptable.model.common.BasePOI;

public class HashtagDetail extends BaseTableAPI {

	private BasePOI[] pois;
	
	private Hashtag hashtag;
	
	public BasePOI[] getPois() {
		return pois;
	}
	
	public Hashtag getHashtag() {
		return hashtag;
	}
	
	@Override
	public String toString() {
		return "HashtagDetail [pois=" + Arrays.toString(pois) + ", hashtag=" + hashtag + "]";
	}

}
