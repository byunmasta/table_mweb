package syruptable.model;

import java.util.Arrays;

import syruptable.model.common.BaseTableAPI;

public class AutocompleteResult extends BaseTableAPI {

	private String[] pois;

	public String[] getPois() {
		return pois;
	}

	@Override
	public String toString() {
		return "AutocompleteResult [pois=" + Arrays.toString(pois) + "]";
	}
	
}
