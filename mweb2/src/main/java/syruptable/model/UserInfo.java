package syruptable.model;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.google.gson.annotations.SerializedName;

import syruptable.model.common.APIError;

public class UserInfo extends APIError {
	
	@SerializedName ("user_id")
	private Integer userId;
	
	@SerializedName ("syrup_member_id")
	private String syrupMemberId;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getSyrupMemberId() {
		return syrupMemberId;
	}

	public void setSyrupMemberId(String syrupMemberId) {
		this.syrupMemberId = syrupMemberId;
	}
	
	public DateTime getExpired_Date() {
		DateTime dt = new DateTime(DateTimeZone.UTC);
		return dt.plusMinutes(10);
	}
	
}
