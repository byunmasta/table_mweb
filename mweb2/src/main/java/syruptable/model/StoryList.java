package syruptable.model;

import java.util.Arrays;

import syruptable.model.common.BaseTableAPI;
import syruptable.model.common.User;

public class StoryList extends BaseTableAPI {
	
	private Story[] stories;
	
	public class Story {
		
		private String description;
		
		private String title;
		
		private String image;
		
		private Scoreboard scoreboard;
		
		private String link;
		
		private User user;
		
		private int id;

		
		public class Scoreboard {
			
			private int likes;
			
			private int views;

			public int getLikes() {
				return likes;
			}

			public int getViews() {
				return views;
			}

			@Override
			public String toString() {
				return "Scoreboard [likes=" + likes + ", views=" + views + "]";
			}

		}

		
		public String getDescription() {
			return description;
		}


		public String getTitle() {
			return title;
		}


		public String getImage() {
			return image;
		}


		public Scoreboard getScoreboard() {
			return scoreboard;
		}


		public String getLink() {
			return link;
		}


		public User getUser() {
			return user;
		}


		public int getId() {
			return id;
		}

		@Override
		public String toString() {
			return "Story [description=" + description + ", title=" + title + ", image=" + image + ", scoreboard="
					+ scoreboard + ", link=" + link + ", user=" + user + ", id=" + id + "]";
		}
	
	}


	public Story[] getStories() {
		return stories;
	}

	@Override
	public String toString() {
		return "StoryList [stories=" + Arrays.toString(stories) + "]";
	}
	
}
