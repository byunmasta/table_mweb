package syruptable.model;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

import syruptable.model.common.BaseTableAPI;
import syruptable.model.common.BasePOI;

public class SearchResult extends BaseTableAPI {

	private BasePOI[] pois;
	
	@SerializedName("analyzed_info")
	private AnalyzedInfo analyzedInfo;
	
	@SerializedName("call_id")
	private String callId;
	
	public class AnalyzedInfo{
		
		@SerializedName("user_query")
		private String userQuery;
		
		@SerializedName("corrected_query")
		private String correctedQuery;
		
		@SerializedName("searched_label")
		private SearchedLabel searchedLabel;
		
		public class SearchedLabel{
			
			private String message;
			
			@SerializedName("label_1")
			private String label1;

			public String getMessage() {
				return message;
			}

			public String getLabel1() {
				return label1;
			}

			@Override
			public String toString() {
				return "SearchedLabel [message=" + message + ", label1=" + label1 + "]";
			}
			
		}

		
		public String getUserQuery() {
			return userQuery;
		}

		public String getCorrectedQuery() {
			return correctedQuery;
		}

		public SearchedLabel getSearchedLabel() {
			return searchedLabel;
		}

		@Override
		public String toString() {
			return "AnalyzedInfo [userQuery=" + userQuery + ", correctedQuery=" + correctedQuery + ", searchedLabel="
					+ searchedLabel + "]";
		}
		
	}

	
	public BasePOI[] getPois() {
		return pois;
	}

	public void setPois(BasePOI[] pois) {
		this.pois = pois;
	}

	public AnalyzedInfo getAnalyzedInfo() {
		return analyzedInfo;
	}

	public void setAnalyzedInfo(AnalyzedInfo analyzedInfo) {
		this.analyzedInfo = analyzedInfo;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

	@Override
	public String toString() {
		return "SearchResult [pois=" + Arrays.toString(pois) + ", analyzedInfo=" + analyzedInfo + ", callId=" + callId
				+ "]";
	}

}
