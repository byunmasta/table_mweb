package syruptable.model;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

import syruptable.model.common.BaseAOI;

public class AOIList {
	
	private AOIDepth[] aois;
	
	public AOIDepth[] getAois() {
		return aois;
	}

	public class AOIDepth {
		
		@SerializedName ("sub_aois")
		private BaseAOI[] subAOIs;
		
		private String name;

		public BaseAOI[] getSubAOIs() {
			return subAOIs;
		}

		public String getName() {
			return name;
		}

		@Override
		public String toString() {
			return "AOIDepth [subAOIs=" + Arrays.toString(subAOIs) + ", name=" + name + "]";
		}
		
	}

	@Override
	public String toString() {
		return "AOIList [aois=" + Arrays.toString(aois) + "]";
	}
	
}
