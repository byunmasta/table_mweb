package syruptable.model;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

import syruptable.model.common.Banner;
import syruptable.model.common.BasePOI;
import syruptable.model.common.BasePick;
import syruptable.model.common.BaseTableAPI;
import syruptable.model.common.Menu;

public class MainPlaces extends BaseTableAPI {

	@SerializedName("lunch_banners")
	private Banner[] lunchBanners;
	
	@SerializedName("lunch_event_visible")
	private boolean lunchEventVisible;
	
	private Suggestion[] suggestions;
	
	private Menu[] menus;
	
	private Card[] cards;
	
	private BasePOI[] pois;
	
	
	public static class Suggestion {
		
		@SerializedName("is_default")
		private boolean isDefault;
		
		private String name;
		
		@SerializedName("p_aoi_id")
		private String pAOIId;
		
		@SerializedName("p_set")
		private String pSet;
		
		@SerializedName("p_category_id")
		private String pCategoryId;
		
		@SerializedName("p_hashtag_id")
		private String pHashtagId;
		
		@SerializedName("p_has_coupon")
		private String pHasCoupon;
		
		@SerializedName("p_has_parking")
		private String pHasParking;
		
		@SerializedName("p_is_popular")
		private String pIsPopular;
		
		@SerializedName("p_is_ranking")
		private String pIsRanking;
		
		@SerializedName("p_has_order")
		private String pHasOrder;

		public void setName(String name) {
			this.name = name;
		}

		public void setpSet(String pSet) {
			this.pSet = pSet;
		}

		public void setpCategoryId(String pCategoryId) {
			this.pCategoryId = pCategoryId;
		}

		public void setpHashtagId(String pHashtagId) {
			this.pHashtagId = pHashtagId;
		}

		public boolean isDefault() {
			return isDefault;
		}

		public String getName() {
			return name;
		}

		public String getpAOIId() {
			return pAOIId;
		}

		public String getpSet() {
			return pSet;
		}

		public String getpCategoryId() {
			return pCategoryId;
		}

		public String getpHashtagId() {
			return pHashtagId;
		}

		public String getpHasCoupon() {
			return pHasCoupon;
		}

		public String getpHasParking() {
			return pHasParking;
		}

		public String getpIsPopular() {
			return pIsPopular;
		}

		public String getpIsRanking() {
			return pIsRanking;
		}

		public String getpHasOrder() {
			return pHasOrder;
		}
		
		@Override
		public String toString() {
			return "Suggestion [isDefault=" + isDefault + ", name=" + name + ", pAOIId=" + pAOIId + ", pSet=" + pSet
					+ ", pCategoryId=" + pCategoryId + ", pHashtagId=" + pHashtagId + ", pHasCoupon=" + pHasCoupon
					+ ", pHasParking=" + pHasParking + ", pIsPopular=" + pIsPopular + ", pIsRanking=" + pIsRanking
					+ ", pHasOrder=" + pHasOrder + "]";
		}

	}

	public static class Card {
		
		private int type;
		
		private CardPOI poi;
		
		private Banner banner;
		
		public static class CardPOI extends BasePOI {
			
			@SerializedName("recent_pick")
			private BasePick recentPick;

			public BasePick getRecentPick() {
				return recentPick;
			}

			@Override
			public String toString() {
				return "CardPOI [recentPick=" + recentPick + "]";
			}
			
		}

		
		public int getType() {
			return type;
		}

		public CardPOI getPoi() {
			return poi;
		}

		public Banner getBanner() {
			return banner;
		}

		@Override
		public String toString() {
			return "Card [type=" + type + ", poi=" + poi + ", banner=" + banner + "]";
		}
		
	}

	
	
	public Banner[] getLunchBanners() {
		return lunchBanners;
	}

	public boolean isLunchEventVisible() {
		return lunchEventVisible;
	}

	public Suggestion[] getSuggestions() {
		return suggestions;
	}

	public Menu[] getMenus() {
		return menus;
	}

	public Card[] getCards() {
		return cards;
	}
	
	public BasePOI[] getPois() {
		return pois;
	}

	@Override
	public String toString() {
		return "MainPlaces [lunchBanners=" + Arrays.toString(lunchBanners) + ", lunchEventVisible=" + lunchEventVisible
				+ ", suggestions=" + Arrays.toString(suggestions) + ", menus=" + Arrays.toString(menus) + ", cards="
				+ Arrays.toString(cards) + ", pois=" + Arrays.toString(pois) + "]";
	}
	
	//custom method
	public boolean hasCategory(String categoryId){
		for(int i=0,length=suggestions.length; i<length; i++){
			if(categoryId.equals(suggestions[i].getpCategoryId())){
				return true;
			}
		}
		return false;
	}
	
}
