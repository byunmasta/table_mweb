package syruptable.model;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

import syruptable.model.common.BaseAOI;
import syruptable.model.common.BasePOI;
import syruptable.model.common.BaseTableAPI;
import syruptable.model.common.Location;
import syruptable.model.common.POIImage;
import syruptable.model.common.User;

public class POIDetail {
	
	private Wisher wishers;
	
	@SerializedName("recent_picks")
	private RecentPicks recentPicks;
	
	private POI poi;
	
	@SerializedName("related_pois")
	private BasePOI[] relatedPOIs;
	
	@SerializedName("verified_pois")
	private BasePOI[] verifiedPOIs;
	
	private Blogs blogs;
	
	@SerializedName("notice_booking")
	private String noticeBooking; 
	
	
	public static class Wisher extends BaseTableAPI {
		
		User[] users;

		public User[] getUsers() {
			return users;
		}

		@Override
		public String toString() {
			return "Wihser [users=" + Arrays.toString(users) + "]";
		}

	}

	public static class RecentPicks extends BaseTableAPI {
		
		private PickDetail[] picks;

		public PickDetail[] getPicks() {
			return picks;
		}

		@Override
		public String toString() {
			return "RecentPicks [picks=" + Arrays.toString(picks) + "]";
		}
		
	}

	public static class POI extends BasePOI {
		
		@SerializedName("tmap_id")
		private int tmapId;
		
		@SerializedName("comms_id")
		private int commsId;
		
		@SerializedName("is_ucp")
		private boolean isUcp;
		
		private POIImage[] images;
		
		private AOI aoi;
		
		@SerializedName("rich_info")
		private RichInfo[] richInfo;
		
		@SerializedName("price_info")
		private PriceInfo priceInfo;
		
		public static class AOI extends BaseAOI{
			
			private Location center;
			
			private Polygon mpolygon;
			
			private BaseAOI[] parents;
			
			
			public static class Polygon {
				
				private String type;
				
				private Object[] coordinates;

				public String getType() {
					return type;
				}

				public Object[] getCoordinates() {
					return coordinates;
				}

				@Override
				public String toString() {
					return "Polygon [type=" + type + ", coordinates=" + Arrays.toString(coordinates) + "]";
				}
				
			}

			public Location getCenter() {
				return center;
			}
			
			public Polygon getMpolygon() {
				return mpolygon;
			}

			public BaseAOI[] getParents() {
				return parents;
			}

			@Override
			public String toString() {
				return "AOI [center=" + center + ", mpolygon=" + mpolygon + ", parents=" + Arrays.toString(parents)
						+ "]";
			}
		}

		public static class RichInfo {
			
			private String code;
			
			private String[] values;
			
			private String name;
			
			private String value;

			public String getCode() {
				return code;
			}

			public String[] getValues() {
				return values;
			}

			public String getName() {
				return name;
			}

			public String getValue() {
				return value;
			}

			@Override
			public String toString() {
				return "RichInfo [code=" + code + ", values=" + Arrays.toString(values) + ", name=" + name + ", value="
						+ value + "]";
			}
			
		}
		
		public static class PriceInfo {
			
			private String code;
			
			private PriceValue[] values;
			
			private String name;
			
			public static class PriceValue {
				
				private String price;
				
				private String name;

				public String getPrice() {
					return price;
				}

				public String getName() {
					return name;
				}

				@Override
				public String toString() {
					return "PriceValue [price=" + price + ", name=" + name + "]";
				}
			}

			
			public String getCode() {
				return code;
			}

			public PriceValue[] getValues() {
				return values;
			}

			public String getName() {
				return name;
			}

			@Override
			public String toString() {
				return "PriceInfo [code=" + code + ", values=" + Arrays.toString(values) + ", name=" + name + "]";
			}
		}
		
		
		public int getTmapId() {
			return tmapId;
		}

		public int getCommsId() {
			return commsId;
		}

		public boolean isUcp() {
			return isUcp;
		}
		
		public POIImage[] getImages() {
			return images;
		}
		
		public AOI getAoi() {
			return aoi;
		}
		
		public RichInfo[] getRichInfo() {
			return richInfo;
		}

		public PriceInfo getPriceInfo() {
			return priceInfo;
		}

		@Override
		public String toString() {
			return "POI [tmapId=" + tmapId + ", commsId=" + commsId + ", isUcp=" + isUcp + ", images="
					+ Arrays.toString(images) + ", aoi=" + aoi + ", richInfo=" + Arrays.toString(richInfo)
					+ ", priceInfo=" + priceInfo + "]";
		}

	}

	public static class Blogs extends BaseTableAPI {
		
		private Review[] reviews;
		
		public static class Review {
			
			private String author;
			
			private String image;
			
			private String title;
			
			private String summary;
			
			private String link;
			
			private String froms;
			
			@SerializedName("created_at")
			private String createdAt;

			public String getAuthor() {
				return author;
			}

			public String getImage() {
				return image;
			}

			public String getTitle() {
				return title;
			}

			public String getSummary() {
				return summary;
			}

			public String getLink() {
				return link;
			}

			public String getFroms() {
				return froms;
			}

			public String getCreatedAt() {
				return createdAt;
			}

			@Override
			public String toString() {
				return "Review [author=" + author + ", image=" + image + ", title=" + title + ", summary=" + summary
						+ ", link=" + link + ", froms=" + froms + ", createdAt=" + createdAt + "]";
			}
			
		}

		
		public Review[] getReviews() {
			return reviews;
		}

		@Override
		public String toString() {
			return "Blogs [reviews=" + Arrays.toString(reviews) + "]";
		}
		
	}

	
	public Wisher getWishers() {
		return wishers;
	}

	public RecentPicks getRecentPicks() {
		return recentPicks;
	}

	public POI getPoi() {
		return poi;
	}

	public BasePOI[] getRelatedPOIs() {
		return relatedPOIs;
	}

	public BasePOI[] getVerifiedPOIs() {
		return verifiedPOIs;
	}

	public Blogs getBlogs() {
		return blogs;
	}

	public String getNoticeBooking() {
		return noticeBooking;
	}

	@Override
	public String toString() {
		return "POIDetail [wishers=" + wishers + ", recentPicks=" + recentPicks + ", poi=" + poi
				+ ", relatedPOIs=" + Arrays.toString(relatedPOIs) + ", verifiedPOIs=" + Arrays.toString(verifiedPOIs)
				+ ", blogs=" + blogs + ", noticeBooking=" + noticeBooking + "]";
	}
	
}
