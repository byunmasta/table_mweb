package syruptable.model;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

import syruptable.model.common.BaseAOI;
import syruptable.model.common.Menu;

public class TodayLunch {
	
	private int attendances;
	
	@SerializedName("lunch_event_visible")
	private boolean lunchEventVisible;
	
	private LunchImages ios;
	
	@SerializedName("android_hd")
	private LunchImages androidHD;
	
	private BaseAOI aoi;
	
	private Menu[] menus;
	
	
	public static class LunchImages {
		
		@SerializedName("like_enable")
		private String likeEnable;
		
		@SerializedName("like_disable")
		private String likeDisable;
		
		@SerializedName("heart_enable")
		private String heartEnable;
		
		private String background;

		public String getLikeEnable() {
			return likeEnable;
		}

		public String getLikeDisable() {
			return likeDisable;
		}

		public String getHeartEnable() {
			return heartEnable;
		}

		public String getBackground() {
			return background;
		}

		@Override
		public String toString() {
			return "LunchImages [likeEnable=" + likeEnable + ", likeDisable=" + likeDisable + ", heartEnable="
					+ heartEnable + ", background=" + background + "]";
		}
		
	}


	
	
	public int getAttendances() {
		return attendances;
	}


	public boolean isLunchEventVisible() {
		return lunchEventVisible;
	}


	public LunchImages getIos() {
		return ios;
	}


	public LunchImages getAndroidHD() {
		return androidHD;
	}


	public BaseAOI getAoi() {
		return aoi;
	}


	public Menu[] getMenus() {
		return menus;
	}

	@Override
	public String toString() {
		return "TodayLunch [attendances=" + attendances + ", lunchEventVisible=" + lunchEventVisible + ", ios=" + ios
				+ ", androidHD=" + androidHD + ", aoi=" + aoi + ", menus=" + Arrays.toString(menus) + "]";
	}
	
}
