package syruptable.model;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

import syruptable.model.common.BaseTableAPI;
import syruptable.model.common.Pick;
import syruptable.model.common.User;

public class PickDetail extends BaseTableAPI {
	
	private Commment[] comments;
	
	private Pick pick;
	
	public class Commment {

		@SerializedName ("created_at")
		private String createdAt;
		
		@SerializedName ("comment_id")
		private int commentId;
		
		private User editor;
		
		private String contents;

		public String getCreatedAt() {
			return createdAt;
		}

		public int getCommentId() {
			return commentId;
		}

		public User getEditor() {
			return editor;
		}

		public String getContents() {
			return contents;
		}

		@Override
		public String toString() {
			return "Commment [createdAt=" + createdAt + ", commentId=" + commentId + ", editor=" + editor
					+ ", contents=" + contents + "]";
		}
		
	}
	
	public Commment[] getComments() {
		return comments;
	}

	public Pick getPick() {
		return pick;
	}

	@Override
	public String toString() {
		return "PickDetail [comments=" + Arrays.toString(comments) + ", pick=" + pick + "]";
	}
	
}
