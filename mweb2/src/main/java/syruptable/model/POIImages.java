package syruptable.model;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

import syruptable.model.common.BasePick;
import syruptable.model.common.BaseTableAPI;
import syruptable.model.common.POIImage;
import syruptable.model.common.Pick;

public class POIImages extends BaseTableAPI {

	private Image[] images;
	
	public Image[] getImages() {
		return images;
	}

	public class Image extends POIImage {
		
		@SerializedName ("sns_user")
		private SNSUser snsUser;
		
		@SerializedName ("blog_review")
		private BlogReview blogReview;
		
		private Pick pick;


		public class SNSUser {
			
			private int id;
			
			private String name;

			public int getId() {
				return id;
			}

			public String getName() {
				return name;
			}

			@Override
			public String toString() {
				return String.format("SNSUser [id=%s, name=%s]", id, name);
			}
			
		}
		
		public class BlogReview {
			
			private String link;
			
			private String title;

			public String getLink() {
				return link;
			}

			public String getTitle() {
				return title;
			}

			@Override
			public String toString() {
				return String.format("BlogReview [link=%s, title=%s]", link,
						title);
			}
			
		}

		

		public SNSUser getSnsUser() {
			return snsUser;
		}

		public BlogReview getBlogReview() {
			return blogReview;
		}
		
		public BasePick getPick() {
			return pick;
		}
		
		@Override
		public String toString() {
			return "Image [snsUser=" + snsUser + ", blogReview=" + blogReview + ", pick=" + pick + "]";
		}
	}

	
	@Override
	public String toString() {
		return "POIImages [images=" + Arrays.toString(images) + "]";
	}
	
}
