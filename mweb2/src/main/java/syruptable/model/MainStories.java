package syruptable.model;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

import syruptable.model.common.Banner;
import syruptable.model.common.BasePOI;
import syruptable.model.common.BasePick;
import syruptable.model.common.Hashtag;
import syruptable.model.common.User;

public class MainStories {
	
	@SerializedName("verified_poi")
	private VerifiedPOI verifiedPOI;
	
	private Banner[] banners;
	
	private Hashtag[] hashtags;
	
	private Story[] stories;
	
	private TopUser[] celebrities; 
	
	
	public static class VerifiedPOI {
		
		private BasePOI[] pois;
		
		private String link;

		public BasePOI[] getPois() {
			return pois;
		}

		public String getLink() {
			return link;
		}

		@Override
		public String toString() {
			return "VerifiedPOI [pois=" + Arrays.toString(pois) + ", link=" + link + "]";
		}
		
	}

	public static class Story {
		
		private String description;
		
		private String title;
		
		private String image;
		
		private Scoreboard scoreboard;
		
		private String link;
		
		private User user;
		
		private int id;
		
		public static class Scoreboard {
			
			private int likes;
			
			private int views;

			public int getLikes() {
				return likes;
			}

			public int getViews() {
				return views;
			}

			@Override
			public String toString() {
				return "Scoreboard [likes=" + likes + ", views=" + views + "]";
			}
			
		}

		
		public String getDescription() {
			return description;
		}

		public String getTitle() {
			return title;
		}

		public String getImage() {
			return image;
		}

		public Scoreboard getScoreboard() {
			return scoreboard;
		}

		public String getLink() {
			return link;
		}

		public User getUser() {
			return user;
		}

		public int getId() {
			return id;
		}

		@Override
		public String toString() {
			return "Story [description=" + description + ", title=" + title + ", image=" + image + ", scoreboard="
					+ scoreboard + ", link=" + link + ", user=" + user + ", id=" + id + "]";
		}

	}
	
	public static class TopUser extends User {
		
		@SerializedName("recent_picks")
		private BasePick[] recentPicks;
	}

	
	
	public VerifiedPOI getVerifiedPOI() {
		return verifiedPOI;
	}

	public Banner[] getBanners() {
		return banners;
	}

	public Hashtag[] getHashtags() {
		return hashtags;
	}

	public Story[] getStories() {
		return stories;
	}

	public TopUser[] getCelebrities() {
		return celebrities;
	}

	@Override
	public String toString() {
		return "MainStories [verifiedPOI=" + verifiedPOI + ", banners=" + Arrays.toString(banners) + ", hashtags="
				+ Arrays.toString(hashtags) + ", stories=" + Arrays.toString(stories) + ", celebrities="
				+ Arrays.toString(celebrities) + "]";
	}
	
}
