package syruptable.model.common;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

public class BasePick {

	@SerializedName ("is_like")
	private boolean isLike;
	
	private Liker[] likers;
	
	@SerializedName ("created_at")
	private String createdAt;

	private Hashtag[] hashtags;
	
	private Scoreboard scoreboard;
	
	@SerializedName ("pick_id")
	private int pickId;
	
	private User editor;
	
	private String[] images;
	
	private String contents;

	public class Scoreboard {
		
		private int images;
		
		@SerializedName("star_rating")
		private int starRating;
		
		private int likes;
		
		private int comments;

		public int getImages() {
			return images;
		}

		public int getStarRating() {
			return starRating;
		}

		public int getLikes() {
			return likes;
		}

		public int getComments() {
			return comments;
		}

		@Override
		public String toString() {
			return "Scoreboard [images=" + images + ", starRating=" + starRating + ", likes=" + likes
					+ ", comments=" + comments + "]";
		}
		
	}
	
	public boolean isLike() {
		return isLike;
	}

	public Liker[] getLikers() {
		return likers;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public Hashtag[] getHashtags() {
		return hashtags;
	}

	public Scoreboard getScoreboard() {
		return scoreboard;
	}

	public int getPickId() {
		return pickId;
	}

	public User getEditor() {
		return editor;
	}

	public String[] getImages() {
		return images;
	}

	public String getContents() {
		return contents;
	}
	
	public class Liker {
		
		private String nickname;
		
		@SerializedName ("user_id")
		private String userId;
		
		@SerializedName ("profile_image")
		private String profileImage;
		
		private String gender;

		public String getNickname() {
			return nickname;
		}

		public String getUserId() {
			return userId;
		}

		public String getProfileImage() {
			return profileImage;
		}

		public String getGender() {
			return gender;
		}

		@Override
		public String toString() {
			return "Liker [nickname=" + nickname + ", userId=" + userId + ", profileImage=" + profileImage
					+ ", gender=" + gender + "]";
		}
	}

	
	
	@Override
	public String toString() {
		return "BasePick [isLike=" + isLike + ", likers=" + Arrays.toString(likers) + ", createdAt=" + createdAt
				+ ", hashtags=" + Arrays.toString(hashtags) + ", scoreboard=" + scoreboard + ", pickId=" + pickId
				+ ", editor=" + editor + ", images=" + Arrays.toString(images) + ", contents=" + contents + "]";
	}
	
}
