package syruptable.model.common;

import com.google.gson.annotations.SerializedName;

public class User {
	
	@SerializedName ("user_id")
	private int userId;
	
	private String description;
	
	@SerializedName ("profile_image")
	private String profileImage;
	
	private Scoreboard scoreboard;
	
	private String badge;
	
	private String gender;
	
	private String nickname;
	
	@SerializedName("is_following")
	private boolean isFollowing;

	public class Scoreboard {

		private int ratings;
		
		private int score;
		
		private int picks;
		
		private int wishes;
		
		private int followers;
		
		private int followees;

		public int getRatings() {
			return ratings;
		}

		public int getScore() {
			return score;
		}

		public int getPicks() {
			return picks;
		}

		public int getWishes() {
			return wishes;
		}

		public int getFollowers() {
			return followers;
		}

		public int getFollowees() {
			return followees;
		}

		@Override
		public String toString() {
			return "Scoreboard [ratings=" + ratings + ", score=" + score + ", picks=" + picks + ", wishes=" + wishes
					+ ", followers=" + followers + ", followees=" + followees + "]";
		} 
		
	}
	
	public int getUserId() {
		return userId;
	}

	public String getDescription() {
		return description;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public Scoreboard getScoreboard() {
		return scoreboard;
	}

	public String getBadge() {
		return badge;
	}

	public String getGender() {
		return gender;
	}

	public String getNickname() {
		return nickname;
	}
	
	public boolean isFollowing() {
		return isFollowing;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", description=" + description + ", profileImage=" + profileImage
				+ ", scoreboard=" + scoreboard + ", badge=" + badge + ", gender=" + gender + ", nickname=" + nickname
				+ ", isFollowing=" + isFollowing + "]";
	}
	
}