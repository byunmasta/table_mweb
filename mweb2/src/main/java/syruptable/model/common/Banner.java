package syruptable.model.common;

import com.google.gson.annotations.SerializedName;

public class Banner {
	
	private String title;
	
	private int type;
	
	private String image;
	
	private String link;
	
	@SerializedName ("created_at")
	private String createdAt;

	public String getTitle() {
		return title;
	}

	public int getType() {
		return type;
	}

	public String getImage() {
		return image;
	}

	public String getLink() {
		return link;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	@Override
	public String toString() {
		return "Banner [title=" + title + ", type=" + type + ", image=" + image + ", link=" + link + ", createdAt="
				+ createdAt + "]";
	}

}
