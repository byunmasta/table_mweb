package syruptable.model.common;

import java.util.Arrays;

public class Location {

	private String type;
	
	private double[] coordinates;

	public String getType() {
		return type;
	}

	public double[] getCoordinates() {
		return coordinates;
	}

	@Override
	public String toString() {
		return String.format("Location [type=%s, coordinates=%s]", type,
				Arrays.toString(coordinates));
	}
}
