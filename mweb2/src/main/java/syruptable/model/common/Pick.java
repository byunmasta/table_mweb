package syruptable.model.common;

public class Pick extends BasePick {
	
	private BasePOI poi;

	public BasePOI getPoi() {
		return poi;
	}

	@Override
	public String toString() {
		return "Pick [poi=" + poi + "]";
	}
	
}