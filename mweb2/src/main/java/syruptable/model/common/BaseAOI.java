package syruptable.model.common;

import com.google.gson.annotations.SerializedName;

public class BaseAOI {
	
	@SerializedName("aoi_id")
	private int aoiId;
	
	private String name;
	
	private String type;

	public int getAoiId() {
		return aoiId;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	@Override
	public String toString() {
		return "BaseAOI [aoiId=" + aoiId + ", name=" + name + ", type=" + type + "]";
	}
	
}
