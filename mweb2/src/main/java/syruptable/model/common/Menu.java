package syruptable.model.common;

import java.util.Arrays;

public class Menu {
	
	private POIImage[] images;
	
	private BasePOI[] pois;
	
	private int id;
	
	private String hashtag;

	public POIImage[] getImages() {
		return images;
	}

	public BasePOI[] getPois() {
		return pois;
	}

	public int getId() {
		return id;
	}

	public String getHashtag() {
		return hashtag;
	}

	@Override
	public String toString() {
		return "Menu [images=" + Arrays.toString(images) + ", pois=" + Arrays.toString(pois) + ", id=" + id
				+ ", hashtag=" + hashtag + "]";
	}
	
}