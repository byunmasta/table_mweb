package syruptable.model.common;

import java.text.DecimalFormat;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.PrecisionModel;

import syruptable.service.SyrupTableService.ImageType;
import syruptable.service.SyrupTableService.SortType;

public class SyrupTable {
	
	/**
	 * GPS 좌표에 대한 값을 cache 하기 위한 최소 값
	 *  0.00001° 차이는 우리나라의 남부지방(위도 35°)에서 0.91m 
	 */
	private static final DecimalFormat decimalFormat = new DecimalFormat(".######");
	
	private int aoiId;
	
	private int poiId;
	
	private int pickId;
	
	private int hashtagId;
	
	private String categoryId;
	
	private int hasCoupon;
	
	private int hasParking;
	
	private String set;
	
	private String cursor;
	
	private int limit;
	
	/**
	 *  popular  // 인기
	 *  ,latest  // 최근
	 *	,nearby  // 거리
	 *	,score 	 // 정확도
	 *	,DI      // DI : 거리순
	 *	,PO      // PO : 인기순
	 *	,BE;     // BE : 혜택순
	 */
	private SortType sortType;
	
	private ImageType imageType;
	
	/**
	 * 경도
	 */
	private double longitude;
	
	/**
	 * 위도
	 */
	private double latitude;
	
	public int getAoiId() {
		return aoiId;
	}
	
	public int getPoiId() {
		return poiId;
	}

	public int getPickId() {
		return pickId;
	}

	public int getHashtagId() {
		return hashtagId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public int getHasCoupon() {
		return hasCoupon;
	}

	public int getHasParking() {
		return hasParking;
	}

	public String getSet() {
		return set;
	}
	
	public String getCursor() {
		return cursor;
	}

	public int getLimit() {
		return limit;
	}

	public SortType getSortType() {
		return sortType;
	}
	
	public ImageType getImageType() {
		return imageType;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}
	
	public String getLongitude6() {
		return decimalFormat.format(longitude);
	}

	public String getLatitude6() {
		return decimalFormat.format(latitude);
	}
	
	/**
	 * Geometry Factory : make Point, Polygon and ETC
	 */
	private static final GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326);
	
	
	public Point getPoint() {
		
		/**
		 * GPS 좌표에 대한 값을 cache 하기 위한 최소 값
		 *  0.00001° 차이는 우리나라의 남부지방(위도 35°)에서 0.91m 
		 */
		double longitude6 = Double.parseDouble(this.getLongitude6());
		double latitude6 = Double.parseDouble(this.getLatitude6());
		
		return geometryFactory.createPoint(new Coordinate(longitude6, latitude6));
	}

	public SyrupTable(Builder builder) {
		this.aoiId = builder.aoiId;
		this.poiId = builder.poiId;
		this.pickId = builder.pickId;
		this.hashtagId = builder.hashtagId;
		this.categoryId = builder.categoryId;
		this.hasCoupon = builder.hasCoupon;
		this.hasParking = builder.hasParking;
		this.set = builder.set;
		this.cursor = builder.cursor;
		this.limit = builder.limit;
		this.latitude = builder.latitude;
		this.longitude = builder.longitude;
		this.sortType = builder.sortType;
		this.imageType = builder.imageType;
	}
	
	public static class Builder {
		
		private int aoiId;
		
		private int poiId;
		
		private int pickId;
		
		private int hashtagId;
		
		private String categoryId;
		
		private int hasCoupon;
		
		private int hasParking;
		
		private String set;
		
		private String cursor;
		
		private int limit;
		
		/**
		 *  popular  // 인기
		 *  ,latest  // 최근
		 *	,nearby  // 거리
		 *	,DI      // DI : 거리순
		 *	,PO      // PO : 인기순
		 *	,BE;     // BE : 혜택순
		 */
		private SortType sortType;
		
		private ImageType imageType;
		
		/**
		 * 경도
		 */
		private double longitude;
		
		/**
		 * 위도
		 */
		private double latitude;
		
		/**
		 * 지역 id
		 * @param aoiId
		 * @return
		 */
		public final Builder setAoiId(int aoiId) {
			this.aoiId = aoiId;
			return this;
		}
		
		public final Builder setPoiId(int poiId) {
			this.poiId = poiId;
			return this;
		}
		
		public final Builder setPickId(int pickId) {
			this.pickId = pickId;
			return this;
		}
		
		public final Builder setHashtagId(int hashtagId) {
			this.hashtagId = hashtagId;
			return this;
		}

		public final Builder setCategoryId(String categoryId) {
			this.categoryId = categoryId;
			return this;
		}

		public final Builder setHasCoupon(int hasCoupon) {
			this.hasCoupon = hasCoupon;
			return this;
		}

		public final Builder setHasParking(int hasParking) {
			this.hasParking = hasParking;
			return this;
		}
		
		public final Builder setSet(String set) {
			this.set = set;
			return this;
		}

		public final Builder setCursor(String cursor) {
			this.cursor = cursor;
			return this;
		}

		public final Builder setLimit(int limit) {
			this.limit = limit;
			return this;
		}

		public final Builder setSortType(SortType sortType) {
			this.sortType = sortType;
			return this;
		}
		
		public final Builder setImageType(ImageType imageType) {
			this.imageType = imageType;
			return this;
		}

		public final Builder setLongitude(double longitude) {
			this.longitude = longitude;
			return this;
		}

		public final Builder setLatitude(double latitude) {
			this.latitude = latitude;
			return this;
		}

		public final SyrupTable build() {
			return new SyrupTable(this);
		}
		
	}

}
