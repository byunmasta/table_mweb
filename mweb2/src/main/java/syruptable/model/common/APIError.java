package syruptable.model.common;

import com.google.gson.annotations.SerializedName;

public class APIError {
	
	private Error error;
	
	public Error getError() {
		return error;
	}

	public class Error {
		
		private String code;
		
		private String name;
		
		@SerializedName ("display_message")
		private String displayMessage;

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDisplayMessage() {
			return displayMessage;
		}

		public void setDisplayMessage(String displayMessage) {
			this.displayMessage = displayMessage;
		}

		@Override
		public String toString() {
			return "Error [code=" + code + ", name=" + name + ", displayMessage=" + displayMessage + "]";
		}
		
	}

	@Override
	public String toString() {
		return "APIError [error=" + error + "]";
	}
	
	

}
