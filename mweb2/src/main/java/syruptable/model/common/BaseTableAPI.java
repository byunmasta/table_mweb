package syruptable.model.common;

import com.google.gson.annotations.SerializedName;

public class BaseTableAPI {

	@SerializedName ("total_count")
	private int totalCount;
	
	/**
	 * @return the totalCount
	 */
	public int getTotalCount() {
		return totalCount;
	}

	@SerializedName ("paging")
	private Paging paging;
	
	public Paging getPaging() {
		return paging;
	}

}
