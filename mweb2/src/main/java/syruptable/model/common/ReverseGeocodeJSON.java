package syruptable.model.common;

import com.google.gson.annotations.SerializedName;
/**
 * TOP 리버스지오코딩 API JSON ERROR 결과 Gson예외처리 위한 클래스
 * 현재 필드는 사용되고 있지 않으나, 향후 사용을 경우를 위해 생성
 * 문서 : https://developers.skplanetx.com/apidoc/kor/t-map/geocoding/
 * 호출 API : http://openmap1.tmap.co.kr/tmap/geo/reversegeocoding?version=1&lat=37.56626139533117&lon=126.97791682833257&coordType=WGS84GEO&addressType=A00&format=json
 * <결과 Json 예시>
 *	{ 
 *		"error":{ 
 *		"id":"400",  
 *		"category":"tmap",
 *		"code":"2200",
 *		"message":"제공되지 않는 주소 범위입니다.([C2A501]제공되지 않는 주소 범위 입니다.)" 
 *		}
 *	}
 */

public class ReverseGeocodeJSON {
	
	/**
	 * TOP 리버스지오코딩 root 
	 * 
	 */
	@SerializedName ("addressInfo")
	public AddressInfo addressInfo;
	
	/**
	 *  주소 정보입니다.
	 *  TOP 리버스지오코딩 root 
	 * @author webmadeup
	 *
	 */
	public class AddressInfo {
		
		/**
		 *  전체 주소입니다.
		 * - ‘addressType = A00’인경우 [행정동,법정동] 순서로 표출되며,
 		 *	구분자(콤마)로 구분 되여 집니다. 
		 * - 예) 서울특별시 강서구 화곡3동,서울특별시 강서구 화곡동 1019-1
		 * "fullAddress": "서울특별시 중구 명동,서울특별시 중구 태평로1가  31",
		 */
		public String fullAddress;
		
		/**
		 * <주소 타입>
		 *	A00 
		 *	- 선택한 좌표계에 해당하는 행정동,법정동 주소 입니다.
		 *	
		 *	A01
		 *	- 선택한 좌표게에 해당하는 행정동 입니다.
		 *	- 예) 망원2동, 일산1동
		 *	
		 *	A02
		 *	- 선택한 좌표계에 해당하는 법정동 주소입니다.
		 *	- 예) 방화동, 목동
		 *	
		 *	A03
		 *	- 선택한 좌표계에 해당하는 새(도로명) 주소입니다.
		 *	
		 *	A04
		 *	- 선택한 좌표계에 해당하는 건물 번호입니다.
		 *	- 예) 양천로 14길 95-11
		 * "addressType": "A00"
		 */
		public String addressType;
		
		/**
		 * "city_do": "서울특별시",
		 */
		public String city_do;
		
		/**
		 *  "gu_gun": "중구"
		 */
		public String gu_gun;
		
		/**
		 * 행정동 명
		 * "adminDong": "명동",
		 */
		public String adminDong;
		
		/**
		 * 행정동 코드
		 * "adminDongCode": "1114055000",
		 */
		public String adminDongCode;
		
		/**
		 * 법정동 명
		 * "legalDong": "태평로1가",
		 */
		public String legalDong;
		
		/**
		 * 행정동 코드
		 * "legalDongCode": "1114010300", 
		 */
		public String legalDongCode;
		
		/***
		 * 리 명
		 * "ri": "",
		 */
		public String ri;
		
		/**
		 * 주소에서 번지에 해당 합니다
		 * "bunji": "31",
		 */
		public String bunji;
		
		/**
		 * 도로명 주소
		 * "roadName": "",
		 */
		public String roadName;
		
		/**
		 * 주소 건물번호 부여체계를 기준으로 한 건물에 부여된 일련번호입니다. 
		 */
		public String buildingIndex;
		
		/**
		 * 주소 건물명에 해당하는 집합건물의 명칭입니다.
		 */
		public String buildingName;
		
		/**
		 * 건물 출입문에 해당하는 위치 정보와 입력 좌표 사이의 거리입니다.
		 */
		public String mappingDistance;
		
		/**
		 * 주소 도로명에 해당하는 코드 12자리입니다.
		 * - 행정표준코드관리시스템(https://www.code.go.kr)에서 조회가 가능합니다.
		 */
		public String roadCode;

		@Override
		public String toString() {
			return "AddressInfo [fullAddress=" + fullAddress + ", addressType=" + addressType + ", city_do=" + city_do
					+ ", gu_gun=" + gu_gun + ", adminDong=" + adminDong + ", adminDongCode=" + adminDongCode
					+ ", legalDong=" + legalDong + ", legalDongCode=" + legalDongCode + ", ri=" + ri + ", bunji="
					+ bunji + ", roadName=" + roadName + ", buildingIndex=" + buildingIndex + ", buildingName="
					+ buildingName + ", mappingDistance=" + mappingDistance + ", roadCode=" + roadCode + "]";
		}
		
	}
	
	/**
	 * 에러발생시 에러 코드표
	 * * <결과 Json 예시>
	 *	{ 
	 *		"error":{ 
	 *		"id":"400",  
	 *		"category":"tmap",
	 *		"code":"2200",
	 *		"message":"제공되지 않는 주소 범위입니다.([C2A501]제공되지 않는 주소 범위 입니다.)" 
	 *		}
	 *	}
	 */
	@SerializedName ("error")
	public Error error;
	
	/**
	 * 에러발생시 에러 코드표
	 */
	public class Error {
		
		/**
		 * B2B App ID입니다 
		 */
		@SerializedName ("id")
		public String id;
		
		/**
		 * B2B App ID의 이름
		 */
		@SerializedName ("category")
		public String category;
		
		/**ErrorCode Message	                                                    HTTP Status Code
		 *  1004	발급되지 않은 BizAppId입니다	                                    401 Unauthorized
			1005	시스템 오류입니다	                                                500 Internal Server Error
			2200	제공되지 않는 주소 범위입니다	                                    400 Bad Request
			1006	잘못된 형식의 좌표입니다 좌표에 문자가 들어오는 경우	                400 Bad Request
			1007	사용할 수 없는 좌표계입니다 다른 좌표계를 사용	                    400 Bad Request
			1009	입력 좌표 오류입니다 입력된 좌표가 좌표의 최소, 최대 범위를 넘는 경우	400 Bad Request
			1100	요청 데이터 오류입니다	                                            400 Bad Request
			-	클라이언트의 요청이 성공적으로 수행되었음을 의미 합니다.	                200 OK
		 */
		@SerializedName ("code")
		public String code;
		
		/**
		 * 에러 메시지
		 */
		@SerializedName ("message")
		public String message;

		@Override
		public String toString() {
			return "Error [id=" + id + ", category=" + category + ", code=" + code + ", message=" + message + "]";
		}
		
	}

	@Override
	public String toString() {
		return String.format("ReverseGeocodeJSON [addressInfo=%s, error=%s]",
				addressInfo, error);
	}
	
	
}
