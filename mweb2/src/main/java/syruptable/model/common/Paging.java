package syruptable.model.common;

public class Paging {

	private String prev;
	
	private String next;

	/**
	 * @return the next
	 */
	public String getPrev() {
		return prev;
	}
	
	public String getNext() {
		return next;
	}

}
