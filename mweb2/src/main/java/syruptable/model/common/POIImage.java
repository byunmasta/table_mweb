package syruptable.model.common;

public class POIImage {
	
	private String image;
	
	private String type;

	public String getImage() {
		return image;
	}

	public String getType() {
		return type;
	}

	@Override
	public String toString() {
		return "POIImage [image=" + image + ", type=" + type + "]";
	}
}
