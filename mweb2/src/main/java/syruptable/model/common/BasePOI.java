package syruptable.model.common;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

public class BasePOI {

	private Category category;
	
	private Activities activities;
	
	@SerializedName ("poi_id")
	private int poiId;
	
	@SerializedName ("main_image")
	private POIImage mainImage;
	
	private String name;
	
	@SerializedName ("road_address")
	private String roadAddress;
	
	@SerializedName ("address_no")
	private String addressNo;
	
	@SerializedName ("abbr_address")
	private String abbrAddress;
	
	private String address;
	
	private String phone;
	
	private Scoreboard scoreboard;
	
	private Hashtag[] hashtags;
	
	@SerializedName ("has_coupon")
	private boolean hasCoupon;
	
	@SerializedName ("is_closed")
	private boolean isClosed;
	
	private Location location;
	
	private String menu;
	
	private String benefit;
	
	private int mid;
	
	private String taid;
	
	@SerializedName ("business_hours")
	private String businessHours;

	@SerializedName ("sorder_benefit")
	private String sorderBenefit;
	
	@SerializedName ("booking_benefit")
	private String bookingBenefit;
	
	private String[] tags;
	
	@SerializedName("is_lunch_liked")
	private boolean isLunchLiked;
	
	
	public Category getCategory() {
		return category;
	}

	public Activities getActivities() {
		return activities;
	}

	public int getPoiId() {
		return poiId;
	}

	public POIImage getMainImage() {
		return mainImage;
	}

	public String getName() {
		return name;
	}

	public String getRoadAddress() {
		return roadAddress;
	}

	public String getAddressNo() {
		return addressNo;
	}

	public String getAbbrAddress() {
		return abbrAddress;
	}
	
	public String getAddress() {
		return address;
	}

	public String getPhone() {
		return phone;
	}

	public Scoreboard getScoreboard() {
		return scoreboard;
	}

	public Hashtag[] getHashtags() {
		return hashtags;
	}

	public boolean isHasCoupon() {
		return hasCoupon;
	}

	public boolean isClosed() {
		return isClosed;
	}

	public Location getLocation() {
		return location;
	}

	public String getMenu() {
		return menu;
	}

	public String[] getTags() {
		return tags;
	}
	
	public String getBenefit() {
		return benefit;
	}

	public int getMid() {
		return mid;
	}

	public String getTaid() {
		return taid;
	}

	public String getBusinessHours() {
		return businessHours;
	}

	public String getSorderBenefit() {
		return sorderBenefit;
	}

	public String getBookingBenefit() {
		return bookingBenefit;
	}

	public boolean isLunchLiked() {
		return isLunchLiked;
	}
	
	public class Category {
		
		@SerializedName ("category_id")
		private String categoryId;
		
		private String type;
		
		private String name;
		
		@SerializedName ("l_name")
		private String lName;

		public String getCategoryId() {
			return categoryId;
		}

		public String getType() {
			return type;
		}

		public String getName() {
			return name;
		}

		public String getlName() {
			return lName;
		}

		@Override
		public String toString() {
			return "Category [categoryId=" + categoryId + ", type=" + type + ", name=" + name + ", lName=" + lName
					+ "]";
		}
		
	}
	
	public class Activities {
		
		private boolean wished;
		
		private boolean picked;

		public boolean isWished() {
			return wished;
		}

		public boolean isPicked() {
			return picked;
		}

		@Override
		public String toString() {
			return String.format("Activities [wished=%s, picked=%s]", wished,
					picked);
		}
		
	}

	public class Scoreboard {
		
		@SerializedName ("star_rating")
		private double starRating;
		
		private int score;
		
		private int pickers;
		
		private int commments;
		
		private int picks;
		
		@SerializedName ("pick_score")
		private int pickScore;
		
		private int wishes;

		private int rater;
		
		private int images;
		
		@SerializedName ("blog_reviews")
		private int blogReviews;
		
		
		public double getStarRating() {
			return starRating;
		}

		public int getScore() {
			return score;
		}

		public int getPickers() {
			return pickers;
		}

		public int getCommments() {
			return commments;
		}

		public int getPicks() {
			return picks;
		}

		public int getPickScore() {
			return pickScore;
		}

		public int getWishes() {
			return wishes;
		}

		public int getRater() {
			return rater;
		}

		public int getImages() {
			return images;
		}

		public int getBlogReviews() {
			return blogReviews;
		}

		@Override
		public String toString() {
			return "Scoreboard [starRating=" + starRating + ", score=" + score + ", pickers=" + pickers
					+ ", commments=" + commments + ", picks=" + picks + ", pickScore=" + pickScore + ", wishes="
					+ wishes + ", rater=" + rater + ", images=" + images + ", blogReviews=" + blogReviews + "]";
		}
	}

	
	@Override
	public String toString() {
		return "BasePOI [category=" + category + ", activities=" + activities + ", poiId=" + poiId + ", mainImage="
				+ mainImage + ", name=" + name + ", roadAddress=" + roadAddress + ", addressNo=" + addressNo
				+ ", abbrAddress=" + abbrAddress + ", address=" + address + ", phone=" + phone + ", scoreboard="
				+ scoreboard + ", hashtags=" + Arrays.toString(hashtags) + ", hasCoupon=" + hasCoupon + ", isClosed="
				+ isClosed + ", location=" + location + ", menu=" + menu + ", benefit=" + benefit + ", mid=" + mid
				+ ", taid=" + taid + ", businessHours=" + businessHours + ", sorderBenefit=" + sorderBenefit
				+ ", bookingBenefit=" + bookingBenefit + ", tags=" + Arrays.toString(tags) + ", isLunchLiked="
				+ isLunchLiked + "]";
	}
	
}
