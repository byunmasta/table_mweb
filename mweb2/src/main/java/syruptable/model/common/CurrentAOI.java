package syruptable.model.common;

import com.google.gson.annotations.SerializedName;

import syruptable.model.GeofenceThemeZone;

public class CurrentAOI {

	@SerializedName ("nearby_aois")
	private NearByAOIs[] nearbyAOIs;
	

	public NearByAOIs[] getNearbyAOIs() {
		return nearbyAOIs;
	}


	public class NearByAOIs {
		
		@SerializedName ("aoi_id")
		private int aoiId;
			
		private String type;
			
		private String name;

		public int getAoiId() {
			return aoiId;
		}

		public String getType() {
			return type;
		}

		public String getName() {
			return name;
		}

		@Override
		public String toString() {
			return String.format("NearByAOIs [aoiId=%s, type=%s, name=%s]",
					aoiId, type, name);
		}
		
		
		
	}
	
	private GeofenceThemeZone aoi;
	
	public GeofenceThemeZone getAoi() {
		return aoi;
	}

	@Override
	public String toString() {
		return String.format("RBSPZone [nearbyAOIs=%s, aoi=%s]", nearbyAOIs,
				aoi);
	}
	
}
