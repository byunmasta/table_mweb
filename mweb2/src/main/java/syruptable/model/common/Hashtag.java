package syruptable.model.common;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

public class Hashtag {
	
	private int id;
	
	private String hashtag;
	
	private POIImage[] images;
	
	@SerializedName("is_banned")
	private boolean isBanned;

	public int getId() {
		return id;
	}

	public String getHashtag() {
		return hashtag;
	}

	public POIImage[] getImages() {
		return images;
	}

	public boolean isBanned() {
		return isBanned;
	}

	@Override
	public String toString() {
		return "Hashtag [id=" + id + ", hashtag=" + hashtag + ", images=" + Arrays.toString(images) + ", isBanned="
				+ isBanned + "]";
	}
	
}
