package syruptable.model.common;

import java.util.List;

public class Polygon {

	private String type;
	
	private List<?> coordinates;

	public String getType() {
		return type;
	}

	public List<?> getCoordinates() {
		return coordinates;
	}

	@Override
	public String toString() {
		return "Polygon [type=" + type + ", coordinates=" + coordinates + "]";
	}
}
