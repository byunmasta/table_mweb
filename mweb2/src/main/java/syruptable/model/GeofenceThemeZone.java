package syruptable.model;

import java.util.Arrays;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class GeofenceThemeZone {

	/**
	 * aoi번호
	 */
	@SerializedName ("aoi_id")
	private int aoiId;
	
	/**
	 * 중심좌표
	 */
	@SerializedName ("center")
	private Center center;
	
	/**
	 * 폴리곤
	 */
	@SerializedName ("polygon")
	private Polygon polygon;
	

	/**
	 * 타입 : THEME
	 */
	@SerializedName ("type")
	private String type;
	
	/**
	 * 영역의 명칭 :가로수길, 명동
	 */
	@SerializedName ("name")
	private String name;

	
	
	public int getAoiId() {
		return aoiId;
	}

	public Center getCenter() {
		return center;
	}

	public Polygon getPolygon() {
		return polygon;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	/**
	 * 중심좌표
	 */
	public class Center {
		
		/**
		 * "type": "Point",
		 */
		@SerializedName ("type")
		private String type;
		
		/**
		 * 경도, 위도 
		 * "coordinates": [126.9759507029,37.566021673]
		 */
		@SerializedName ("coordinates")
		private double[] coordinates;
		
		/**
		 * 폴리곤 영역 리스트
		 */
		@SerializedName ("polygon")
		private Polygon polygon;
		
		

		public String getType() {
			return type;
		}



		public double[] getCoordinates() {
			return coordinates;
		}



		public Polygon getPolygon() {
			return polygon;
		}



		@Override
		public String toString() {
			return String.format(
					"Center [type=%s, coordinates=%s, polygon=%s]", type,
					Arrays.toString(coordinates), polygon);
		}
		
	}
	
	/**
	 * 폴리곤 영역 리스트
	 */
	public class Polygon {
		
		/**
		 * "type": "Polygon",
		 */
		@SerializedName ("type")
		private String type;
		
		/**
		 * "coordinates": [
								[
								 [126.9773629855,37.5670835499],
								 [126.9778731329,37.5670872559],
								 [126.9780805101,37.5670828184]
								]
							]
		 */
		@SerializedName ("coordinates")
		private List<?> coordinate;
		
		

		public String getType() {
			return type;
		}



		public List<?> getCoordinate() {
			return coordinate;
		}



		@Override
		public String toString() {
			return String.format("Polygon [type=%s, coordinate=%s]", type,
					coordinate);
		}
		
		
	}

	@Override
	public String toString() {
		return String
				.format("GeofenceThemeZone [aoiId=%s, center=%s, polygon=%s, type=%s, name=%s]",
						aoiId, center, polygon, type, name);
	}

	

}
