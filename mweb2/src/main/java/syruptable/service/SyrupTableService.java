package syruptable.service;

import org.json.JSONObject;

import syruptable.config.HttpAPIList;
import syruptable.model.AOIList;
import syruptable.model.AutocompleteResult;
import syruptable.model.Categories;
import syruptable.model.HashtagDetail;
import syruptable.model.MainPlaces;
import syruptable.model.MainStories;
import syruptable.model.Notices;
import syruptable.model.POIDetail;
import syruptable.model.POIImages;
import syruptable.model.PickDetail;
import syruptable.model.PopularWords;
import syruptable.model.SearchResult;
import syruptable.model.StoryList;
import syruptable.model.TodayLunch;
import syruptable.model.UserInfo;
import syruptable.model.common.CurrentAOI;
import syruptable.model.common.ReverseGeocodeJSON;

/**
 * Syrup Table 구현물
 * @author webmadeup
 *
 */
public interface SyrupTableService {
	
	/**
	 * POI에 대한 정렬정의
	 * @author webmadeup
	 *
	 */
	public static enum SortType {
		popular		// 인기
		,latest		// 최근
		,nearby		// 거리
		,distance	// 거리
		,score		// 정확도
		,DI			// DI : 거리순
		,PO			// PO : 인기순
		,BE;		// BE : 혜택순
	}
	
	
	/**
	 * 이미지보기시 이미지별 출처[picks, blogs]
	 * @author webmadeup
	 *
	 */
	public static enum ImageType {
		picks(HttpAPIList.POIIMAGEFORPICK), 
		blogs(HttpAPIList.POIIMAGEFORBLOG);
		
		private HttpAPIList url;
		
		/**
		 * HttpAPIList 의 값을 리턴
		 * @return
		 */
		public HttpAPIList getUrl() {
			return this.url;
		}
		/**
		 * enum 객체를 생성할때, properties에 존재하지 않을 경우를 대비하여, 생성자에게 기본 값을 설정한다.
		 * @param url
		 */
		ImageType(HttpAPIList url) {
			this.url = url;
		}
	}
	
	
	/**
	 * Story contents type
	 * @author byunmasta
	 *
	 */
	public static enum StoryType {
		MDTE  	// 미디어+텍스트
		,TEXT  	// 텍스트
		,MEDI  	// 미디어
		,POTE   // 
	}
	
	/**
	 * 카테고리 전체 리스트
	 * @return
	 */
	public Categories getCategoryList();
	
	
	/**
	 * 공지사항 전체 리스트
	 */
	public Notices getNotices(final String cursor, final int limit);
	
	/**
	 * 공지사항 상세보기
	 * @param noticeId
	 * @return
	 */
	public Notices getNoticeDetail(final int noticeId);
	
	
	/**
	 * 메인의 주변목록 가져온다.
	 * @param aoiId 지역 id
	 * @param set 초기 탭 (latest, ranking, coupon, sorder)
	 * @param categoryId 카테고리 업종 아이디
	 * @param hashtagId 해시태그 아이디
	 * @param longitude 경도
	 * @param latitude 위도
	 * @param cursor 다음 페이지 요청
	 * @param limit 요청개수 (default : 20)
	 * @return
	 */
	public MainPlaces getMainPlaces(final int aoiId, final String set, final String lat, final String lng
									, final int hashtagId, final String categoryId, final String cursor, final int limit);
	
	/**
	 * 메인의 스토리목록을 가져온다.
	 * @return
	 */
	public MainStories getMainStories();
	
	/**
	 * 오늘의 점심
	 * @return
	 */
	public TodayLunch getTodayLunch(final int aoiId);
	
	/**
	 * 스토리 리스트 
	 * @return
	 */
	public StoryList getStoryList(String cursor, int limit);
	
	
	/**
	 * geofence된 AOI의 목록을 가져온다.
	 * @return
	 */
	public AOIList getAOIList();
	
	
	/**
	 * 지역별 키워드 목록에서 사용할 geofence 영역을 저장
	 * @param aoiId
	 * @return
	 */
	public JSONObject getGeofenceZoneAOI(int aoiId);
	
	/**
	 * POI 상세보기
	 * @param poiId
	 * @return POIDetail
	 */
	public POIDetail getPOIDetail(final int poiId);
	
	/**
	 * 좌표를 통해 주소 정보를 전송합니다.
	 * @param longitude 경도(필수) longitude
	 * @param latitude 위도(필수) latitude
	 * @return
	 */
	public ReverseGeocodeJSON getReverseGeocode(final String longitude, final String latitude);
	
	/**
	 * Syrup Table 의 geofenceZone를 조회한다.
	 * @param longitude
	 * @param latitude
	 * @return
	 */
	public CurrentAOI getCurrentAOI(final String longitude, final String latitude);
	
	/**
	 * poi상세 리뷰 상세보기
	 * @param pickId
	 * @param cursor
	 * @param limit
	 * @return
	 */
	public PickDetail getPickDetail(final int pickId, final String cursor, final int limit);
	
	/**
	 * POI에서 상세에서 사용자들이 등록한 이미지 목록 조회
	 * @param imageType picks, blogs 으로 정의 되어 있음
	 * @param poiId
	 * @param cursor
	 * @param limit
	 * @return
	 */
	public POIImages getPOIImageList(final ImageType imageType, final int poiId, final String cursor, final int limit);
	
	
	/**
	 * User 정보 얻어오기
	 * @param token
	 * @return
	 */
	public UserInfo getUserInfo(final String token);
	
	
	/**
	 * Hashtag별 poi리스트 얻어오기
	 * @param hashtagId
	 * @param sort(popular|distance)
	 * @param lng
	 * @param lat
	 * @param cursor
	 * @param limit
	 * @return
	 */
	public HashtagDetail getHashtagDetail(final int hashtagId ,final String sort, final String lng, final String lat, final String cursor, final int limit);

	
	/**
	 * 검색결과 가져오기
	 * @param query
	 * @param aoiName
	 * @param sort
	 * @param hasCoupon
	 * @param lng
	 * @param lat
	 * @param refLng
	 * @param refLat
	 * @param cursor
	 * @param limit
	 * @return
	 */
	public SearchResult getSearchResult(final String query, final String aoiName, final String sort, final int hasCoupon, final String lng, final String lat, final String refLng, final String refLat, final String cursor, final int limit);
	
	/**
	 * 자동완성
	 * @param query
	 * @return
	 */
	public AutocompleteResult getAutocompleteResult(final String query);
	
	/**
	 * 인기키워드 목록
	 * @return
	 */
	public PopularWords getPopularWords();
}
