package syruptable.service;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import syruptable.config.HttpAPIList;
import syruptable.exception.TableAPIErrorHandler;
import syruptable.exception.UserTokenAPIErrorHandler;
import syruptable.model.AOIList;
import syruptable.model.AutocompleteResult;
import syruptable.model.Categories;
import syruptable.model.GeofenceThemeZone;
import syruptable.model.HashtagDetail;
import syruptable.model.MainPlaces;
import syruptable.model.MainStories;
import syruptable.model.Notices;
import syruptable.model.POIDetail;
import syruptable.model.POIImages;
import syruptable.model.PickDetail;
import syruptable.model.PopularWords;
import syruptable.model.SearchResult;
import syruptable.model.StoryList;
import syruptable.model.TodayLunch;
import syruptable.model.UserInfo;
import syruptable.model.common.CurrentAOI;
import syruptable.model.common.ReverseGeocodeJSON;
import syruptable.support.ApiHelper;

/**
 * Syrup Table 구현
 * @author webmadeup
 *
 */
@Service
public class SyrupTableServiceImpl implements SyrupTableService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ApiHelper apiHelper;

	/**
	 * 카테고리 리스트
	 * https://sapi.pickat.in/v1/categories
	 */
	@Override
	@Cacheable("category")
	public Categories getCategoryList() {
		logger.info("===============category==================");
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.CATEGORYLIST)
				.setHttpMethod(HttpMethod.GET)
				.setErrorHandler(new TableAPIErrorHandler())
				.setResponse(String.class)
				.build();

		Categories categories =  new Gson().fromJson(responseNode, Categories.class);
		logger.debug(categories.toString());
		
		return categories;
	}
	
	/**
	 * 공지사항 리스트
	 * https://sapi.pickat.in/v1/notices?cursor={cursor}&limit={limit}
	 */
	@Override
	@Cacheable("notices")
	public Notices getNotices(final String cursor, final int limit) {
		logger.info("===============notices==================");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("cursor", cursor);  //상용 발행 DB
		variables.put("limit", limit);  //상용 발행 DB
		
		final String responseNode = apiHelper.getPickatAuthHttp()
									.setApiName(HttpAPIList.NOTICELIST)
									.setHttpMethod(HttpMethod.GET)
									.setUriVariableValues(variables)
									.setErrorHandler(new TableAPIErrorHandler())
									.setResponse(String.class)
									.build();
		
		Notices notices =  new Gson().fromJson(responseNode, Notices.class);
		logger.debug(notices.toString());
		return notices;
	}

	/**
	 * 공지사항 상세보기
	 * https://sapi.pickat.in/v1/notices/{notice_id}
	 */
	@Override
	public Notices getNoticeDetail(final int noticeId) {
		
		logger.info("===============noticesDetail==================");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("notice_id", noticeId);  //상용 발행 DB
		
		final String responseNode = apiHelper.getPickatAuthHttp()
									.setApiName(HttpAPIList.NOTICEDETAIL)
									.setHttpMethod(HttpMethod.GET)
									.setUriVariableValues(variables)
									.setErrorHandler(new TableAPIErrorHandler())
									.setResponse(String.class)
									.build();
		
		Notices notices =  new Gson().fromJson(responseNode, Notices.class);
		logger.debug(notices.getNoticeDetail().toString());
		
		return notices;
		
	}
	
	
	/**
	 * 메인 주변 보기
	 */
	@Override
	@Cacheable("mainPlaces")
	public MainPlaces getMainPlaces(int aoiId, String set, String lat, String lng, int hashtagId, String categoryId,
			String cursor, int limit) {

		logger.info("===============MainPlaces==================");
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("aoi_id", aoiId);
		variables.put("set", set);
		variables.put("lat", lat);
		variables.put("lng", lng);
		variables.put("hashtag_id", hashtagId);
		variables.put("category_id", categoryId);
		variables.put("cursor", cursor);
		variables.put("limit", limit);
		
		
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.MAINPLACES)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setErrorHandler(new TableAPIErrorHandler())
				.setResponse(String.class)
				.build();
		logger.debug("mainPlaces responseNode {}", responseNode);
		
		MainPlaces mainPlaces =  new Gson().fromJson(responseNode, MainPlaces.class);
		return mainPlaces;
	}
	
	/**
	 * 메인 스토리 보기
	 */
	@Override
	@Cacheable("mainStories")
	public MainStories getMainStories() {

		logger.info("===============MainStories==================");
		
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.MAINSTORIES)
				.setHttpMethod(HttpMethod.GET)
				.setErrorHandler(new TableAPIErrorHandler())
				.setResponse(String.class)
				.build();
		logger.debug("mainStories responseNode {}", responseNode);
		
		MainStories mainStories =  new Gson().fromJson(responseNode, MainStories.class);
		return mainStories;
	}
	
	/**
	 * 오늘의 점심
	 */
	@Override
	@Cacheable("todayLunch")
	public TodayLunch getTodayLunch(int aoiId){
		
		logger.info("===============todayLunch==================");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("aoi_id", aoiId);
		
		final String responseNode = apiHelper.getPickatAuthHttp()
									.setApiName(HttpAPIList.TODAYLUNCH)
									.setHttpMethod(HttpMethod.GET)
									.setUriVariableValues(variables)
									.setErrorHandler(new TableAPIErrorHandler())
									.setResponse(String.class)
									.build();
		
		TodayLunch todayLunch =  new Gson().fromJson(responseNode, TodayLunch.class);
		logger.debug(todayLunch.toString());
		return todayLunch;
		
	}
	
	/**
	 * 스토리 리스트 
	 */
	@Override
	@Cacheable("storyList")
	public StoryList getStoryList(String cursor, int limit) {

		logger.info("===============Story List==================");
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("cursor", cursor);
		variables.put("limit", limit);
		
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.STORYLIST)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setErrorHandler(new TableAPIErrorHandler())
				.setResponse(String.class)
				.build();
		logger.debug("StoryList responseNode {}", responseNode);
		
		StoryList storyList =  new Gson().fromJson(responseNode, StoryList.class);
		return storyList;
	}
	

	/**
	 * geofence된 AOI의 목록을 가져온다.
	 * @return
	 */
	@Override
	@Cacheable("aoiList")
	public AOIList getAOIList() {
		
		logger.info("===============AOIList==================");
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("type", "theme");  
		
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.AOILIST)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setErrorHandler(new TableAPIErrorHandler())
				.setResponse(String.class)
				.build();
		
		logger.debug(responseNode);
		
		AOIList aoiList =  new Gson().fromJson(responseNode, AOIList.class);
		return aoiList;
	}
	
	/**
	 * 테마속 장소에서 사용할 geofence 영역을 저장
	 * @param model
	 * @param cursor
	 * @param limit
	 * @return
	 */
	@Cacheable("geofenceZoneAOI")
	public JSONObject getGeofenceZoneAOI(int aoiId) {
		
		logger.info("=============== geofence 영역을 저장 getGeofenceZoneAOI ==================");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("aoi_id", aoiId);
		
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.AOIZONE)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setErrorHandler(new TableAPIErrorHandler())
				.setResponse(String.class)
				.build();
		
		logger.debug("geofence 영역을 {}", responseNode);
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(responseNode);
		String response= element.getAsJsonObject().get("aoi").toString();
		
		GeofenceThemeZone geofenceZoneAOI =  new Gson().fromJson(response, GeofenceThemeZone.class);
		
		JSONObject geofence = new JSONObject();
		
		if( geofenceZoneAOI!= null && geofenceZoneAOI.getPolygon() != null) {
			geofence.put("center", geofenceZoneAOI.getCenter().getCoordinates());
			geofence.put("aoiName", geofenceZoneAOI.getName());
			geofence.put("aoiType", geofenceZoneAOI.getType());
			JSONObject geometry = new JSONObject();
			geometry.put("type", geofenceZoneAOI.getPolygon().getType());
			geometry.put("coordinates", geofenceZoneAOI.getPolygon().getCoordinate());
			geofence.put("geometry", geometry);
		} else {
//			logger.error(geofenceThemeZone.toString());
		}
		
		return geofence;
	}

	/**
	 * POI 상세보기
	 * @param poiId
	 * @return POIDetail
	 */
	@Cacheable("poiDetail")
	@Override
	public POIDetail getPOIDetail(int poiId) {
		
		logger.info("=============== POI 상세보기 getPOIDetail ==================");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("poi_id", poiId);  
		
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.POIDETAIL)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setErrorHandler(new TableAPIErrorHandler())
				.setResponse(String.class)
				.build();
		
		logger.debug("POI 상세보기 {}", responseNode);
		
		POIDetail poiDetail =  new Gson().fromJson(responseNode, POIDetail.class);
		
		return poiDetail;
	}

	private static final String BASEADDRESS = "{\"addressInfo\": {\"fullAddress\": \"서울특별시 중구 명동,서울특별시 중구 명동2가  32-26\", \"addressType\": \"A00\", \"city_do\": \"서울특별시\",\"gu_gun\": \"중구\", \"eup_myun\": \"\", \"adminDong\": \"명동\",\"adminDongCode\": \"1114055000\", \"legalDong\": \"명동2가\", \"legalDongCode\": \"1114012700\", \"ri\": \"\", \"bunji\": \"32-26\", \"roadName\": \"\", \"buildingIndex\": \"\", \"buildingName\": \"\", \"mappingDistance\": \"\", \"roadCode\": \"\" }}";
	private static final String BASERBSP = "{\"nearby_aois\":[{\"aoi_id\":117936,\"type\":\"THEME\",\"name\":\"종각피아노거리\"},{\"aoi_id\":117937,\"type\":\"THEME\",\"name\":\"종로\"},{\"aoi_id\":117958,\"type\":\"THEME\",\"name\":\"남산\"},{\"aoi_id\":117961,\"type\":\"THEME\",\"name\":\"다동\\/무교동 음식문화특화거리\"},{\"aoi_id\":117994,\"type\":\"THEME\",\"name\":\"을지로골뱅이골목\"}],\"aoi\":{\"aoi_id\":117965,\"center\":{\"type\":\"Point\",\"coordinates\":[126.9849551207,37.5627117943]},\"parents\":[],\"polygon\":{\"type\":\"Polygon\",\"coordinates\":[[[126.9835174003,37.5659308715],[126.9835951724,37.5659389518],[126.9840027632,37.5659420753],[126.9840913848,37.5654642937],[126.984123763,37.5654370004],[126.9841475389,37.5654199751],[126.9841488971,37.5654177226],[126.984236363,37.5650458058],[126.9842422469,37.5649984104],[126.9842933094,37.5649829257],[126.9844132864,37.5649874494],[126.9844972727,37.5649871871],[126.9845006668,37.5649870094],[126.9845032721,37.5649858374],[126.9846248545,37.564895034],[126.9847436318,37.5647012487],[126.9848737657,37.5643297882],[126.9849406911,37.5641819463],[126.9850428386,37.5639245491],[126.9850969705,37.5637877835],[126.9852342621,37.5638075362],[126.9852358453,37.5638079862],[126.9858251759,37.5639352868],[126.9858766687,37.5639513162],[126.9860559459,37.5640071238],[126.9860995189,37.5640258723],[126.9861568987,37.5640506565],[126.9862478916,37.5640897699],[126.9864550019,37.5641918778],[126.986531735,37.5642297301],[126.9865837931,37.564255414],[126.9869605412,37.5644966552],[126.9871073338,37.5645482987],[126.9872368179,37.564568318],[126.9873481932,37.5645677006],[126.9873703771,37.5645659902],[126.9874336522,37.5645439245],[126.9874369353,37.5645419415],[126.9874410099,37.5645397807],[126.9874432766,37.5645337454],[126.9875069167,37.5643688722],[126.9875612706,37.5642401251],[126.9876123385,37.5641190381],[126.9876871794,37.5639893969],[126.9877735635,37.563857859],[126.9878844018,37.563696414],[126.9879098661,37.5636593289],[126.9879570866,37.5635905589],[126.9880126724,37.5635094753],[126.9880882438,37.5634001924],[126.988119208,37.5633554155],[126.9882809906,37.5631203666],[126.9884114113,37.5629231542],[126.9884826242,37.5628064836],[126.9885373145,37.5626700791],[126.9885885035,37.5624923184],[126.9886028931,37.5624153753],[126.9886262309,37.5622750957],[126.9886474185,37.5621472478],[126.9886755214,37.5619525471],[126.9887082456,37.5618688468],[126.9887601523,37.561449349],[126.9887600414,37.5614437636],[126.9887457919,37.5613764577],[126.9887447754,37.5613740247],[126.988711391,37.5613326681],[126.9885947054,37.5613222061],[126.9877709793,37.5612485004],[126.9876178441,37.5612347562],[126.9875823018,37.5612315795],[126.9872740095,37.5612040247],[126.9869769141,37.5611774087],[126.9868679251,37.5611676635],[126.9866116871,37.5611310523],[126.9866068209,37.5611285303],[126.986601616,37.561126547],[126.9862451016,37.5610944251],[126.9861548968,37.5610863051],[126.9859970125,37.5610721401],[126.9859293332,37.5610654647],[126.9858574626,37.561055634],[126.9855931949,37.5610192004],[126.9855435082,37.5610155889],[126.9852005756,37.5609904025],[126.9851840495,37.5609906716],[126.9850429162,37.5609814619],[126.9839479178,37.5608629151],[126.983365726,37.5608104805],[126.9832738268,37.5608056016],[126.9832631889,37.5608024463],[126.9822145904,37.5607277672],[126.9822065522,37.560727767],[126.9821912732,37.560728575],[126.9821766718,37.5607300134],[126.9821679558,37.5607330768],[126.9821600343,37.560735868],[126.9821524516,37.5607387508],[126.9821462215,37.5607418143],[126.9821407887,37.5607456859],[126.9821364861,37.5607510018],[126.9821329781,37.560756315],[126.9821308254,37.560759198],[126.9821079543,37.5607966763],[126.9819253105,37.5611091126],[126.9818709634,37.561194154],[126.9817136055,37.5613274766],[126.9815434041,37.5616660404],[126.9815420459,37.561669465],[126.9815107262,37.5619781377],[126.9815322108,37.5620548149],[126.9819357519,37.5627509869],[126.982088356,37.5630637486],[126.9822123301,37.5633561401],[126.9822664501,37.5637372639],[126.9822689356,37.5637526731],[126.9823438785,37.5640082719],[126.9824582344,37.5643982885],[126.9828139234,37.5643812299],[126.983233501,37.5644033358],[126.9832328268,37.564448435],[126.9832229068,37.5649239509],[126.9834642962,37.5649236952],[126.9834673458,37.5649777511],[126.9834922451,37.5654191048],[126.9835174003,37.5659308715]]]},\"type\":\"THEME\",\"name\":\"명동\"}}";
	/**
	 * 좌표를 통해 주소 정보를 전송합니다.
	 *  https://developers.skplanetx.com/apidoc/kor/t-map/geocoding/
	 *  Home > API > T map > Geocoding
	 
	 * lat : 지구상의 지점 위치를 나타내는 위도 좌표입니다.
	 * lon : 지구상의 지점 위치를 나타내는 경도 좌표입니다.
	 * coordType : 	EPSG3857(Google Mercator)
				   	WGS84GEO(경위도)
				   	KATECH(TM128(Transverse Mercator:횡메카토르): 한국 표준)
					BESSELGEO(GCS_Bessel_1841 타원체를 사용)
					BESSELTM(Transverse_Mercator 투영법과 GCS_Bessel_1841 타원체를 사용)
					GRS80GEO(GRS_1980 타원체를 사용)
					GRS80TM(Transverse_Mercator 투영법과 GRS_1980 타원체를 사용)
					BESSEL(베셀 정규화 좌표계)
		addressType : A00  선택한 좌표계에 해당하는 행정동,법정동 주소 입니다.
					  A01 선택한 좌표게에 해당하는 행정동 입니다.
					  A02 선택한 좌표계에 해당하는 법정동 주소입니다.
					  A03 선택한 좌표계에 해당하는 새주소 길입니다.
					  A04 선택한 좌표계에 해당하는 건물 번호입니다.
		format : json
		 		 xml
		callback : jsonp 포맷에서 사용하는 callback 함수명 정보입니다. 		 
	 * 좌표를 통해 주소 정보를 전송합니다.
	 * @param longitude 경도(필수) longitude
	 * @param latitude 위도(필수) latitude
	 * @return
	 */
	@Cacheable("reverseGeocode")
	@Override
	public ReverseGeocodeJSON getReverseGeocode(String longitude, String latitude) {
	
		
		logger.info("=============== ReverseGeocodeJSON ==================");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("lon", longitude);         // 경도 필수
		variables.put("lat", latitude);			// 위도 필수
		
		String responseNode = apiHelper.getHttp()
				.setApiName(HttpAPIList.REVERSEGEOCODE)
				.setHttpMethod(HttpMethod.GET)
				.setBaseBody(BASEADDRESS)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		logger.info("ReverseGeocode {}", responseNode);
		ReverseGeocodeJSON reverseGeocodeJSON =  new Gson().fromJson(responseNode, ReverseGeocodeJSON.class);
		
		return reverseGeocodeJSON;
	}

	/**
	 * Syrup Table 의 geofenceZone를 조회한다.
	 * @param longitude
	 * @param latitude
	 * @return
	 */
	@Cacheable("currentAOI")
	@Override
	public CurrentAOI getCurrentAOI(String longitude, String latitude) {
		
		logger.info("=============== currentAOI ==================");
		
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("lon", longitude);         // 경도 필수
		variables.put("lat", latitude);			// 위도 필수
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.CURRENTAOI)
				.setHttpMethod(HttpMethod.GET)
				.setBaseBody(BASERBSP)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		logger.debug("currentAOI {}", responseNode);
		
		CurrentAOI currentAOI =  new Gson().fromJson(responseNode, CurrentAOI.class);
		
		return currentAOI;
	}

	/**
	 * poi상세 리뷰 상세보기
	 * @param pickId
	 * @param cursor
	 * @param limit
	 * @return
	 */
	@Cacheable("pickDetail")
	@Override
	public PickDetail getPickDetail(int pickId, String cursor, int limit) {
		
		logger.info("===============  poi의 리뷰 리스트에서 특정 Pick의 커멘트 상세보기 ==================");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("pick_id", pickId);
		variables.put("cursor", cursor);
		variables.put("limit", limit);
		
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.PICKDETAIL)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		
		logger.debug("PickDetail {}", responseNode);
		PickDetail pickDetail =  new Gson().fromJson(responseNode, PickDetail.class);
		
		return pickDetail;
	}

	/**
	 * POI에서 상세에서 사용자들이 등록한 이미지 목록 조회
	 * @param imageType picks, blogs 으로 정의 되어 있음
	 * @param poiId
	 * @param cursor
	 * @param limit
	 * @return
	 */
	@Cacheable("poiImageList")
	@Override
	public POIImages getPOIImageList(ImageType imageType, int poiId, String cursor, int limit) {
		
		logger.info("===============  POI에서 상세에서 사용자들이 등록한 이미지 목록 조회  ==================");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("poi_id", poiId);
		variables.put("cursor", cursor);
		variables.put("limit", limit);
		
		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(imageType.getUrl())
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		
		logger.debug("POIImages {}", responseNode);
		POIImages poiImages =  new Gson().fromJson(responseNode, POIImages.class);
		
		return poiImages;
	}

	
	/**
	 * User 정보 가져오기
	 * @param token
	 * @return
	 */
	@Override
	public UserInfo getUserInfo(String token) {
		
		logger.info("===============  User 정보 가져오기  ==================");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("pitoken", token);

		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.USERINFO)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setErrorHandler(new UserTokenAPIErrorHandler())
				.setResponse(String.class)
				.build();
		
		logger.debug("TokenInfo {}", responseNode);
 		UserInfo userInfo =  new Gson().fromJson(responseNode, UserInfo.class);
		if(userInfo.getError() == null){
			return userInfo;
		}else{
			return null;
		}
	}

	
	/**
	 * Hashtag별 poi리스트 얻어오기
	 * @param hashtagId
	 * @param sort(popular|distance)
	 * @param lon
	 * @param lat
	 * @param cursor
	 * @param limit
	 * @return
	 */
	@Cacheable("hashtagDetail")
	@Override
	public HashtagDetail getHashtagDetail(int hashtagId, String sort, String lng, String lat, String cursor, int limit) {
		// TODO Auto-generated method stub
		logger.info("===============  Hashtag ID별 POI목록 가져오기  ==================");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("hashtag_id", hashtagId);
		variables.put("sort", sort);
		variables.put("lng", lng);
		variables.put("lat", lat);
		variables.put("cursor", cursor);
		variables.put("limit", limit);

		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.HASHTAGDETAIL)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		
		logger.debug("HashtagDetail {}", responseNode);
		HashtagDetail hashtagDetail =  new Gson().fromJson(responseNode, HashtagDetail.class);
		
		return hashtagDetail;
	}

	
	/**
	 * 검색결과 가져오기
	 * @param query
	 * @param aoiName
	 * @param sort
	 * @param hasCoupon
	 * @param lng
	 * @param lat
	 * @param refLng
	 * @param refLat
	 * @param cursor
	 * @param limit
	 * @return
	 */
	@Cacheable("searchResult")
	@Override
	public SearchResult getSearchResult(String query, String aoiName, String sort, int hasCoupon, String lng,
			String lat, String refLng, String refLat, String cursor, int limit) {

		logger.info("===============  검색결과 가져오기  ==================");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("query", query);
		variables.put("aoi_name", aoiName);
		variables.put("sort", sort);
		variables.put("has_coupon", hasCoupon);
		variables.put("lng", lng);
		variables.put("lat", lat);
		variables.put("ref_lng", refLng);
		variables.put("ref_lat", refLat);
		variables.put("cursor", cursor);
		variables.put("limit", limit);

		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.SEARCH)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		
		logger.debug("SearchResult {}", responseNode);
		SearchResult searchResult =  new Gson().fromJson(responseNode, SearchResult.class);
		
		return searchResult;
	}

	/**
	 * 자동완성
	 * @param query
	 * @return
	 */
	@Cacheable("autocompleteResult")
	@Override
	public AutocompleteResult getAutocompleteResult(String query) {

		logger.info("===============  자동완성 결과 가져오기  ==================");
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("query", query);

		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.AUTOCOMPLETE)
				.setHttpMethod(HttpMethod.GET)
				.setUriVariableValues(variables)
				.setResponse(String.class)
				.build();
		
		logger.debug("AutocompleteResult {}", responseNode);
		AutocompleteResult autocompleteResult =  new Gson().fromJson(responseNode, AutocompleteResult.class);
		
		return autocompleteResult;
	}

	/**
	 * 인기키워드 목록
	 * @return
	 */
	@Cacheable("popularWords")
	@Override
	public PopularWords getPopularWords() {

		logger.info("===============  인기키워드 목록 가져오기  ==================");

		final String responseNode = apiHelper.getPickatAuthHttp()
				.setApiName(HttpAPIList.POPULARWORDS)
				.setHttpMethod(HttpMethod.GET)
				.setResponse(String.class)
				.build();
		
		logger.debug("PopularWords {}", responseNode);
		PopularWords popularWords =  new Gson().fromJson(responseNode, PopularWords.class);
		
		return popularWords;
	}
	
}
