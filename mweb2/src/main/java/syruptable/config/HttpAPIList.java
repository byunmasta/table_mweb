package syruptable.config;

/**
 * Http 통신을 통해 연결을 할 경우 APIList에 추가하여 사용하도록 함
 * params.properties에 없을 경우 생성자에 있는 값을 사용합니다.
 * @author webmadeup
 *
 */
public enum HttpAPIList {
	
	/**
	 * POI 상세
	 */
	POIDETAIL("https://sapi.pickat.in/v3/pois/{poi_id}"),
	
	/**
	 * POI의 pick 이미지 조회
	 */
	POIIMAGEFORPICK("https://sapi.pickat.in/v2/pois/{poi_id}/images/picks?cursor={cursor}&limit={limit}"),
	
	/**
	 * POI의 블로그 이미지 조회
	 */
	POIIMAGEFORBLOG("https://sapi.pickat.in/v2/pois/{poi_id}/images/blogs?cursor={cursor}&limit={limit}"),
	
	
	/**
	 * POI의 리뷰 목록을 조회한다.(리뷰)
	 */
	REVIEWLIST("https://sapi.pickat.in/v2/pois/{poi_id}/news?cursor={cursor}&limit={limit}"),
	
	/**
	 * Pick 리뷰상세 및 댓글 목록 조회
	 */
	PICKDETAIL("https://sapi.pickat.in/v3/picks/{pick_id}/comments?cursor={cursor}&limit={limit}"),
	
	/**
	 * 피켓의 RBSP 조회
	 * 좌표가 포함된 aoi를 조회한다.
	 */
	CURRENTAOI("https://sapi.pickat.in/v1/aois/current?lng={lon}&lat={lat}&include=nearby"),
	
	/**
	 * 메인>장소 조회
	 * 곧 deprecate
	 */
	MAINPOILIST("https://sapi.pickat.in/v3/main/places?keyword_id={keyword_id}&category_id={category_id}&sort={sort}&has_coupon={has_coupon}&has_parking={has_parking}&aoi_id={aoi_id}&lng={lon}&lat={lat}&cursor={cursor}&limit={limit}"),
	
	/**
	 * 메인>내주변 조회
	 */
	MAINPLACES("https://sapi.pickat.in/v3/main/places?aoi_id={aoi_id}&set={set}&lat={lat}&lng={lng}&hashtag_id={hashtag_id}&category_id={caregory_id}&cursor={cursor}&limit={limit}"),
	/**
	 * 메인>스토리 조회
	 */
	MAINSTORIES("https://sapi.pickat.in/v3/main/stories"),
	
	/**
	 * 메인>오늘의 점심
	 */
	TODAYLUNCH("https://sapi.pickat.in/v3/lunch/places?aoi_id={aoi_id}"),
	
	/**
	 * 먹스토리>스토리 리스트
	 */
	STORYLIST("https://api.pickat.in/v3/stories?cursor={cursor}&limit={limit}"),
	
	/**
	 * 공지사항 목록 조회
	 */
	NOTICELIST("https://sapi.pickat.in/v1/notices?cursor={cursor}&limit={limit}"),
	
	/**
	 * 공지사항 상세 조회
	 */
	NOTICEDETAIL("https://sapi.pickat.in/v1/notices/{notice_id}"),
	
	
	/**
	 * 카테고리 전체목록
	 */
	CATEGORYLIST("https://sapi.pickat.in/v1/categories"),
	
	
	/**
	 * 리버스지오코딩
	 */
	REVERSEGEOCODE("http://openmap1.tmap.co.kr/tmap/geo/reversegeocoding?version=1&lat={lat}&lon={lon}&coordType=WGS84GEO&addressType=A00&format=json"),
	
	
	/**
	 * pickat영역에서 사용되는 AOI
	 */
	AOILIST("https://sapi.pickat.in/v1/aois?type={type}"),
	
	
	/**
	 * POI의 블로그 리뷰를 조회한다.
	 */
	BLOGREVIEW("https://sapi.pickat.in/v2/pois/{poi_id}/blogs?cursor={cursor}&limit={limit}"),
	
	
	/**
	 * USER의 로그인 정보를 획득한다.
	 */
//	USERINFO("https://sapi.pickat.in/v2/users/{user_id}"),
	
	USERTOKEN("https://sapi.pickat.in/v2.6/users/me/pitoken?usage=stable_mweb"),
	
	USERINFO("https://sapi.pickat.in/v2.6/users/personal-info?token={pitoken}"),
	
	AOIZONE("https://sapi.pickat.in/v1/aois/{aoi_id}"),
	
	/**
	 * 해시태그별 POI리스트를 조회한다.
	 */
	HASHTAGDETAIL("https://api.pickat.com/v3/hashtags/{hashtag_id}/pois?sort={sort}&lat={lat}&lng={lng}&cursor={cursor}&limit={limit}"),
	
	/**
	 * 검색
	 */
	SEARCH("https://api.pickat.com/v3/search/pois?&query={query}&aoi_name={aoi_name}&sort={sort}&has_coupon={has_coupon}&lat={lat}&lng={lng}&ref_lat={ref_lat}&ref_lng={ref_lng}&cursor={cursor}&limit={limit}"),
	/**
	 * 자동완성
	 */
	AUTOCOMPLETE("https://api.pickat.com/v3/search/autocomplete?query={query}"),
	/**
	 * 인기키워드 목록
	 */
	POPULARWORDS("https://api.pickat.com/v1/search/popular-words"),
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * 지역별 핫테마
	 *//*
	HOTTHEMES("https://api2.pickat.com/natemobile/hot-themes-local?sort={sort}&page={page}&page_size={page_size}&lat={lat)&lng={lon}"),

	*//**
	 * 인기피플
	 *//*
	CELEBRITIES("https://api2.pickat.com/natemobile/celebrities?page={page}&page_size={page_size}"),

	*//**
	 * 카테고리별 장소
	 *//*
	POIBYCATEGORY("https://api2.pickat.com/natemobile/poi-by-category?category_code={category_code}&sort={sort}&lat={lat}&lng={lon}&page={page}&page_size={page_size}&radius={radius}&coupononly={coupononly}"),

	*//**
	 * 지역별 인기 장소 리스트
	 *//*
	FAVORITECATEGORY("https://api2.pickat.com/natemobile/popular-pois?lat={lat}&lng={lon}&raidus={raidus}&sort={sort}&page={page}&page_size={page_size}&coupononly={coupononly}"),
	

	*//**
	 * 인기피플의 사용자별 테마 리스트를 반환한다.
	 *//*
	THEMESBYUSER("https://api2.pickat.com/natemobile/themes-by-user?user_id={user_id}&page={page}&page_size={page_size}"),

	*//**
	 * 피켓에서 노출하는 배너를 가져온다
	 *//*
	BANNERS("https://api2.pickat.com/natemobile/banners"),

	*//**
	 * POI에 대한 쿠폰 API
	 *//*
	COUPONPOI("http://indexapi.pickat.com/pickat/couponListByPoi?poiTp={poi_type}&poiId={natePOI}"),*/

	
	/**
	 * POI가 리뷰(pick)된 테마목록을 조회한다.
	 * new?
	 *//*
	PICKTHEMES("http://sapi.pickat.in/v1/pois/{poi_id}/themes?cursor={cursor}&limit={limit}"),*/
	
	/**
	 * POI에 리뷰(pick)한 사용자 목록을 조회한다.
	 * new?
	 *//*
	PICKUSERS("http://sapi.pickat.in/v1/pois/{poi_id}/users?cursor={cursor}&limit={limit}"),*/

	
	/**
	 * 개발 실수를 줄이기 위해 필요 맨 마지막 값을 임의로 셋팅 최근 윗줄을 복사해서 값만 변경 할 것
	 * 
	 */
	END("end");

	private String url;

	/**
	 * 생성지의 값을 리턴
	 */
	public String toString() {
		return this.url;
	}

	/**
	 * enum 객체를 생성할때, properties에 존재하지 않을 경우를 대비하여, 생성자에게 기본 값을 설정한다.
	 * @param url
	 */
	HttpAPIList(String url) {
		this.url = url;
	}
}
