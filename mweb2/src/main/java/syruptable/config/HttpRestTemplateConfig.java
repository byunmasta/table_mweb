package syruptable.config;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * Spring 에서 제공하는 RestTemplate의 기본 구현체는 
 * HttpURLConnection 이기 때문에 HttpClient를 사용하기 위해 기본적인 설정을 해야 해서
 * @Configuration 기반으로 설정하였고, 
 * HTTP 통신시 발생하는 Time wait 와 close wait를 줄이기 위해 PoolingHttpClientConnectionManager 를 사용하였습니다.
 * @author webmadeup
 *
 */
@Configuration
public class HttpRestTemplateConfig {

	@Autowired
	private MessageSourceAccessor msAccessor;
	
	private static final int DEFAULT_MAX_TOTAL_CONNECTIONS = 100;

	private static final int DEFAULT_MAX_CONNECTIONS_PER_ROUTE = 5;

	private static final int DEFAULT_READ_TIMEOUT_MILLISECONDS = (10 * 1000);
	
	
	/**
	 * HttpComponentsClientHttpRequestFactory 로 초기화시 기본적인
	 * 셋팅을 한다(타임아웃, PoolingHttpClientConnectionManager)
	 * @return
	 */
	@Bean
	public ClientHttpRequestFactory httpRequestFactory() {
		return new HttpComponentsClientHttpRequestFactory(httpClient());
	}
	
	/**
	 * Spring 의 RestTemplate의 구현체를 httpClient로 초기화한다.
	 * @return
	 */
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate(httpRequestFactory());
		return restTemplate;
	}
	
	/**
	 * 특정 URL에 대해 3 way handshake 하지 않도록 PoolingHttpClientConnectionManager를 사용한다.
	 * @return
	 */
	@Bean
	public HttpClient httpClient() {
	
		
		final PoolingHttpClientConnectionManager poolConnectionManager = new PoolingHttpClientConnectionManager();
		poolConnectionManager.setMaxTotal(DEFAULT_MAX_TOTAL_CONNECTIONS);
		poolConnectionManager.setDefaultMaxPerRoute(DEFAULT_MAX_CONNECTIONS_PER_ROUTE);
		
		/**
		 * 서버 위치 : 일산서버
		 * 담당자 : 김형균 매니저
		 * API : 리버스지오코딩
		 */
		final String TOP_API =  msAccessor.getMessage("skplanet.syruptable.TOP");						//openmap1.tmap.co.kr
		
		/**
		 *  서버 위치 : 일산 서버
		 *  담당자 : 조성근 매니저
		 *  API : Nate POI_ID 에 해당하는 쿠폰 정보
		 */
		final String COMMERCE_COUPON = msAccessor.getMessage("skplanet.syruptable.coupon");  			//indexapi.pickat.com
		
		/**
		 * 서버 위치 : 일산서버
		 * 담당자 : 손경찬 매니저
		 * API : 피켓 API
		 */
		final String SYRUPTABLE_API = msAccessor.getMessage("skplanet.syruptable.mobileAPI");  				//api2.pickat.com
		
		
		poolConnectionManager.setMaxPerRoute(new HttpRoute(new HttpHost(TOP_API)), 20);				// TOP 리버스지오코딩  	 172.19.107.53
		poolConnectionManager.setMaxPerRoute(new HttpRoute(new HttpHost(COMMERCE_COUPON)), 20);		// 모바일커머스 쿠폰 API   172.19.112.87
		poolConnectionManager.setMaxPerRoute(new HttpRoute(new HttpHost(SYRUPTABLE_API)), 40);		// Pickat API  			 172.21.2.249
		
		final RequestConfig requestConfig = RequestConfig.custom()
												.setConnectTimeout(DEFAULT_READ_TIMEOUT_MILLISECONDS).build();
		
		final CloseableHttpClient defaultHttpClient = HttpClientBuilder.create()
				    									.setConnectionManager(poolConnectionManager)
				    									.setDefaultRequestConfig(requestConfig)
				    									.build();

		return defaultHttpClient;
		
	}
	
}
