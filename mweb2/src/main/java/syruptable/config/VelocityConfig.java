package syruptable.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.velocity.VelocityConfigurer;
import org.springframework.web.servlet.view.velocity.VelocityViewResolver;

@Configuration
@ComponentScan(basePackages = "syruptable.config")
public class VelocityConfig implements ApplicationContextAware{
	
	private ApplicationContext context;

	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		this.context = context;
	}
	
	@Bean
	public ViewResolver viewResolver() {
	    VelocityViewResolver viewResolver = new VelocityViewResolver();
//	    viewResolver.setPrefix("classpath:/templates");
	    viewResolver.setExposeRequestAttributes(true);
	    viewResolver.setExposeSessionAttributes(true);
	    viewResolver.setExposeSpringMacroHelpers(true);
	    viewResolver.setRequestContextAttribute("rc");
	    viewResolver.setCache(true);
	    viewResolver.setPrefix("/WEB-INF/vm");
	    viewResolver.setSuffix(".vm");
	    viewResolver.setContentType("text/html; charset=utf-8");
//	    viewResolver.setToolboxConfigLocation("/WEB-INF/velocity/toolbox.xml");
//	    viewResolver.setOrder(Ordered.LOWEST_PRECEDENCE - 20);	    
	    return viewResolver;
	}
	
	@Bean
	public VelocityConfigurer velocityConfigurer() {
	    VelocityConfigurer configurer = new VelocityConfigurer();
	    configurer.setResourceLoaderPath("/WEB-INF/vm");
	    configurer.setConfigLocation(context.getResource("/WEB-INF/velocity/velocity.properties"));
	    return configurer;
	}

}
