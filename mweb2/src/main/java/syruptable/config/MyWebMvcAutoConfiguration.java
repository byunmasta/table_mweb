package syruptable.config;

import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import syruptable.support.StatisticsInterceptor;


@Configuration
@EnableWebMvc
public class MyWebMvcAutoConfiguration extends WebMvcAutoConfigurationAdapter {
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
		registry.addResourceHandler("/css/**").addResourceLocations("/resources/css/");
		registry.addResourceHandler("/js/**").addResourceLocations("/resources/js/");
		registry.addResourceHandler("/img/**").addResourceLocations("/resources/img/");
		registry.addResourceHandler("/m/**").addResourceLocations("/resources/event/");
		registry.addResourceHandler("/html/**").addResourceLocations("/resources/html/");
		registry.addResourceHandler("/Tmap4_0download/**").addResourceLocations("/resources/html/tmap_download/");
		registry.addResourceHandler("/favicon.ico").addResourceLocations("/resources/");
		registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
	}
	
	@Bean
	public EmbeddedServletContainerCustomizer containerCustomizer() {

		return new EmbeddedServletContainerCustomizer() {
			@Override
			public void customize(ConfigurableEmbeddedServletContainer container) {

				container.addErrorPages(new ErrorPage(HttpStatus.BAD_REQUEST, "/resources/pageNotFound.jsp"));
				container.addErrorPages(new ErrorPage(HttpStatus.UNAUTHORIZED, "/resources/pageNotFound.jsp"));
				container.addErrorPages(new ErrorPage(HttpStatus.PAYMENT_REQUIRED, "/resources/pageNotFound.jsp"));
				container.addErrorPages(new ErrorPage(HttpStatus.FORBIDDEN, "/resources/pageNotFound.jsp"));
				container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/resources/pageNotFound.jsp"));
				container.addErrorPages(new ErrorPage(HttpStatus.METHOD_NOT_ALLOWED, "/resources/pageNotFound.jsp"));

				container.addErrorPages(new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/resources/internalError.jsp"));
				container.addErrorPages(new ErrorPage(HttpStatus.NOT_IMPLEMENTED, "/resources/internalError.jsp"));
				container.addErrorPages(new ErrorPage(HttpStatus.BAD_GATEWAY, "/resources/internalError.jsp"));
				container.addErrorPages(new ErrorPage(HttpStatus.SERVICE_UNAVAILABLE, "/resources/internalError.jsp"));
				container.addErrorPages(new ErrorPage(HttpStatus.GATEWAY_TIMEOUT, "/resources/internalError.jsp"));
			}
		};
	}

	/* @Bean 
	 public HandlerExceptionResolver simpleMappingExceptionResolver(){
		  Properties mappings=new Properties();
		  
		  mappings.put(NoHandlerFoundException.class.getName(),"/error/404");
		  mappings.put(NoSuchRequestHandlingMethodException.class.getName(),"/error/404");
		  mappings.put(HttpRequestMethodNotSupportedException.class.getName(),"/error/404");
		  SimpleMappingExceptionResolver exceptionResolver=new SimpleMappingExceptionResolver();
		  exceptionResolver.setExceptionMappings(mappings);
		  exceptionResolver.setDefaultErrorView("/error/default");
		  exceptionResolver.setDefaultStatusCode(HttpStatus.BAD_REQUEST.value());
		  exceptionResolver.setOrder(1);
		  return exceptionResolver;
		}*/
	 
	 @Bean
	 public StatisticsInterceptor statisticsInterceptor() {
		 return new StatisticsInterceptor();
	 }
	 
	 @Override
	 public void addInterceptors(InterceptorRegistry registry) {
	        registry.addInterceptor(statisticsInterceptor());
	  }
	 
}
