package syruptable.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import syruptable.config.HttpRestTemplateConfig;
import syruptable.support.HttpRestTemplateBuilder.Builder;



/**
 * API를 사용하기 위해 기본적으로 필요한, Http 연결, DB 연결, XML or JSON ObjectMapper 을 편하게 사용 할 수 있게 생성
 * @author webmadeup
 */
@Component
public class ApiHelper {
	
	@Autowired
	private HttpRestTemplateConfig httpRestTemplateConfig;
	
	/*@Autowired
	private XmlMapper xmlMapper;*/
	
	@Autowired
	private MessageSourceAccessor msAccessor;

	private final static HttpHeaders pickatHeaders = new HttpHeaders();
	
	/**
	 * 피캣 API 2.0 부터 OAuth 2.0 인증 형태로 사용함
	 * 토큰값의 만료기간 없음
	 */
	static {
		/*System.out.println(msAccessor.getMessage("sk.map.pickat.auth.UserAgent"));
		System.out.println(msAccessor.getMessage("sk.map.pickat.auth.Authorization"));*/
//		System.setProperty("jsse.enableSNIExtension", "false");
		pickatHeaders.set("Accept", MediaType.ALL_VALUE);
		pickatHeaders.set("User-Agent", "Natemobile/2000");
		pickatHeaders.set("Authorization", "Bearer eyJhbGciOiAiSFMyNTYiLCAidHlwIjogIkpXVCIsICJraWQiOiAiMSJ9.eyJpc3MiOiAiMSIsICJpYXQiOiAxMzk4ODQ3MzUxLCAianRpIjogInhvTzJjRy1ZSmdTLWlBNWVfOUlOM3c9PSIsICJzdWIiOiAiMTAwMDA5IiwgImF1ZCI6ICJ1cm46cGlja2F0OmNsYWltczpjb25zdW1lcl9rZXkifQ.--4sdrJ_ooZAIdZhshGxAQ6Mw5svQ87326nfH2sTuSs");
		pickatHeaders.set("Accept-Language", "ko");
	}
	


	/*static {
		javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {
					public boolean verify(String hostname,
							javax.net.ssl.SSLSession sslSession) {
								System.out.println("hostname:[" + hostname + "] ssl force trust.");
						return true;
					}
				});
	}*/
	
	public Builder getHttp() {
		return new HttpRestTemplateBuilder.Builder()
					.setRestTemplate(httpRestTemplateConfig)
					.setMsAccessor(msAccessor);
	}
	
	/**
	 * RestTemplate를 사용하기 위한 기본적인 설정이 끝난 HttpClient를 전달함
	 * @return
	 */
	public Builder getPickatAuthHttp() {
		
		return new HttpRestTemplateBuilder.Builder()
					.setRestTemplate(httpRestTemplateConfig)
					.setHeaders(pickatHeaders)
					.setMsAccessor(msAccessor);
	}
	public Builder getPickatToken() {
		
		HttpHeaders tokenHeaders = new HttpHeaders();
		tokenHeaders.set("Accept", MediaType.ALL_VALUE);
		tokenHeaders.set("User-Agent", "Natemobile/2000");
		tokenHeaders.set("Authorization", "Bearer eyJhbGciOiAiSFMyNTYiLCAidHlwIjogIkpXVCIsICJraWQiOiAiMSJ9.eyJpc3MiOiAiMTAwMDA2IiwgImlhdCI6IDE0MzgzMjI3NDcsICJqdGkiOiAiRm5ncXpSTWdOc3BwT0RscmRLLW1HZz09IiwgInN1YiI6ICIxMDI0MzI3IiwgImF1ZCI6ICJ1cm46cGlja2F0OmNsYWltczphY2Nlc3NfdG9rZW4ifQ.sID81y7iDSZ156kqEvcZKyjWSzuMhhZniF6PnW1zFFc");
	
		
		return new HttpRestTemplateBuilder.Builder()
				.setRestTemplate(httpRestTemplateConfig)
				.setHeaders(tokenHeaders)
				.setMsAccessor(msAccessor);
	}
	/**
	 * Jackson은 다음과 같이 JSON을 처리하는 위한 3가지 방법을 제공한다.
		- Streaming API
		- Data Binding
		- Tree Model
		Streaming API는 성능이 중요할 때 사용하고,
		Data Binding은 편리한 사용을 원할 때 사용하며,
		Tree Model은 유연성이 필요할 때 사용한다.
		Streaming API만을 사용한다면 core 라이브러리만으로도 가능하고,
		Data Binding을 사용한다면 mapper 라이브러리도 필요하다.
	 */
	/*public XmlMapper getXmlMapper() {
		*//**
		 * XML 파싱중 존재하지 않는 properties에 대해 Exception 를 발생하지 않게한다. 
		 * 혹은 method에 추가 @JsonIgnoreProperties(ignoreUnknown = true)
		 *//*
		this.xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false); 
		return this.xmlMapper;
	}*/

}
