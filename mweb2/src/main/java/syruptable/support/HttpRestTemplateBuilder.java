package syruptable.support;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import syruptable.config.HttpAPIList;
import syruptable.config.HttpRestTemplateConfig;





/**
 * spring 의 RestTemplate를 편하게 상용하기 위한 Warpper 클래스 입니다.
 * @author webmadeup
 * @param <T>
 *
 */
public class HttpRestTemplateBuilder<T>{
		
	/**
	 * 로그 설정
	 */
	private static final Logger logger = LoggerFactory.getLogger(HttpRestTemplateBuilder.class);
	
	
	/**
	 * ResponseBody 의 값
	 */
	private T resultResponse;
	
	/**
	 * Http의 ResponseBody 의 값을 최종적으로 저장
	 * @param builder
	 */
	@SuppressWarnings("unchecked")
	private HttpRestTemplateBuilder(Builder builder) {
		this.resultResponse = (T) builder.response;
	}
	
	/**
	 * HttpClient 의 기본적인 설정을 Builder 패턴을 전환
	 * @author webmadeup
	 *
	 */
	public static class Builder {
		
		private CleanParameter cleanParameter = new CleanParameter();
		
		private HttpRestTemplateConfig httpRestTemplateConfig;
		
		private MessageSourceAccessor msAccessor;
		
		private Enum<HttpAPIList> apiName;
		
		private Enum<HttpMethod> httpMethod = HttpMethod.POST;
		
		private HttpHeaders headers;
		
		private HttpEntity<?> httpEntity;
		
		private MultiValueMap<String, Object> uriVariablePostValues;

		@SuppressWarnings("rawtypes")
		private Map uriVariableValues;
		
		private String encoding = "UTF-8";
		
		private Object response;
		
		private ResponseEntity<?> responseEntity;
		
		private ResponseErrorHandler errorHandler;
		
		private String baseBody;

		/**
		 * RestTemplate 구현 객체
		 * @param pickatRestTemplateConfig
		 * @return
		 */
		public final Builder setRestTemplate(HttpRestTemplateConfig httpRestTemplateConfig) {
			this.httpRestTemplateConfig = httpRestTemplateConfig;
			return this;
		}
		
		/**
		 * 기본적인 outbound 에 필요한 properties값
		 * @param msAccessor
		 * @return
		 */
		public final Builder setMsAccessor(MessageSourceAccessor msAccessor) {
			this.msAccessor = msAccessor;
			return this;
		}
		
		/**
		 * HttpAPILis 참조
		 * @param apiName
		 * @return
		 */
		public final Builder setApiName(Enum<HttpAPIList> apiName) {
			this.apiName = apiName;
			return this;
		}
		
		/**
		 * HttpMethod.POST, HttpMethod.GET
		 * @param httpMethod
		 * @return
		 */
		public final Builder setHttpMethod(Enum<HttpMethod> httpMethod) {
			this.httpMethod = httpMethod;
			return this;
		}
		
		/**
		 * HttpHeaders headers = new HttpHeaders();
		 * headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		 * headers.set("appkey", "33f55ab1-52f6-36a0-8209-4034e6226678");
		 * @param headers
		 * @return
		 */
		public final Builder setHeaders(HttpHeaders headers) {
			this.headers = headers;
			return this;
		}

		/**
		 * HttpHeaders headers = new HttpHeaders();
		 * headers.setContentType(MediaType.APPLICATION_JSON);
		 * headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		 * HttpEntity<String> request2 = new HttpEntity<String>("{\"mid\":\"1862285\"}", headers);
		 * @param t1
		 * @return
		 */
		public final <T1> Builder setHttpEntity(T1 t1) {
			
			this.httpEntity = (HttpEntity<?>) t1;
			return this;
		}
		
		/**
		 * param 설정
		 * @param uriVariableValues
		 * @return
		 */
		@SuppressWarnings("unchecked")
		public final Builder setUriVariableValues(@SuppressWarnings("rawtypes") Map uriVariableValues) {
			
			if(uriVariableValues.containsKey("cursor") ) {
				String cursor = (String) uriVariableValues.get("cursor");
				uriVariableValues.put("cursor", cursor.equals("1") ? "" : cleanParameter.getClean(cursor));
			}
			
			if(uriVariableValues.containsKey("aoi_id") && ( uriVariableValues.get("aoi_id").toString().equals("0")
															|| uriVariableValues.get("aoi_id").toString().equals("1")) ) {
				uriVariableValues.put("aoi_id", "");
			}
			
			if(uriVariableValues.containsKey("category_id") && uriVariableValues.get("category_id").equals("00-00")) {
				uriVariableValues.put("category_id", "");
			}
			
			if(uriVariableValues.containsKey("set") && uriVariableValues.get("set").equals("0")) {
				uriVariableValues.put("set", "");
			}
			
			this.uriVariableValues = uriVariableValues;
			return this;
		}

		/**
		 * POST 전송시 필요한 내부 method
		 * @param uriVariableValues
		 */
		private final void setUriVariablePostValues(Map<String, Object> uriVariableValues) {
			this.uriVariablePostValues = new LinkedMultiValueMap<String, Object>();
			this.uriVariablePostValues.setAll(uriVariableValues);
		}
		
		/**
		 * UTF-8
		 * @param encoding
		 * @return
		 */
		public final Builder setEncoding(String encoding) {
			this.encoding = encoding;
			return this;
		}
		
		/**
		 * build() 에게 전달 받을 객체
		 * @param response
		 * @return
		 */
		public final Builder setResponse(Object response) {
			this.response = response;
			return this;
		}
		
		/**
		 * new PickatAPIErrorHandler()
		 * @param errorHandler
		 * @return
		 */
		public final Builder setErrorHandler(ResponseErrorHandler errorHandler) {
			this.errorHandler = errorHandler;
			return this;
		}
		
		/**
		 * 통신 장에시 기본적인 body 값을 전달하고 하는 경우 생성
		 * @param baseBody
		 * @return
		 */
		public final Builder setBaseBody(String baseBody) {
			this.baseBody = baseBody;
			return this;
		}
		
		/**
		 * 
		 * @return  ResponseBody 의 값
		 */
		@SuppressWarnings("unchecked")
		public final <T> T build() {
			UriComponents uri = null;
			try {
				final String url = msAccessor.getMessage(apiName.name(), apiName.toString());
//				if(uriVariableValues != null && this.httpMethod.compareTo(HttpMethod.GET) == 0) {
				if(uriVariableValues != null) {
					uri = UriComponentsBuilder.fromUriString(url).build().expand(uriVariableValues).encode(encoding);
				} else {
					uri = UriComponentsBuilder.fromUriString(url).build().encode(encoding);
				}
				logger.info("Call URL : {}", uri);
			} catch (IllegalArgumentException illegalArgumentException) {
				//프로퍼티에 정의된 파라메타 명으로 유입 되지 않을 경우 발생
				illegalArgumentException.printStackTrace();
				throw illegalArgumentException;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				throw new AssertionError(encoding + " is unknown : " + uriVariableValues);
			} 
			
			if(this.errorHandler != null) {
				this.httpRestTemplateConfig.restTemplate().setErrorHandler(errorHandler);
			}
			try {
				
				final Class<?> clazz = this.response.getClass().getName().getClass();
				
				if(this.httpMethod.compareTo(HttpMethod.GET) == 0) {
					final HttpEntity<Map<String , String>> request = new HttpEntity<Map<String , String>>(this.headers);
					this.responseEntity = this.httpRestTemplateConfig.restTemplate().exchange(uri.toUri(), HttpMethod.GET, request, clazz);
				} else {
					this.responseEntity = this.httpRestTemplateConfig.restTemplate().postForEntity(uri.toUri(), this.httpEntity, clazz);
				}
				
				this.headers = this.responseEntity.getHeaders();
				this.response = this.responseEntity.getBody();
			
			} catch(RestClientException e) {
				e.printStackTrace();
				
				/**
				 * Exception 발생시 기본적인 결과를 원할 경우, 해당 body를 반환하게 함
				 */
				if(this.baseBody != null) {
					logger.error("비상 {}" , this.baseBody);
					this.response = this.baseBody;
				} else {
					throw e;
				}
			}
			return  new HttpRestTemplateBuilder<T>(this).resultResponse;
			
		}

		/**
		 * header 
		 * @return HttpHeaders
		 */
		public HttpHeaders getHeaders() {
			return headers;
		}
		
		
	}
	
	

}
