package syruptable.support;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import kr.co.skplanet.crypto.EncryptCustomerInfo;

import org.apache.commons.lang.time.FastDateFormat;

import com.skcc.harmony.keygenerator.KeyGenerator;

/**
 * SK Planet 고객센터로 서비스 정보를 전달하기 위한 
 * 서비스 식별자 정보 파라미터(POC PaRam)값 생성한다.
 * 
 */

public class CSInfra {
	private static final FastDateFormat fastDateFormat = FastDateFormat.getInstance("yyyyMMddHHmmss", TimeZone.getTimeZone("GMT+09:00"), Locale.KOREA);
	private final String path = this.getClass().getClassLoader().getResource("enckeys").getPath();
	private static final String pw = "ThisIsACSAdminSystemParamPassword";
	
	//서비스 식별자 정보 파라미터(POC PaRam)
	private String pocpr;
	
	/**
	 * 암호화된 값
	 * @return
	 */
	public String getPocpr() {
		return pocpr;
	}
	
	/**
	 * 암호화 된 파라미터(POC PaRam)값 생성
	 * @throws Exception
	 */
	public CSInfra() throws Exception {
		try {
			final String skey = KeyGenerator.generate();
			final Date date = new Date();
			final String stime = fastDateFormat.format(date);
			StringBuffer plaintext = new StringBuffer();			//암호화할 평문 (서비스 식별자 정보)
			plaintext.append("svcId:").append("5").append("|");		//(1)서비스ID (피캣 5)
			plaintext.append("opt29:").append("02").append("|");	//(2)채널구분   (mobile web 02)
			plaintext.append("mbrYn:").append("N").append("|");		//(3)회원여부   (비회원 N)
			plaintext.append("cutid:").append("").append("|");		//(4)고객식별자 (비회원 공백)
			plaintext.append("skey:").append(skey).append("|");		//generated key
			plaintext.append("stime:").append(stime).append("|");	//stime yyyyMMddHHmmss
			final String serviceInfo = plaintext.toString();
			
			final String[] values = EncryptCustomerInfo.getValues(path, pw);
			String key = values[0];
			String iv = values[1];
			String encryptedPocpr = EncryptCustomerInfo.encrypt(serviceInfo.getBytes("UTF-8"), key, iv); //암호화
			this.pocpr = encryptedPocpr;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}