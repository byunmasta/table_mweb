package syruptable.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;

@Component
public class CacheService {
	
	private final Logger logger = LoggerFactory.getLogger(CacheService.class);
	
	@CacheEvict(value = "mainPlaces", allEntries=true)
	public void evict_mainPlaces() {
		logger.info("evicted mainPlaces");
	}
	
	@CacheEvict(value = "mainStories", allEntries=true)
	public void evict_mainStories() {
		logger.info("evicted mainStories");
	}

	@CacheEvict(value = "category", allEntries=true)
	public void evict_category() {
		logger.info("evicted category");
	}
	
	@CacheEvict(value = "aoiList", allEntries=true)
	public void evict_aoiList() {
		logger.info("evicted aoiList");
	}
	
	@CacheEvict(value = "currentAOI", allEntries=true)
	public void evict_currentAOI() {
		logger.info("evicted currentAOI");
	}
	
	@CacheEvict(value = "poiDetail", allEntries=true)
	public void evict_poiDetail() {
		logger.info("evicted poiDetail");
	}

	@CacheEvict(value = "poiImageList", allEntries=true)
	public void evict_poiImageList() {
		logger.info("evicted poiImageList");
	}
	
	@CacheEvict(value = "pickDetail", allEntries=true)
	public void evict_pickDetail() {
		logger.info("evicted pickDetail");
	}
	
	@CacheEvict(value = "hashtagDetail", allEntries=true)
	public void evict_hashtagDetail() {
		logger.info("evicted hashtagDetail");
	}
	
	@CacheEvict(value = "storyPois", allEntries=true)
	public void evict_storyPois() {
		logger.info("evicted storyPois");
	}
	
	@CacheEvict(value = "allVerifiedPOIs", allEntries=true)
	public void evict_allVerifiedPOIs() {
		logger.info("evicted allVerifiedPOIs");
	}
	
	@CacheEvict(value = "verifiedAOIs", allEntries=true)
	public void evict_verifiedAOIs() {
		logger.info("evicted verifiedAOIs");
	}
	
	@CacheEvict(value = "verifiedPOI", allEntries=true)
	public void evict_verifiedPOI() {
		logger.info("evicted verifiedPOI");
	}
	
	@CacheEvict(value = "searchResult", allEntries=true)
	public void evict_searchResult() {
		logger.info("evicted searchResult");
	}
	
	@CacheEvict(value = "autocompleteResult", allEntries=true)
	public void evict_autocompleteResult() {
		logger.info("evicted autocompleteResult");
	}
	
	@CacheEvict(value = "notices", allEntries=true)
	public void evict_notices() {
		logger.info("evicted notices");
	}
	
	@CacheEvict(value = "geofenceZoneAOI", allEntries=true)
	public void evict_geofenceZoneAOI() {
		logger.info("evicted geofenceZoneAOI");
	}
	
	@CacheEvict(value = "reverseGeocode", allEntries=true)
	public void evict_reverseGeocode() {
		logger.info("evicted reverseGeocode");
	}
	
}
