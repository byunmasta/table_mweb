package syruptable.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 내가 호출하는 서버나 API가 혹시나 보안에 취약 할 수 있으니
 * 내가 삭제 하고 전달한다.
 */

@Component
public class CleanParameter {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final String CLEANSQL = "\'|\"|=|<|>|\\(|\\)|\\\\|sp_|xp_|select|drop|union|--|#|;|cmdshell|&|scrip";
	
	public String getClean(final String parameter, final String replace) {
		try {
			return parameter.replaceAll(CLEANSQL, replace);
		} catch (Exception ignored){
			ignored.printStackTrace();
			return "";
		}
		
	}
	
	public String getClean(final String parameter) {
		logger.info(parameter);
		try {
			return parameter.replaceAll(CLEANSQL, "");
		} catch (Exception ignored){
			ignored.printStackTrace();
			return "";
		}
		
	}


}
