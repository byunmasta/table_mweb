package syruptable.support;

public class TokenInfo {
	private int userId;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "TokenInfo [userId=" + userId + "]";
	}

	
	
}