package syruptable.support;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import syruptable.exception.NotFoundExcption;
import syruptable.util.MobileUtil;


/**
 * 통계시스템에 로그를 적재하기위한 기본적인 통계 파마메타 처리
 * @author webmadeup
 *
 */
public class StatisticsInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory.getLogger("Statistics");
	
	/**
	 * view 가 실행되고 난후 서버에서 실행
	 */
	@Override
	public void afterCompletion(HttpServletRequest request,
								HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}

	/**
	 * 컨트롤러가 View 에게 response 를 전달 할때
	 */
	@Override
	public void postHandle(HttpServletRequest request,
							HttpServletResponse response, Object handler,
							ModelAndView modelAndView) throws Exception {
		String externalLink = request.getParameter("externalLink") == null ? "-" : request.getParameter("externalLink");
		String referer = request.getHeader("referer") == null ? "" : request.getHeader("referer");
		String prePage = request.getParameter("prePage") == null ? "C00C00" : request.getParameter("prePage");
		
		request.setAttribute("externalLink", StringEscapeUtils.escapeJava(externalLink));
		request.setAttribute("referer",  StringEscapeUtils.escapeJava(referer));
		request.setAttribute("prePage",  StringEscapeUtils.escapeJava(prePage));
		super.postHandle(request, response, handler, modelAndView);
	}
	
	/**
	 * 컨트롤로가 view에게 전달하기 전에
	 */
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		
			logger.info("externalLink {}", request.getParameter("externalLink"));
			logger.info("Request Referer:{} URI:{}{}", request.getHeader("referer"), request.getRequestURI(), request.getQueryString() != null ? "?" + request.getQueryString() : "" );
			logger.info("user-agent:{}", request.getHeader("user-agent"));
			
			String requestMethod = request.getMethod();
			logger.info("method:{}", requestMethod);
			/**
			 * 허용할 메소드 추가
			 */
			if( ("GET".equals(requestMethod) || "POST".equals(requestMethod)) == false ){
				logger.info("unMethod:{}", requestMethod);
				throw new NotFoundExcption(HttpStatus.NOT_FOUND);
//				response.sendRedirect("/exception/pageNotFound");
//				return false;
			}
			
			try {
				String userAgent = request.getHeader("user-agent").toLowerCase();
				String requestUri = request.getRequestURI();
				String referer = request.getHeader("referer");
				String externalLink = request.getParameter("externalLink");
				
				/**
				 * IE 8,9 이상에서도 'msie 7.0' 이라고 나오는 경우가 있다. (호환성보기 사용)
				 * 그렇게 되면 'msie 7.0' 만 가지고는
				 * 브라우져가 7,8,9,10 중에 어떤것 인지 확인이 불가능하다.	
				 * 그런데 이럴때도 7,8,9,10 간에 차이는 있다.
				 * mise 뒤쪽에 trident/4.0; 이 부분으로 식별하면 된다.
				 * 8버전 이상의 trident 값은 아래와 같다.
				 * 7이하의 버전에서는 trident 값이 없다.	
				 */
				if (userAgent == null){
				}else if( userAgent.indexOf("msie 6.0") > -1 
						|| userAgent.indexOf("msie 7.0") > -1 && (userAgent.indexOf("trident/5.0") < 0 && userAgent.indexOf("trident/6.0") < 0)
						|| userAgent.indexOf("msie 8.0") > -1 && (userAgent.indexOf("trident/5.0") < 0 && userAgent.indexOf("trident/6.0") < 0) 
						|| userAgent.indexOf("msie 9.0") > -1 && (userAgent.indexOf("trident/5.0") < 0 && userAgent.indexOf("trident/6.0") < 0)) {  // IE 7 이하는 trident 가 없다. IE 8 = trident/4.0 , IE 9 = trident/5.0, IE 10 = trident/6.0, IE 11 = trident/7.0 
					
					response.sendRedirect("/html/for_legacy_page.html");
					return false;
				}
				

				//이제 무조건 앱부터 실행하고 본다!
				//모바일이고 레퍼러가 없거나 invokeApp.html로 들어올경우 
				//앱 웹뷰에서 user-agent에 pickat이 있을경우 모웹으로
				//poi상세와 web으로 들어갈 경우에는 제외한다.
				if(MobileUtil.isMobile(userAgent.toLowerCase()) && userAgent.indexOf("pickat") == -1
						&& (!requestUri.startsWith("/web") && !requestUri.startsWith("/poi"))
						&& externalLink == null
						&& referer == null){
					
					logger.info("App Invoking");
					response.sendRedirect("/html/invokeApp.html?uri="+requestUri);
					return false;
				}
				
			} catch (Exception ignore) {
				logger.error("USERAGENT_ERROR:{}", request.getHeader("user-agent"));
				ignore.printStackTrace();
			}
			
			return super.preHandle(request, response, handler);
			
	}
}
