package syruptable.support;

import java.security.InvalidKeyException;
import java.security.SignatureException;
import java.util.Calendar;
import java.util.List;

import org.joda.time.Instant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.google.common.collect.Lists;
import com.google.gson.JsonObject;

import net.oauth.jsontoken.Checker;
import net.oauth.jsontoken.JsonToken;
import net.oauth.jsontoken.JsonTokenParser;
import net.oauth.jsontoken.crypto.HmacSHA256Signer;
import net.oauth.jsontoken.crypto.HmacSHA256Verifier;
import net.oauth.jsontoken.crypto.SignatureAlgorithm;
import net.oauth.jsontoken.crypto.Verifier;
import net.oauth.jsontoken.discovery.VerifierProvider;
import net.oauth.jsontoken.discovery.VerifierProviders;

/**
 * 
 * @author webmadeup
 *
 */
@Component
public class AuthHelper {

	private static final String AUDIENCE = "syruptable.claims.consumer.key";
	
	private static final String ISSUER = "SyrupVerticalProduct";
	
	@Value("${jwt.key.hmacsha256:Pw0vypVuZxA66Mt4JcsZ1+aNbIqe/umRt1u/L5fnPeg=}")
	private String SIGNING_KEY;
	
	public String createJsonWebToken(final int userId, Long durationMinute) {
		
		Calendar calender = Calendar.getInstance();
		HmacSHA256Signer signer;
		try {
			signer = new HmacSHA256Signer(ISSUER, null, SIGNING_KEY.getBytes());
		} catch (InvalidKeyException e) {
			throw new RuntimeException(e);
		}
		
		JsonToken token = new JsonToken(signer);
		token.setAudience(AUDIENCE);
		token.setIssuedAt(new Instant(calender.getTimeInMillis()));
		token.setExpiration(new Instant(calender.getTimeInMillis() + 1000L * 60L * durationMinute));
		
		JsonObject request = new JsonObject();
		request.addProperty("userId", userId);
		
		
		JsonObject payload = token.getPayloadAsJsonObject();
		payload.add("info", request);
		
		try {
			return token.serializeAndSign();
		} catch (SignatureException e) {
			throw new RuntimeException(e);
		}
	}
	
	//jwt에서 user정보 가져오기
	public TokenInfo verifyToken(final String jwtoken) {
		
		try {
			
			final Verifier hmacVerfier = new HmacSHA256Verifier(SIGNING_KEY.getBytes());
			
			VerifierProvider hmacLocator = new VerifierProvider() {
				
				@Override
				public List<Verifier> findVerifier(String id, String key) {
					return Lists.newArrayList(hmacVerfier);
				}
			};
			
			VerifierProviders locators = new VerifierProviders();
			locators.setVerifierProvider(SignatureAlgorithm.HS256, hmacLocator);
			
			/**
			 * Allows any audience (even null).
			 * 
			 */
			final Checker ignoreAudience = new Checker() {
				
				/**
				 * Allow any audience after thie signature has already been checked.
				 */
				@Override
				public void check(JsonObject payload)  {
					// allow anything.
				}
			};
			
			//Ignore Audience does not mean that the signature is ignored..
			JsonTokenParser parser = new JsonTokenParser(locators, ignoreAudience);
			JsonToken jsonToken;
			try {
				/**
				 * 토큰 만료 시간 체크 
				 */
				jsonToken = parser.verifyAndDeserialize(jwtoken);
			} catch (SignatureException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			} catch (IllegalStateException e) {
				//토큰이 만료 되었을 경우 처
				e.printStackTrace();
				throw new RuntimeException(e);
			}
			
			JsonObject payload = jsonToken.getPayloadAsJsonObject();
			TokenInfo tokenInfo = null;
			
			String issuer = payload.getAsJsonPrimitive("iss").getAsString();
			int userId =  payload.getAsJsonObject("info").getAsJsonPrimitive("userId").getAsInt();
			
			if (issuer.equals(ISSUER) && !StringUtils.isEmpty(userId)) {
				tokenInfo = new TokenInfo();
				tokenInfo.setUserId(userId);
            } 
			
			return tokenInfo;
		
		} catch (InvalidKeyException e) {
			throw new RuntimeException(e);
		}
		
	}
	
}
