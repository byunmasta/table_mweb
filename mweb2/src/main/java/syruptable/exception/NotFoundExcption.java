package syruptable.exception;

import org.springframework.http.HttpStatus;


/**
 * 
 * 해당 컨텐츠가 없을 경우
 * @author webmadeup
 *
 */
public class NotFoundExcption extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Table API에서 해당 컨텐츠를 찾을 수 없는 경우
	 * @param id
	 */
	public NotFoundExcption(final HttpStatus httpStatus) {
		super("컨텐츠가 존재 하지 않는 경우 발생" + httpStatus.value());
	}
	

}
