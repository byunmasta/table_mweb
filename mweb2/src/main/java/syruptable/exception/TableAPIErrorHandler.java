package syruptable.exception;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;


/**
 * Pickat API 2.0 에서는 자료가 없을 경우
 * HttpStatus 의 코드를 404로 전달하여 Response의 body를 읽기위해 사용한다.
 * @author webmadeup
 *
 */
public class TableAPIErrorHandler implements ResponseErrorHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(TableAPIErrorHandler.class);

	@Override
	public boolean hasError(ClientHttpResponse clienthttpresponse) throws IOException {
		if (clienthttpresponse.getStatusCode() != HttpStatus.OK) {
			logger.error("Status code : " + clienthttpresponse.getStatusCode());
			logger.error("Response " + clienthttpresponse.getStatusText());
			
			/**
			 * 자료가 없을경우 httpStatus 의 값이 404로 발생이 되어 아래와 같이 처리함
			 */
			if (clienthttpresponse.getStatusCode() == HttpStatus.NOT_FOUND
					|| clienthttpresponse.getStatusCode() == HttpStatus.BAD_REQUEST
					|| clienthttpresponse.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR
					|| clienthttpresponse.getStatusCode() == HttpStatus.GATEWAY_TIMEOUT
					|| clienthttpresponse.getStatusCode() == HttpStatus.HTTP_VERSION_NOT_SUPPORTED
							) {
				logger.info("Call returned ");
				this.handleError(clienthttpresponse);
				return false;
			} 
		}
		return false;
	}

	@Override
	public void handleError(ClientHttpResponse clienthttpresponse) throws IOException {
		
		if(clienthttpresponse.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR
				|| clienthttpresponse.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR
				|| clienthttpresponse.getStatusCode() == HttpStatus.GATEWAY_TIMEOUT
				|| clienthttpresponse.getStatusCode() == HttpStatus.HTTP_VERSION_NOT_SUPPORTED) {
			throw new APIServerExcption(clienthttpresponse.getStatusCode());
		}
		
		if(clienthttpresponse.getStatusCode() == HttpStatus.NOT_FOUND
				|| clienthttpresponse.getStatusCode() == HttpStatus.BAD_REQUEST ) {
			throw new NotFoundExcption(clienthttpresponse.getStatusCode());
		}
			
	}

}
