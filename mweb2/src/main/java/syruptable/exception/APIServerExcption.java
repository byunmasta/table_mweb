package syruptable.exception;

import org.springframework.http.HttpStatus;

/**
 * 
 * Table Syrup 에서 발생하는 API 오류
 * @author webmadeup
 *
 */
public class APIServerExcption extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Table Syrup 에서 발생하는 API 오류
	 * @param HttpStatus
	 */
	public APIServerExcption(final HttpStatus httpStatus) {
		super(" API 서버에 문제발생 API : " + httpStatus.value());
	}
	

}
