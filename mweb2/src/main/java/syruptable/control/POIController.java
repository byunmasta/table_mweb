package syruptable.control;

import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.skplanet.syruptable.entity.sb.Sb_Pois;
import com.skplanet.syruptable.service.POIServiceImpl;

import syruptable.model.HashtagDetail;
import syruptable.model.POIDetail;
import syruptable.model.PickDetail;
import syruptable.service.SyrupTableService;
import syruptable.service.SyrupTableService.SortType;
import syruptable.util.TimeViewFormat;

@Controller
public class POIController {
	
	@Autowired
	private SyrupTableService syrupTableService;
	
	@Autowired
	private POIServiceImpl poiServiceImpl;
	
	@Autowired
	private TimeViewFormat timeViewFormat;
	
	private static final DecimalFormat decimalFormat = new DecimalFormat(".######");
	
	//========================= POI 상세 =========================
	/**
	 * POI 상세 보기
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/poi/{poiId}")
    public String poiDetail(Model model
    		,@PathVariable("poiId") int poiId) {
		
		POIDetail poiDetail = syrupTableService.getPOIDetail(poiId);
		
		model.addAttribute("poiDetail", poiDetail);
		
		timeViewFormat.setNowDate();
		model.addAttribute(timeViewFormat);
		
		return "poi/poiDetail";
	}
	
	/**
	 * POI 상세 보기 (T전화용 페이지)
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/poi/{poiId}/tphone")
    public String poiDetailTphone(Model model
    		,@PathVariable("poiId") int poiId) {
		
		POIDetail poiDetail = syrupTableService.getPOIDetail(poiId);
		
		model.addAttribute("poiDetail", poiDetail);
		model.addAttribute("tPhone", true);
		
		timeViewFormat.setNowDate();
		model.addAttribute(timeViewFormat);
		
		return "poi/poiDetail";
	}
	
	/**
	 * POI 상세 지도
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/poi/{poiId}/map")
    public String poiDetailMap(Model model
    		,@PathVariable("poiId") int poiId
			,@CookieValue(value="latitude", required=false, defaultValue="37.5627117943") double latitude
			,@CookieValue(value="longitude", required=false, defaultValue="126.9849551207") double longitude
			) {
		
		POIDetail poiDetail = syrupTableService.getPOIDetail(poiId);
		
		model.addAttribute("poiDetail", poiDetail);
		
		model.addAttribute("latitude", latitude);
		model.addAttribute("longitude", longitude);
		
		return "poi/poiDetailMap";
	}
	
	//========================= Pick 상세 =========================
	/**
	 * Pick 상세 페이지
	 * @param model
	 * @param pickId
	 * @return
	 */
	@RequestMapping(value = "/pick/{pickId}")
    public String pickDetail(Model model
    						,@PathVariable("pickId") int pickId) {
		
		PickDetail pickDetail = syrupTableService.getPickDetail(pickId, "", 20);
		
		model.addAttribute("pickDetail", pickDetail);
		
		timeViewFormat.setNowDate();
		model.addAttribute(timeViewFormat);
		
		return "poi/pickDetail";
	}
	
	/**
	 * Pick 상세 댓글 더보기
	 * @param model
	 * @param pickId
	 * @param cursor
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/pick/{pickId}/{cursor}/{limit}")
    public String pickDetailMore(Model model
    						,@PathVariable("pickId") int pickId
    						,@PathVariable("cursor") String cursor
    						,@PathVariable("limit") int limit) {
		
		PickDetail pickDetail = syrupTableService.getPickDetail(pickId, cursor, limit);
		
		model.addAttribute("pickDetail", pickDetail);
		
		timeViewFormat.setNowDate();
		model.addAttribute(timeViewFormat);
		
		return "poi/pickDetailMore";
	}
	
	
	//========================= 해시태그(키워드별 POI) =========================
	/**
	 * 해쉬태그 별 POI리스트 보기
	 * @param hashtagId
	 * @return
	 */
	@RequestMapping(value = "/hashtag/{hashtagId}")
    public String hashtagDetail(Model model
    		,HttpServletRequest request
			,HttpServletResponse response
    		,@PathVariable("hashtagId") int hashtagId
    		,@CookieValue(value="latitude", required=false, defaultValue="37.5627117943") double latitude
			,@CookieValue(value="longitude", required=false, defaultValue="126.9849551207") double longitude) {
		
		String lat = decimalFormat.format(latitude);
		String lng = decimalFormat.format(longitude);

		HashtagDetail hashtagDetail = syrupTableService.getHashtagDetail(hashtagId, "popular", lng, lat, "", 20);

		model.addAttribute("hashtagDetail", hashtagDetail);
		model.addAttribute("sort", "popular");
		
		return "poi/hashtagDetail";
    }
	
	/**
	 * 해쉬태그 별 POI리스트 보기 정렬
	 * @param hashtagId
	 * @param sort
	 * @return
	 */
	@RequestMapping(value = "/hashtag/{hashtagId}/{sort:popular|distance}")
    public String hashtagDetailSort(Model model
    		,HttpServletRequest request
			,HttpServletResponse response
    		,@PathVariable("hashtagId") int hashtagId
    		,@PathVariable("sort") SortType sortType
    		,@CookieValue(value="latitude", required=false, defaultValue="37.5627117943") double latitude
			,@CookieValue(value="longitude", required=false, defaultValue="126.9849551207") double longitude) {
		
		String lat = decimalFormat.format(latitude);
		String lng = decimalFormat.format(longitude);

		HashtagDetail hashtagDetail = syrupTableService.getHashtagDetail(hashtagId, sortType.name(), lng, lat, "", 20);

		model.addAttribute("hashtagDetail", hashtagDetail);
		model.addAttribute("sort", sortType.name());
		
		return "poi/hashtagDetail";
    }
	
	/**
	 * 해쉬태그 별 POI리스트 더보기
	 * @param hashtagId
	 * @param cursor
	 * @param limit
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/hashtag/{hashtagId}/{sort}/{cursor}/{limit}")
    public String noticesMore(Model model
    		,@PathVariable("hashtagId") int hashtagId
    		,@PathVariable("sort") SortType sortType
    		,@PathVariable("cursor") String cursor
    		,@PathVariable("limit") int limit
    		,@CookieValue(value="latitude", required=false, defaultValue="37.5627117943") double latitude
			,@CookieValue(value="longitude", required=false, defaultValue="126.9849551207") double longitude) {
       
		String lat = decimalFormat.format(latitude);
		String lng = decimalFormat.format(longitude);
		
		HashtagDetail hashtagDetail = syrupTableService.getHashtagDetail(hashtagId, sortType.name(), lng, lat, cursor, limit);
		model.addAttribute("hashtagDetail", hashtagDetail);
		
		return "poi/hashtagDetailMore";
    }
	
	
	//========================= 예약가능 POI 목록 (앱에서 사용) =========================
	/**
	 * 예약가능한 POI 리스트
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/poi/booking")
    public String poiBookingDetail(Model model) {
		
		int cursor = 0;
		int limit = 20;
		
		List<Sb_Pois> pois = poiServiceImpl.getBookingPois(cursor, limit);
		model.addAttribute("pois", pois);
		model.addAttribute("cursor", cursor+limit);
		
		return "poi/bookingList";
	}
	
	/**
	 * 예약가능한 POI 리스트 더보기
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/poi/booking/{cursor}/{limit}")
    public String poiBookingDetailMore(Model model
    						,@PathVariable("cursor") int cursor
    						,@PathVariable("limit") int limit) {
		
		List<Sb_Pois> pois = poiServiceImpl.getBookingPois(cursor, limit);
		
		int count = poiServiceImpl.countByBookingPois();
		model.addAttribute("pois", pois);
		
		if(count>(cursor+limit))
			model.addAttribute("cursor", cursor+limit);
		
		return "poi/bookingListMore";
	}
}
