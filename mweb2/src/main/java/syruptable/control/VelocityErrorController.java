package syruptable.control;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

/**
 * vm(velocity) 에서 에러가 발생했을 경우 처리
 * 다른 방법이 보이면 더 개선하길
 * @author webmadeup
 *
 */
@RestController
public class VelocityErrorController implements ErrorController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final ErrorAttributes errorAttributes;
	
	public VelocityErrorController() {
		this.errorAttributes = errorAttributes();
	}
	
	private static final String PATH = "/error";
	
	/**
	 * Spring boot에서 관리된 error 
	 * @param request
	 * @return
	 */
    @RequestMapping(value = PATH)
    public ModelAndView error(HttpServletRequest request) {
    	Map<String, Object> body = getErrorAttributes(request, getTraceParameter(request));
    	logger.error(body.toString());
    	ModelAndView mav = new ModelAndView();
        mav.setViewName("/exception/internalError");
        return mav;
    }
    
	private ErrorAttributes errorAttributes() {
	     return new DefaultErrorAttributes() {

	         @Override
	         public Map<String, Object> getErrorAttributes(
	                 RequestAttributes requestAttributes,
	                 boolean includeStackTrace) {
	             Map<String, Object> errorAttributes = super.getErrorAttributes(requestAttributes, includeStackTrace);
	             Throwable error = getError(requestAttributes);
	             return errorAttributes;
	         }

	     };
	 }

    private boolean getTraceParameter(HttpServletRequest request) {
    	
		String parameter = request.getParameter("trace");
		if (parameter == null) {
			return false;
		}
		return !"false".equals(parameter.toLowerCase());
	}
    
    private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
		RequestAttributes requestAttributes = new ServletRequestAttributes(request);
		return this.errorAttributes.getErrorAttributes(requestAttributes,
				includeStackTrace);
	}
    
    @Override
    public String getErrorPath() {
        return PATH;
    }

}
