package syruptable.control;

import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import syruptable.exception.NotFoundExcption;

/**
 * http://docs.spring.io/spring/docs/4.1.5.RELEASE/spring-framework-reference/htmlsingle/#mvc-exceptionhandlers
 * @author webmadeup
 *
 */

@ControllerAdvice
public class GlobalDefaultExceptionHandler  {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 404 (Not Found)
	 * @param req
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@ExceptionHandler(value = {NoHandlerFoundException.class
							, NoSuchRequestHandlingMethodException.class
							, HttpRequestMethodNotSupportedException.class})  // 405 (Method Not Allowed)
    public ModelAndView notFoundErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
            throw e;
        
        logger.error("======== 404 ===========" + e);
        // Otherwise setup and send the user to a default error-view.
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("/exception/pageNotFound");
        return mav;
    }
	
	/**
	 * 400 (Bad Request) 
	 * @param req
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@ExceptionHandler(value = {TypeMismatchException.class
								,MissingServletRequestParameterException.class
								,MethodArgumentNotValidException.class})
    public ModelAndView badRequestHandler(HttpServletRequest req, Exception e) throws Exception {
        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
            throw e;
        
        logger.error("======== 400 ===========" + e);
        // Otherwise setup and send the user to a default error-view.
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("/exception/pageNotFound");
        return mav;
    }
	
	/**
	 * 500 (Internal Server Error)
	 * @param req
	 * @param e
	 * @return
	 * @throws Exception
	 */
	
	@ExceptionHandler(value = {HttpMessageNotWritableException.class, ConversionNotSupportedException.class, Exception.class})
    public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
            throw e;
       
        logger.error("exception: {}  url: {}" , e.getClass().getSimpleName(), req.getRequestURL());
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());
        if(e instanceof NotFoundExcption || e instanceof ExecutionException) {
        	mav.setViewName("/exception/pageNotFound");
        } else {
        	mav.setViewName("/exception/internalError");
        }
        
        return mav;
    }
	
}
