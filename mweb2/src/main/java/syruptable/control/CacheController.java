package syruptable.control;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import syruptable.support.CacheService;
import syruptable.util.CacheEvictCallable;

/**
 * 캐시삭제 기능 페이지
 * @author Yongho Byun
 *
 */
@Controller
public class CacheController {
	
	private static final Logger logger = LoggerFactory.getLogger(CacheController.class);
	
	private final static String URL_WILDCARD = "*";
	
	@Value("${cache.ipcheck:true}")
	private boolean isCheckIp;
	
	@Value("${cache.allow.ip:0.0.0.0}")
	private String allowIpList;
	
	@Value("${cache.target.ip:127.0.0.1}")
	private String targetIpList;
	
	
	public enum Caches {
		mainPlaces
		, mainStories
		, category
		, aoiList
		, currentAOI
		, poiDetail
		, poiImageList
		, pickDetail
		, hashtagDetail
		, story
		, verifieds
		, searchResult
		, autocompleteResult
		, notices
		, geofenceZoneAOI
		, reverseGeocode
		
		, END;
	}
	
	@Autowired
	private CacheService cacheService;
	
	
	/**
	 * 캐시삭제 메인
	 * 토큰값 틀릴경우 404리턴
	 * @param token
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/cache/{token}", method = RequestMethod.GET)
	public String cache(@PathVariable("token") String token
						, HttpServletRequest request
						, Model model) {
		logger.info("Access IP : "+request.getRemoteAddr());
		if(!isCheckIp || (isAllow(request.getRemoteAddr(), allowIpList) && token.equals("변용호짱"))){
			model.addAttribute("token", token);
			return "/others/cacheEvict";
		}
		return "/exception/pageNotFound";
	}
	
	/**
	 * 뷰에서 캐시삭제 요청
	 * @param cache
	 * @param token
	 * @param request
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	@RequestMapping(value = "/cache/evict/{cacheName}/{token}")
	@ResponseBody
	public void evict(@PathVariable("cacheName") Caches cache
					, @PathVariable("token") String token
					, HttpServletRequest request) throws InterruptedException, ExecutionException{
		
		if(isCheckIp && !token.equals("변용호짱")){
			logger.info("Denied by wrong token : "+token);
			return;
		}
		
		ExecutorService executor = Executors.newFixedThreadPool(targetIpList.split(",").length);
		
		for (String target : targetIpList.split(",")){
			target = target.trim();
			CacheEvictCallable<Boolean> evictCallable = new CacheEvictCallable<Boolean>(target, cache.name(), token);
			FutureTask<Boolean> evictFuture = new FutureTask<Boolean>(evictCallable);
			executor.execute(evictFuture);
			logger.info("Cache Evict Result : "+evictFuture.get());
		}
		
		executor.shutdown();
	}
	
	/**
	 * 응답받은 서버에서 각 서버로 캐시삭제 요청 및 수행
	 * @param cache
	 * @param token
	 * @param request
	 */
	@RequestMapping(value = "/cache/evict/!/{cache}/{token}")
	@ResponseBody
	public void evictRun(@PathVariable("cache") Caches cache
						, @PathVariable("token") String token
						, HttpServletRequest request){
		
		if(!request.getHeader("user-agent").equals("SK Planet (SyrupTable 2.0) made by Yongho Byun/87.05.13")){
			logger.info("Denied by disallowed user-agent : "+request.getHeader("user-agent"));
			return;
		}
		if(!isAllow(request.getRemoteAddr(), targetIpList)){
			logger.info("Denied by disallowed ip : "+request.getRemoteAddr());
			return;
		}
		
		if(isCheckIp && !token.equals("변용호짱")){
			logger.info("Denied by wrong token : "+token);
			return;
		}
		
		switch(cache){
		case mainPlaces:
			cacheService.evict_mainPlaces();
			break;
		case mainStories:
			cacheService.evict_mainStories();
			break;
		case category:
			cacheService.evict_category();
			break;
		case aoiList:
			cacheService.evict_aoiList();
			break;
		case currentAOI:
			cacheService.evict_currentAOI();
			break;
		case poiDetail:
			cacheService.evict_poiDetail();
			break;
		case poiImageList:
			cacheService.evict_poiImageList();
			break;
		case pickDetail:
			cacheService.evict_pickDetail();
			break;
		case hashtagDetail:
			cacheService.evict_hashtagDetail();
			break;
		case story:
			cacheService.evict_storyPois();
			break;
		case verifieds:
			cacheService.evict_allVerifiedPOIs();
			cacheService.evict_verifiedAOIs();
			cacheService.evict_verifiedPOI();
			break;
		case searchResult:
			cacheService.evict_searchResult();
			break;
		case autocompleteResult:
			cacheService.evict_autocompleteResult();
			break;
		case notices:
			cacheService.evict_notices();
			break;
		case geofenceZoneAOI:
			cacheService.evict_geofenceZoneAOI();
			break;
		case reverseGeocode:
			cacheService.evict_reverseGeocode();
			break;
		
		default:
			break;
		}
	}
	
	
	/**
	 * 허용된 사용자인지 체크
	 * @param remoteip
	 * @param ipList
	 * @return
	 */
	public boolean isAllow(String remoteip, String ipList){
		if (remoteip==null){ return false;}

        String[] matcherList = ipList.split(",");

        for (String allowIp : matcherList){
        	allowIp = allowIp.trim();
            int idx = allowIp.indexOf(URL_WILDCARD);

            if (idx>0 && allowIp.startsWith(URL_WILDCARD)){
                if (remoteip.endsWith(allowIp.substring(idx+1))) return true;
            }
            else if (idx>0 && allowIp.endsWith(URL_WILDCARD)){
                if (remoteip.startsWith(allowIp.substring(0, idx))) return true;
            }
            else{
                if (remoteip.equals(allowIp)) return true;
            }
        }
    	logger.info("Denied IP : "+remoteip);
        return false;
    }
	
}
