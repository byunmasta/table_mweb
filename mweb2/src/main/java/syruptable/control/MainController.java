package syruptable.control;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import syruptable.model.MainPlaces;
import syruptable.model.MainPlaces.Suggestion;
import syruptable.model.MainStories;
import syruptable.model.TodayLunch;
import syruptable.model.common.CurrentAOI;
import syruptable.service.SyrupTableService;

/**
 * @author Yongho Byun
 *
 */
@Controller
public class MainController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	/**
	 * GPS 좌표에 대한 값을 cache 하기 위한 최소 값
	 *  0.00001° 차이는 우리나라의 남부지방(위도 35°)에서 0.91m 
	 */
	private static final DecimalFormat decimalFormat = new DecimalFormat(".######");

	@Autowired
	private SyrupTableService syrupTableService;
	
	
	/**
	 * 메인의 주변목록 가져온다.
	 * @param model
	 * @param latitude
	 * @param longitude
	 * @param aoiId
	 * @param aoiName
	 * @return
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	@RequestMapping(value = {"/","/main"})
    public String mainHome(Model model, HttpServletRequest request, HttpServletResponse response
    						,@CookieValue(value="latitude", required=false, defaultValue="37.5627117943") double latitude
    						,@CookieValue(value="longitude", required=false, defaultValue="126.9849551207") double longitude
    						,@CookieValue(value="aoiId", required=false, defaultValue="117965") int aoiId
				    		,@CookieValue(value="aoiName", required=false, defaultValue="명동") String aoiName) 
				    				throws InterruptedException, ExecutionException {
		
		//쿠키가 없을경우 기본위치 명동으로 설정
//		if(latitude==0 || longitude==0){
//			latitude = 37.5627117943;
//			longitude = 126.9849551207; 
//		}
		String latitude6 = decimalFormat.format(latitude);
		String longitude6 = decimalFormat.format(longitude);
		
		// AOI ID가 없을경우 좌표값으로 AOI정보를 가져온다.
		if(aoiId == 0){
			CurrentAOI currentAOI = syrupTableService.getCurrentAOI(longitude6, latitude6);
			aoiId = currentAOI.getAoi().getAoiId();
			aoiName = currentAOI.getAoi().getName();
		}
		
		MainPlaces mainPlaces = syrupTableService.getMainPlaces(aoiId, "latest", latitude6, longitude6, 0, "", "", 20);
		model.addAttribute("mainPlaces", mainPlaces);
		model.addAttribute("suggestions", mainPlaces.getSuggestions());
		model.addAttribute("set", "latest");
		model.addAttribute("hashtagId", "0");
		model.addAttribute("categoryId", "00-00");
		
		model.addAttribute("aoiId", aoiId);
		model.addAttribute("aoiName", aoiName);
		model.addAttribute("latitude", latitude);
		model.addAttribute("longitude", longitude);
		
		return "main/mainPlaces";
    }
	
	/**
	 * 메인 주변
	 * @param model
	 * @param pSet
	 * @param pHashtagId
	 * @param pCateogoryId
	 * @param startIdx
	 * @param latitude
	 * @param longitude
	 * @param aoiId
	 * @param aoiName
	 * @return
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	@RequestMapping(value = {"/main/{set:0|latest|ranking|sorder}/{hashtagId}/{categoryId}"})
    public String mainHome(Model model
    						,@PathVariable("set") String set
    						,@PathVariable("hashtagId") int hashtagId
    						,@PathVariable("categoryId") String categoryId
    						,@RequestParam(value="startIdx" ,required=false, defaultValue="1") int startIdx
    						,@CookieValue(value="latitude", required=false, defaultValue="37.5627117943") double latitude
    						,@CookieValue(value="longitude", required=false, defaultValue="126.9849551207") double longitude
    						,@CookieValue(value="aoiId", required=false, defaultValue="117965") int aoiId
				    		,@CookieValue(value="aoiName", required=false, defaultValue="명동") String aoiName) 
				    				throws InterruptedException, ExecutionException {
       
		String latitude6 = decimalFormat.format(latitude);
		String longitude6 = decimalFormat.format(longitude);
		
		// AOI ID가 없을경우 좌표값으로 AOI정보를 가져온다.
		if(aoiId == 0){
			CurrentAOI currentAOI = syrupTableService.getCurrentAOI(longitude6, latitude6);
			aoiId = currentAOI.getAoi().getAoiId();
			aoiName = currentAOI.getAoi().getName();
		}
		MainPlaces mainPlaces = syrupTableService.getMainPlaces(aoiId, set, latitude6, longitude6, hashtagId, categoryId, "", 20);
		
		model.addAttribute("mainPlaces", mainPlaces);
		
		List<Suggestion> suggestions = new ArrayList<Suggestion>(Arrays.asList(mainPlaces.getSuggestions()));
		if(!categoryId.equals("00-00") && !mainPlaces.hasCategory(categoryId)){
			Suggestion suggestion = new Suggestion();
			suggestion.setpCategoryId(categoryId);
			suggestions.add(suggestion);
		}
		model.addAttribute("suggestions", suggestions);
		
		model.addAttribute("aoiId", aoiId);
		model.addAttribute("aoiName", aoiName);
		model.addAttribute("set", set);
		model.addAttribute("hashtagId", hashtagId);
		model.addAttribute("categoryId", categoryId);
		model.addAttribute("latitude", latitude);
		model.addAttribute("longitude", longitude);
		if(set.equals("ranking")){
			model.addAttribute("startIdx", startIdx);
		}
		return "main/mainPlaces";
    }
	
	/**
	 * 메인 주변 더보기
	 * @param model
	 * @param pSet
	 * @param pHashtagId
	 * @param pCateogoryId
	 * @param cursor
	 * @param limit
	 * @param startIdx
	 * @param latitude
	 * @param longitude
	 * @param aoiId
	 * @param aoiName
	 * @return
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	@RequestMapping(value = {"/main/{set:0|latest|ranking|sorder}/{hashtagId}/{cateogoryId}/{cursor}/{limit}"})
    public String mainMore(Model model
    						,@PathVariable("set") String set
    						,@PathVariable("hashtagId") int hashtagId
    						,@PathVariable("cateogoryId") String cateogoryId
    						,@PathVariable("cursor") String cursor
    						,@PathVariable("limit") int limit
    						,@RequestParam(value="startIdx" ,required=false, defaultValue="1") int startIdx
    						,@CookieValue(value="latitude", required=false, defaultValue="37.5627117943") double latitude
    						,@CookieValue(value="longitude", required=false, defaultValue="126.9849551207") double longitude
    						,@CookieValue(value="aoiId", required=false, defaultValue="117965") int aoiId
				    		,@CookieValue(value="aoiName", required=false, defaultValue="명동") String aoiName) 
				    				throws InterruptedException, ExecutionException {
       
		String latitude6 = decimalFormat.format(latitude);
		String longitude6 = decimalFormat.format(longitude);
		
		MainPlaces mainPlaces = syrupTableService.getMainPlaces(aoiId, set, latitude6, longitude6, hashtagId, cateogoryId, cursor, limit);
		
		model.addAttribute("mainPlaces", mainPlaces);
		model.addAttribute("set", set);
		
		model.addAttribute("latitude", latitude);
		model.addAttribute("longitude", longitude);
		if(set.equals("ranking")){
			model.addAttribute("startIdx", startIdx);
		}
		return "main/mainPlacesMore";
    }
	
	/**
	 * 메인 주변 지도
	 * @param model
	 * @param latitude
	 * @param longitude
	 * @param aoiId
	 * @param aoiName
	 * @return
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	@RequestMapping(value = {"/main-map/{set:0|latest|ranking|sorder}/{hashtagId}/{categoryId}/{cursor}/{limit}"})
    public String mainMap(Model model
							,@PathVariable("set") String set
							,@PathVariable("hashtagId") int hashtagId
							,@PathVariable("categoryId") String categoryId
							,@PathVariable("cursor") String cursor
    						,@PathVariable("limit") int limit
    						,@RequestParam(value="startIdx" ,required=false, defaultValue="1") int startIdx
    						,@CookieValue(value="latitude", required=false, defaultValue="37.5627117943") double latitude
    						,@CookieValue(value="longitude", required=false, defaultValue="126.9849551207") double longitude
    						,@CookieValue(value="aoiId", required=false, defaultValue="117965") int aoiId
    						,@CookieValue(value="aoiName", required=false, defaultValue="명동") String aoiName) 
				    				throws InterruptedException, ExecutionException {
       
		String latitude6 = decimalFormat.format(latitude);
		String longitude6 = decimalFormat.format(longitude);
		
		if(aoiId == 0){
			CurrentAOI currentAOI = syrupTableService.getCurrentAOI(longitude6, latitude6);
			aoiId = currentAOI.getAoi().getAoiId();
			aoiName = currentAOI.getAoi().getName();
		}
		
		MainPlaces mainPlaces = syrupTableService.getMainPlaces(aoiId, set, latitude6, longitude6, hashtagId, categoryId, cursor, limit);
		model.addAttribute("mainPlaces", mainPlaces);
		
		List<Suggestion> suggestions = new ArrayList<Suggestion>(Arrays.asList(mainPlaces.getSuggestions()));
		if(!mainPlaces.hasCategory(categoryId) && !categoryId.equals("00-00")){
			Suggestion suggestion = new Suggestion();
			suggestion.setpCategoryId(categoryId);
			suggestions.add(suggestion);
		}
		model.addAttribute("suggestions", suggestions);
		
		model.addAttribute("aoiName", aoiName);
		model.addAttribute("aoiId", aoiId);
		model.addAttribute("set", set);
		model.addAttribute("hashtagId", hashtagId);
		model.addAttribute("categoryId", categoryId);
		model.addAttribute("latitude", latitude);
		model.addAttribute("longitude", longitude);
		if(set.equals("ranking")){
			model.addAttribute("startIdx", startIdx);
		}
		
		return "main/mainMap";
    }
	
	/**
	 * 메인 주변 지도 탭 클릭
	 * @param model
	 * @param latitude
	 * @param longitude
	 * @param aoiId
	 * @param aoiName
	 * @return
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	@RequestMapping(value = {"/main-map/{set:0|latest|ranking|sorder}/{hashtagId}/{categoryId}"})
    public String mainMapMore(Model model
							,@PathVariable("set") String set
							,@PathVariable("hashtagId") int hashtagId
							,@PathVariable("categoryId") String categoryId
							,@RequestParam(value="aoiId" ,required=true, defaultValue="117965") int aoiId
    						,@RequestParam(value="startIdx" ,required=false, defaultValue="1") int startIdx
    						,@CookieValue(value="latitude", required=false, defaultValue="37.5627117943") double latitude
    						,@CookieValue(value="longitude", required=false, defaultValue="126.9849551207") double longitude) 
				    				throws InterruptedException, ExecutionException {
       
		String latitude6 = decimalFormat.format(latitude);
		String longitude6 = decimalFormat.format(longitude);
		
		MainPlaces mainPlaces = syrupTableService.getMainPlaces(aoiId, set, latitude6, longitude6, hashtagId, categoryId, "", 20);
		model.addAttribute("mainPlaces", mainPlaces);
		model.addAttribute("aoiId", aoiId);
		model.addAttribute("set", set);
		model.addAttribute("hashtagId", hashtagId);
		model.addAttribute("categoryId", categoryId);
		model.addAttribute("latitude", latitude);
		model.addAttribute("longitude", longitude);
		if(set.equals("ranking")){
			model.addAttribute("startIdx", startIdx);
		}
		
		return "main/mainMapMore";
    }
	
	
	/**
	 * 메인 먹스토리
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/main-story")
	public String eatStory(Model model) {
       
		MainStories mainStories = syrupTableService.getMainStories();
		model.addAttribute("mainStories", mainStories);
		
		return "main/mainStory";
    }
	
	/**
	 * 오늘의 점심
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/today-lunch")
    public String todaylunch(Model model
    		,@CookieValue(value="latitude", required=false, defaultValue="37.5627117943") double latitude
			,@CookieValue(value="longitude", required=false, defaultValue="126.9849551207") double longitude
    		,@CookieValue(value="aoiId", required=false, defaultValue="117965") int aoiId) {

		if(aoiId == 0){
			String latitude6 = decimalFormat.format(latitude);
			String longitude6 = decimalFormat.format(longitude);
			CurrentAOI currentAOI = syrupTableService.getCurrentAOI(longitude6, latitude6);
			aoiId = currentAOI.getAoi().getAoiId();
		}
		
		TodayLunch todayLunch = syrupTableService.getTodayLunch(aoiId);
		model.addAttribute(todayLunch);
		
		return "main/todayLunch";
    }
	
}
