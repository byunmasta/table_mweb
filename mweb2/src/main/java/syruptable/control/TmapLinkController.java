package syruptable.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TmapLinkController {

	/**
	 * 티맵에서 주변버튼 등 피캣연동할 때 피캣 미설치 시 호출되는 웹뷰URL
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{redirectUri:launching|openevent}")
	public String redirectStore(HttpServletRequest request
									,HttpServletResponse response
									,@PathVariable("redirectUri") String redirectUri) throws Exception {
		if(redirectUri.equals("openevent")){
			return "redirect:https://itunes.apple.com/kr/app/pickat-pikaes/id489789094?mt=8";
		}else{
			return "redirect:http://tsto.re/0000250761";
		}
	}

}
