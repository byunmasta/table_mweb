package syruptable.control;

import java.text.DecimalFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import syruptable.model.AutocompleteResult;
import syruptable.model.PopularWords;
import syruptable.model.SearchResult;
import syruptable.service.SyrupTableService;
import syruptable.service.SyrupTableService.SortType;


@Controller
public class SearchController {
	
	private static final DecimalFormat decimalFormat = new DecimalFormat(".######");
	
	@Autowired
	private SyrupTableService syrupTableService;
	
	/**
	 * 검색메인
	 * @return
	 */
	@RequestMapping(value = "/search")
    public String searchResult(Model model) {

		PopularWords popularWords = syrupTableService.getPopularWords();
		model.addAttribute("popularWords", popularWords);
		
		return "search/searchMain";
    }
	
	/**
	 * 검색결과
	 * @param query
	 * @return
	 */
	@RequestMapping(value = "/search/{query}")
    public String searchResult(Model model
    		,@PathVariable("query") String query
    		,@CookieValue(value="latitude", required=false, defaultValue="37.5627117943") double latitude
			,@CookieValue(value="longitude", required=false, defaultValue="126.9849551207") double longitude) {
		
		String lat = decimalFormat.format(latitude);
		String lng = decimalFormat.format(longitude);

		SearchResult searchResult = syrupTableService.getSearchResult(query, "", "score", 0, lng, lat, lng, lat, "", 20);
		model.addAttribute("searchResult", searchResult);
		model.addAttribute("sort", "score");
		
		PopularWords popularWords = syrupTableService.getPopularWords();
		model.addAttribute("popularWords", popularWords);
		
		return "search/searchResult";
    }
	
	/**
	 * 검색결과 정렬
	 * @param query
	 * @param sort
	 * @return
	 */
	@RequestMapping(value = "/search/{query}/{sort:popular|distance|score}")
    public String searchResultSort(Model model
    		,@PathVariable("query") String query
    		,@PathVariable("sort") SortType sort
    		,@CookieValue(value="latitude", required=false, defaultValue="37.5627117943") double latitude
			,@CookieValue(value="longitude", required=false, defaultValue="126.9849551207") double longitude) {
		
		String lat = decimalFormat.format(latitude);
		String lng = decimalFormat.format(longitude);

		SearchResult searchResult = syrupTableService.getSearchResult(query, "", sort.name(), 0, lng, lat, lng, lat, "", 20);
		model.addAttribute("searchResult", searchResult);
		model.addAttribute("sort", sort.name());
		
		PopularWords popularWords = syrupTableService.getPopularWords();
		model.addAttribute("popularWords", popularWords);
		
		return "search/searchResult";
    }
	
	/**
	 * 검색결과 더보기
	 * @param query
	 * @param cursor
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/search/{query}/{sort:popular|distance|score}/{cursor}/{limit}")
    public String searchResult(Model model
    		,@PathVariable("query") String query
    		,@PathVariable("sort") SortType sort
    		,@PathVariable("cursor") String cursor
    		,@PathVariable("limit") int limit
    		,@CookieValue(value="latitude", required=false, defaultValue="37.5627117943") double latitude
			,@CookieValue(value="longitude", required=false, defaultValue="126.9849551207") double longitude) {
		
		String lat = decimalFormat.format(latitude);
		String lng = decimalFormat.format(longitude);

		SearchResult searchResult = syrupTableService.getSearchResult(query, "", sort.name(), 0, lng, lat, lng, lat, cursor, limit);
		model.addAttribute("searchResult", searchResult);
		
		return "search/searchResultMore";
    }
	
	/**
	 * 자동완성
	 * @param query
	 * @return
	 */
	@RequestMapping(value = "/autocomplete/{query}")
    public String autocompleteResult(Model model
    		,@PathVariable("query") String query) {
		
		AutocompleteResult autocompleteResult = syrupTableService.getAutocompleteResult(query);
		model.addAttribute("autocompleteResult", autocompleteResult);
		model.addAttribute("query", query);
		
		return "search/autocompleteResult";
    }
	
}
