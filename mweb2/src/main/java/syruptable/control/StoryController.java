package syruptable.control;

import java.util.Base64;
import java.util.Base64.Decoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.skplanet.syruptable.entity.story.Stories;
import com.skplanet.syruptable.entity.story.Story_Likes;
import com.skplanet.syruptable.entity.story.Story_Replies;
import com.skplanet.syruptable.response.story.StoryDetail;
import com.skplanet.syruptable.response.story.StoryPois;
import com.skplanet.syruptable.response.story.StoryReplies;
import com.skplanet.syruptable.service.StoryServiceImpl;

import syruptable.model.StoryList;
import syruptable.model.UserInfo;
import syruptable.service.SyrupTableService;
import syruptable.support.AuthHelper;
import syruptable.util.TimeViewFormat;

@Controller
public class StoryController {

	private static final long TOKEN_EXPIRED_TIME = 60L;
	
	@Autowired
	private SyrupTableService syrupTableService;
	
	@Autowired
	private StoryServiceImpl storyServiceImpl;
	
	@Autowired
	private TimeViewFormat timeViewFormat;
	
	@Autowired
	private AuthHelper authHelper;
	
	private Decoder decoder = Base64.getDecoder();
	
	/**
	 * 스토리 리스트 보기
	 * @return
	 */
	@RequestMapping(value = "/story")
    public String storyList(Model model) {
		
		StoryList storyList = syrupTableService.getStoryList("", 7);
		model.addAttribute("storyList", storyList);
		
		return "story/storyList";
	}
	
	/**
	 * 스토리 상세 보기
	 * @param storyId
	 * @return
	 */
	@RequestMapping(value = "/story/{storyId}")
    public String storyDetail(Model model
    					,HttpServletRequest request
    					,HttpServletResponse response
    					,@RequestHeader("User-Agent") final String userAgent
						,@PathVariable("storyId") int storyId
						,@RequestParam(value="authToken" ,required=false) String authToken
						,@RequestParam(value="jwt" ,required=false) String jwt) {
		
		//없는 스토리ID일 경우 404처리
		if(!storyServiceImpl.isValidStory(storyId)){
			return "exception/pageNotFound";
		}
		
		//앱내 웹뷰인지 체크
		if(userAgent.indexOf("pickat") >= 0){
			model.addAttribute("type", "app");
		}

		StoryDetail storyDetail = storyServiceImpl.getStoryContents(storyId);
		model.addAttribute("storyId", storyId);
		model.addAttribute("storyDetail", storyDetail);
		
		StoryPois storyPois = storyServiceImpl.getStoryPois(storyId, 0, 3);
		model.addAttribute("storyPois", storyPois);
		
		//인증된 사용자인지 체크
		if (authToken != null) {
			try{
				UserInfo userInfo = syrupTableService.getUserInfo(authToken);
				if(userInfo != null){
					int userId = userInfo.getUserId();
					String token = authHelper.createJsonWebToken(userId, TOKEN_EXPIRED_TIME);
					model.addAttribute("jwt", token);
					model.addAttribute("isLiked", storyServiceImpl.isLiked(storyId, userId));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}else if(jwt != null){
			try{
				int userId = authHelper.verifyToken(jwt).getUserId();
				model.addAttribute("jwt", jwt);
				model.addAttribute("isLiked", storyServiceImpl.isLiked(storyId, userId));
				
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		
		timeViewFormat.setNowDate();
		model.addAttribute(timeViewFormat);
		
		return "story/storyDetail";
	}
	
	/**
	 * 스토리 Detail Refresh용 data
	 * @param storyId
	 * @return
	 */
	@RequestMapping(value = "/story/{storyId}/data")
    public String storyDetailData(Model model
						,@PathVariable("storyId") int storyId
						,@RequestParam(value="jwt" ,required=false) String jwt) {
		
		StoryDetail storyDetail = storyServiceImpl.getStoryContents(storyId);
		model.addAttribute("storyId", storyId);
		model.addAttribute("storyDetail", storyDetail);
		
		if(jwt != null){
			try{
				int userId = authHelper.verifyToken(jwt).getUserId();
				model.addAttribute("jwt", jwt);
				model.addAttribute("isLiked", storyServiceImpl.isLiked(storyId, userId));
				
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		
		timeViewFormat.setNowDate();
		model.addAttribute(timeViewFormat);
		
		return "story/storyDetailData";
	}
	
	/**
	 * 스토리 댓글목록 보기 
	 * @param storyId
	 * @return
	 */
	@RequestMapping(value = "/story/{storyId}/reply")
    public String storyReply(Model model
    					,HttpServletRequest request
    					,HttpServletResponse response
						,@PathVariable("storyId") int storyId
						,@RequestHeader("User-Agent") final String userAgent
						,@RequestParam(value="jwt" ,required=false) String jwt) {
		
		if(userAgent.indexOf("pickat") >= 0){
			model.addAttribute("type", "app");
		}
			
		int userId = 0;
		if(jwt != null){
			try{
				userId = authHelper.verifyToken(jwt).getUserId();
				model.addAttribute("jwt", jwt);
				
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		
		StoryReplies storyReplies = storyServiceImpl.getStoryReplies(storyId, userId, 0, 20);
		model.addAttribute("storyId", storyId);
		model.addAttribute("storyReplies", storyReplies);
		model.addAttribute("storyImage", storyServiceImpl.getStoryImage(storyId));
		
		timeViewFormat.setNowDate();
		model.addAttribute(timeViewFormat);
		
		return "story/storyReply";
	}
	
	/**
	 * 스토리 댓글 더보기
	 * @param storyId
	 * @param cursor
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/story/{storyId}/reply/{cursor}/{limit}")
    public String storyReplyMore(Model model
						,@PathVariable("storyId") int storyId
						,@PathVariable("cursor") String cursor
			    		,@PathVariable("limit") int limit
			    		,@RequestParam(value="jwt" ,required=false) String jwt) {
		
		int userId = 0;
		if(jwt != null){
			try{
				userId = authHelper.verifyToken(jwt).getUserId();
				model.addAttribute("jwt", jwt);
				
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		
		int cur = cursor.equals("") || cursor.equals("0") || cursor == null ? 0 : Integer.parseInt(new String(decoder.decode(cursor)));
		StoryReplies storyReplies = storyServiceImpl.getStoryReplies(storyId, userId, cur, limit);
		
		model.addAttribute("storyId", storyId);
		model.addAttribute("storyReplies", storyReplies);
		
		timeViewFormat.setNowDate();
		model.addAttribute(timeViewFormat);
		
		return "story/storyReplyMore";
	}
	
	/**
	 * 스토리 댓글쓰기
	 * @param storyId
	 * @param replyText
	 * @return
	 */
	@RequestMapping(value = "/story/{storyId}/reply/write")
	@ResponseBody
    public ResponseEntity<Integer> storyReply(Model model, HttpServletRequest request
						,@PathVariable("storyId") int storyId
						,@RequestParam(value="replyText" ,required=true) String replyText
						,@RequestParam(value="jwt" ,required=true) String jwt) {
		
		try{
			int userId = authHelper.verifyToken(jwt).getUserId();
			Story_Replies result = storyServiceImpl.setReplyWrite(storyId, userId, replyText);
			if(result == null){
				return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		
		} catch(Exception e){
			e.printStackTrace();
			return new ResponseEntity<Integer>(HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<Integer>(HttpStatus.OK);
	}

	/**
	 * 스토리 댓글쓰기
	 * @param storyId
	 * @param replyId
	 * @param replyText
	 * @return
	 */
	/*
	@RequestMapping(value = "/story/{storyId}/reply/write")
	@ResponseBody
    public ResponseEntity<Integer> storyReply(Model model, HttpServletRequest request
						,@PathVariable("storyId") int storyId
						,@RequestParam(value="replyId" ,required=false) Integer replyId
						,@RequestParam(value="replyText" ,required=true) String replyText
						,@RequestParam(value="jwt" ,required=true) String jwt) {
		
		try{
			int userId = authHelper.verifyToken(jwt).getUserId();
			Story_Replies result = storyServiceImpl.setReply(storyId, userId, replyId, replyText);
			if(result == null){
				return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		
		} catch(Exception e){
			e.printStackTrace();
			return new ResponseEntity<Integer>(HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<Integer>(HttpStatus.OK);
	}
	*/
	/**
	 * 스토리 댓글수정
	 * @param replyId
	 * @param replyText
	 * @return
	 */
	/*
	@RequestMapping(value = "/story/{storyId}/reply/modify")
	@ResponseBody
    public ResponseEntity<Integer> storyReplyModfiy(Model model
			    		,@PathVariable("storyId") int storyId
						,@RequestParam(value="replyId" ,required=true) Integer replyId
						,@RequestParam(value="replyText" ,required=true) String replyText
						,@RequestParam(value="jwt" ,required=true) String jwt) {
		try{
			int userId = authHelper.verifyToken(jwt).getUserId();
			Story_Replies result = storyServiceImpl.setReplyModify(replyId, userId, replyText);
			if(result == null){
				return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} catch(Exception e){
			e.printStackTrace();
			return new ResponseEntity<Integer>(HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<Integer>(HttpStatus.OK);
	}
*/	
	/**
	 * 스토리 댓글삭제
	 * @param replyId
	 * @return
	 */
	@RequestMapping(value = "/story/{storyId}/reply/delete")
	@ResponseBody
    public ResponseEntity<Integer> storyReplyDelete(Model model
    					,@PathVariable("storyId") int storyId
    					,@RequestParam(value="replyId" ,required=true) Integer replyId
						,@RequestParam(value="jwt" ,required=true) String jwt) {
		try{
			int userId = authHelper.verifyToken(jwt).getUserId();
			Story_Replies result = storyServiceImpl.setReplyDelete(replyId, userId);
			if(result == null){
				return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
			}

		} catch(Exception e){
			e.printStackTrace();
			return new ResponseEntity<Integer>(HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<Integer>(HttpStatus.OK);
	}
	
	/**
	 * 스토리 POI 목록 보기 
	 * @param storyId
	 * @return
	 */
	@RequestMapping(value = "/story/{storyId}/pois")
    public String storyPois(Model model, HttpServletRequest request
    					,HttpServletResponse response
    					,@RequestHeader("User-Agent") final String userAgent
    					,@PathVariable("storyId") int storyId
						,@RequestParam(value="jwt" ,required=false) String jwt) {

		if(userAgent.indexOf("pickat") >= 0){
			model.addAttribute("type", "app");
		}

		StoryPois storyPois = storyServiceImpl.getStoryPois(storyId, 0, 20);
		model.addAttribute("jwt", jwt);
		model.addAttribute("storyId", storyId);
		model.addAttribute("storyPois", storyPois);
		model.addAttribute("storyImage", storyServiceImpl.getStoryImage(storyId));
		
		return "story/storyPois";
	}
	
	/**
	 * 스토리 POI 목록 더보기 
	 * @param storyId
	 * @param cursor
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/story/{storyId}/pois/{cursor}/{limit}")
    public String storyPoisMore(Model model, HttpServletRequest request
    					,@RequestHeader("User-Agent") final String userAgent
    					,@PathVariable("storyId") int storyId
						,@PathVariable("cursor") String cursor
			    		,@PathVariable("limit") int limit
			    		,@RequestParam(value="jwt" ,required=false) String jwt) {
		
		if(userAgent.indexOf("pickat") >= 0){
			model.addAttribute("type", "app");
		}
		
		int cur = cursor.equals("") || cursor.equals("0") || cursor == null ? 0 : Integer.parseInt(new String(decoder.decode(cursor)));
		StoryPois storyPois = storyServiceImpl.getStoryPois(storyId, cur, limit);
		
		model.addAttribute("jwt", jwt);
		model.addAttribute("storyId", storyId);
		model.addAttribute("storyPois", storyPois);
		
		return "story/storyPoisMore";
	}
	
	/**
	 * 스토리 뷰카운트
	 * @param storyId
	 * @return
	 */
	@RequestMapping(value = "/story/{storyId}/view")
	@ResponseBody
    public int storyView(Model model
						,@PathVariable("storyId") int storyId) {
		
		try{
			Stories result = storyServiceImpl.viewAction(storyId);
			return result.getViewCount();
			
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}
	
	/**
	 * 스토리 공유하기카운트
	 * @param storyId
	 * @return
	 */
	@RequestMapping(value = "/story/{storyId}/share")
	@ResponseBody
    public int storyShare(Model model
						,@PathVariable("storyId") int storyId) {
		try{
			Stories result = storyServiceImpl.shareAction(storyId);
			return result.getShareCount();
			
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}
	
	/**
	 * 스토리 좋아요
	 * @param storyId
	 * @return
	 */
	@RequestMapping(value = "/story/{storyId}/like")
	@ResponseBody
    public ResponseEntity<Integer> storyLike(Model model, HttpServletRequest request
						,@PathVariable("storyId") int storyId
						,@RequestParam(value="jwt" ,required=true) String jwt) {
		
		try{
			int userId = authHelper.verifyToken(jwt).getUserId();
			Story_Likes result = storyServiceImpl.likeAction(storyId, userId);
			if(result == null){
				return new ResponseEntity<Integer>(500, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}catch(Exception e){
			return new ResponseEntity<Integer>(403, HttpStatus.FORBIDDEN);
		}
		
		return new ResponseEntity<Integer>(200, HttpStatus.OK);
	}
	
}
