package syruptable.control;

import java.util.Base64;
import java.util.Base64.Decoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import com.skplanet.syruptable.response.verified.VerifiedAOIPlaces;
import com.skplanet.syruptable.response.verified.VerifiedAOIPlaces.VerifiedPoi;
import com.skplanet.syruptable.service.VerifiedServiceImpl;

@Controller
public class VerifiedsController {

	@Autowired
	private VerifiedServiceImpl verifiedServiceImpl ;
	
	private Decoder decoder = Base64.getDecoder();
	
	@RequestMapping(value = "/verifieds")
    public String verifieds(Model model
    					,HttpServletRequest request 
    					,HttpServletResponse response
    					,@RequestHeader("User-Agent") final String userAgent) {
		
		if(userAgent.indexOf("pickat") >= 0){
			model.addAttribute("type", "app");
		}
		
		VerifiedAOIPlaces verifiedAOIPlaces = verifiedServiceImpl.getAllVerifiedAOIs();
		model.addAttribute("verifieds", verifiedAOIPlaces);
		
		return "verifieds/verifieds";
	}
	
	//내보내기용 aoiId 포함
	@RequestMapping(value = "/verifieds/{aoiId}")
    public String verifieds(Model model
			    		,HttpServletRequest request 
						,HttpServletResponse response
						,@RequestHeader("User-Agent") final String userAgent
						,@PathVariable("aoiId") int aoiId) {
		
		if(userAgent.indexOf("pickat") >= 0){
			model.addAttribute("type", "app");
		}

		VerifiedAOIPlaces verifiedAOIPlaces = verifiedServiceImpl.getAllVerifiedAOIs();
		model.addAttribute("verifieds", verifiedAOIPlaces);
		model.addAttribute("aoiId", aoiId);
		
		return "verifieds/verifieds";
	}
	
	@RequestMapping(value = "/verifieds/{aoiId}/{cursor}/{limit}")
    public String verifiedsMore(Model model, HttpServletRequest request
				    		,@RequestHeader("User-Agent") final String userAgent
				    		,@PathVariable("aoiId") int aoiId
				    		,@PathVariable("cursor") String cursor
				    		,@PathVariable("limit") int limit) {
    	
		if(userAgent.indexOf("pickat") >= 0){
			model.addAttribute("type", "app");
		}
		
		int cur = cursor.equals("") || cursor.equals("0") || cursor == null ? 0 : Integer.parseInt(new String(decoder.decode(cursor)));
		
		VerifiedPoi verified = verifiedServiceImpl.getVerifiedAOI(aoiId, cur, limit);
		model.addAttribute("verified", verified);
		
		return "verifieds/verifiedsMore";
	}
	
}
