package syruptable.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import syruptable.model.AOIList;
import syruptable.model.Categories;
import syruptable.model.Notices;
import syruptable.service.SyrupTableService;
import syruptable.support.CSInfra;

/**
 * 주요기능 외 나머지 페이지들 컨트롤러
 * 위치변경, 카테고리, 공지사항, 고객센터
 * @author Yongho Byun
 *
 */

@Controller
public class OthersController {

	@Autowired
	private SyrupTableService syrupTableService;
	
	/**
	 * 위치변경 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/location")
    public String aoiList(Model model) {
       
		AOIList aoiList = syrupTableService.getAOIList();
		model.addAttribute("aois", aoiList.getAois());
		
		return "others/location";
    }
	
	
	/**
	 * 카테고리 리스트 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/category")
    public String category(Model model) {
       
		Categories categories = syrupTableService.getCategoryList();
		model.addAttribute("categories", categories.getCaregories());
		
		return "others/category";
    }
	
	/**
	 * 공지사항 전체 보기
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/notice")
    public String notices(Model model) {
       
		Notices notices = syrupTableService.getNotices("", 20);
		model.addAttribute("notices", notices);
		
		return "others/notice";
    }
	
	/**
	 * 공지사항 더보기
	 * @param cursor
	 * @param limit
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/notice/{cursor}/{limit}")
    public String noticesMore(@PathVariable("cursor") String cursor, @PathVariable("limit") int limit, Model model) {
       
		Notices notices = syrupTableService.getNotices(cursor, limit);
		model.addAttribute("notices", notices);
		
		return "others/noticeMore";
    }
	
	
	/**
	 * SKP 고객센터로 redirect
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/{cs:cs|csInfra}")
	public String csInfra() throws Exception {
		CSInfra csInfra = new CSInfra();
		String pocpr = csInfra.getPocpr(); // 서비스 식별자 정보
		String redirectPath = new StringBuffer("redirect:https://mcs.skplanet.com/poc/mob/?pocpr=").append(pocpr).toString();
		return redirectPath;
	}
}
