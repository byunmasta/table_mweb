package syruptable.control;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import syruptable.config.HttpAPIList;
import syruptable.model.POIDetail;
import syruptable.model.POIImages;
import syruptable.model.PickDetail;
import syruptable.model.common.SyrupTable;
import syruptable.service.SyrupTableService;
import syruptable.service.SyrupTableService.ImageType;
import syruptable.util.TableSyrupCallable;
import syruptable.util.TimeViewFormat;

/**
 * @author Yongho Byun
 *
 */
@Controller
public class ImageController {

	@Autowired
	private SyrupTableService syrupTableService;
	
	@Autowired
	private TimeViewFormat timeViewFormat;
	
	//========================= POI 이미지 =========================
	/**
	 * POI 이미지 썸네일 리스트
	 * @param model
	 * @return
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	@RequestMapping(value = "/poi/{poiId}/image-list")
    public String poiImageList(Model model, @PathVariable("poiId") int poiId) throws InterruptedException, ExecutionException {
		
		SyrupTable syrupTable = new SyrupTable.Builder()
								.setPoiId(poiId)
								.setImageType(ImageType.picks)
								.setCursor("")
								.setLimit(30)
								.build();
		
		TableSyrupCallable<POIImages> pickImagesCallable = new TableSyrupCallable<POIImages>(
														HttpAPIList.POIIMAGEFORPICK
														,syrupTable
														,syrupTableService);

		
		SyrupTable blogSyrupTable = new SyrupTable.Builder()
								.setPoiId(poiId)
								.setImageType(ImageType.blogs)
								.setCursor("")
								.setLimit(30)
								.build();
		
		TableSyrupCallable<POIImages> blogImagesCallable = new TableSyrupCallable<POIImages>(
															HttpAPIList.POIIMAGEFORBLOG
															,blogSyrupTable
															,syrupTableService);
		
		FutureTask<POIImages> pickImageFuture = new FutureTask<POIImages>(pickImagesCallable);
		FutureTask<POIImages> blogImageFuture = new FutureTask<POIImages>(blogImagesCallable);
		
		ExecutorService executor = Executors.newFixedThreadPool(2);
		
		executor.execute(pickImageFuture);
		executor.execute(blogImageFuture);
		
		executor.shutdown();
		
		POIImages pickImages = pickImageFuture.get();
		POIImages blogImages = blogImageFuture.get();
		
		model.addAttribute("picks", pickImages);
		model.addAttribute("blogs", blogImages);
		model.addAttribute("poiId", poiId);
		  
		return "image/poi/imageList";
	}
	
	/**
	 * 이미지 썸네일 타입별로 보기
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/poi/{poiId}/image-list/{imageType}")
	public String imageTypeList(Model model
    		,@PathVariable("imageType") ImageType imageType
    		,@PathVariable("poiId") int poiId
			) {
	
		POIImages images = syrupTableService.getPOIImageList(imageType, poiId, "", 30);
		model.addAttribute(imageType.name(), images);
		model.addAttribute("ImageType", imageType.name());
		model.addAttribute("paging", images.getPaging());
		model.addAttribute("poiId", poiId);
		
		return "image/poi/imageList";
	}
	
	/**
	 * 이미지 썸네일 타입별로 더보기
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/poi/{poiId}/image-list/{imageType}/{cursor}/{limit}")
	public String imageTypeListMore(Model model
    		,@PathVariable("imageType") ImageType imageType
    		,@PathVariable("poiId") int poiId
    		,@PathVariable("cursor") String cursor
			,@PathVariable("limit") int limit
			) {
	
		POIImages images = syrupTableService.getPOIImageList(imageType, poiId, cursor, limit);
		model.addAttribute(imageType.name(), images);
		model.addAttribute("ImageType", imageType.name());
		model.addAttribute("paging", images.getPaging());
		model.addAttribute("poiId", poiId);
		
		return "image/poi/imageListMore";
	}
	
	/**
	 * 이미지 크게보기
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/poi/{poiId}/image-view")
	public String poiImageView(Model model
    		,@PathVariable("poiId") int poiId
			) {
		
		POIImages pickImages = syrupTableService.getPOIImageList(ImageType.picks, poiId, "", 30);
		model.addAttribute("picks", pickImages);
		model.addAttribute("poiId", poiId);
		
		POIDetail poiDetail = syrupTableService.getPOIDetail(poiId);
		model.addAttribute("poiName", poiDetail.getPoi().getName());
		
		timeViewFormat.setNowDate();
		model.addAttribute(timeViewFormat);
		
		return "image/poi/imageView";
	}
	
	/**
	 * 이미지 크게보기 더보기
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/poi/{poiId}/image-view/{cursor}/{limit}")
	public String poiImageViewMore(Model model
    		,@PathVariable("poiId") int poiId
    		,@PathVariable("cursor") String cursor
			,@PathVariable("limit") int limit
			,@RequestParam(value="poiName" ,required=false, defaultValue="") String poiName
			) {
		POIImages pickImages = syrupTableService.getPOIImageList(ImageType.picks, poiId, cursor, limit);
		model.addAttribute("picks", pickImages);
		
		timeViewFormat.setNowDate();
		model.addAttribute(timeViewFormat);
		
		return "image/poi/imageViewMore";
	}
	
	
	//========================= Pick 이미지 =========================
	/**
	 * Pick상세 이미지 크게보기
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/pick/{pickId}/image-view")
	public String pickImageViewer(Model model
    		,@PathVariable("pickId") int pickId) {
		
		PickDetail pickDetail = syrupTableService.getPickDetail(pickId, "", 20);
		model.addAttribute("pickDetail", pickDetail);
		
		timeViewFormat.setNowDate();
		model.addAttribute(timeViewFormat);
		
		return "image/pick/imageView";
	}
}
