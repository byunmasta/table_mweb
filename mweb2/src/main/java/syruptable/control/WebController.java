package syruptable.control;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WebController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping(value = "/web/{referer}/{version}")
    public String web(Model model
    		, @PathVariable("referer") String referer
    		, @PathVariable("version") int version
    		, @RequestParam(value="uri", required=true, defaultValue="main") String uri) throws UnsupportedEncodingException {
		
		StringBuilder redirectUrl = new StringBuilder();
		String[] uris = uri.split("/");
		for(int i=0; i<uris.length; i++){
			uris[i] = URLEncoder.encode(uris[i], "UTF-8");
		}
		redirectUrl.append(StringUtils.arrayToDelimitedString(uris, "/"));
		redirectUrl.append("?externalLink=");
		redirectUrl.append(referer);
		
		logger.info("redirect to : {}", redirectUrl.toString());
		
		return "redirect:/" + redirectUrl.toString();
	}
	
	@RequestMapping(value = "/web/{target}/{referer}/{version}")
    public String web(Model model
    		, @PathVariable("target") String target
    		, @PathVariable("referer") String referer
    		, @PathVariable("version") int version) {
		
		StringBuilder redirectUrl = new StringBuilder();
		redirectUrl.append(target);
		redirectUrl.append("?externalLink=");
		redirectUrl.append(referer);
		
		logger.info("redirect to : {}", redirectUrl.toString());
		
		return "redirect:/" + redirectUrl.toString();
	}
	
	@RequestMapping(value = "/web/{target}/{targetId}/{referer}/{version}")
    public String web(Model model
    		, @PathVariable("target") String target
    		, @PathVariable("targetId") Object targetId
    		, @PathVariable("referer") String referer
    		, @PathVariable("version") int version) throws UnsupportedEncodingException {
		
		StringBuilder redirectUrl = new StringBuilder();
		redirectUrl.append(StringUtils.arrayToDelimitedString(new String[]{target, URLEncoder.encode(targetId.toString(), "UTF-8")}, "/"));
		redirectUrl.append("?externalLink=");
		redirectUrl.append(referer);
		
		logger.info("redirect to : {}", redirectUrl.toString());
		
		return "redirect:/" + redirectUrl.toString();
	}
	
	@RequestMapping(value = "/web/{target}/{targetId}/{option}/{referer}/{version}")
    public String web(Model model
    		, @PathVariable("target") String target
    		, @PathVariable("targetId") Object targetId
    		, @PathVariable("option") String option
    		, @PathVariable("referer") String referer
    		, @PathVariable("version") int version) throws UnsupportedEncodingException {
		
		StringBuilder redirectUrl = new StringBuilder();
		redirectUrl.append(StringUtils.arrayToDelimitedString(new String[]{target, URLEncoder.encode(targetId.toString(), "UTF-8"), option}, "/"));
		redirectUrl.append("?externalLink=");
		redirectUrl.append(referer);
		
		logger.info("redirect to : {}", redirectUrl.toString());
		
		return "redirect:/" + redirectUrl.toString();
	}
	
}
